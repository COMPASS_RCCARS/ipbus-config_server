#!/usr/bin/perl

#print "$#ARGV\n";
#print "@ARGV\n";

if($#ARGV < 1) {
    print("USAGE: load_port.pl <source ID> <port number> [<port number> <port number> ...]\n");
    print("\tIt is working with ifTDC front-end cards only!\n");
    exit 0;
}

print("loading registers for SrcID $ARGV[0] ports: $ARGV[1] ");
for(my $n = 2; $n <= $#ARGV; $n++) {
    print("$ARGV[$n] ");
    if($ARGV[$n] < 0 || $ARGV[$n] > 15) {
	print "\nBad port number: $ARGV[$n], aborted.\n";
	exit 1;
    }
}
print "\n";


my $cmd = "/online/soft/bin/dimclient";
my $SrcID = $ARGV[0];
my $port_mask = 0;
for(my $n = 1; $n <= $#ARGV; $n++) {
    $port_mask |= (1<<$ARGV[$n]);
}

print "port mask: $port_mask\n";

$port_mask |= 0x20000000;

#print("$cmd $SrcID/Initialize $port_mask\n");
system("$cmd cmd $SrcID/Initialize $port_mask");

sleep 10;

system("$cmd print $SrcID/ErrorString");
