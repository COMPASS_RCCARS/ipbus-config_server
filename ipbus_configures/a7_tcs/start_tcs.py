#!/usr/bin/python

#------------------------------------
#
#   Script to configure 
#   running conditions 
#   A7 TCS Controller
#
#------------------------------------

import time
import uhal
def write_register(name,value):
    hw.getNode(name).write(value)
    hw.dispatch()
    var = hw.getNode(name).read()
    hw.dispatch()
    print "{0:} = {1:}".format(name,value)
    hw.dispatch()
#    time.sleep(0.1)

def read_register(name):
    var = hw.getNode(name).read()
    hw.dispatch()
    print "{0:} = {1:}".format(name,var)
    hw.dispatch()
#    time.sleep(0.1)


#delay_sin = input('Select a delay value for trigger : ')
#delay_dif = input('Select a delay value for others : ')


uhal.setLogLevelTo(uhal.LogLevel.WARNING)
manager = uhal.ConnectionManager("file://connection.xml")
hw = manager.getDevice("tcs_cntr")

# Reset FE
write_register("SLW_CMD",0x18800)
# Configure receivers
# REC 0: Any
write_register("CFG_FIFO",0x80187) # EV_TP=1 ACC=1 PT=1 DM=1
write_register("CFG_FIFO",0x8018B) # EV_TP=2 ACC=1 PT=0 DM=0
write_register("CFG_FIFO",0xD0001) # Main DAQ
# REC 1: BMS
write_register("CFG_FIFO",0x80480)
write_register("CFG_FIFO",0xD0101) # Main DAQ
# REC 2: Veto
write_register("CFG_FIFO",0x80880)
write_register("CFG_FIFO",0xD0201) # Main DAQ
# REC 3
write_register("CFG_FIFO",0x80c80)
write_register("CFG_FIFO",0xD0301) # Main DAQ
# REC 4
write_register("CFG_FIFO",0x81080)
write_register("CFG_FIFO",0xD0401) # Main DAQ
# REC 5
write_register("CFG_FIFO",0x81480)
write_register("CFG_FIFO",0xD0501) # Main DAQ
# REC 6
write_register("CFG_FIFO",0x81880)
write_register("CFG_FIFO",0xD0601) # Main DAQ
# REC 7
write_register("CFG_FIFO",0x81C80)
write_register("CFG_FIFO",0xD0701) # Main DAQ
# REC 8
write_register("CFG_FIFO",0x82080)
write_register("CFG_FIFO",0xD0801) # Main DAQ
# REC 9
write_register("CFG_FIFO",0x82480)
write_register("CFG_FIFO",0xD0901) # Main DAQ
# REC 10
write_register("CFG_FIFO",0x82880)
write_register("CFG_FIFO",0xD0A01) # Main DAQ
# REC 11
write_register("CFG_FIFO",0x82C80)
write_register("CFG_FIFO",0xD0B01) # Main DAQ
# REC 12
write_register("CFG_FIFO",0x83080)
write_register("CFG_FIFO",0xD0C01) # Main DAQ
# REC 13
write_register("CFG_FIFO",0x83480)
write_register("CFG_FIFO",0xD0D01) # Main DAQ
# REC 14
write_register("CFG_FIFO",0x83880)
write_register("CFG_FIFO",0xD0E01) # Main DAQ
# REC 15
write_register("CFG_FIFO",0x83C80)
write_register("CFG_FIFO",0xD0F01) # Main DAQ
# REC 16
write_register("CFG_FIFO",0x84080)
write_register("CFG_FIFO",0xD1001) # Main DAQ
# REC 17
write_register("CFG_FIFO",0x84480)
write_register("CFG_FIFO",0xD1101) # Main DAQ
# REC 18
write_register("CFG_FIFO",0x84880)
write_register("CFG_FIFO",0xD1201) # Main DAQ
# REC 19
write_register("CFG_FIFO",0x84C80)
write_register("CFG_FIFO",0xD1301) # Main DAQ
# REC 20
write_register("CFG_FIFO",0x85080)
write_register("CFG_FIFO",0xD1401) # Main DAQ
# REC 21
write_register("CFG_FIFO",0x85480)
write_register("CFG_FIFO",0xD1501) # Main DAQ

# Reset FE
write_register("SLW_CMD",0x10800)

# Set PreTrigger 1
write_register("PERIOD_EVTYPE_1",0x2) # Echo period 1
#write_register("MODE_EVTYPE_1",0x1) # Echo mode 1
write_register("DELAY_EVTYPE_1",0x07) # Echo fine delay 1
# Set PreTrigger 2
write_register("PERIOD_EVTYPE_2",0x2) # Echo period 2
#write_register("MODE_EVTYPE_2",0x2) # Echo mode 2
write_register("DELAY_EVTYPE_2",0x8) # Echo fine delay 2
# Set PreTrigger 3
write_register("PERIOD_EVTYPE_3",0x2) # Echo period 1
#write_register("MODE_EVTYPE_3",0x3) # Echo mode 1
write_register("DELAY_EVTYPE_3",0x9) # Echo fine delay 1
# activate PreTrigger for MainDAQ + all SubDAQs
write_register("DAQ_ACT_MAP",0xFF)

# DEADTIME MainDAQ: 2.5 us FDT, 10 in 20 us VDT A, 3 in 6 us VDT B
write_register("FIXED_DT_DAQ0",0x60)
write_register("NR_TRG_DTA_DAQ0",0xA)
write_register("WIN_DTA_DAQ0",0x320)
write_register("FIXED_DT_DAQ0",0x60)
write_register("NR_TRG_DTB_DAQ0",0x5)
write_register("WIN_DTB_DAQ0",0xF0)
# deadtime SubDAQ 1: 400 us FDT, 4 in 40 us VDT
write_register("FIXED_DT_DAQ1",0x3E80)
write_register("NR_TRG_DTA_DAQ1",0xA)
write_register("WIN_DTA_DAQ1",0x640)
# deadtime SubDAQ 2: 400 us FDT, 4 in 40 us VDT
write_register("FIXED_DT_DAQ2",0x3E80)
write_register("NR_TRG_DTA_DAQ2",0x4)
write_register("WIN_DTA_DAQ2",0x640)

# Start TCS controller
write_register("RC.BC2",0x1)
write_register("RC.RUN",0x1)

