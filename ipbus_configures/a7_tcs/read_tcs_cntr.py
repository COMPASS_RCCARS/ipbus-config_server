#!/usr/bin/python
#
#------------------------------------
#
#   TCS Set script
#
#------------------------------------

import time
import uhal

from argparse import ArgumentParser

uhal.setLogLevelTo(uhal.LogLevel.WARNING)
manager = uhal.ConnectionManager("file://connection.xml")
hw = manager.getDevice("tcs_cntr")

def read_register(name):
    var = hw.getNode(name).read()
    hw.dispatch()
    print "{0:} = {1:}".format(name,var)
    hw.dispatch()

def write_register(name,value):
    hw.getNode(name).write(value)
    hw.dispatch()
    var = hw.getNode(name).read()
    hw.dispatch()
    print "{0:} = {1:}".format(name,var)
    hw.dispatch()

uhal.setLogLevelTo(uhal.LogLevel.WARNING)
manager = uhal.ConnectionManager("file://connection.xml")
hw = manager.getDevice("tcs_cntr")


read_register("STATUS")
read_register("SPILL_EV_NR0");

