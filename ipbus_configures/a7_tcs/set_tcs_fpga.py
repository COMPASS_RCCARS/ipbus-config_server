#!/usr/bin/python
#
#------------------------------------
#
#   TCS Set script
#
#------------------------------------

import time
import uhal

from argparse import ArgumentParser

uhal.setLogLevelTo(uhal.LogLevel.WARNING)
manager = uhal.ConnectionManager("file://connection.xml")
hw = manager.getDevice("tcs_fpga")

def read_register(name):
    var = hw.getNode(name).read()
    hw.dispatch()
    print "{0:} = {1:}".format(name,var)
    hw.dispatch()

def write_register(name,value):
    hw.getNode(name).write(value)
    hw.dispatch()
    var = hw.getNode(name).read()
    hw.dispatch()
    print "{0:} = {1:}".format(name,var)
    hw.dispatch()

uhal.setLogLevelTo(uhal.LogLevel.WARNING)
manager = uhal.ConnectionManager("file://connection.xml")
hw = manager.getDevice("tcs_fpga")

read_register("FW_ID")

write_register("SET_REG.CLK_SRC",0)
write_register("SET_REG.SPS_SRC",0)
write_register("SET_REG.TRG_SRC",0)

write_register("SPILL_LENGTH",3)
write_register("PAUSE_LENGTH",4)
write_register("TRG_PERIOD",1000000)

write_register("SET_REG.TCS_RST",1)
write_register("SET_REG.TCS_RST",0)


