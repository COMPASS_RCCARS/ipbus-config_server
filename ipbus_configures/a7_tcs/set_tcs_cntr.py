#!/usr/bin/python
#
#------------------------------------
#
#   TCS Set script
#
#------------------------------------

import time
import uhal

from argparse import ArgumentParser

uhal.setLogLevelTo(uhal.LogLevel.WARNING)
manager = uhal.ConnectionManager("file://connection.xml")
hw = manager.getDevice("tcs_cntr")

def read_register(name):
    var = hw.getNode(name).read()
    hw.dispatch()    
    print "{0:} = {1:}".format(name,var)
    hw.dispatch()

def write_register(name,value):
    hw.getNode(name).write(value)
    hw.dispatch()
    var = hw.getNode(name).read()
    hw.dispatch()
    print "{0:} = {1:}".format(name,var)
    hw.dispatch()

uhal.setLogLevelTo(uhal.LogLevel.WARNING)
manager = uhal.ConnectionManager("file://connection.xml")
hw = manager.getDevice("tcs_cntr")


write_register("TEST_REG",0)
write_register("SLW_CMD",0)
# RC register : 
# bit 0 - RUN; 4 - PAUSE; 8 - BC2; 12 - RST; 17-23 -SUB DAQ
write_register("RC",0)
write_register("FLT_PERIOD", 1000)
write_register("FLT_RND_DOWNSCALE",200)


