#!/usr/bin/python

import uhal

import argparse
import math
import time

uhal.disableLogging()

cm = uhal.ConnectionManager("file://connection.xml")
hw = cm.getDevice("prescaler")

FW = hw.getNode("FW_ID").read()
hw.dispatch()
print("Firmware:   " + str(FW))

hw.getNode("SCR.IPB_RST").write(1)
hw.dispatch()
hw.getNode("SCR.IPB_RST").write(0)
hw.dispatch()
print("RESET")
hw.dispatch()
hw.getNode("SCR.BUSY_EN").write(0)
hw.dispatch()
busy = hw.getNode("SCR.BUSY_EN").read()
hw.dispatch()
print("BUSY_EN = "+str(busy))
hw.getNode("PRESCALER0").write(1000)
hw.getNode("PRESCALER1").write(100)
hw.getNode("PRESCALER2").write(0)
hw.getNode("PRESCALER3").write(0)
hw.getNode("PRESCALER4").write(0)
hw.getNode("PRESCALER5").write(0)
hw.getNode("PRESCALER6").write(0)
hw.getNode("PRESCALER7").write(0)
hw.dispatch()
prescaler0 = hw.getNode("PRESCALER0").read()
prescaler1 = hw.getNode("PRESCALER1").read()
prescaler2 = hw.getNode("PRESCALER2").read()
prescaler3 = hw.getNode("PRESCALER3").read()
prescaler4 = hw.getNode("PRESCALER4").read()
prescaler5 = hw.getNode("PRESCALER5").read()
prescaler6 = hw.getNode("PRESCALER6").read()
prescaler7 = hw.getNode("PRESCALER7").read()
hw.dispatch()
print("PRESCALER0 = "+str(prescaler0))
print("PRESCALER1 = "+str(prescaler1))
print("PRESCALER2 = "+str(prescaler2))
print("PRESCALER3 = "+str(prescaler3))
print("PRESCALER4 = "+str(prescaler4))
print("PRESCALER5 = "+str(prescaler5))
print("PRESCALER6 = "+str(prescaler6))
print("PRESCALER7 = "+str(prescaler7))
hw.dispatch()




