#!/usr/bin/python

import uhal

import argparse
import math
import time

def printPRESCALER(hw):
    cnt_0         = hw.getNode("CNT_0").read()
    cnt_pre_0     = hw.getNode("CNT_PRE_0").read()
    ls_cnt_0      = hw.getNode("LS_CNT_0").read()
    ls_cnt_pre_0  = hw.getNode("LS_CNT_PRE_0").read()
    cnt_1         = hw.getNode("CNT_1").read()
    cnt_pre_1     = hw.getNode("CNT_PRE_1").read()
    ls_cnt_1      = hw.getNode("LS_CNT_1").read()
    ls_cnt_pre_1  = hw.getNode("LS_CNT_PRE_1").read()
    cnt_2         = hw.getNode("CNT_2").read()
    cnt_pre_2     = hw.getNode("CNT_PRE_2").read()
    ls_cnt_2      = hw.getNode("LS_CNT_2").read()
    ls_cnt_pre_2  = hw.getNode("LS_CNT_PRE_2").read()
    cnt_3         = hw.getNode("CNT_3").read()
    cnt_pre_3     = hw.getNode("CNT_PRE_3").read()
    ls_cnt_3      = hw.getNode("LS_CNT_3").read()
    ls_cnt_pre_3  = hw.getNode("LS_CNT_PRE_3").read()
    cnt_4         = hw.getNode("CNT_4").read()
    cnt_pre_4     = hw.getNode("CNT_PRE_4").read()
    ls_cnt_4      = hw.getNode("LS_CNT_4").read()
    ls_cnt_pre_4  = hw.getNode("LS_CNT_PRE_4").read()
    cnt_5         = hw.getNode("CNT_5").read()
    cnt_pre_5     = hw.getNode("CNT_PRE_5").read()
    ls_cnt_5      = hw.getNode("LS_CNT_5").read()
    ls_cnt_pre_5  = hw.getNode("LS_CNT_PRE_5").read()
    cnt_6         = hw.getNode("CNT_6").read()
    cnt_pre_6     = hw.getNode("CNT_PRE_6").read()
    ls_cnt_6      = hw.getNode("LS_CNT_6").read()
    ls_cnt_pre_6  = hw.getNode("LS_CNT_PRE_6").read()
    cnt_7         = hw.getNode("CNT_7").read()
    cnt_pre_7     = hw.getNode("CNT_PRE_7").read()
    ls_cnt_7      = hw.getNode("LS_CNT_7").read()
    ls_cnt_pre_7  = hw.getNode("LS_CNT_PRE_7").read()
    or_cnt        = hw.getNode("OR_CNT").read()
    or_bsy_cnt    = hw.getNode("OR_BSY_CNT").read()
    ls_or_cnt     = hw.getNode("LS_OR_CNT").read()
    ls_or_bsy_cnt = hw.getNode("LS_OR_BSY_CNT").read()
    hw.dispatch()
    print("cnt0      :"+str(cnt_0)+str("   ")+str(cnt_pre_0))
    print("ls_cnt0   :"+str(ls_cnt_0)+str("   ")+str(ls_cnt_pre_0))
    print("cnt1      :"+str(cnt_1)+str("   ")+str(cnt_pre_1))
    print("ls_cnt1   :"+str(ls_cnt_1)+str("   ")+str(ls_cnt_pre_1))
    print("cnt2      :"+str(cnt_2)+str("   ")+str(cnt_pre_2))
    print("ls_cnt2   :"+str(ls_cnt_2)+str("   ")+str(ls_cnt_pre_2))
    print("cnt3      :"+str(cnt_3)+str("   ")+str(cnt_pre_3))
    print("ls_cnt3   :"+str(ls_cnt_3)+str("   ")+str(ls_cnt_pre_3))
    print("cnt4      :"+str(cnt_4)+str("   ")+str(cnt_pre_4))
    print("ls_cnt4   :"+str(ls_cnt_4)+str("   ")+str(ls_cnt_pre_4))
    print("cnt5      :"+str(cnt_5)+str("   ")+str(cnt_pre_5))
    print("ls_cnt5   :"+str(ls_cnt_5)+str("   ")+str(ls_cnt_pre_5))
    print("cnt6      :"+str(cnt_6)+str("   ")+str(cnt_pre_6))
    print("ls_cnt6   :"+str(ls_cnt_6)+str("   ")+str(ls_cnt_pre_6))
    print("cnt7      :"+str(cnt_7)+str("   ")+str(cnt_pre_7))
    print("ls_cnt7   :"+str(ls_cnt_7)+str("   ")+str(ls_cnt_pre_7))
    print("or_cnt    :"+str(or_cnt)+str("   ")+str(or_bsy_cnt))
    print("ls_or_cnt :"+str(ls_or_cnt)+str("   ")+str(ls_or_bsy_cnt))

uhal.disableLogging()

cm = uhal.ConnectionManager("file://connection.xml")
hw = cm.getDevice("prescaler")

printPRESCALER(hw)


