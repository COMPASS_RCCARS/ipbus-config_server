#pragma once

#define NUM_REGS_MAX 350

typedef struct Reg_Data{
   unsigned char Reg_Addr;
   unsigned char Reg_Val;
   unsigned char Reg_Mask;
} Reg_Data;

extern Reg_Data const Reg_Store_155In_155Out[NUM_REGS_MAX];
