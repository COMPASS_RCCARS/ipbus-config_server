#!/usr/bin/python

import time

import uhal

import sys

def write_register(name,value):
    hw.getNode(name).write(value)
    hw.dispatch()
    var = hw.getNode(name).read()
    hw.dispatch()
    print "{0:} = {1:}".format(name,value)
    hw.dispatch()

def read_register(name):
    value = hw.getNode(name).read()
    hw.dispatch()
    #if value != 0 :
    print "{0:} = {1:}".format(name,value)
    hw.dispatch()
    return value
    


uhal.setLogLevelTo(uhal.LogLevel.WARNING)
manager = uhal.ConnectionManager("file://connection.xml")
hw = manager.getDevice("TDC01")

#reset_scalers
#write_register("OPEN_0",1)
#write_register("OPEN_0",0)

#time.sleep(1.5) #1.5s sleep

#read_register("FIRMWARE_ID")
#read_register("TRIGGER_WINDOW")
#read_register("SIGNAL_DELAY")

delay_dif = 26
delay_sin = 1

var = read_register("TEMP_REG")
var = var * 503/1024 - 274
print "{0:} = {1:}".format("TEMP = ",var)
read_register("VOLTAGE_MON")
read_register("FIRMWARE")
read_register("STAT_REG.CLK200_LOCKED")
read_register("STAT_REG.TDCCLK_LOCKED")
#

for i in range(65):
    read_register("SCALER_{0:02}".format(i))
