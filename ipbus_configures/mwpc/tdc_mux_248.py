#!/usr/bin/python

import uhal

import argparse
import math
import time

def printClk(hw):
    freq0 = hw.getNode("freq0.freq").read()
    freq1 = hw.getNode("freq1.freq").read()
    hw.getNode("static_out1.srcid").write(458)
    hw.dispatch()
    id = hw.getNode("static_out1.srcid").read()
    hw.dispatch()
    print("Source ID: " + str(id))
    freq0 = int(freq0)*1e-6
    freq1 = int(freq1)*1e-6
    print("Frequency0: " + str(freq0) + " MHz")
    print("Frequency1: " + str(freq1) + " MHz")
    phase_phase = hw.getNode("phase.phase").read()
    phase_cnt = hw.getNode("phase.cnt").read()
    hw.dispatch()
    phase = round(int(phase_phase)*1./int(phase_cnt)*360*100)/100.
    print("Phase:      " + str(phase))

def printTCS(hw):
    tcs_status   = hw.getNode("tcs_status.tcs_status").read()
    hitcnt0 = hw.getNode("hitcnt0").read()
    hitcnt1 = hw.getNode("hitcnt1").read()
    hitcnt2 = hw.getNode("hitcnt2").read()
    hitcnt3 = hw.getNode("hitcnt3").read()
    hw.dispatch()
    print("TCS status: " + str(tcs_status))
    print("hitcnt0:    "+str(hitcnt0))
    print("hitcnt1:    "+str(hitcnt1))
    print("hitcnt2:    "+str(hitcnt2))
    print("hitcnt3:    "+str(hitcnt3))

uhal.disableLogging()

cm = uhal.ConnectionManager("file://connection.xml")
hw = cm.getDevice("tdc_mux_248")

# 0 - 125 MHz, 1 - 155.52 MHz from SiLab
hw.getNode("static_out1.useGenClk").write(1)
FW = hw.getNode("fw_version").read()
hw.dispatch()
print("Firmware:   " + str(FW))

fw = hw.getNode("fw_version").read()
hw.getNode("ip_addr_base").write(0x0a984a1F)    #Start Address of TDC cards
hw.dispatch()
ip = hw.getNode("ip_addr_base").read()    #Start Address of TDC cards
hw.dispatch()
print("FW: " + str(fw))
print("IP: " + str(hex(ip)))
hw.dispatch()


hw.getNode("static_out0.LinkReset").write(1)
hw.dispatch()
time.sleep(1)
hw.getNode("static_out0.LinkReset").write(0)
hw.dispatch()
hw.getNode("static_out0.CDCReset").write(1)
hw.dispatch()
hw.getNode("static_out0.CDCReset").write(0)

ucf_status = hw.getNode("ucf_status").read()
hw.dispatch()
if ucf_status != 0:
    print("UCF Status: " + str(hex(ucf_status)))
else:
    print("UCF Status: disconnected")

printClk(hw)
printTCS(hw)

hw.getNode("static_out3.timeout").write(0xffff)
hw.getNode("static_out2.port_en").write(0xffff)
hw.dispatch()
active_port = hw.getNode("static_out2.port_en").read()
hw.dispatch()
print("TDC port port enabled: " + str(hex(active_port)))


