#include <time.h>

#include <uhal/uhal.hpp>
#include <uhal/log/log.hpp>
#include <uhal/log/exception.hpp>
#include <iomanip>
#include <iostream>
#include <thread>
#include <string>

#include "I2CDeviceOpencores.hpp"
#include "si5338_configs.h"
#include "Si5338Driver3.hpp"


int main()
{
    uhal::setLogLevelTo(uhal::Error());
    unsigned int i2c_prescaler = 0xFC;
    int status = 0;
    const Reg_Data *reg = nullptr;
    reg = Reg_Store_155In_155Out;

    std::string boardId = "tdc_mux";
    uhal::ConnectionManager manager("file://connection.xml"); 
    uhal::HwInterface hw = manager.getDevice(boardId);           

    Si5338Driver3 si5338(hw, 0x70, reg, i2c_prescaler, 155.52);
    status = si5338.initSi5338();
    return status;

    //From here it is for debugging

    uhal::ValWord<uint32_t> sr = hw.getNode("TCS_FREQUENCY").read();
    hw.dispatch();
    std::cout << sr << std::endl;

    si5338.readClockFrequency();
    si5338.readI2CAddress();


}
