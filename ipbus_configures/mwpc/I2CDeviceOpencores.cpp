#include <uhal/uhal.hpp>
#include <uhal/log/exception.hpp>
#include <typeinfo>
#include <vector>
#include <iostream>
#include <iomanip>
#include <chrono>
#include <thread>
#include "I2CDeviceOpencores.hpp"

//#include <logger.h>
//#include <output_stream_stomp.h>
//#include <subsystem_registrator.h>

using namespace uhal;
using namespace std;

//namespace{
//    static SuS::logfile::subsystem_registrator log_id("I2CDeviceOpencores");
//}


I2CDeviceOpencores::I2CDeviceOpencores(uhal::HwInterface &hwt, uint16_t clockPrescale) : 
    hw(hwt)
{
    //DEBUG//DEBUG
    try{
//        SuS_LOG_STREAM(info, log_id(), "I2C clock prescaler = 0x" << std::hex << clockPrescale);
        ValWord<uint32_t> sr = hw.getNode("I2C_OPENCORE.SR").read();
        sr = hw.getNode("I2C_OPENCORE.SR").read();
        hw.dispatch();
//        SuS_LOG_STREAM(info, log_id(), "I2C Status at initialization = 0x" << std::hex << sr.value());

        hw.getNode("I2C_OPENCORE.CTR").write(0x00);
        hw.dispatch();
        hw.getNode("I2C_OPENCORE.PRERLO").write(clockPrescale & 0xFF);
        hw.getNode("I2C_OPENCORE.PRERHI").write((clockPrescale >> 8) & 0xFF);
        // enable master, disable interrupts
        hw.getNode("I2C_OPENCORE.CTR").write(0x80);
        hw.dispatch();
        this->failure_cnt = 0;
    }catch(uhal::exception::exception &e){
//        SuS_LOG_STREAM(severe, log_id(), "Could not initiate connection to device" );
        this->failure_cnt = FAILURE_MAX;
    }
}

void I2CDeviceOpencores::sleep()
{
    std::this_thread::sleep_for(std::chrono::microseconds(1));
}

void I2CDeviceOpencores::setSlaveAddr(uint8_t addr){
    this->addr = addr;
}

int I2CDeviceOpencores::writeByte(uint8_t data, uint8_t cr){
    try{
        hw.getNode("I2C_OPENCORE.TXR").write(data);
        hw.getNode("I2C_OPENCORE.CR").write(cr);
        hw.dispatch();
        ValWord<uint32_t> sr = hw.getNode("I2C_OPENCORE.SR").read();
        hw.dispatch();
        int loopCnt = 0;
        // wait until transfer finishes
        while((sr.value() & 2) != 0 && loopCnt < LOOPS_MAX){
            loopCnt++;
            sr = hw.getNode("I2C_OPENCORE.SR").read();
            hw.dispatch();
            sleep();
    //        cout << "WRITE SR value = " << std::hex << sr.value() << "; Data = " << std::hex << static_cast<uint32_t>(data & 0xFF) << endl;
        }
        sr = hw.getNode("I2C_OPENCORE.SR").read();
        hw.dispatch();
        if((sr.value() & 0x80) != 0){
//            SuS_LOG_STREAM(severe, log_id(), "Slave does NOT acknowledge write. SR = 0x" << std::hex << sr.value());
            return -1;
        }
        this->failure_cnt = 0;
    }catch(uhal::exception::exception &e){
//        SuS_LOG_STREAM(severe, log_id(), "Dispatch error in I2C writeByte");
        return -2;
    }
    return 0;
}

int I2CDeviceOpencores::readByte(uint8_t &data){
    try{
        hw.getNode("I2C_OPENCORE.CR").write(0x68);
        hw.dispatch();
        ValWord<uint32_t> sr = hw.getNode("I2C_OPENCORE.SR").read();
        hw.dispatch();
        int loopCnt = 0;
        // wait until transfer finishes
        while((sr.value() & 2) != 0 && loopCnt < LOOPS_MAX){
            loopCnt++;
            sr = hw.getNode("I2C_OPENCORE.SR").read();
            hw.dispatch();
            sleep();
    //        cout << "READ SR value = " << std::hex << sr.value() << endl;
        }
        sr = hw.getNode("I2C_OPENCORE.SR").read();
        hw.dispatch();
    //    if((sr.value() & 0x80) != 0){
    //        cerr << "Slave does NOT acknowledge read. SR = 0x" << std::hex << sr.value()  << endl;
    //        return -1;
    //    }
        ValWord<uint32_t> readWord = hw.getNode("I2C_OPENCORE.RXR").read();
        hw.dispatch();
        data = static_cast<uint8_t>(readWord.value() & 0xFF);
        this->failure_cnt = 0;
    }catch(uhal::exception::exception &e){
//        SuS_LOG_STREAM(severe, log_id(), "Dispatch error in I2C readByte");
        return -2;
    }
    return 0;
}

int I2CDeviceOpencores::write(uint8_t regAddr, uint8_t data){

//    SuS_LOG_STREAM(finest, log_id(), "Writing register " << static_cast<unsigned int>(regAddr) << " value 0x" << std::hex << std::setw(2) << std::setfill('0') << static_cast<unsigned int>(data));
    // write device address
    int status = writeByte((this->addr << 1) + 0, 0x90);
    if(status < 0)
    {
        if(this->failure_cnt != FAILURE_MAX)
        {
            this->failure_cnt++;
            status = this->write(regAddr, data);
        }
        else
        {
//            SuS_LOG_STREAM(severe, log_id(), "Failed writing chip address");
        }
        return status;
    }

    // write memory address
    status = writeByte(regAddr, 0x10);
    if(status < 0)
    {
        if(this->failure_cnt != FAILURE_MAX)
        {
            this->failure_cnt++;
            status = this->write(regAddr, data);
        }
        else
        {
//            SuS_LOG_STREAM(severe, log_id(), "Failed writing register address");
        }
        return status;
    }

    // write data
    status = writeByte(data, 0x50);
    if(status < 0)
    {
        if(this->failure_cnt != FAILURE_MAX)
        {
            this->failure_cnt++;
            status = this->write(regAddr, data);
        }
        else
        {
//            SuS_LOG_STREAM(severe, log_id(), "Failed writing register value");
        }
        return status;
    }
    this->failure_cnt = 0;
    return 0;
}

int I2CDeviceOpencores::writea16d16(uint16_t regAddr, uint16_t data){

//    SuS_LOG_STREAM(finest, log_id(), "Writing register " << static_cast<unsigned int>(regAddr) << " value 0x" << std::hex << std::setw(4) << std::setfill('0') << static_cast<unsigned int>(data));
    // write device address
    int status = writeByte((this->addr << 1) + 0, 0x90);
    if(status < 0)
    {
        if(this->failure_cnt != FAILURE_MAX)
        {
            this->failure_cnt++;
            status = this->write(regAddr, data);
        }
        else
        {
//            SuS_LOG_STREAM(severe, log_id(), "Failed writing chip address");
        }
        return status;
    }

    // write lower byte of the memory address
    status = writeByte(static_cast<uint8_t>(regAddr & 0xFF), 0x10);
    if(status < 0)
    {
        if(this->failure_cnt != FAILURE_MAX)
        {
            this->failure_cnt++;
            status = this->write(regAddr, data);
        }
        else
        {
//            SuS_LOG_STREAM(severe, log_id(), "Failed writing register address");
        }
        return status;
    }

    // write higher byte of the memory address
    status = writeByte(static_cast<uint8_t>(regAddr >> 8), 0x10);
    if(status < 0)
    {
        if(this->failure_cnt != FAILURE_MAX)
        {
            this->failure_cnt++;
            status = this->write(regAddr, data);
        }
        else
        {
//            SuS_LOG_STREAM(severe, log_id(), "Failed writing register address");
        }
        return status;
    }

    // write lower byte of the data
    status = writeByte(static_cast<uint8_t>(data & 0xFF), 0x10);
    if(status < 0)
    {
        if(this->failure_cnt != FAILURE_MAX)
        {
            this->failure_cnt++;
            status = this->write(regAddr, data);
        }
        else
        {
//            SuS_LOG_STREAM(severe, log_id(), "Failed writing register value");
        }
        return status;
    }

    // write higher byte of the data
    status = writeByte(static_cast<uint8_t>(data >> 8), 0x50);
    if(status < 0)
    {
        if(this->failure_cnt != FAILURE_MAX)
        {
            this->failure_cnt++;
            status = this->write(regAddr, data);
        }
        else
        {
//            SuS_LOG_STREAM(severe, log_id(), "Failed writing register value");
        }
        return status;
    }
    this->failure_cnt = 0;
    return 0;
}

int I2CDeviceOpencores::writea16d8(uint16_t regAddr, uint8_t data){

//    SuS_LOG_STREAM(finest, log_id(), "Writing register " << static_cast<unsigned int>(regAddr) << " value 0x" << std::hex << std::setw(2) << std::setfill('0') << static_cast<unsigned int>(data));
    // write device address
    int status = writeByte((this->addr << 1) + 0, 0x90);
    if(status < 0)
    {
        if(this->failure_cnt != FAILURE_MAX)
        {
            this->failure_cnt++;
            status = this->write(regAddr, data);
        }
        else
        {
//            SuS_LOG_STREAM(severe, log_id(), "Failed writing chip address");
        }
        return status;
    }

    // write lower byte of the memory address
    status = writeByte(static_cast<uint8_t>(regAddr & 0xFF), 0x10);
    if(status < 0)
    {
        if(this->failure_cnt != FAILURE_MAX)
        {
            this->failure_cnt++;
            status = this->write(regAddr, data);
        }
        else
        {
//            SuS_LOG_STREAM(severe, log_id(), "Failed writing register address");
        }
        return status;
    }

    // write higher byte of the memory address
    status = writeByte(static_cast<uint8_t>(regAddr >> 8), 0x10);
    if(status < 0)
    {
        if(this->failure_cnt != FAILURE_MAX)
        {
            this->failure_cnt++;
            status = this->write(regAddr, data);
        }
        else
        {
//            SuS_LOG_STREAM(severe, log_id(), "Failed writing register address");
        }
        return status;
    }

    // write data
    status = writeByte(data, 0x50);
    if(status < 0)
    {
        if(this->failure_cnt != FAILURE_MAX)
        {
            this->failure_cnt++;
            status = this->write(regAddr, data);
        }
        else
        {
//            SuS_LOG_STREAM(severe, log_id(), "Failed writing register value");
        }
        return status;
    }
    this->failure_cnt = 0;
    return 0;
}

int I2CDeviceOpencores::read(uint8_t regAddr, uint8_t &readData){
    int status = writeByte((addr << 1) + 0, 0x90);
    if(status < 0)
    {
        if(this->failure_cnt != FAILURE_MAX)
        {
            this->failure_cnt++;
            status = this->read(regAddr, readData);
        }
        else
        {
//            SuS_LOG_STREAM(severe, log_id(), "Failed writing chip address");
        }
        return status;
    }
    
    status = writeByte(regAddr, 0x10);
    if(status < 0)
    {
        if(this->failure_cnt != FAILURE_MAX)
        {
            this->failure_cnt++;
            status = this->read(regAddr, readData);
        }
        else
        {
//            SuS_LOG_STREAM(severe, log_id(), "Failed writing register address");
        }
        return status;
    }
    
    status = writeByte((addr << 1) + 1, 0x90);
    if(status < 0)
    {
        if(this->failure_cnt != FAILURE_MAX)
        {
            this->failure_cnt++;
            status = this->read(regAddr, readData);
        }
        else
        {
//            SuS_LOG_STREAM(severe, log_id(), "Failed writing register address");
        }
        return status;
    }
    
    status = readByte(readData);
    if(status < 0)
    {
        if(this->failure_cnt != FAILURE_MAX)
        {
            this->failure_cnt++;
            status = this->read(regAddr, readData);
        }
        else
        {
//            SuS_LOG_STREAM(severe, log_id(), "Failed writing register value");
        }
        return status;
    }

//    SuS_LOG_STREAM(finest, log_id(), "Read register " << static_cast<unsigned int>(regAddr) << " value 0x" << std::hex << std::setw(2) << std::setfill('0') << static_cast<unsigned int>(readData));
    this->failure_cnt = 0;
    return 0;
}

int I2CDeviceOpencores::reada16d8(uint16_t regAddr, uint8_t &readData){
    int status = writeByte((addr << 1) + 0, 0x90);
    if(status < 0)
    {
        if(this->failure_cnt != FAILURE_MAX)
        {
            this->failure_cnt++;
            status = this->read(regAddr, readData);
        }
        else
        {
//            SuS_LOG_STREAM(severe, log_id(), "Failed writing chip address");
        }
        return status;
    }

    status = writeByte(static_cast<uint8_t>(regAddr & 0xFF), 0x10);
    if(status < 0)
    {
        if(this->failure_cnt != FAILURE_MAX)
        {
            this->failure_cnt++;
            status = this->read(regAddr, readData);
        }
        else
        {
//            SuS_LOG_STREAM(severe, log_id(), "Failed writing register address");
        }
        return status;
    }

    status = writeByte(static_cast<uint8_t>(regAddr >> 8), 0x10);
    if(status < 0)
    {
        if(this->failure_cnt != FAILURE_MAX)
        {
            this->failure_cnt++;
            status = this->read(regAddr, readData);
        }
        else
        {
//            SuS_LOG_STREAM(severe, log_id(), "Failed writing register address");
        }
        return status;
    }

    status = writeByte((addr << 1) + 1, 0x90);
    if(status < 0)
    {
        if(this->failure_cnt != FAILURE_MAX)
        {
            this->failure_cnt++;
            status = this->read(regAddr, readData);
        }
        else
        {
//            SuS_LOG_STREAM(severe, log_id(), "Failed writing register address");
        }
        return status;
    }

    status = readByte(readData);
    if(status < 0)
    {
        if(this->failure_cnt != FAILURE_MAX)
        {
            this->failure_cnt++;
            status = this->read(regAddr, readData);
        }
        else
        {
//            SuS_LOG_STREAM(severe, log_id(), "Failed writing register value");
        }
        return status;
    }

//    SuS_LOG_STREAM(finest, log_id(), "Read register " << static_cast<unsigned int>(regAddr) << " value 0x" << std::hex << std::setw(2) << std::setfill('0') << static_cast<unsigned int>(readData));
    this->failure_cnt = 0;
    return 0;
}


