#!/usr/bin/python

import time

import uhal

import sys

def write_register(name,value):
    hw.getNode(name).write(value)
    hw.dispatch()
    var = hw.getNode(name).read()
    hw.dispatch()
    print "{0:} = {1:}".format(name,value)
    hw.dispatch()

def read_register(name):
    value = hw.getNode(name).read()
    hw.dispatch()
    #if value != 0 :
    print "{0:} = {1:}".format(name,value)
    hw.dispatch()
    return value
    


uhal.setLogLevelTo(uhal.LogLevel.WARNING)
manager = uhal.ConnectionManager("file://connection.xml")

if len(sys.argv) > 1:
  dev_name = sys.argv[1]
  print "Device name :", str(dev_name)
  hw = manager.getDevice(dev_name)
else:
  hw = manager.getDevice(TDC10)


#reset_scalers
#write_register("OPEN_0",1)
#write_register("OPEN_0",0)

#time.sleep(1.5) #1.5s sleep

read_register("FIRMWARE")
#read_register("TRIGGER_WINDOW")
#read_register("SIGNAL_DELAY")

delay_dif = 1
delay_sin = 1

#
for i in range(1):
    write_register("TDC_{0:02}.WRITE.EN".format(i),0)
    write_register("TDC_{0:02}.WRITE.CNTVALUEIN_0".format(i),0)
    write_register("TDC_{0:02}.WRITE.CNTVALUEIN_1".format(i),delay_dif)
    write_register("TDC_{0:02}.WRITE.LD_0".format(i),0)
    write_register("TDC_{0:02}.WRITE.LD_1".format(i),0)
    write_register("TDC_{0:02}.WRITE.MODE".format(i),0)
    write_register("TDC_{0:02}.WRITE.LD_0".format(i),1)
    write_register("TDC_{0:02}.WRITE.LD_1".format(i),1)
    write_register("TDC_{0:02}.WRITE.LD_0".format(i),0)
    write_register("TDC_{0:02}.WRITE.LD_1".format(i),0)

#for i in [0,4,8,12,16,20,24,28,32,36,40,44,48,52,56,60]:
#    write_register("TDC_{0:02}.WRITE.EN".format(i),1)

#write_register("TDC_{0:02}.WRITE.EN".format(64),1)
#write_register("TDC_{0:02}.WRITE.CNTVALUEIN_0".format(64),1)
#write_register("TDC_{0:02}.WRITE.CNTVALUEIN_1".format(64),delay_sin)
#write_register("TDC_{0:02}.WRITE.LD_0".format(64),1)
#write_register("TDC_{0:02}.WRITE.LD_1".format(64),1)
#write_register("TDC_{0:02}.WRITE.MODE".format(64),0)
#write_register("TDC_{0:02}.WRITE.LD_0".format(64),1)
#write_register("TDC_{0:02}.WRITE.LD_1".format(64),1)
#write_register("TDC_{0:02}.WRITE.LD_0".format(64),1)
#write_register("TDC_{0:02}.WRITE.LD_1".format(64),1)

write_register("TRG_DELAY",10)
write_register("TRG_LATENCY",50)
write_register("TRG_WINDOW",55)
