#!/usr/bin/python
import time
import uhal
from argparse import ArgumentParser

uhal.setLogLevelTo(uhal.LogLevel.WARNING)
manager = uhal.ConnectionManager("file://connection.xml")
hw = manager.getDevice("tdc_mux")

var = hw.getNode("FIRMWARE_ID").read()
hw.dispatch()
print ("Firmware ID =", str(var))
hw.dispatch()
var = hw.getNode("PORT_STATUS").read()
hw.dispatch()
print ("Port Status =", "%8.8x"% var)
hw.dispatch()
# Reset
hw.getNode("RESET").write(int("0x00000001",16))
hw.dispatch()
hw.getNode("RESET").write(int("0x00000000",16))
hw.dispatch()
print("TDC_MUX RESET")
# One more Reset
hw.getNode("RESET").write(int("0x00000002",16))
hw.dispatch()
hw.getNode("RESET").write(int("0x00000000",16))
hw.dispatch()
print("TDC_MUX RESET 2")
# Set SRC ID
hw.getNode("SRC_ID").write(int("0x000002F8",16))
hw.dispatch()
var = hw.getNode("SRC_ID").read()
hw.dispatch()
print ("SRC_ID =", str(var))
hw.dispatch()
# Port Enable
hw.getNode("PORT_EN").write(int("0x0000000f",16))
hw.dispatch()
print("Enable ports")
var = hw.getNode("PORT_EN").read()
hw.dispatch()
print ("PORT_EN =", "%4.4x" % var)
hw.dispatch()
# Data timeout
hw.getNode("TIMEOUT").write(int("0x000000ff",16))
hw.dispatch()
print("SET TIMEOUT")
var = hw.getNode("TIMEOUT").read()
hw.dispatch()
print ("TIMEOUT =", str(var))
hw.dispatch()



