#ifndef _I2CDEVICEOPENCORES_HPP_
#define _I2CDEVICEOPENCORES_HPP_

#include <uhal/uhal.hpp>
#include <typeinfo>
#include <vector>

#define LOOPS_MAX 1000
#define FAILURE_MAX 10

class I2CDeviceOpencores{
        uint8_t addr;
        uhal::HwInterface &hw;
        uint8_t failure_cnt;
    public:
        I2CDeviceOpencores(uhal::HwInterface &hwt, uint16_t clockPrescale);
        int read(uint8_t regAddr, uint8_t &readData);
        int reada16d8(uint16_t regAddr, uint8_t &readData);

        /**
         * \Brief Write data to the given 8 bit register address. 
         *      In case of errors the function will try to repeat the process
         *      by calling itself recursively until maximum number of retries is reached.
         * \param RegAddr register address
         * \param Data data to write
         * \return Write status
         */
        int write(uint8_t regAddr, uint8_t data);

        /**
         * \Brief Write 8 bit data into the given 16 bit register address. 
         *      In case of errors the function will try to repeat the process
         *      by calling itself recursively until maximum number of retries is reached.
         * \param RegAddr register address
         * \param Data data to write
         * \return Write status
         */
        int writea16d8(uint16_t regAddr, uint8_t data);

        /**
         * \Brief Write 16 bit data into the given 16 bit register address. 
         *      In case of errors the function will try to repeat the process
         *      by calling itself recursively until maximum number of retries is reached.
         * \param RegAddr register address
         * \param Data data to write
         * \return Write status
         */
        int writea16d16(uint16_t regAddr, uint16_t data);

        /**
         * \Brief Set address of the device which will be used during the communication
         * \param addr Device address
         */
        void setSlaveAddr(uint8_t addr);
    protected:
        void sleep();
        int readByte(uint8_t &data);
        int writeByte(uint8_t data, uint8_t cr);
};

#endif

