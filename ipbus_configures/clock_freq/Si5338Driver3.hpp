#pragma once

#include <uhal/uhal.hpp>
#include "I2CDeviceOpencores.hpp"
#include "si5338_configs.h"

class Si5338Driver3
{
    public:
        Si5338Driver3(uhal::HwInterface &_hw, int _slaveAddr, const Reg_Data *_RegStore, int prescaler, double _clock = 0);
        ~Si5338Driver3(){};

        int initSi5338();
        double readClockFrequency();
        void readI2CAddress();

    protected:
        int initSi5338_manual();
        int initSi5338_web();
        I2CDeviceOpencores si5338;
        int slaveAddr;
        const Reg_Data* Reg_Store;
        bool failFlag;
        double clock;
        
        void programRegisters();
        void disable_outputs();
        void pauseLOL();
        void validateInputClockStatus();
        void configurePLLforLocking();
        void softReset();
        void restartLOL();
        void confirmPLLLockStatus();
        void copyFCALValues(bool useWebProcedure);
        void setPLLtoUseFCAL();
        void enableOutputs();
        void enableOutput3();

        double readMSDivisionFactor(uint8_t firstAddr);

};

