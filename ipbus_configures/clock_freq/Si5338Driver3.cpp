#include <uhal/uhal.hpp>
#include <uhal/log/log.hpp>
#include <uhal/log/exception.hpp>

#include "I2CDeviceOpencores.hpp"
#include <typeinfo>
#include <iostream>
#include <string>
#include <vector>


#include "Si5338Driver3.hpp"
#include "si5338_configs.h"

namespace{
//    static SuS::logfile::subsystem_registrator log_id("Si5338Driver3");
}

Si5338Driver3::Si5338Driver3(uhal::HwInterface &_hw, int _slaveAddr, const Reg_Data *_Reg_Store, int prescaler, double _clock) : 
    si5338(_hw, prescaler & 0xFFFF),
    slaveAddr(_slaveAddr),
    Reg_Store(_Reg_Store),
    failFlag(false),
    clock(_clock)
{
    si5338.setSlaveAddr(slaveAddr);

}

int Si5338Driver3::initSi5338()
{
    return initSi5338_manual();
}

int Si5338Driver3::initSi5338_manual()
{
//    SuS_LOG_STREAM(fine, log_id(), "Starting Si5338 configuration using initSi5338");

    uint8_t i2creg;
    int status = 0;
    uint8_t i2ctmp;
    int fail_cnt = 0;

    disable_outputs();
    if(failFlag)
    {
//        SuS_LOG_STREAM(severe, log_id(), "Configiration failed after disable_output");
        std::cerr << "Configiration failed after disable_output" <<std::endl;
        return -1;
    }
    pauseLOL();
    if(failFlag)
    {
//        SuS_LOG_STREAM(severe, log_id(), "Configiration failed after pauseLOL");
        std::cerr<<"Configiration failed after pauseLOL"  << std::endl;
        return -1;
    }
    programRegisters();
    if(failFlag)
    {
//        SuS_LOG_STREAM(severe, log_id(), "Configiration failed after programRegisters");
        std::cerr<<"Configiration failed after programRegisters"  << std::endl;
        return -1;
    }
    validateInputClockStatus();

    if(failFlag)
    {
//        SuS_LOG_STREAM(severe, log_id(), "Configiration failed after validateInputClockStatus");
        std::cerr<< "Configiration failed after validateInputClockStatus" << std::endl;
        return -1;
    }

    configurePLLforLocking();
    if(failFlag)
    {
//        SuS_LOG_STREAM(severe, log_id(), "Configiration failed after configurePLLforLocking");
        std::cerr<< "Configiration failed after configurePLLforLocking" << std::endl;
        return -1;
    }
    softReset();
    if(failFlag)
    {
//        SuS_LOG_STREAM(severe, log_id(), "Configiration failed after softReset");
        std::cerr<< "Configiration failed after softReset" << std::endl;
        return -1;
    }

    sleep(1);
    
    enableOutput3();
    if(failFlag)
    {
//        SuS_LOG_STREAM(severe, log_id(), "Configiration failed after enableOutput3");
        std::cerr<<"Configiration failed after enableOutput3"  << std::endl;
        return -1;
    }
    restartLOL();
    if(failFlag)
    {
//        SuS_LOG_STREAM(severe, log_id(), "Configiration failed after restartLOL");
        std::cerr<<"Configiration failed after restartLOL"  << std::endl;
        return -1;
    }
    confirmPLLLockStatus();

    if(failFlag)
    {
//        SuS_LOG_STREAM(severe, log_id(), "Configiration failed after confirmPLLLockStatus");
        std::cerr<< "Configiration failed after confirmPLLLockStatus" << std::endl;
        return -1;
    }

    copyFCALValues(false);
    if(failFlag)
    {
//        SuS_LOG_STREAM(severe, log_id(), "Configiration failed after copyFCALValues");
        std::cerr<< "Configiration failed after copyFCALValues" << std::endl;
        return -1;
    }
    setPLLtoUseFCAL();
    if(failFlag)
    {
//        SuS_LOG_STREAM(severe, log_id(), "Configiration failed after setPLLtoUseFCAL()");
        std::cerr<<"Configiration failed after setPLLtoUseFCAL()"  << std::endl;
        return -1;
    }
    enableOutputs();

    if(failFlag)
    {
//        SuS_LOG_STREAM(severe, log_id(), "Configiration failed");
        std::cerr<< "Configiration failed" << std::endl;
        return -1;
    }

//    SuS_LOG_STREAM(config, log_id(), "Si5338 Initialized");
        std::cerr<<"Si5338 Initialized"  << std::endl;
    return status;
}

int Si5338Driver3::initSi5338_web()
{
    uint8_t i2creg;
    int status = 0;
    uint8_t i2ctmp;
    int fail_cnt = 0;

//    SuS_LOG_STREAM(fine, log_id(), "Starting Si5338 configuration using initSi5338_web");

    disable_outputs();

    setPLLtoUseFCAL();
    restartLOL();

    programRegisters();
    
    validateInputClockStatus();

    if(failFlag)
    {
//        SuS_LOG_STREAM(severe, log_id(), "Configiration failed after validateInputClockStatus");
        return -1;
    }

    configurePLLforLocking();
    softReset();

    sleep(1);
    
//    confirmPLLLockStatus();

    if(failFlag)
    {
//        SuS_LOG_STREAM(severe, log_id(), "Configiration failed after confirmPLLLockStatus");
        return -1;
    }

    copyFCALValues(false);
    setPLLtoUseFCAL();

    enableOutputs();

    if(failFlag)
    {
//        SuS_LOG_STREAM(severe, log_id(), "Configiration failed");
        return -1;
    }

//    SuS_LOG_STREAM(config, log_id(), "Si5338 Initialized");
    return status;
}

void Si5338Driver3::programRegisters()
{
    int status = 0;
    uint8_t i2creg = 0;
    uint8_t i2ctmp;
//    SuS_LOG_STREAM(finer, log_id(), "Write register map");
    for(int i = 0; i < NUM_REGS_MAX; i++)
    {
        if(Reg_Store[i].Reg_Mask == 0xFF)
        {
            status = si5338.write(static_cast<uint8_t>(Reg_Store[i].Reg_Addr), static_cast<uint8_t>(Reg_Store[i].Reg_Val));
            if(status != 0)
            {
                failFlag = true;
                return;
            }
        }
        else if(Reg_Store[i].Reg_Mask != 0)
        {
            status = si5338.read(Reg_Store[i].Reg_Addr, i2creg);
            if(status != 0)
            {
                failFlag = true;
                return;
            }
            i2ctmp = (i2creg & ~(Reg_Store[i].Reg_Mask)) | (Reg_Store[i].Reg_Mask & Reg_Store[i].Reg_Val);
            status = si5338.write(static_cast<uint8_t>(Reg_Store[i].Reg_Addr), i2ctmp);
            if(status != 0)
            {
                failFlag = true;
                return;
            }
        }
    }
    
}

void Si5338Driver3::disable_outputs()
{
//    SuS_LOG_STREAM(finer, log_id(), "Disabling outputs");
    int status = 0;
    status = si5338.write(230, 0x10);
    if(status != 0)
        failFlag = true;
}


void Si5338Driver3::pauseLOL()
{
//    SuS_LOG_STREAM(finer, log_id(), "Pausing LOL");
    int status = 0;
    status = si5338.write(241, 0xE5);
    if(status != 0)
        failFlag = true;
}

void Si5338Driver3::validateInputClockStatus()
{

    // Wait until input clock is stable
//    SuS_LOG_STREAM(finer, log_id(), "Wait until input clock is stable");

    int status = 0;
    uint8_t i2creg;
    unsigned int fail_cnt = 0;

    do{
        status = si5338.read(218, i2creg);
        if(status != 0)
            failFlag = true;

        fail_cnt++;
        if(fail_cnt >= LOOPS_MAX)
        {
            failFlag = true;
            break;
        }
    }while((i2creg & 0x04) == 0x04 );
}

void Si5338Driver3::configurePLLforLocking()
{
//    SuS_LOG_STREAM(finer, log_id(), "Configure PLL for locking");
    int status = 0;
    uint8_t i2creg;

    status = si5338.read(49, i2creg);
    if(status != 0)
        failFlag = true;

    status = si5338.write(49, i2creg & 0x7F);
    if(status != 0)
        failFlag = true;
}

void Si5338Driver3::softReset()
{
//    SuS_LOG_STREAM(finer, log_id(), "Soft reset");
    int status = 0;
    status = si5338.write(246, 0x02);
    if(status != 0)
        failFlag = true;
}

void Si5338Driver3::restartLOL()
{
//    SuS_LOG_STREAM(finer, log_id(), "Restart LOL");
    int status = 0;
    status = si5338.write(241, 0x65);
    if(status != 0)
        failFlag = true;
}

void Si5338Driver3::confirmPLLLockStatus()
{
//    SuS_LOG_STREAM(finer, log_id(), "Confirm PLL Lock");
    int status = 0;
    uint8_t i2creg = 0;
    unsigned int fail_cnt = 0;

    do{        
        status = si5338.read(218, i2creg);
        if(status != 0)
            failFlag = true;

        fail_cnt++;
        // exit if lock not detected
        if(fail_cnt >= LOOPS_MAX)
        {
            failFlag = true;
            break;
        }
//        sleep(1);
//  0x15 - alarm flags, exit when all relevant alarms are cleared
    }while(i2creg & 0x15);
}

void Si5338Driver3::copyFCALValues(bool useWebProcedure)
{

    // Set FCAL values
//    SuS_LOG_STREAM(finer, log_id(), "Copy FCAL values");
    int status = 0;
    uint8_t i2creg = 0;

    status = si5338.read(235, i2creg);
    if(status != 0)
        failFlag = true;
    status = si5338.write(45, i2creg);
    if(status != 0)
        failFlag = true;

    status = si5338.read(236, i2creg);
    if(status != 0)
        failFlag = true;
    status = si5338.write(46, i2creg);
    if(status != 0)
        failFlag = true;

    status = si5338.read(237, i2creg);
    if(status != 0)
        failFlag = true;

    uint8_t i2creg_read;
    status = si5338.read(47, i2creg_read);
    if(status != 0)
        failFlag = true;

    // conflicting info in manual if 47[7:2] have to be b"000101" or b"000000"
    if(useWebProcedure)
        status = si5338.write(47,  (i2creg & 0x03));
    else
        status = si5338.write(47, 0x14 | (i2creg & 0x03));

    if(status != 0)
        failFlag = true;
}

void Si5338Driver3::setPLLtoUseFCAL()
{

    // Set PLL to use FCAL
    //SuS_LOG_STREAM(finer, log_id(), "Set PLL to use FCAL");
    int status = 0;
    uint8_t i2creg = 0;

    status = si5338.read(49, i2creg);
    if(status != 0)
        failFlag = true;

    // FCAL_OVRD_EN = 1
    status = si5338.write(49, i2creg | 0x80);
    if(status != 0)
        failFlag = true;
}

void Si5338Driver3::enableOutput3()
{
//    SuS_LOG_STREAM(finer, log_id(), "Enable output 3 for the feedback");
    int status = si5338.write(230, 0x07);
    if(status != 0)
        failFlag = true;
}

void Si5338Driver3::enableOutputs()
{
//    SuS_LOG_STREAM(finer, log_id(), "Enable outputs 0, 1, 2, and 3");
    int status = si5338.write(230, 0x00);
    if(status != 0)
        failFlag = true;
}

double Si5338Driver3::readMSDivisionFactor(uint8_t firstAddr)
{
    int status = 0;
    uint8_t tmp;
    unsigned int ms = 0;

    status |= si5338.read(firstAddr, tmp);
    ms      = tmp;
    status |= si5338.read(firstAddr + 1, tmp);
    ms     += tmp << 8;
    status |= si5338.read(firstAddr + 2, tmp);
    ms     += (tmp & 0x03) << 16;

//    SuS_LOG_STREAM(finer, log_id(), "MS = " << ms);

    if( status != 0)
        failFlag = true;

    return ms / 128.0 + 4.0;
}

void Si5338Driver3::readI2CAddress()
{
    uint8_t tmp;
    int status = 0;
    status |= si5338.read(27, tmp);
    std::cout << "Status: " << status << " Value: "<< std::hex << (int)tmp << std::endl;

}

double Si5338Driver3::readClockFrequency()
{
    int status = 0;
    failFlag   = false;

    uint8_t tmp;
    unsigned int p1div = 0;
    unsigned int r0div = 0;
    unsigned int r1div = 0;
    double ms_n        = 0;
    double ms_0        = 0;
    double ms_1        = 0;
    double ms_2        = 0;
    double ms_3        = 0;
    double frequency0  = 0;
    double frequency1  = 0;

    status |= si5338.read(29, tmp);
    p1div   = tmp & 0x07;

    status |= si5338.read(31, tmp);
    r0div   = (tmp & 0x1C) >> 2;

    status |= si5338.read(32, tmp);
    r1div   = (tmp & 0x1C) >> 2;

    ms_n    = readMSDivisionFactor(97);
    ms_0    = readMSDivisionFactor(53);
    ms_1    = readMSDivisionFactor(64);
    ms_2    = readMSDivisionFactor(75);
    ms_3    = readMSDivisionFactor(86);

    std::cout << "P0DIV = " << p1div << "; MSN = " << ms_n << "; MS0 = " << ms_0 << "; R1DIV = " << r0div << std::endl;
    std::cout << "P1DIV = " << p1div << "; MSN = " << ms_n << "; MS1 = " << ms_1 << "; R1DIV = " << r1div << std::endl;
//    std::cout << "P2DIV = " << p1div << "; MSN = " << ms_n << "; MS2 = " << ms_2 << "; R1DIV = " << r2div << std::endl;

    frequency0 = clock / std::pow(2, p1div) * ms_n / ms_1 / std::pow(2, r1div);
    frequency1 = clock / std::pow(2, p1div) * ms_n / ms_0 / std::pow(2, r0div);

    if(status == 0 && failFlag == false)
    {
//        SuS_LOG_STREAM(fine, log_id(), "Clock frequency is " << frequency << " MHz");
        std::cout << "Clock frequency0 is " << frequency0 << " MHz and frequency1 is " << frequency1 <<" MHz"<< std::endl;
        return frequency0;
    }

//    SuS_LOG_STREAM(severe, log_id(), "Failed reading clock frequency");
    failFlag = true;
    return -1;
}
