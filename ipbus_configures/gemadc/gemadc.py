#!/usr/bin/python
#Example script for configuration of the gemadc device
#I2C not tested yet

import uhal
import numpy as np
import argparse


uhal.disableLogging()

def i2cInit(hw, apv, clockPrescale):
    print("I2C clock prescaler = " +  hex(clockPrescale))
    node = "I2C_APV" + str(apv) + "."
    sr = hw.getNode(node + "SR").read();
    sr = hw.getNode(node + "SR").read();
    hw.dispatch();
    print("I2C Status at initialization = " + hex(sr.value()) )
   
    hw.getNode(node + "CTR").write(0x00);
    hw.dispatch();
    hw.getNode( node + "PRERLO").write(clockPrescale & 0xFF);
    hw.getNode( node + "PRERHI").write((clockPrescale >> 8) & 0xFF);
    # enable master, disable interrupts
    hw.getNode(node + "CTR").write(0x80);
    hw.dispatch();


def i2cWriteByte(hw, node, data, cr):
    LOOPS_MAX = 10
    hw.getNode(node + "TXR").write(data)
    hw.getNode(node + "CR").write(cr)
    hw.dispatch()
    sr = hw.getNode(node + "SR").read()
    hw.dispatch()
    loopCnt = 0
    while (sr.value() & 2) != 0 and loopCnt < LOOPS_MAX :
        loopCnt = loopCnt + 1
        sr = hw.getNode(node + "SR").read()
        hw.dispatch()
        time.sleep(1)
        print sr.value()
    sr = hw.getNode(node + "SR").read()
    hw.dispatch()
    if (sr.value() & 0x80) != 0 :
        print( "Slave does NOT acknowledge write. SR = " + hex(sr.value()) )
        return -1
    return 0

def i2cReadByte(hw,node):
    LOOPS_MAX = 10
    hw.getNode(node + "CR").write(0x68)
    hw.dispatch()
    sr = hw.getNode(node + "SR").read()
    hw.dispatch()
    loopCnt = 0
    while (sr.value() & 2) != 0 and loopCnt < LOOPS_MAX :
        loopCnt = loopCnt + 1
        sr = hw.getNode(node + "SR").read()
        hw.dispatch()
        time.sleep(1)
    sr = hw.getNode(node + "SR").read()
    hw.dispatch()
    readWord = hw.getNode(node + "RXR").read();
    hw.dispatch();
    data = readWord.value() & 0xFF;
    return data

def apvI2cWrite(hw, apv, addr, reg, value):
    addr = 0x20 + (addr & 0x1F)
    node = "I2C_APV" + str(apv) + "."
    status = i2cWriteByte(hw, node, addr<<1, 0x90)
    if status != 0 :
        return status
    status = i2cWriteByte(hw, node, reg<<1, 0x10)
    if status != 0 :
        return status
    status = i2cWriteByte(hw, node, value, 0x50)
    if status != 0 :
        return status
    return 0

def apviI2cRead(hw, apv, addr, reg):
    addr = 0x20 + (addr & 0x1F)
    node = "I2C_APV" + str(apv) + "."
    status = i2cWriteByte(hw, node, addr<<1, 0x90)
    if status != 0 :
        return status
    status = i2cWriteByte(hw, node, (reg<<1)+1, 0x50)
    if status != 0 :
        return status
    status = i2cWriteByte(hw, node, addr<<1+1, 0x90)
    if status != 0 :
        return status
    data = i2cReadByte(hw, node)
    return data


cm = uhal.ConnectionManager("file://connection.xml")
hw = cm.getDevice("gemadc.0")

#Enable power on ADC board
hw.getNode("CONFIG3.PWR_UP").write(1)
hw.dispatch()
#Enable automatic adc receiver phase adjustment
hw.getNode("CONFIG3.ENABLE_PHASE_DET").write(1)
#configure ADC and DACs
hw.getNode("CONFIG3.ADC_CONFIG_REG0").write(0)
hw.getNode("CONFIG3.ADC_CONFIG_REG1").write(0)
hw.getNode("CONFIG3.ADC_POWERDOWN_MASK").write(0)
hw.getNode("CONFIG3.ADC_CUSTOM_PATTERN").write(0)
hw.getNode("DAC_VALUE.VALUE0").write(1000)
hw.getNode("DAC_VALUE.VALUE1").write(1000)
hw.dispatch()
hw.getNode("RST_REG.ADC_WRITE").write(1)
hw.dispatch()
#SYNC receiver to ADCs
hw.getNode("RST_REG.ADC_RECEIVER_RESET").write(1)
hw.dispatch()

#Write thresholds
hw.getNode("THR_BRAM_ADDR").write(0)
hw.dispatch()
for APV in range (16):
    data = list()
    for CH in range(128):
        val = (APV<<7)+CH
        data.append(val)
    hw.getNode("THR_BRAM_DATA").writeBlock(data)
    hw.dispatch()

###Write pedestals
hw.getNode("PED_BRAM_ADDR").write(0)
hw.dispatch()
for APV in range (16):
    data = list()
    for CH in range(128):
        val = (APV<<7)+CH
        data.append(val)
    hw.getNode("PED_BRAM_DATA").writeBlock(data)
    hw.dispatch()


#read thresholds
print " THR_BRAM_DATA1="
hw.getNode("THR_BRAM_ADDR").write(0)
hw.dispatch()
for APV in range (16):
    rb=hw.getNode("THR_BRAM_DATA").readBlock(128)
    hw.dispatch()
    print(rb)

#read pedestals
print " PED_BRAM_DATA1="
hw.getNode("PED_BRAM_ADDR").write(0)
hw.dispatch()
for APV in range (16):
    rb=hw.getNode("PED_BRAM_DATA").readBlock(128)
    hw.dispatch()
    print(rb)

i2cInit(hw,0,4)
status = apvI2cWrite(hw, 0, 0, 0, 1)
print(status)

