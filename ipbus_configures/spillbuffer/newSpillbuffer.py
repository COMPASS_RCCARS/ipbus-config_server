#!/usr/bin/python

import uhal
def write_register(hw,name,value):
    hw.getNode(name).write(value)
    hw.dispatch()
    var = hw.getNode(name).read()
    hw.dispatch()
    print("{0:30} = {1:10},  readback {2:10}".format(name,value,var))
    hw.dispatch()

def read_register(hw,name):
    var = hw.getNode(name).read()
    hw.dispatch()
    print("{0:30} =  {1:10}".format(name,var))
    return var

def doit(spillbuffer):
       uhal.setLogLevelTo(uhal.LogLevel.WARNING)
       manager = uhal.ConnectionManager("file://./connection.xml")
       hw = manager.getDevice(spillbuffer)
       read_register(hw, "GB.FPGAFV")
       read_register(hw, "GB.IP_ADDRESS")

       read_register(hw, "GB.MEMORY_STATUS.CALIB_COMPLETE")

       status_l0 = read_register(hw,"S0.AURORA_STATUS.link_up")
       status_l1 = read_register(hw,"S1.AURORA_STATUS.link_up")
       status_l2 = read_register(hw,"S2.AURORA_STATUS.link_up")
       link_status = int(status_l0)+(int(status_l1)<<1)+(int(status_l2)<<2)


       write_register(hw,"GB.SourceID.SID",123)
       write_register(hw,"GB.INPUT_SEL.input_sel",1)
       write_register(hw,"GB.multi_input.input_mask",link_status)
       write_register(hw,"GB.multi_input.multi_switch_mode",0)

       for link in range(0,3):
           write_register(hw,"S"+str(link)+".Timeout",1000000)

doit("spillbuffer2")

