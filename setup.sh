#!/bin/bash

MY_HOST=`hostname -s`
export DB_CLIENT=$MY_HOST
export DEBUG_CONFIG_SERVER=1
export CACTUS_PATH=/opt/cactus

if [ $MY_HOST == "nu73-101" ]; then
    export DB_SERVER=spdpc02
    export DB_DB=devdb
    export DB_USER=daq
    export DB_PASSWD=na58daq
    export DIM_DNS_NODE=nu73-101
    export DIM_DNS_NODE_CS=nu73-101
    export LD_LIBRARY_PATH=/online/daq/lib:/online/soft/lib:$CACTUS_PATH/lib:$LD_LIBRARY_PATH
elif [ $MY_HOST == "spdpc02" ]; then
    export DB_SERVER=spdpc02
    export DB_DB=devdb
    export DB_USER=daq
    export DB_PASSWD=na58daq
    export DIM_DNS_NODE=nu73-101
    export DIM_DNS_NODE_CS=nu73-101
    export LD_LIBRARY_PATH=/online/daq/lib:/online/soft/lib:$CACTUS_PATH/lib:$LD_LIBRARY_PATH
elif [ $MY_HOST == "pccore15" ]; then
    export DB_SERVER=pccodb00
    export DB_DB=devdb
    export DB_USER=daq
    export DB_PASSWD=na58daq
    export DIM_DNS_NODE=pccofs00
    export DIM_DNS_NODE_CS=pccofs00
    export LD_LIBRARY_PATH=/online/daq/lib:/online/soft/lib:$CACTUS_PATH/lib:$LD_LIBRARY_PATH
elif [ $MY_HOST == "pccore11" ]; then
    export DB_SERVER=pccodb00
    export DB_DB=devdb
    export DB_USER=daq
    export DB_PASSWD=na58daq
    export DIM_DNS_NODE=pccofs00
    export DIM_DNS_NODE_CS=pccofs00
    export LD_LIBRARY_PATH=/online/daq/lib:/online/soft/lib:$CACTUS_PATH/lib:$LD_LIBRARY_PATH
elif [ $MY_HOST == "pccore12" ]; then
    export DB_SERVER=pccodb00
    export DB_DB=devdb
    export DB_USER=daq
    export DB_PASSWD=na58daq
    export DIM_DNS_NODE=pccofs00
    export DIM_DNS_NODE_CS=pccofs00
    export LD_LIBRARY_PATH=/online/daq/lib:/online/soft/lib:$CACTUS_PATH/lib:$LD_LIBRARY_PATH
elif [ $MY_HOST == "pccore01" ]; then
    export DB_SERVER=pccore01
    export DB_DB=devdb
    export DB_USER=daq
    export DB_PASSWD=na58daq
    export DIM_DNS_NODE=pccore01
    export DIM_DNS_NODE_CS=pccore01
    export LD_LIBRARY_PATH=/online/daq/lib:/online/soft/lib:$CACTUS_PATH/lib:$LD_LIBRARY_PATH
elif [ $MY_HOST == "pccosa01" ]; then
    export DB_SERVER=pccore01
    export DB_DB=devdb
    export DB_USER=daq
    export DB_PASSWD=na58daq
    export DIM_DNS_NODE=pccore01
    export DIM_DNS_NODE_CS=pccore01
    export LD_LIBRARY_PATH=/online/daq/lib:/online/soft/lib:$CACTUS_PATH/lib:$LD_LIBRARY_PATH
elif [ $MY_HOST == "pccosa11" ]; then
    export DB_SERVER=pccore01
    export DB_DB=devdb
    export DB_USER=daq
    export DB_PASSWD=na58daq
    export DIM_DNS_NODE=pccore01
    export DIM_DNS_NODE_CS=pccore01
    export LD_LIBRARY_PATH=/online/daq/lib:/online/soft/lib:$CACTUS_PATH/lib:$LD_LIBRARY_PATH
elif [ $MY_HOST == "pcdmre01" ]; then
    export DB_SERVER=pcdmfs01
    export DB_DB=devdb
    export DB_USER=daq
    export DB_PASSWD=daq
    export DIM_DNS_NODE=pcdmfs01
    export DIM_DNS_NODE_CS=pcdmfs01
    export LD_LIBRARY_PATH=/online/daq/lib:/online/soft/lib:$CACTUS_PATH/lib:$LD_LIBRARY_PATH
else
    export DB_SERVER=dbod-amber-01
    export DB_DB=devdb
    export DB_USER=daq
    export DB_PASSWD=na66daq
    export DB_PORT=5506
    export DIM_DNS_NODE=pcamma00
    export DIM_DNS_NODE_CS=pcamma00
    export CACTUS_PATH=/online/soft/opt/cactus/latest
    export DIM_PATH=/online/soft/opt/dim/latest
    export LD_LIBRARY_PATH=$DIM_PATH/linux:$CACTUS_PATH/lib:$LD_LIBRARY_PATH
fi
