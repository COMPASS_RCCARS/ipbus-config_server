#include <stdio.h>
#include <stdint.h>

#include <uhal/uhal.hpp>
#include <iostream>
#include <sstream>
#include <fstream>

#include "main.h"
#include "Module.h"
#include "wait.h"
#include "IpbusModule.h"
#include "int2string.h"
#include "IpbusFeMux.h"
#include "IpbusMwpcTdcCard.h"

using namespace std;
using namespace uhal;


IpbusMwpcTdcCard::IpbusMwpcTdcCard(IpbusFECard::FECardType cardType, const char* name, IpbusFeMux* parent, int _port) :
  IpbusTdcCard(cardType, name, parent, _port)
{
  DEBUG("Create MWPC ifTDC Card with IPBUS name %s, in port %d.", name, _port);
  fw_id = 0;
  ipbus_read("FIRMWARE_ID", fw_id);
  DEBUG("MWPC ifTDC card firmware ID: %u", fw_id);
  uint32_t test = GetFwIdFromDB();
  DEBUG("MWPC ifTDC card firmware ID from DB: %u", test);
  if(fw_id != test)
    {
      throw Module::DBError("FW ID is not match for SrcID " + mydec(parent->getSourceID()) + " port " + mydec(GetPort()));
    }
}

IpbusMwpcTdcCard::~IpbusMwpcTdcCard()
{
  DEBUG("Destroy MWPC ifTDC Card, port %d.", GetPort());
}

void IpbusMwpcTdcCard::set_thresholds()
{
  if(GetCardType() == MWPC_T0)
    return; // this card type does not use detector inputs
  GetModule()->SendDot();
  DEBUG("IpbusMwpcTdcCard::set_thresholds for SrcID=%u",GetModule()->getSourceID());
  list<DatabaseEntryFrontend>& module_db = GetModule()->GetFrontendDB();
  list<DatabaseEntryFrontend>::iterator it;
  for(it=module_db.begin(); it!=module_db.end(); ++it)
    {
      if(it->port == GetPort())
	{
	  uint32_t CMAD_C = 8;
	  uint32_t CMAD_R = 8;
	  map<string,string> config = parseSettings(it->config, string());
	  map<string, string >::iterator cit;
	  for(cit = config.begin(); cit != config.end(); cit++)
	    {
	      if(cit->first == "CMAD_C")
		{
		  CMAD_C = toUnsigned(cit->second);
		}
	      if(cit->first == "CMAD_R")
		{
		  CMAD_R = toUnsigned(cit->second);
		}
	    }
	  uint32_t th[64];
	  uint32_t baseline[64];
	  for(uint32_t i = 0; i < 64; i++)
	    {
	      th[i] = 45;
	      baseline[i] = 650;
	    }
	  vector<string> calib_files = split(it->calib,' ');
	  for(uint32_t i = 0; i < calib_files.size(); i++)
	    {
	      string fname = calib_files[i];
	      DEBUG("Calibration file: '%s'",fname.c_str());
	      if(access(fname.c_str(), R_OK) != 0)
		throw Module::DBError(GetModule(), "File in DB " + fname + " does not exist.");
	      FILE* f = fopen(fname.c_str(),"r");
	      if(!f)
		throw Module::DBError(GetModule(), "Cannot open calibration file" + fname);
	      if(strstr(fname.c_str(),".thr")) // this is threshold file
		{
		  int nl = 0;
		  uint64_t ch_mask = 0;
		  char l[256];
		  while(fgets(l,255,f))
		    {
		      l[255] = 0;
		      nl++;
		      if(l[0] == '#') // it is commentary
			continue;
		      uint32_t v  = 0;
		      uint32_t ch = 0;
		      if(sscanf(l,"%u %u", &ch, &v) != 2 || ch >= 64 || v >= 650)
			{
			  throw Module::FileError(GetModule(),"Threshold file " + fname + " error in line " + mydec(nl));
			}
		      th[ch] = v;
		      uint64_t m = 1<<ch;
		      ch_mask |= m;
		    }
		  if(ch_mask != 0xFFFFFFFFFFFFFFFF)
		    {
		      throw Module::FileError(GetModule(),"Threshold file " + fname + " error. some channels are missing");
		    }
		}
	      else if(strstr(fname.c_str(),".bline")) // this is baseline calibration file
		{
		  int nl = 0;
		  uint64_t ch_mask = 0;
		  char l[256];
		  while(fgets(l,255,f))
		    {
		      l[255] = 0;
		      nl++;
		      if(l[0] == '#') // it is commentary
			continue;
		      uint32_t v  = 0;
		      uint32_t ch = 0;
		      uint32_t x  = 0;
		      if(sscanf(l," %u %u %u", &ch, &v, &x) != 3 || ch >= 64 || v >= 1024)
			{
			  throw Module::FileError(GetModule(),"Baseline calibration file " + fname + " error in line " + mydec(nl));
			}
		      baseline[ch] = v;
		      uint64_t m = 1<<ch;
		      ch_mask |= m;
		    }
		  if(ch_mask != 0xFFFFFFFFFFFFFFFF)
		    {
		      throw Module::FileError(GetModule(),"Baseline calibration file " + fname + " error. some channels are missing");
		    }
		}
	      fclose(f);
	    }
	  
	  for(uint32_t cmad = 0; cmad < 8; cmad++)
	    {
	      select_cmad(cmad);
	      for(uint32_t ch = 0; ch < 8; ch++)
		{
		  DEBUG("Load threshold for cmad=%u, ch=%u, baseline=%u, thresh=%u", cmad, ch, baseline[cmad*8+ch], th[cmad*8+ch]);
		  load_ch_config(ch, CMAD_C, CMAD_R, 650, baseline[cmad*8+ch]-th[cmad*8+ch]);
		}
	    }
	}
    }
}

void IpbusMwpcTdcCard::rst(const char* node_name)
{
  try
    {
      ipbus_write(node_name, 0);
      ipbus_write(node_name, 1);
    }
  catch(Module::Error& e)
    {
      DEBUG("ERROR: %s",e.what().c_str());
      throw Module::IpbusError(e.what());
    }
}

void IpbusMwpcTdcCard::select_cmad(uint32_t n)
{
  const char* node = "CMAD_A";
  rst(node);

  load_bit(node, (n>>2)&0x01);
  load_bit(node, (n>>1)&0x01);
  load_bit(node, (n   )&0x01);

  load_comm(node);
}

void IpbusMwpcTdcCard::load_bit(const char* node_name, uint32_t value)
{
  if(value == 0)
    {
      ipbus_write(node_name, 1);
      ipbus_write(node_name, 9);
      ipbus_write(node_name, 1);
    }
  else
    {
      ipbus_write(node_name, 3);
      ipbus_write(node_name, 11);
      ipbus_write(node_name, 1);
    }
}

void IpbusMwpcTdcCard::load_comm(const char* node_name)
{
  ipbus_write(node_name, 5);
  ipbus_write(node_name, 1);
}

void IpbusMwpcTdcCard::load_ch_config(uint32_t ch_num, uint32_t C, uint32_t R, uint32_t baseline, uint32_t thr)
{
  unsigned int ch_add[8] =
    {
      1, 2, 3, 4, 5, 6, 7, 8
    };
  unsigned int op_code[6] =
    {
      0,  // D4...D0 -> b9 ...b5 Thr DAQ
      2,  // D4...D0 -> b4 ...b0 Thr DAQ
      1,  // D4...D0 -> b9 ...b5 baseline DAQ
      3,  // D4...D0 -> b4 ...b0 baseline DAQ
      4,  // D3...D0 -> C value
      6   // D3...D0 -> R value
    };
  
  //DEBUG("Loading values for ch=%u : C=%u, R=%u, baseline=%u, Thr=%u", ch_num, C, R, baseline, thr);
  
  uint32_t B_1 = baseline & 0x0000001F;
  uint32_t B_2 = baseline >> 5;
  
  uint32_t Thr_1 = thr & 0x0000001F;
  uint32_t Thr_2 = thr >> 5;
    
  for(int i = 0; i < 6; i++)
    {
      uint32_t value = 0;
      switch (i)
	{
	case 0: //definde the C word
	  value = (ch_add[ch_num] << 8) + (op_code[4] << 5) + C;
	  break;
	  
	case 1: //define the R word
	  value = (ch_add[ch_num] << 8) + (op_code[5] << 5) + R;
	  break;

	case 2: //define the baseline first word
	  value = (ch_add[ch_num] << 8) + (op_code[3] << 5) + B_1;
	  break;
	  
	case 3: //define the baseline second word
	  value = (ch_add[ch_num] << 8) + (op_code[2] << 5) + B_2;
	  break;

	case 4: //define the threshold first word
	  value = (ch_add[ch_num] << 8) + (op_code[1] << 5) + Thr_1;
	  break;

	case 5: //define the threshold second word
	  value = (ch_add[ch_num] << 8) + (op_code[0] << 5) + Thr_2;
	  break;
	}
      for(int j = 0; j < 12; j++)  // Send the 12 bit data
	{
	  uint32_t bit = (value >> (11 - j)) & 0x0001;
	  load_bit("CMAD_B", bit);
	}

      load_comm("CMAD_B");
    }
}

void IpbusMwpcTdcCard::set_registers()
{
  list<DatabaseEntryFrontend> fe_db = GetModule()->GetFrontendDB();
  list<DatabaseEntryFrontend>::iterator it;
  DEBUG("IpbusMwpcTdcCard::set_registers");
  for(it = fe_db.begin(); it != fe_db.end(); it++)
    {
      if(it->port == GetPort())
	{
	  map<string,string> config = parseSettings(it->config, string());
	  map<string, string >::iterator cit;
	  for(cit = config.begin(); cit != config.end(); cit++)
	    {
	      if(cit->first == "FIRMWARE_ID") continue;
	      if(cit->first == "CMAD_R") continue;
	      if(cit->first == "CMAD_C") continue;
	      uint32_t value = toUnsigned(cit->second);
	      ipbus_write(cit->first.c_str(), value);
	    }
	  break;
	}
    }
}
