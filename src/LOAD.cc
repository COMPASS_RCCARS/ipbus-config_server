#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <list>
#include <map>
#include <string>
#include <algorithm>
#include <sstream>
#include <time.h>

#include "gs_todo.h"
#include "int2string.h"
#include "Aux.h"

using namespace std;

bool verbose;

uint32_t DB_PORT = 0;

ExecContext ctx;

int main( int argc, char* argv[] )
{
  if(!getenv ("DB_SERVER") || !getenv("DB_USER") || 
     !getenv("DB_PASSWD") || !getenv("DB_DB") || !getenv("DB_DEVLOG_DB"))
  {
    cout<<"No DB-Server environments set!"<<endl;
  }
  if(!getenv ("DB_PORT"))
    {
      cout << "$DB_PORT is not defined. Port 0 selected." << endl;
    }
  else
    {
      DB_PORT = atoi(getenv ("DB_PORT"));
    }

  int rv = 0;
  
  opterr=0;
  CommandOptions co( ctx );

  if ( (rv=co.processOptions( argc, argv )) )
    return rv;


  DBConnection devconn( getenv("DB_SERVER"),
                        getenv("DB_USER"),
                        getenv("DB_PASSWD"),
                        getenv("DB_DB"),
                        ctx );
  DBConnection devlogconn( getenv("DB_SERVER"),
                           getenv("DB_USER"),
                           getenv("DB_PASSWD"),
                           getenv("DB_DEVLOG_DB"),
                           ctx,
                           co.loggingDisabled() );

  CommandLogger logger( devlogconn, ctx );
  ModuleContainer mc( co, devconn, ctx, logger );
  
  logger.commandStart( co.strOpts() );

  // find correct setting for DIM_DNS_NODE
  char *dim_dns;
  if(!(dim_dns=getenv("DIM_DNS_NODE_CS")))
    {
      dim_dns=(char*)"pccofs00";
    }
  DimClient::setDnsNode(dim_dns);

  //map<int,string> sourceIDs;
  while( optind < argc )
    {
      mc.findSourceIDs( argv[optind++], co.toDo(), co.strOpts() );
    }

  // for -A
  if ( co.toDo() == GS_DO_ALL )
    {
      if ( (rv=mc.findModuleOptions()) )
	{
	  //exit( rv );
	}
    }
  // for -n or -A, requires reloading of minor modules
  if ( co.toDo() & GS_LOAD_FW )
    {
      if ( (rv=mc.findMinorModulesAndOptions()) )
	{
	  //exit( rv );
	}
    }
  mc.findModuleType();

  mc.runCommand();

  logger.commandFinish();
  logger.report();
  
  return 0;
}
