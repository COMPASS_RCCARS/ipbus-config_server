// $Id: tcsOpt.cc 3381 2009-06-05 08:21:13Z kraemerm $

/*!
   \file    tcsOpt.cc
   \brief   Compass Options Interpreter Class.
   \author  Benigno Gobbo 
   \version $Revision: 3381 $
   \date    $Date: 2009-06-05 10:21:13 +0200 (Fri, 05 Jun 2009) $
*/

#if defined(__GNUG__) && defined(__i386__)
#  include <getopt.h>
#  include <stdio.h>
#endif
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <regex.h>
#ifdef __SUNPRO_CC
#  include <strings.h>
#  include <stdio.h>
#endif
#include "tcsOpt.h"

#include <typeinfo>
#include <math.h>
#include "Module.h"
#include "main.h"
#include "mysql++_defs.h"

using namespace std;

// Some masks to analyze TCS receiver configuration words
#define REC_MAP_MASK         0xf0000
#define REC_MAP_CODE         0xd0000
#define REC_MAP_DAQ          0xff
#define REC_MAP_ID           0xff00
#define REC_MAP_POWER        8
#define REC_CFG_MASK         0xc0000
#define REC_CFG_CODE         0x80000
#define REC_CFG_ID           0x3fc00
#define REC_CFG_POWER        10
#define REC_DEL_MASK         0xf0000
#define REC_DEL_CODE         0xe0000
#define REC_DEL_ID           0xff00
#define REC_DEL_POWER        8
#define REC_RES_CODE         0xf0000
#define REC_RES_BITS         0x3
#define REC_RES_POWER        8

#define ZERO(x) memset(x, 0, sizeof(x))

tcsOpt::tcsOpt(const char* optfile, string _host) : host(_host) {
  conn = NULL;

  ZERO(subTimeSlice);
  ZERO(spillCounter);
  configWord = 0;
  resetWord = 0;
  deadTimeSN = 0;
  deadTimeSW = 0;
  ZERO(recMap);
  ZERO(recCfg);
  ZERO(nrRecCfg);
  ZERO(recDel);
  ZERO(recRes);

  getOptions( optfile );
}

tcsOpt::~tcsOpt()
{
}

void tcsOpt::clearComments( char* line ) {
  // It looks for "//" and replaces it with "\0"...
  regex_t re;
  regmatch_t pmatch;
  (void) regcomp( &re, "//", 0 );
  int i = regexec( &re, line, (size_t) 1, &pmatch, 0 ); 
  regfree( &re );
  if( i == 0 ) line[pmatch.rm_so] = '\0';
  // replace tabs with spaces...
  //for( unsigned int j=0; j<strlen(line); j++ ) 
    //if( line[j] == '\t' ) line[j] = ' ';
}

void tcsOpt::cleanFinalBlanks( char* line ) {
  // From end of string, find last ' ' or '\t' (tab) and replece it with '\0'
  int i;
  for( i=strlen(line)-1; i>=0 && (line[i]==' '||line[i]=='\t'); i-- );
  if( line[i+1]  == ' ' || line[i+1] == '\t' ) line[i+1] = '\0';
}

void tcsOpt::getOptions( const char* optfile, int nesting ) {

  const int lineSize = 512; 
  char line[lineSize];
  string str;
  string key = "include"; 

  // Read all lines in option file. Remove comments and final blanks and tabs

  if (!nesting) {
    conn = new mysqlpp::Connection(true);
    conn->connect(db_db,db_server,db_user,db_passwd,db_port);
  }

  mysqlpp::Query query = conn->query();
  MYSQLPP_RESULT result;
  mysqlpp::Row row;

  query << "select VALUE from FILES_PROT where PATH='/tcs/" << optfile
	<< "' and HOST='" << host << "'";
  result = query.store();
  if (result.size() != 1 && !strcmp(optfile, "config/deadtime.config")) {
    query << "select VALUE from FILES where PATH='/tcs/" << optfile
	  << "' and HOST='" << host << "'";
    result = query.store();
  }
  if (result.size() != 1) {
    throw Module::DBError(string(optfile)+" for host '" + host + "' missing in DB");
  }
  row = result.at(0);
  string optfilecontents = row.at(0).c_str();
  istringstream f( optfilecontents );

  while( f.getline( line, lineSize ) ) {

    clearComments( line );
    cleanFinalBlanks( line );

    // look for included files...    
    istringstream is(line);
    is >> str;
    if( str == key ) {
      is >> str; 
      getOptions( str.c_str(), nesting+1 ) ;
    }
    else {
      str = line;
      if( !str.empty() ) {
	options_.push_back( str );
	unused_.push_back( str );
      }
    }
  }

  if (!nesting) {
    delete conn;
    conn = NULL;
  }
}

void tcsOpt::dump( int dumplevel ) {

  // Obvious, isn't it?
  list<string>::iterator is;
  cout << "----------------------" << endl;
  cout << "   Option File Dump   " << endl;
  cout << "----------------------" << endl;
  for(is=options_.begin(); is!=options_.end(); is++ )
    cout << *is << endl;
  if( dumplevel > 0 ) {
    cout << "----------------------" << endl;
    cout << "   not used options   " << endl;
    cout << "----------------------" << endl;
    for(is=unused_.begin(); is!=unused_.end(); is++ )
      cout << *is << endl;
    if( dumplevel > 1 ) {
      cout << endl;
      cout << "----------------------" << endl;
      cout << "     wrong options    " << endl;
      cout << "----------------------" << endl;
      for(is=wrong_.begin(); is!=wrong_.end(); is++ )
	cout << *is << endl;
      cout << endl;
    }
  }
}


bool tcsOpt::getOpt( string key ) {
  // search for a line with key at the beginning
  bool status = false;
  list<string>::iterator i;
  for(i=options_.begin(); i!=options_.end(); i++ ) {
    istringstream is((*i).c_str());
    if( checkTags( key, is ) ) {
      unused_.remove( *i );
      status = true;
    }
  }
  return( status );
}


bool tcsOpt::getOpt( string key, int& num ) {
  // read an integer number following key string in a line
  bool status = false;
  list<string>::iterator i;
  for(i=options_.begin(); i!=options_.end(); i++ ) {
    istringstream is( (*i).c_str() );
    if( checkTags( key, is ) ) {
      status = getNumber( is, num );
      unused_.remove( *i );
      if( !status ) {
	wrong_.push_back( *i );
      }
    }
  }
  return( status );
}


bool tcsOpt::getOpt( string key, unsigned int& num ) {
  // read an unsigned integer number following key string in a line
  bool status = false;
  list<string>::iterator i;
  for(i=options_.begin(); i!=options_.end(); i++ ) {
    istringstream is( (*i).c_str() );
    if( checkTags( key, is ) ) {
      status = getNumber( is, num );
      unused_.remove( *i );
      if( !status ) {
	wrong_.push_back( *i );
      }
    }
  }
  return( status );
}


bool tcsOpt::getOpt( string key, unsigned long& num ) {
  // read an unsigned long number following key string in a line
  bool status = false;
  list<string>::iterator i;
  for(i=options_.begin(); i!=options_.end(); i++ ) {
    istringstream is( (*i).c_str() );
    if( checkTags( key, is ) ) {
      status = getNumber( is, num );
      unused_.remove( *i );
      if( !status ) {
	wrong_.push_back( *i );
      }
    }
  }
  return( status );
}


bool tcsOpt::getOpt( string key, double& num ) {
  // read a real number following key string in a line
  bool status = false;
  list<string>::iterator i;
  for(i=options_.begin(); i!=options_.end(); i++ ) {
    istringstream is( (*i).c_str() );
    if( checkTags( key, is ) ) {
      status = getNumber( is, num );
      unused_.remove( *i );
      if( !status ) {
	wrong_.push_back( *i );
      }
    }
  }
  return( status );
}

bool tcsOpt::getOpt( string key, float& num ) {
  // read a real number following key string in a line
  bool status = false;
  list<string>::iterator i;
  for(i=options_.begin(); i!=options_.end(); i++ ) {
    istringstream is( (*i).c_str() );
    if( checkTags( key, is ) ) {
      status = getNumber( is, num );
      unused_.remove( *i );
      if( !status ) {
	wrong_.push_back( *i );
      }
    }
  }
  return( status );
}


bool tcsOpt::getOpt( string key, string& str ) {
  // read a string following key string in a line
  bool status = false;
  list<string>::iterator i;
  for(i=options_.begin(); i!=options_.end(); i++ ) {
    istringstream is( (*i).c_str() );
    if( checkTags( key, is ) ) {
      is >> str;
      if( str.size() != 0 ) status = true;
      unused_.remove( *i );
      if( !status ) {
	wrong_.push_back( *i );
      }
    }
  }
  return( status );
}

bool tcsOpt::getOptRec( string key, string& str ) {
  // read a string following key string in a line
  bool status = false;
  list<string>::iterator i;
  for(i=unused_.begin(); i!=unused_.end(); i++ ) {
    istringstream is( (*i).c_str() );
    if( checkTags( key, is ) ) {
      is >> str;
      if( str.size() != 0 ) status = true;
      unused_.remove( *i );
      if( !status ) {
	wrong_.push_back( *i );
      }
      return( status );
    }
  }
  return( status );
}


bool tcsOpt::getOpt( string key, list<string>& strlist ) {
  // read a list of strings following key string in a line
  bool status = false;
  list<string>::iterator i;
  for(i=options_.begin(); i!=options_.end(); i++ ) {
    istringstream is( (*i).c_str() );
    if( checkTags( key, is ) ) {
      strlist.clear();
      status = true;
      while ( !is.eof() && status ) {
	string str;
	is >> str;
	strlist.push_back( str );
      }
      unused_.remove( *i );
      if( strlist.empty() ) {
	wrong_.push_back( *i );
	status = false;
      }
    }
  }
  return( status );
  } 


bool tcsOpt::getOptRec( string key, list<string>& strlist ) {
  // read a list of strings following key string in a line
  bool status = false;
  list<string>::iterator i;
  for(i=unused_.begin(); i!=unused_.end(); i++ ) {
    istringstream is( (*i).c_str() );
    if( checkTags( key, is ) ) {
      strlist.clear();
      status = true;
      while ( !is.eof() && status ) {
	string str;
	is >> str;
	strlist.push_back( str );
      }
      unused_.remove( *i );
      if( strlist.empty() ) {
	wrong_.push_back( *i );
	status = false;
      }
      return( status );
    }
  }
  return( status );
  } 


template <class T> 
bool tcsOpt::myGetOpt( string key, list<T>& Tlist ) {
  // read a list of numbers (type T) following key strings in a line
  bool status = false;
  Tlist.clear();
  list<string>::iterator i;
  for(i=options_.begin(); i!=options_.end(); i++ ) {
    istringstream is( (*i).c_str() );
    if( checkTags( key, is ) ) {
      status = getNumbersList( is, Tlist );
      unused_.remove( *i );
      if( !status ) {
	wrong_.push_back( *i );
      }
    }
  }
  return( status );
}


template <class T>
bool tcsOpt::myGetOpt( string key, vector<T>& numbers ) {
  // read a vector of numbers (type T) following key string in a line
  bool status = false;
  numbers.clear();

  list<int> elem;
  list<T> values;

  list<string>::iterator i;
  for(i=options_.begin(); i!=options_.end(); i++ ) {
    istringstream is( (*i).c_str() );
    if( checkTags( key, is ) ) {
      char* text = new char[512];
      is.getline( text, 512, '\0' );
      // get the list of vector elements
      status = getElementsList( text, elem );
      // get the corresponding list of numbers
      istringstream is( text );
      status = getNumbersList( is, values );
      unused_.remove( *i );
      if( status ) {
	// store numbers in the right positions
	list<int>::iterator j;
	typename list<T>::iterator k;
	for( j=elem.begin(), k=values.begin(); 
	     j!=elem.end() && k!=values.end(); j++, k++ ) {
	  if( int(numbers.size()) < ((*j)+1) )
	    numbers.resize( (*j)+1 );
	  numbers[(*j)] = (*k);
	}
	// the number of elements must be equal to the number of numbers
	if( j!=elem.end() || k!=values.end() ){
	  status = false;
	  wrong_.push_back( *i );
	  numbers.clear();
	}
      }
      else {
	wrong_.push_back( *i );
      }
      delete []text;
    }
  }
  
  return( status );
}

template bool tcsOpt::myGetOpt( string key, vector<unsigned int>& numbers );

bool tcsOpt::checkTags( string key, istringstream& is ) {

  // verify if line starts with <key> string
  bool status = false;
  string s1, s2;
  istringstream it(key.c_str());
  do {
    it >> s2;
    if( !it.fail() ) is >> s1;
  } while( s1 == s2 && !it.fail() );

  if( it.fail() ) {
    status = true;
  }
  return( status );
}

template <class T> bool tcsOpt::getNumber( istringstream& is, T& num ) {

  // copy a number from <is> to <num> and check if all is ok
  bool status = true;
  if( (typeid( num ) == typeid( unsigned int )) ||
      (typeid( num ) == typeid( unsigned long )) ) {
    if( intMode_ == 0 ) {
      is >> num;
    }
    else if( intMode_ == 1 ) {
      is >> hex >> num;
    }
    else if( intMode_ == 2 ) {
      is >> oct >> num;
    }
  }
  else {
    is >> num;
  }
  if( !is.fail() ) {
    string st; is >> st;
    if( st.size() != 0 ) {
      num = 0;
      status = false;
    }
  }
  else {
    num = 0;
    status = false;
  }
  return( status );
} 

template <class T>
bool tcsOpt::getNumbersList( istringstream& is, list<T>& numbers ) {

  // copy a list of numbers from <is> to <numbers> and check if all is ok
  bool status;

  if( is.eof() ) {
    numbers.clear();
    status = false;
  }
  else {
    T prev = 0, act = 0;
    bool first = true;
    numbers.clear();
    status = true;
    while( !is.eof() ) {
      string st;
      is >> st;
      if( first ) {
	first = false;
	istringstream isi(st.c_str());
	act = 0; 
	if( (typeid( act ) == typeid( unsigned int )) ||
	    (typeid( act ) == typeid( unsigned long )) ) {
	  if( intMode_ == 0 ) {
	    isi >> act;
	  }
	  else if( intMode_ == 1 ) {
	    isi >> hex >> act;
	  }
	  else if( intMode_ == 2 ) {
	    isi >> oct >> act;
	  }
	}
	else {
	  isi >> act;
	}
	if( !isi.fail() ) {
	  st.erase(); isi >> st;
	  if( st.size() == 0 ) {
	    numbers.push_back( act );
	    prev = act;
	  }
	  else {
	    numbers.clear();
	    status = false;
	  }
	}
	else {
	  numbers.clear();
	  status = false;
	}
      }
      else {
	// "N to M", "N - M", "N -> M" can be used to select 
	// numbers from N to M. Spaces are mandatory
	if( st == "to" || st == "-" || st == "->" ) {
	  // allow this only for integer numbers
	  if( typeid(numbers) == typeid(list<int>) ) {
	    is >> st;
	    istringstream isi(st.c_str());
	    act = 0; 
	    if( (typeid( act ) == typeid( unsigned int )) ||
		(typeid( act ) == typeid( unsigned long )) ) {
	      if( intMode_ == 0 ) {
		isi >> act;
	      }
	      else if( intMode_ == 1 ) {
		isi >> hex >> act;
	      }
	      else if( intMode_ == 2 ) {
		isi >> oct >> act;
	      }
	    }
	    else {
	      isi >> act;
	    }
	    st.erase(); isi >> st;
	    if( st.size() == 0 ) {
	      // N must be less than M 
	      if( prev < act ) {
		for( int i=int(prev)+1; i<=int(act); i++ ) {
		  numbers.push_back( i );
		  prev = 0;
		  first = true;
		}
	      }
	      else if( prev > act ) {
		numbers.clear();
		status = false;
	      }
	    }
	    else if( prev > act ) {
	      numbers.clear();
	      status = false;
	    }
	  }
	  else {
	    numbers.clear();
	    status = false;
	  }
	}
	// "N times M" or "N # M" can be used to select
	// N times the M number
	else if( st == "times" || st == "#" ) {
	  is >> st;
	  istringstream isi(st.c_str());
	  act = 0; 
	  if( (typeid( act ) == typeid( unsigned int )) ||
	      (typeid( act ) == typeid( unsigned long )) ) {
	    if( intMode_ == 0 ) {
	      isi >> act;
	    }
	    else if( intMode_ == 1 ) {
	      isi >> hex >> act;
	    }
	    else if( intMode_ == 2 ) {
	      isi >> oct >> act;
	    }
	  }
	  else {
	    isi >> act;
	  }
	  st.erase(); isi >> st;
	  // "N" must be integer and positive
	  if( st.size() == 0 && prev > 0 && floor((double)prev) == (double)prev ) {
	    numbers.pop_back();
	    for( T i=0; i<prev; i++ ) {
	      numbers.push_back( act );
	    }
	    prev = 0;
	    first = true;
	  }
	  else {
	    numbers.clear();
	    status = false;
	  }
	}
	else {
	  // At this point next string must be a number
	  istringstream isi(st.c_str());
	  act = 0; 
	  if( (typeid( act ) == typeid( unsigned int )) ||
	      (typeid( act ) == typeid( unsigned long )) ) {
	    if( intMode_ == 0 ) {
	      isi >> act;
	    }
	    else if( intMode_ == 1 ) {
	      isi >> hex >> act;
	    }
	    else if( intMode_ == 2 ) {
	      isi >> oct >> act;
	    }
	  }
	  else {
	    isi >> act;
	  }
	  if( !isi.fail() ) {
	    st.erase(); isi >> st;
	    if( st.size() == 0 ) {
	      numbers.push_back( act );
	      prev = act;
	    }
	    else {
	      numbers.clear();
	      status = false;
	    }
	  }
	  else {
	    numbers.clear();
	    status = false;
	  }
	}
      }
    }
  }
  return( status );
}

bool tcsOpt::getElementsList( char* text, list<int>& elem ) {

  //Extract numbers inside brackets. Separators can be "," or "-".
  //"N-M" means "from N to M".
  // e.g.: [1,3,4,6-9] = 1 3 4 6 7 8 9
  // spaces and tabs are ignored. Numbers are stored in a list.
  bool status = true;
  regex_t re;
  regmatch_t pm;
  int an, pn;

  // Well, on linux this introduces a storage area overwrite during delete...
  //char* linebuff = new char[strlen(text)];
  // Use this array, instead (hope it is long enough...).
  char linebuff[512];
  char* line = linebuff;

  strcpy( line, text );
  elem.clear();

  // remove initial spaces (in any )
  while( line[0] == ' ' || line[0] == '\t' ) line += 1;
  // At this point "[" must be at the beginning
  (void) regcomp( &re, "[[]", REG_EXTENDED );
  int i = regexec( &re, line, (size_t) 1, &pm, 0 ); 
  regfree( &re );
  if( i == 0 && pm.rm_so == 0 ) {
    line += 1;
    while( line[0] == ' ' || line[0] == '\t' ) line += 1;
    // At this point a number must be at the beginning
    (void) regcomp( &re, "[0-9]+", REG_EXTENDED );
    int i = regexec( &re, line, (size_t) 1, &pm, 0 ); 
    regfree( &re );
    if( i==0 && pm.rm_so==0 ) {
      pn = atoi( line );
      if( pn >= 0 ) {
	elem.push_back( pn ); 
      }
      else {
	status = false;
	elem.clear();
	line[0] = '\0';
      }
      line +=pm.rm_eo;
      while( line[0] == ' ' || line[0] == '\t' ) line += 1;
      while( strlen( line ) > 0 ) {
	// Look for a "]"
	(void) regcomp( &re, "[]]", REG_EXTENDED );
	int i = regexec( &re, line, (size_t) 1, &pm, 0 ); 
	regfree( &re );
	if( i == 0 && pm.rm_so == 0 ) {
	  line += 1;
	  while( line[0] == ' ' || line[0] == '\t' ) line += 1;
	  if( strlen( line ) != 0 ) {
	    strcpy( text, line );
	    line[0] = '\0';
	  }
	}
	else { 
	  // Look for a ","
	  (void) regcomp( &re, "[,]", REG_EXTENDED );
	  int i = regexec( &re, line, (size_t) 1, &pm, 0 ); 
	  regfree( &re );
	  if( i == 0 && pm.rm_so == 0 ) {
	    line += 1;
	    while( line[0] == ' ' || line[0] == '\t' ) line += 1;
	    // At this point a number must be at the beginning
	    (void) regcomp( &re, "[0-9]+", REG_EXTENDED );
	    int i = regexec( &re, line, (size_t) 1, &pm, 0 ); 
	    regfree( &re );
	    if( i==0 && pm.rm_so==0 ) {
	      pn = atoi( line );
	      if( pn >= 0 ) {
		elem.push_back( pn ); 
	      }
	      else {
		status = false;
		elem.clear();
		line[0] = '\0';
	      }
	      line +=pm.rm_eo;
	      while( line[0] == ' ' || line[0] == '\t' ) line += 1;
	    }
	    else {
	      status = false;
	      elem.clear();
	      line[0] = '\0';
	    }
	  }
	  else {
	    // Look for a "-"
	    (void) regcomp( &re, "[-]", REG_EXTENDED );
	    int i = regexec( &re, line, (size_t) 1, &pm, 0 ); 
	    regfree( &re );
	    if( i == 0 && pm.rm_so == 0 ) {
	      line += 1;
	      while( line[0] == ' ' || line[0] == '\t' ) line += 1;
	      // At this point a number must be at the beginning
	      (void) regcomp( &re, "[0-9]+", REG_EXTENDED );
	      int i = regexec( &re, line, (size_t) 1, &pm, 0 ); 
	      regfree( &re );
	      if( i==0 && pm.rm_so==0 ) {
		an = atoi( line );
		if( pn < an ) {
		  for( int i=pn+1; i<=an; i ++ ) {
		    elem.push_back( i );
		  }
		}
		else {
		  status = false;
		  elem.clear();
		  line[0] = '\0';
		}
		line +=pm.rm_eo;
		while( line[0] == ' ' || line[0] == '\t' ) line += 1;
	      }
	      else {
		status = false;
		elem.clear();
		line[0] = '\0';
	      }
	    }
	    else {
	      status = false;
	      elem.clear();
	      line[0] = '\0';
	    }
	  }
	}
      }
    }
  }
  //delete []linebuff;
  return( status );
}

void tcsOpt::setIntMode( string mode ) {
  if( mode == "dec" ) {
    intMode_ = 0;
  }
  else if( mode == "hex" ) {
    intMode_ = 1;
  }
  else if( mode == "oct" ) {
    intMode_ = 2;
  }
  DEBUG("intMode_ is '%s'=%d", mode.c_str(),intMode_);
}

bool tcsOpt::parse()
{
  int i,j,l;
  unsigned int recNr, daqMask;
  char nr[4];
  string RecLine("ReceiverXXX");
  string FileLine("ConfFileXXX"); 
  string FileName(128u, ' ');

  setIntMode( "hex" );

  // Allocate place in timeSlice
  if( int(timeSlice.size()) < (MAX_DAQS+1) )
    {
      timeSlice.resize( MAX_DAQS+1 );
    }

  if(!getOpt("Timeslice",timeSlice))
    {
      cerr << "No time slices found" << endl;
      return false;
    }
  else
    {
      i=0;
      while(timeSlice[i]!=0)
	{
	  subTimeSlice[i] = timeSlice[i];
	  // cout << "Time slice for DAQ " << i << " is " << hex << subTimeSlice[i] <<endl;
	  i++;
	}
    }

  // All DAQs to be stopped at configuration time...
  for(i=0; i<MAX_DAQS; i++)
    {
      timeSlice[i]=0;
    }

  // Loop over receiver lines
  confRec.clear();
  for(l=0; l<=MAX_REC; l++)
    {
      sprintf(nr,"%3.3d",l);
      RecLine="Receiver";
      RecLine.append(nr);

      // Reset confSubRec
      confSubRec.clear();

      if(getOpt(RecLine,confSubRec))
	{
	  DEBUG(" Found receiver %s", RecLine.c_str());
	  
	  // Create a map of receivers -- we need it for selective CATCH resets
	  i=0;
	  while(confSubRec[i]!=0)
	    {
	      confRec.push_back(confSubRec[i]);
	      // Mapping of receiver to DAQ
	      if((confSubRec[i]&REC_MAP_MASK)==REC_MAP_CODE)
		{
		  daqMask= (confSubRec[i]&REC_MAP_DAQ);
		  recNr  = (confSubRec[i]&REC_MAP_ID) >> REC_MAP_POWER;
		  recMap[recNr]=daqMask;
		  // cout << "Receiver " << recNr << ": DAQ " << recMap[recNr] << endl;
		  // Configuration of receiver
		}
	      else if ((confSubRec[i]&REC_CFG_MASK)==REC_CFG_CODE)
		{
		  recNr= (confSubRec[i]&REC_CFG_ID) >> REC_CFG_POWER;
		  if (nrRecCfg[recNr] < MAX_REC)
		    {
		      j=nrRecCfg[recNr]++;
		      recCfg[recNr][j]=confSubRec[i];
		    }
		  else
		    {
		      cout << "overflow at " << RecLine << endl;
		    }
		  recRes[recNr]=REC_RES_CODE + (recNr << REC_RES_POWER) + REC_RES_BITS;
		  // cout << "Receiver " << recNr << hex << ": CFG " << recCfg[recNr][j] << " reRes: " 
		  //      << recRes[recNr] << dec << endl;
		}
	      else if ((confSubRec[i]&REC_DEL_MASK)==REC_DEL_CODE)
		{
		  recNr= (confSubRec[i]&REC_DEL_ID) >> REC_DEL_POWER;
		  recDel[recNr]=confSubRec[i];
		  // cout << "Receiver " << recNr << ": DEL " << recDel[recNr] << endl;
		}
	      i++;
	    }
	}
    }
  DEBUG("Found %ld receiver words", confRec.size());
  confRec.push_back(0);
  
  if(confRec.size() == 1)
    {
      cerr << "No TCS receiver configuration found" << endl;
      return false;
    }

  if(!getOpt("TCSconfig",configWord))
    {
      cerr << "No TCS config word found" << endl;
      return false;
    }

  if(!getOpt("TCSreset",resetWord))
    {
      cerr << "No TCS reset word found" << endl;
      return false;
    }

  if(!getOpt("DeadtimeF",deadTimeF))
    {
      cerr << "No fixed deadtime config word found" << endl;
      return false;
    }

  if(!getOpt("DeadtimeN",deadTimeN))
    {
      cerr << "No triggers for variable deadtime config word found" << endl;
      return false;
    }

  if(!getOpt("DeadtimeW",deadTimeW))
    {
      cerr << "No window for variable deadtime config word found" << endl;
      return false;
    }

  if(!getOpt("DeadtimeSN",deadTimeSN))
    {
      cerr << "No triggers for 2nd variable deadtime config word found" << endl;
      // return false;
    }

  if(!getOpt("DeadtimeSW",deadTimeSW))
    {
      cerr << "No window for 2nd variable deadtime config word found" << endl;
      // return false;
    }

  return true;
}

bool tcsOpt::readStatusFile(string host)
{
  mysqlpp::Connection conn(true);
  conn.connect(db_db,db_server,db_user,db_passwd,db_port);
  mysqlpp::Query query = conn.query();
  MYSQLPP_RESULT result;
  mysqlpp::Row row;
  
  query << "select VALUE from FILES where PATH='/tcs/tcs.status' and HOST='"
	<< host << "'";
  result = query.store();
  if (result.size() == 1)
    {
      row = result.at(0);
      string s = row.at(0).c_str();
      istringstream statFile(s);
      for(int i=0; i<MAX_DAQS; i++)
	{
	  statFile >> hex >> timeSlice[i];
	  // cout << "Reading Time slice " << i << " : " << hex << timeSlice[i] << endl;
	}
    }
  else
    { 
      for(int i=0; i<MAX_DAQS; i++)
	{
	  timeSlice[i]=0;
	}
      return false;
    }
  return true;
}

bool tcsOpt::writeStatusFile(string host)
{
  ostringstream statFile;
  if(!statFile.bad())
    { 
      for(int i=0; i<MAX_DAQS; i++)
	{
	  statFile << hex << timeSlice[i] <<endl;
	}
      mysqlpp::Connection conn(true);
      conn.connect(db_db,db_server,db_user,db_passwd,db_port);
      mysqlpp::Query query = conn.query();
      query << "replace FILES (PATH,HOST,VALUE) values ('/tcs/tcs.status','"
	    << host << "','" << statFile.str() << "')";
      query.execute();
    }
  else
    {
      cerr << "Error writing tcs.status" << endl;
      return false;
    }
  return true;
}
