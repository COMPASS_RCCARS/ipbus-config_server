#include <mysql++.h>
#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "mysql++_defs.h"

using namespace std;

void usage()
{
  cout << "usage: file_db_interface [-P] [-h host] [-d db] [-u user] "
       <<"[-p passwd] " << "src dest" << endl << endl
       << "If login info are not given they are taken from DATE_DB_*." << endl
       << "src and dest either specify local files or <HOST>:<PATH>" << endl;
  exit(1);
}

void read_from_db(string host, string db, string user, string passwd, uint32_t port,
                  string src, string dest, bool prot)
{
  size_t pos = src.find(':');
  string path_host = src.substr(0, pos);
  string path = src.substr(pos + 1, string::npos);

  try {
    mysqlpp::Connection conn(true);
    conn.connect(db.c_str(), host.c_str(), user.c_str(), passwd.c_str(), port);
    mysqlpp::Query query = conn.query();
    MYSQLPP_RESULT result;
    mysqlpp::Row row;

    query << "select VALUE from FILES" << (prot?"_PROT":"")
	  << " where PATH=%0q and HOST=%1q";
    query.parse();
    result = query.store(path, path_host);
    if (result.size() != 1) {
      cout << "HOST=" << path_host << ", PATH=" << path << " not found"
	   << endl;
      return;
    }
    row = result.at(0);
    string contents = row.at(0).c_str();
    if (dest == "-") {
      cout << contents;
    } else {
      ofstream file(dest.c_str());
      file << contents;
    }
  } catch (exception&e) {
    cerr << "DB error: " << e.what() << endl;
  }
}

void write_to_db(string host, string db, string user, string passwd, uint32_t port,
                 string src, string dest, bool prot)
{
  size_t pos = dest.find(':');
  string path_host = dest.substr(0, pos);
  string path = dest.substr(pos + 1, string::npos);

  struct stat statbuf;
  if (stat(src.c_str(), &statbuf) < 0) {
    perror("stat");
  }
  size_t size = statbuf.st_size;
  if (size > 1000000) {
    cout << "cannot put file of size " << size << " into the DB" << endl;
    return;
  }
  char *buf = new char[size];
  FILE *file = fopen(src.c_str(), "r");
  if (!file) {
    perror("fopen");
  }
  if (fread(buf, 1, size, file) != size) {
    perror("fread");
  }
  fclose(file);
  string contents(buf, size);

  try {
    mysqlpp::Connection conn(true);
    conn.connect(db.c_str(), host.c_str(), user.c_str(), passwd.c_str(), port);
    mysqlpp::Query query = conn.query();
    MYSQLPP_RESULT result;
    mysqlpp::Row row;

    query << "replace FILES" << (prot?"_PROT":"")
	  << " (HOST,PATH,VALUE) values (%0q,%1q,%2q)";
    query.parse();
    query.store(path_host, path, contents);
  } catch (exception&e) {
    cerr << "DB error: " << e.what() << endl;
  }
}

int main(int argc, char**argv)
{
  string host;
  string db;
  string user;
  string passwd;
  int c;
  bool prot = false;
  uint32_t port = 0;

  if (getenv("DATE_DB_MYSQL_HOST")) {
    host = getenv("DATE_DB_MYSQL_HOST");
  }
  if (getenv("DATE_DB_MYSQL_DB")) {
    db = getenv("DATE_DB_MYSQL_DB");
  }
  if (getenv("DATE_DB_MYSQL_USER")) {
    user = getenv("DATE_DB_MYSQL_USER");
  }
  if (getenv("DATE_DB_MYSQL_PWD")) {
    passwd = getenv("DATE_DB_MYSQL_PWD");
  }
  if(getenv("DB_PORT")) {
    port = atoi(getenv("DB_PORT"));
  }

  while ((c = getopt(argc, argv, "Ph:d:u:p:")) != -1) {
    switch(c) {
    case 'P':
      prot = true;
      break;
    case 'h':
      host = optarg;
      break;
    case 'd':
      db = optarg;
      break;
    case 'u':
      user = optarg;
      break;
    case 'p':
      passwd = optarg;
      break;
    default:
      usage();
    }
  }

  if (optind + 2 > argc) {
    usage();
  }

  if (strchr(argv[optind], ':')) {
    read_from_db(host, db, user, passwd, port, argv[optind], argv[optind+1], prot);
  } else if (strchr(argv[optind+1], ':')) {
    write_to_db(host, db, user, passwd, port, argv[optind], argv[optind+1], prot);
  } else {
    cout << "at least one of src/dest must be DB path" << endl;
    usage();
  }

  return 0;
}
