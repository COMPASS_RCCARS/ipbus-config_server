#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include <pthread.h>

#include <string>
#include <list>
#include <map>

#include "main.h"
#include "wait.h"
#include "int2string.h"
#include "IpbusTcsController.h"
#include "IpbusTcsFpga.h"
#include "tcsOpt.h"

using namespace std;

IpbusTcsController::IpbusTcsController(const char* name, IpbusTcsFpga* parent) : 
  IpbusFECard(IpbusFECard::TCS_CONTROLLER, name, parent, 0), 
  tcs_dim("TCS/" + parent->get_tcs_name() + "/"), spill_mutex(NULL)
{
  fpga = parent;
  isThreadRunning = false;
  tcsThrottle     = 0;
  spillcastnum    = 0;

  memset(spillStatus,0,sizeof(spillStatus));
  memset(calTrigMode,0,sizeof(calTrigMode));
  memset(calTrigPeriod,0,sizeof(calTrigPeriod));

  ReadConfigCalib();
  GetModule()->GetServiceMap()["i:RC.RUN"]            = 0;
  GetModule()->GetServiceMap()["i:RC.PAUSE"]          = 0;
  GetModule()->GetServiceMap()["i:RC.BC2"]            = 0;
  GetModule()->GetServiceMap()["i:RC.RST"]            = 0;
  GetModule()->GetServiceMap()["i:RC.SUB_DAQ"]        = 0;
  GetModule()->GetServiceMap()["i:SLW_CMD"]           = 0;
  GetModule()->GetServiceMap()["i:CFG_FIFO"]          = 0;
  GetModule()->GetServiceMap()["i:STATUS"]            = 0;
  GetModule()->GetServiceMap()["i:FLT_PERIOD"]        = 0;
  GetModule()->GetServiceMap()["i:FLT_RND_DOWNSCALE"] = 0;
  GetModule()->GetServiceMap()["i:DAQ_ACT_MAP"]       = 0;

  tcs_dim["i:Burst"]       = 0;
  tcs_dim["i:BurstNumber"] = 0;
  tcs_dim["i:CmdTag"]      = 0;
  tcs_dim["c:Status"]      = "";
  
  DEBUG("A7 TCS Controller Card is created.");
  spill_mutex = new pthread_mutex_t;
  pthread_mutex_init(spill_mutex,NULL);
  spill_tid = pthread_create(&spill_tid, NULL, spill_thread, this);
}
#include <errno.h>
IpbusTcsController::~IpbusTcsController()
{
  DEBUG("Destroy A7 TCS Controller Card, port %d.", GetPort());
  isThreadRunning = false;
  pthread_join(spill_tid,NULL);
  if(pthread_mutex_destroy(spill_mutex))
    {
      printf("%s\n", strerror(errno));
      //mywait(100000);
    }
  delete spill_mutex;
  DEBUG("Done!");
}

void IpbusTcsController::ReadStatus(std::string &status)
{
  uint32_t RC;
  uint32_t SLW_CMD;
  uint32_t CFG_FIFO;
  uint32_t STATUS;
  uint32_t FLT_PERIOD;
  uint32_t FLT_RND_DOWNSCALE;
  uint32_t DAQ_ACT_MAP;
  uint32_t TIME_SLICE[MAX_DAQS];
  uint32_t FIXED_DT_DAQ[MAX_DAQS];
  uint32_t NR_TRG_DTA_DAQ[MAX_DAQS];
  uint32_t WIN_DTA_DAQ[MAX_DAQS];
  uint32_t SPILL_EV_NR[MAX_DAQS];
  uint32_t NR_TRG_DTB_DAQ0;
  uint32_t WIN_DTB_DAQ0;
  uint32_t THROTTL_DT;
  uint32_t TEST_REG;
  uint32_t PERIOD_EVTYPE[MAX_CAL_TRG];
  uint32_t MODE_EVTYPE[MAX_CAL_TRG];
  uint32_t DELAY_EVTYPE[MAX_CAL_TRG];
  uint32_t PATTERN_LENGTH;
  //uint32_t PATTERN_MEM[512];

  char str[2048];
  int i = GetPort();
  DEBUG("IpbusTcsController: port %d ReadStatus(...).\n", i);

  lock_mutex();
  ipbus_read("RC",                RC);
  ipbus_read("SLW_CMD",           SLW_CMD);
  ipbus_read("CFG_FIFO",          CFG_FIFO);
  ipbus_read("STATUS",            STATUS);
  ipbus_read("FLT_PERIOD",        FLT_PERIOD);
  ipbus_read("FLT_RND_DOWNSCALE", FLT_RND_DOWNSCALE);
  ipbus_read("DAQ_ACT_MAP",       DAQ_ACT_MAP);

  char reg_name[64];
  sprintf(reg_name,"TIME_SLICE%d",i);
  ipbus_read(reg_name,TIME_SLICE[i]);
  sprintf(reg_name,"FIXED_DT_DAQ%d",i);
  ipbus_read(reg_name,FIXED_DT_DAQ[i]);
  sprintf(reg_name,"NR_TRG_DTA_DAQ%d",i);
  ipbus_read(reg_name,NR_TRG_DTA_DAQ[i]);
  sprintf(reg_name,"WIN_DTA_DAQ%d",i);
  ipbus_read(reg_name,WIN_DTA_DAQ[i]);
  sprintf(reg_name,"SPILL_EV_NR%d",i);
  ipbus_read(reg_name,SPILL_EV_NR[i]);

  sprintf(str,"Port %d\nTIME_SLICE=%u FIXED_DT=%u NR_TRG_DTA=%u WIN_DTA=%u SPILL_EV=%u\n",i,
	  TIME_SLICE[i],FIXED_DT_DAQ[i],NR_TRG_DTA_DAQ[i],WIN_DTA_DAQ[i],SPILL_EV_NR[i]);

  status += str;

  ipbus_read("NR_TRG_DTB_DAQ0",NR_TRG_DTB_DAQ0);
  ipbus_read("WIN_DTB_DAQ0",   WIN_DTB_DAQ0);
  ipbus_read("THROTTL_DT",     THROTTL_DT);
  ipbus_read("TEST_REG",       TEST_REG);

  sprintf(str,"NR_TRG_DTB_DAQ0=%u WIN_DTB_DAQ0=%u THROTTL_DT=%u TEST_REG=%u\n",
	  NR_TRG_DTB_DAQ0,WIN_DTB_DAQ0,THROTTL_DT,TEST_REG);

  for(int j = 0; j < MAX_CAL_TRG; j++)
    {
      char reg_name[64];
      sprintf(reg_name,"PERIOD_EVTYPE_%d",j+1);
      ipbus_read(reg_name,PERIOD_EVTYPE[j]);
      sprintf(reg_name,"MODE_EVTYPE_%d",j+1);
      ipbus_read(reg_name,MODE_EVTYPE[j]);
      sprintf(reg_name,"DELAY_EVTYPE_%d",j+1);
      ipbus_read(reg_name,DELAY_EVTYPE[j]);
    }

  ipbus_read("PATTERN_LENGTH", PATTERN_LENGTH);
  // std::vector<uint32_t> data;
  // data.resize(512);
  // ipbus_read_block("PATTERN_MEM",data,512);
  // memcpy(PATTERN_MEM,data.data(),512*sizeof(uint32_t));
  unlock_mutex();

  GetModule()->GetServiceMap()["RC.RUN"]            = (int)(RC & 0x000001)      ;
  GetModule()->GetServiceMap()["RC.PAUSE"]          = (int)(RC & 0x000010) >>  4;
  GetModule()->GetServiceMap()["RC.BC2"]            = (int)(RC & 0x000100) >>  8;
  GetModule()->GetServiceMap()["RC.RST"]            = (int)(RC & 0x001000) >> 12;
  GetModule()->GetServiceMap()["RC.SUB_DAQ"]        = (int)(RC & 0xFE0000) >> 17;
  GetModule()->GetServiceMap()["SLW_CMD"]           = (int)SLW_CMD;
  GetModule()->GetServiceMap()["CFG_FIFO"]          = (int)CFG_FIFO;
  GetModule()->GetServiceMap()["STATUS"]            = (int)STATUS;
  GetModule()->GetServiceMap()["FLT_PERIOD"]        = (int)FLT_PERIOD;
  GetModule()->GetServiceMap()["FLT_RND_DOWNSCALE"] = (int)FLT_RND_DOWNSCALE;
  GetModule()->GetServiceMap()["DAQ_ACT_MAP"]       = (int)DAQ_ACT_MAP;
}

void IpbusTcsController::ReadConfigCalib()
{
  config.clear();
  calib.clear();
  list<DatabaseEntryFrontend>& fe_db = GetModule()->GetFrontendDB();
  list<DatabaseEntryFrontend>::iterator it;
  for(it=fe_db.begin(); it!=fe_db.end(); ++it)
    {
      if(it->port == GetPort())
	{
	  config = parseSettings(it->config, string());
	  if(config["SW_VERSION"] != SW_VERSION)
	    throw Module::DBError(GetModule(),"Bad config_server SW version in devdb");
	  config.erase(config.find("SW_VERSION"));
	  map<string, string > m = parseSettings(it->calib, string());
	  map<string, string >::iterator cit;
	  for(cit = m.begin(); cit != m.end(); cit++)
	    {
	      if(cit->first == "none")
		continue;
	      uint32_t param = 0;
	      if(sscanf(cit->second.c_str(), "%u", &param) != 1)
		{
		  throw Module::DBError(GetModule(),"Calibration parameter error '" + cit->first
					+ "=" + cit->second.c_str() + "'");
		}
	      calib[cit->first] = param;
	    }
	}
    }
}

void IpbusTcsController::Control(const char* cmd)
{
  DEBUG("IpbusTcsController::Control('%s')", cmd);
  if(strncmp(cmd,"SetCntrlReg",11) == 0)
    {
      string p = &cmd[11];
      map<string, mynumber<int> > params = parseSettings(p, mynumber<int>("0"));
      map<string, mynumber<int> >::iterator it;
      for(it = params.begin(); it != params.end(); it++)
	{
	  ipbus_write(it->first.c_str(),(uint32_t)it->second);
	}
      return;
    }

  /* split off first token to be prepended to final status */
  char *token;
  int daqNr        = 0;
  int throttleTime = 0;
  int triggerNr    = 0;
  int period       = 0;
  int mode         = 0;
  const char* p    = cmd;
  p = strchr(cmd, ' ');
  if (!p || (cmd + 2 > p))
    {
      p = cmd;
      token = NULL;
    }
  else
    {
      int len = p - cmd;
      p++;
      token = (char*)malloc(len + 1);
      memcpy(token, cmd, len);
      token[len] = 0;
    }
  int tag = 0;
  if (token)
    {
      tag = strtoul(token, NULL, 0) & 0x7fff;
    }

  /* init status message so that state transitions are visible */
  GetModule()->GetServiceMap()["ErrorString"] = "";

  try
    {
      switch(p[0])
	{
	case RESET_SPILL_COUNTER:
	  if(ResetSpillCounter())
	    {
	      cout << "Spill counter is reset" << endl;
	    }
	  else
	    {
	      tag |= 0x8000;
	    }
	  break;
	case STOP_COMMAND:  // Stop run
	  if(sscanf(p,"S %d",&daqNr) == 1)
	    {
	      if(Stop(daqNr))
		{
		  cout << "Run stopped on DAQ " << daqNr << endl;
		}
	      else
		{
		  tag |= 0x8000;
		}
	    }
	  else
	    {
	      cerr << "No DAQ number given" << endl;
	      tag |= 0x8000;
	    }
	  break;
	case SUPERSTOP_COMMAND:  // Stop run w/o waiting of the off-spill
	  if(sscanf(p,"O %d",&daqNr) == 1)
	    {
	      if(SuperStop(daqNr))
		{
		  cout << "Run stopped on DAQ " << daqNr << endl;
		}
	      else
		{
		  tag |= 0x8000;
		}
	    }
	  else
	    {
	      cerr << "No DAQ number given" << endl;
	      tag |= 0x8000;
	    }
	  break;
	case SUPERSTART_COMMAND:
	  if(sscanf(p,"A %d",&daqNr) == 1)
	    {
	      if(SuperStart(daqNr))
		{
		  cout << "Run super-started on DAQ " << daqNr << endl;
		}
	      else
		{
		  tag |= 0x8000;
		}
	    }
	  else
	    {
	      cerr << "No DAQ number given" << endl;
	      tag |= 0x8000;
	    }
	  break;
	case RAPIDSTART_COMMAND:
	  if(sscanf(p,"G %d",&daqNr) == 1)
	    {
	      if(RapidStart(daqNr))
		{
		  cout << "DAQ " << daqNr << " is started in rapid mode." << endl;
		}
	      else
		{
		  tag |= 0x8000;
		}
	    }
	  else
	    {
	      cerr << "No DAQ number given" << endl;
	      tag |= 0x8000;
	    }
	  break;
	case RAPIDSTOP_COMMAND:
	  if(sscanf(p,"H %d",&daqNr) == 1)
	    {
	      if(RapidStop(daqNr))
		{
		  cout << "DAQ " << daqNr << " is stopped in rapid mode." << endl;
		}
	      else
		{
		  tag |= 0x8000;
		}
	    }
	  else
	    {
	      cerr << "No DAQ number given" << endl;
	      tag |= 0x8000;
	    }
	  break;
	case START_COMMAND:
	  if(sscanf(p,"R %d",&daqNr) == 1)
	    {
	      if(Start(daqNr))
		{
		  cout << "Run started on DAQ " << daqNr << endl;
		}
	      else
		{
		  tag |= 0x8000;
		}
	    }
	  else
	    {
	      cerr << "No DAQ number given" << endl;
	      tag |= 0x8000;
	    }
	  break;
	case STARTFAST_COMMAND:
	  if(sscanf(p,"F %d",&daqNr) == 1)
	    {
	      if(StartFast(daqNr))
		{
		  cout << "Run started fast on DAQ " << daqNr << endl;
		}
	      else
		{
		  tag |= 0x8000;
		}
	    }
	  else
	    {
	      cerr << "No DAQ number given" << endl;
	      tag |= 0x8000;
	    }
	  break;
	case PAUSE_COMMAND:
	  if(sscanf(p,"P %d",&daqNr) == 1)
	    {
	      if(Pause(daqNr))
		{
		  cout << "Run paused on DAQ " << daqNr << endl;
		}
	      else
		{
		  tag |= 0x8000;
		}
	    }
	  else
	    {
	      cerr << "No DAQ number given" << endl;
	      tag |= 0x8000;
	    }
	  break;
	case CONTINUE_COMMAND:
	  if(sscanf(p,"C %d",&daqNr) == 1)
	    {
	      if(Continue(daqNr))
		{
		  cout << "Run continuing on DAQ " << daqNr << endl;
		}
	      else
		{
		  tag |= 0x8000;
		}
	    }
	  else
	    {
	      cerr << "No DAQ number given" << endl;
	      tag |= 0x8000;
	    }
	  break;
	case THROTTLE_COMMAND:  // Lower onspill length
	  if(sscanf(p,"L %d",&throttleTime)>0)
	    {
	      Throttle(throttleTime);
	    }
	  else
	    {
	      cerr << "No throttle time given" << endl;
	      tag |= 0x8000;
	    }
	  break;
	case TRIGGER_COMMAND:                        // Trigger request
	  if(sscanf(p,"T %d %d %d %d",&daqNr,&triggerNr, &period, &mode) == 4)
	    {
	      if(StoreTrigger(daqNr,triggerNr,period,mode))
		{
		  cout << "Requested trigger " << triggerNr <<" on DAQ " << daqNr
		       << endl;
		}
	      else
		{
		  tag |= 0x8000;
		}
	    }
	  else
	    {
	      cerr << "No DAQ number, trigger number, period or mode given" << endl;
	      tag |= 0x8000;
	    }
	  break;
	case RESET_COMMAND:  // Reset TCS and CATCHes
	  if(Reset())
	    {
	      cout << "Resetting TCS and CATCHes..." << endl;
	    }
	  else
	    {
	      GetModule()->GetServiceMap()["ErrorString"] = "Reset only when all DAQs stopped!";
	      cerr << "Reset only when all DAQs stopped!" << endl;
	      tag |= 0x8000;
	    }
	  break;
	default:
	  GetModule()->GetServiceMap()["ErrorString"] = "Unknown command: " + string(p);
	  tag |= 0x8000;
	  break;
	}

    }
  catch (Module::Error& e)
    {
      GetModule()->GetServiceMap()["ErrorString"] = e.what();
      cerr << "TCS Error: " << e.what() << endl;
      tag |= 0x8000;
    }
  catch (exception& e)
    {
      GetModule()->GetServiceMap()["ErrorString"] = e.what();
      cerr << "TCS DB Error: " << e.what() << endl;
      tag |= 0x8000;
    }

  if (token)
    {
      tcs_dim["CmdTag"] = tag;
      free(token);
    }
}

void IpbusTcsController::Configure()
{
  options = fpga->GetOptions();
  unsigned int subslice = 0;
  // Check read time slices for consistency
  for(int i = 0; i<MAX_DAQS; i++)
    {
      subslice = options->timeSlice[i];
      if( (subslice!=options->subTimeSlice[i]) && (subslice!=0) )
	{
	  cout << "Resetting wrong time slice for DAQ " << i << ": " 
	       << hex << subslice <<endl;
	  options->timeSlice[i] = subslice;
	}
    }
  lock_mutex();
  // Stop ALL DAQs:
  DEBUG("Stop ALL DAQs...");
  ipbus_write("TIME_SLICE0",0);
  // Write configuration registers:
  for(map<string, string >::iterator it = config.begin(); it != config.end(); it++)
    {
      DEBUG("CONFIGURATION: %s=%s",it->first.c_str(),it->second.c_str());
      ipbus_write(it->first.c_str(),atoi(it->second.c_str()));
    }
  // Write Calibration registers:
  for(map<string, uint32_t >::iterator it = calib.begin(); it != calib.end(); it++)
    {
      DEBUG("CALIBRATION: %s=%u",it->first.c_str(), it->second);
      ipbus_write(it->first.c_str(),it->second);
    }
  // Configure receivers
  int i = 0;
  while(options->confRec[i] != 0)
    {
      ipbus_write("CFG_FIFO",options->confRec[i++]);
      // VMEwrite(TCS_CMD_FIFO,options->confRec[i++]);
    }
  // Set up long/short word and run bit
  ipbus_write("RC", options->configWord);
  // VMEwrite(TCS_TCS_CONFIG,options->configWord);
  unlock_mutex();
}

int IpbusTcsController::GetSpillStatus()
{
  return int(spillStatus[0]&SPILL_STATUS_MASK); 
}

int  IpbusTcsController::SpillStatus()
{
  // Read spill status
  // spillStatus[0]=VMEread(TCS_INFO_CONFIG);
  ipbus_read("STATUS",spillStatus[0]);

  for(int i=0; i<MAX_DAQS; i++)
    {
      char reg_name[32];
      sprintf(reg_name,"SPILL_EV_NR%d", i);
      ipbus_read(reg_name, spillStatus[i+1]);
    }
#ifdef PRINT_STATUS
  static int c = 0;
  c++;
  static int status = -1;
  int _status = spillStatus[0];
  static int spill = -1;
  int _spill = BurstNumber(0);
  struct timeval tv;
  gettimeofday(&tv,NULL);
  if(_status != status)
    {
      printf("STATUS Status = %08x; Spill number: %3d %d.%06d %5d\n",
	     (uint32_t) spillStatus[0], _spill, (int)tv.tv_sec, (int)tv.tv_usec, c);      
      status = _status;
    }
  if(_spill != spill)
    {
      printf("SPILL  Status = %08x; Spill number: %3d %d.%06d %5d\n",
	     (uint32_t) spillStatus[0], _spill, (int)tv.tv_sec, (int)tv.tv_usec, c);      
      spill = _spill;
    }
#endif
  return GetSpillStatus(); 
}

int IpbusTcsController::BurstNumber(int daqNr)
{
  int ret = int((spillStatus[daqNr+1] & BURST_NR_MASK)>>BURST_POWER);
  return ret;
}

int  IpbusTcsController::Burst()
{
  return (((spillStatus[0] & ONSPILL_MASK) == ONSPILL_MASK) ? 1 : 0);
}

// Throttle rate by lowering onspill length
void IpbusTcsController::Throttle()
{
  static int old_value = -1;
  // if(tcsThrottle) VMEwrite(TCS_THROTTLE,tcsThrottle);
  if(old_value != (int)tcsThrottle)
    {
      ipbus_write("THROTTL_DT", tcsThrottle);
      old_value = (int)tcsThrottle;
    }
}

void IpbusTcsController::SetBurstStatus()
{
  tcs_dim["Burst"] = Burst();
}

void IpbusTcsController::SetBurstNumber()
{
  tcs_dim["BurstNumber"] = BurstNumber(0);
}

// Return event number
int IpbusTcsController::EventNumber(int daqNr)
{
  return (int)(spillStatus[daqNr+1] & EVENT_NR_MASK);
}

// Check if a certain artificial trigger is set and return period and mode
bool IpbusTcsController::FindTrigger(int daqNr, int triggerNr, int& period, int& mode)
{
  uint32_t trmode   = 0;
  uint32_t trperiod = 0;
  char reg_name[32];
  sprintf(reg_name,"MODE_EVTYPE_%d", triggerNr);
  ipbus_read(reg_name,trmode);
  mode = (int)trmode;
  sprintf(reg_name,"PERIOD_EVTYPE_%d",triggerNr);
  ipbus_read(reg_name,trperiod);
  period = (int)trperiod;

  // modreg = TRG_MODE_REG + (triggerNr-1)*4;
  // periodreg = TRG_PERIOD_REG + (triggerNr-1)*4;

  // *mode = VMEread(modreg);
  // *period = VMEread(periodreg);

  if(mode != 0)
    {
      list<ArtTriggers>::iterator it;
      for(it = art_triggers.begin(); it != art_triggers.end(); it++)
	{
	  if(it->daqNr == daqNr && it->trigNr == triggerNr)
	    {
	      it->period = period;
	      it->mode   = mode;
	      break;
	    }
	}
      if(it == art_triggers.end())
	{
	  ArtTriggers newTrigger;
	  newTrigger.daqNr  = daqNr;
	  newTrigger.trigNr = triggerNr;
	  newTrigger.period = period;
	  newTrigger.mode   = mode;
	  art_triggers.push_back(newTrigger);
	}
      return true;
    }
  return false;
}

bool IpbusTcsController::WaitOffspill() // waiting for next offspill
{
  int retries = OFFSPILL_WAIT_MAXCOUNT;
  uint32_t scn = spillcastnum;
  bool ret = true;
  while (scn == spillcastnum)
    {
      if (!--retries)
	{
	  cout << "Timeout waiting for offspill!" << endl;
	  ret = false;
	  break;
	}
      usleep(OFFSPILL_WAIT_INTERVAL);
    }
  return ret;
}

void* IpbusTcsController::spill_thread(void* p)
{
  IpbusTcsController* tcs = (IpbusTcsController*)p;
  tcs->isThreadRunning = true;
  tcs->spillcastnum    = 0;
  int status     = 0;
  int oldstatus  = 0;
  int spill      = 0;
  int oldspill   = 0;
  try
    {
      while(tcs->isThreadRunning)
	{
	  oldstatus = status;
	  tcs->lock_mutex();
	  status = tcs->SpillStatus();
	  spill  = tcs->BurstNumber(0);
	  if(status != oldstatus)
	    {
	      if(!tcs->Burst())
		tcs->Throttle();
	      //printf(">>> status = %x, spill = %d time = %d\n", (uint32_t)status, spill, time(NULL));
	    }
	  tcs->unlock_mutex();
	  if(spill != oldspill)
	    {
	      tcs->SetBurstNumber();
	      oldspill  = spill;
	    }
	  if((status&ONSPILL_MASK) != (oldstatus&ONSPILL_MASK))
	    {
	      if(!tcs->Burst())
		{
		  tcs->spillcastnum++;
		}
	      tcs->SetBurstStatus();
	      // I forgot why i wrote this:
	      // if((status&ONSPILL_MASK) != 0)
	      // tcs->SetCalTriggers();
	    }
	  usleep(OFFSPILL_WAIT_INTERVAL);
	}
    }
  catch(...)
    {
      printf("FATAL ERROR: exception in the burst thread.\n");
    }
  tcs->isThreadRunning = false;
  return NULL;
}

void IpbusTcsController::ReadStatus(int daqNr)
{
  int  burstNr[MAX_DAQS];
  int  eventNr[MAX_DAQS];
  int  trPeriod[MAX_CAL_TRG];
  int  trMode[MAX_CAL_TRG];
  bool trExists[MAX_CAL_TRG];

  lock_mutex();
  int spstat = ((GetSpillStatus() & ONSPILL_MASK)!=0)?1:0;
  for(int i = 0; i < MAX_DAQS; ++i)
    {
      burstNr[i] = BurstNumber(i);
      eventNr[i] = EventNumber(i);
    }
  for(int i = 0; i < MAX_CAL_TRG; i++)
    {
      trExists[i] = FindTrigger(0, i+1, trPeriod[i], trMode[i]);
    }
  unlock_mutex();
  ostringstream out;
  out << "DAQ# |  Run | Spill | Spill # | Evt #" << endl;
  for(int i = 0; i < MAX_DAQS; i++)
    {
      char str[128];
      sprintf(str," %1.1d   | %5x|   %1.1d   |  %5d  | %d",
	      i, options->timeSlice[i], spstat, burstNr[i], eventNr[i]);
      out << str << endl;
    }
  int ntr = 0;
  for(int i = 0; i < MAX_CAL_TRG; i++)
    {
      if(trExists[i])
	{
	  char str[128];
	  sprintf(str, " %d/%d/%d ", i+1, trMode[i], trPeriod[i]);
	  out << str;
	  ntr++;
	}
    }
  if(ntr) out << endl;
  tcs_dim["Status"] = out.str();
}

bool IpbusTcsController::Init(bool wait)
{
  DEBUG("IpbusTcsController::Init(%d)", (int)wait);
  // Zero throttle value
  tcsThrottle=0;

  if(!fpga->UpdateOptions())
    return false;
  options = fpga->GetOptions();

  if(!wait || WaitOffspill())
    {
      Configure();
    }
  else
    return false;
  return true;
}

bool IpbusTcsController::ResetSpillCounter()
{
  ipbus_write("RC.RST_SPILL",1);
  ipbus_write("RC.RST_SPILL",0);
  return true;
}

void IpbusTcsController::IgorStart()
{
  ipbus_write("SLW_CMD",0x18800);
  // Configure receivers
  // REC 0: Any
  ipbus_write("CFG_FIFO",0x80187); // EV_TP=1 ACC=1 PT=1 DM=1
  ipbus_write("CFG_FIFO",0x8018B); // EV_TP=2 ACC=1 PT=0 DM=0
  ipbus_write("CFG_FIFO",0xD0001); // Main DAQ
  // REC 1: BMS
  ipbus_write("CFG_FIFO",0x80480);
  ipbus_write("CFG_FIFO",0xD0101); // Main DAQ
  // REC 2: Veto
  ipbus_write("CFG_FIFO",0x80880);
  ipbus_write("CFG_FIFO",0xD0201); // Main DAQ
  // REC 3
  ipbus_write("CFG_FIFO",0x80c80);
  ipbus_write("CFG_FIFO",0xD0301); // Main DAQ
  // REC 4
  ipbus_write("CFG_FIFO",0x81080);
  ipbus_write("CFG_FIFO",0xD0401); // Main DAQ
  // REC 5
  ipbus_write("CFG_FIFO",0x81480);
  ipbus_write("CFG_FIFO",0xD0501); // Main DAQ
  // REC 6
  ipbus_write("CFG_FIFO",0x81880);
  ipbus_write("CFG_FIFO",0xD0601); // Main DAQ
  // REC 7
  ipbus_write("CFG_FIFO",0x81C80);
  ipbus_write("CFG_FIFO",0xD0701); // Main DAQ
  // REC 8
  ipbus_write("CFG_FIFO",0x82080);
  ipbus_write("CFG_FIFO",0xD0801); // Main DAQ
  // REC 9
  ipbus_write("CFG_FIFO",0x82480);
  ipbus_write("CFG_FIFO",0xD0901); // Main DAQ
  // REC 10
  ipbus_write("CFG_FIFO",0x82880);
  ipbus_write("CFG_FIFO",0xD0A01); // Main DAQ
  // REC 11
  ipbus_write("CFG_FIFO",0x82C80);
  ipbus_write("CFG_FIFO",0xD0B01); // Main DAQ
  // REC 12
  ipbus_write("CFG_FIFO",0x83080);
  ipbus_write("CFG_FIFO",0xD0C01); // Main DAQ
  // REC 13
  ipbus_write("CFG_FIFO",0x83480);
  ipbus_write("CFG_FIFO",0xD0D01); // Main DAQ
  // REC 14
  ipbus_write("CFG_FIFO",0x83880);
  ipbus_write("CFG_FIFO",0xD0E01); // Main DAQ
  // REC 15
  ipbus_write("CFG_FIFO",0x83C80);
  ipbus_write("CFG_FIFO",0xD0F01); // Main DAQ
  // REC 16
  ipbus_write("CFG_FIFO",0x84080);
  ipbus_write("CFG_FIFO",0xD1001); // Main DAQ
  // REC 17
  ipbus_write("CFG_FIFO",0x84480);
  ipbus_write("CFG_FIFO",0xD1101); // Main DAQ
  // REC 18
  ipbus_write("CFG_FIFO",0x84880);
  ipbus_write("CFG_FIFO",0xD1201); // Main DAQ
  // REC 19
  ipbus_write("CFG_FIFO",0x84C80);
  ipbus_write("CFG_FIFO",0xD1301); // Main DAQ
  // REC 20
  ipbus_write("CFG_FIFO",0x85080);
  ipbus_write("CFG_FIFO",0xD1401); // Main DAQ
  // REC 21
  ipbus_write("CFG_FIFO",0x85480);
  ipbus_write("CFG_FIFO",0xD1501); // Main DAQ

  // Reset FE
  ipbus_write("SLW_CMD",0x10800);

  // Set PreTrigger 1
  ipbus_write("PERIOD_EVTYPE_1",0x2); // Echo period 1
  //ipbus_write("MODE_EVTYPE_1",0x1); // Echo mode 1
  ipbus_write("DELAY_EVTYPE_1",0x07); // Echo fine delay 1
  // Set PreTrigger 2
  ipbus_write("PERIOD_EVTYPE_2",0x2); // Echo period 2
  //ipbus_write("MODE_EVTYPE_2",0x2); // Echo mode 2
  ipbus_write("DELAY_EVTYPE_2",0x8); // Echo fine delay 2
  // Set PreTrigger 3
  ipbus_write("PERIOD_EVTYPE_3",0x2); // Echo period 1
  //ipbus_write("MODE_EVTYPE_3",0x3); // Echo mode 1
  ipbus_write("DELAY_EVTYPE_3",0x9); // Echo fine delay 1
  // activate PreTrigger for MainDAQ + all SubDAQs
  ipbus_write("DAQ_ACT_MAP",0xFF);

  // DEADTIME MainDAQ: 2.5 us FDT, 10 in 20 us VDT A, 3 in 6 us VDT B
  ipbus_write("FIXED_DT_DAQ0",0x60);
  ipbus_write("NR_TRG_DTA_DAQ0",0xA);
  ipbus_write("WIN_DTA_DAQ0",0x320);
  ipbus_write("FIXED_DT_DAQ0",0x60);
  ipbus_write("NR_TRG_DTB_DAQ0",0x5);
  ipbus_write("WIN_DTB_DAQ0",0xF0);
  // deadtime SubDAQ 1: 400 us FDT, 4 in 40 us VDT
  ipbus_write("FIXED_DT_DAQ1",0x3E80);
  ipbus_write("NR_TRG_DTA_DAQ1",0xA);
  ipbus_write("WIN_DTA_DAQ1",0x640);
  // deadtime SubDAQ 2: 400 us FDT, 4 in 40 us VDT
  ipbus_write("FIXED_DT_DAQ2",0x3E80);
  ipbus_write("NR_TRG_DTA_DAQ2",0x4);
  ipbus_write("WIN_DTA_DAQ2",0x640);

  // Start TCS controller
  ipbus_write("RC.BC2",0x1);
  ipbus_write("RC.RUN",0x1);
}

bool IpbusTcsController::Start(int daqNr, bool wait)
{
  DEBUG("IpbusTcsController::Start(%d,%d)", daqNr, wait);
  // Reset internal spill counter
  options->spillCounter[daqNr] = -1;
  if(!wait || WaitOffspill())
    {
      lock_mutex();
#if 0
      IgorStart();
#else
      ResetFrontends(daqNr);
      SetDeadtime(daqNr);
      ActivatePretriggers(daqNr);
      StartController();
#endif
      StartDaq(daqNr);
      unlock_mutex();
      options->writeStatusFile(fpga->get_tcs_name());
    }
  else
    return false;
  return true;
}

bool IpbusTcsController::Stop(int daqNr, bool wait)
{
  DEBUG("Time slice %d = %u", daqNr, options->timeSlice[daqNr]);
  if(options->timeSlice[daqNr] == 0)
    {
      return true;
    }
  DEBUG("Try to stop run...");
  if(!wait || WaitOffspill())
    {
      lock_mutex();
      // Zero the sub timeslice corresponding to the selected DAQ
      options->timeSlice[daqNr] = 0;
      char reg_name[32];
      sprintf(reg_name,"TIME_SLICE%d",daqNr);
      ipbus_write(reg_name,0);
      // unsigned int sd_offs=daqNr*4;  
      // VMEwrite(TCS_TS_CONFIG+sd_offs,timeSlice[daqNr]);
      unlock_mutex();
      options->writeStatusFile(fpga->get_tcs_name());
      DEBUG("... done!");
    }
  else
    {
      DEBUG("... failed!");
      return false;
    }
  return true;
}

static bool IsArtSpillStructure()
{
  return false;
}

bool IpbusTcsController::SuperStart(int daqNr)
{
  if(IsArtSpillStructure())
    return Start(daqNr);
  cout << "Start of super-start procedure." << endl;

  // zero step: wait for offspill
  if(!WaitOffspill())
    {
      cerr << "offspill waiting error during super-start procedure." << endl;
      return false;
    }
  // First, stop the run if it is still running!
  if(options->timeSlice[daqNr] != 0)
    {
      options->timeSlice[daqNr] = 0;
      char reg_name[32];
      sprintf(reg_name,"TIME_SLICE%d", daqNr);
      lock_mutex();
      ipbus_write(reg_name,options->timeSlice[daqNr]);
      unlock_mutex();
      options->writeStatusFile(fpga->get_tcs_name());
      
      // unsigned int sd_offs = daqNr*4;  
      // VMEwrite(TCS_TS_CONFIG+sd_offs,timeSlice[daqNr]);
      // WriteStatusFile();
    }
  // Second, init TCS w/o waiting offspill:
  Init(false);

  // Last, start DAQ w/o waiting offspill:
  if(!Start(daqNr, false))
    return false;
  
  cout << "End of Super-start procedure." << endl;
  return true;
}

bool IpbusTcsController::SuperStop(int daqNr)
{
  if(options->timeSlice[daqNr] == 0)
    return true;

  if(IsArtSpillStructure())
    return Stop(daqNr);

  return Stop(daqNr, false);
}

// Start run by using artificial spill structure
bool IpbusTcsController::RapidStart(int daqNr)
{ // currently does not work, because SSS module is not using anymore
  return Start(daqNr);
#if 0
  // 1. save current spill structure mode:
  int sssMode = 0; // getDimInt("SSS.status"); <-- does not work anymore
  
  if(sssMode == 0) // currend spill structure mode is SPS
    {
      // 2. wait for off-spill status if it is not:
      int ss = GetSpillStatus();
      if((ss & ONSPILL_MASK) == ONSPILL_MASK)
	{
	  WaitOffspill();
	}

      // 3. go to artificial spill structure:
      if(!sendDimCmd("SSS.command",1)) // switch to 
	{
	  cerr << "Cannot chand spill structure mode!" << endl;
	  //Doesn't matter. Continue to start in current mode....
	}
    }

  // 4. start the run:
  if(!Start(daqNr))
    {
      if(sssMode != 1) // original spill structure mode was ART
	{
	  return sendDimCmd("SSS.command",sssMode);
	}
      return false;
    }

  if(sssMode == 0) // original spill structure mode is SPS
    {
      // 5. wait for off-spill status if it is not:
      int ss = GetSpillStatus();
      if((ss & ONSPILL_MASK) == ONSPILL_MASK)
	{
	  WaitOffspill();
	}

      // 6. restore original spill structure mode:
      return sendDimCmd("SSS.command",sssMode);
    }
  return true;
#endif  
}

// Stop run by using artificial spill structure
bool IpbusTcsController::RapidStop(int daqNr)
{
  return Stop(daqNr);
#if 0
  // 1. save current spill structure mode:
  int sssMode = getDimInt("SSS.status");
  
  // 3. go to artificial spill structure:
  if(sssMode == 0) // currend spill structure mode is SPS
    {
      // 2. wait for off-spill status if it is not:
      int ss = GetSpillStatus();
      if((ss & ONSPILL_MASK) == ONSPILL_MASK)
	{
	  WaitOffspill();
	}

      if(!sendDimCmd("SSS.command",1)) // switch to 
	{
	  cerr << "Cannot change spill structure mode!" << endl;
	  //Doesn't matter. Continue to start in current mode....
	}
    }

  // 4. stop the run:
  int ss = GetSpillStatus();
  if((ss & ONSPILL_MASK) == ONSPILL_MASK)
    {
      WaitOffspill();
    }
  timeSlice[daqNr] = 0;
  unsigned int sd_offs=daqNr*4;  
  VMEwrite(TCS_TS_CONFIG+sd_offs,timeSlice[daqNr]);
  WriteStatusFile();
 
  if(sssMode == 0) // original spill structure mode was SPS
    {
      // 5. wait for off-spill status if it is not:
      ss = GetSpillStatus();
      if((ss & ONSPILL_MASK) == ONSPILL_MASK)
	{
	  WaitOffspill();
	}

      // 6. restore original spill structure mode:
      return sendDimCmd("SSS.command",sssMode);
    }
  return true;
#endif
}

// Start run in DAQ without waiting
bool IpbusTcsController::StartFast(int daqNr)
{
  // Reset internal spill counter
  options->spillCounter[daqNr]=-1;

  // only do this during offspill
  //GetSpillStatus();
  if(!Burst())
    {
      // Set the sub timeslice corresponding to the selected DAQ
      options->timeSlice[daqNr] = options->subTimeSlice[daqNr];
      char reg_name[32];
      sprintf(reg_name,"TIME_SLICE%d",daqNr);
      ipbus_write(reg_name,options->timeSlice[daqNr]);
      options->writeStatusFile(fpga->get_tcs_name());
      // unsigned int sd_offs=daqNr*4;  
      // VMEwrite(TCS_TS_CONFIG+sd_offs,timeSlice[daqNr]);
      // WriteStatusFile();
      //GetSpillStatus();
      return !Burst();
    }
  return false;
}

bool IpbusTcsController::Pause(int)
{
  // unsigned int pauseWord;
  // Set pause bit in config register
  // uint32_t pauseWord = (1<<TCS_PAUSE_BIT) | options->configWord;
  // VMEwrite(TCS_TCS_CONFIG,pauseWord);
  ipbus_write("RC.PAUSE",1);
  return true;
}

bool IpbusTcsController::Continue(int)
{
  ipbus_write("RC.PAUSE",0);
  return true;
}

// Reset TCS receivers and CATCHes
bool IpbusTcsController::Reset(bool wait)
{
  DEBUG("IpbusTcsController::Reset(%d)", wait);
  bool slices_zero = (0==0);

  if(!wait || WaitOffspill())
    {
      for(int i=0; i<MAX_DAQS; i++)
	{
	  slices_zero = ((slices_zero) && (options->timeSlice[i] == 0));
	}
      if(slices_zero)
	{
	  lock_mutex();
	  ipbus_write("SLW_CMD", options->resetWord);
	  // VMEwrite(TCS_SLOW_FIFO,resetWord);
	  // After 0.1 sec reinitialize TCS receivers. RK: This is totally bogus
	  // since the TCS_CMD_FIFO is _only_ sent out at the beginning of spill,
	  // so this sleep does not insert a pause between the receiver reset and
	  // the receiver programming.
#if 0
	  usleep(100000);
#endif
	  unlock_mutex();
	  return Init(false);
	}
    }
  return false;
}
  
// Request for DAQ # daqNr an artificial trigger of type trigger Nr
// with a given period between triggers
bool IpbusTcsController::StoreTrigger(int daqNr, int triggerNr, int period, int mode)
{
  list<ArtTriggers>::iterator it;
  for(it = art_triggers.begin(); it != art_triggers.end(); it++)
    {
      if(it->daqNr == daqNr && it->trigNr == triggerNr)
	{
	  it->period = period;
	  it->mode   = mode;
	  break;
	}
    }
  if(it == art_triggers.end())
    {
      ArtTriggers newTrigger;
      newTrigger.daqNr  = daqNr;
      newTrigger.trigNr = triggerNr;
      newTrigger.period = period;
      newTrigger.mode   = mode;
      art_triggers.push_back(newTrigger);
    }

  return SendTrigger(daqNr, triggerNr, period, mode);
}

bool IpbusTcsController::SendTrigger(int daqNr, int triggerNr, int period, int mode)
{
  // Check bounds
  if(period > MAX_PERIOD)
    period = MAX_PERIOD;

  mode=(mode&0x3);

  // Get map of DAQs for which artificial's are sent
  // uint32_t daqmap = VMEread(TRG_DAQ_MAP);
  
  uint32_t daqmap = 0;
  ipbus_read("DAQ_ACT_MAP", daqmap);

  // Set some registers for this trigger
  // modreg=TRG_MODE_REG+(triggerNr-1)*4;
  // periodreg=TRG_PERIOD_REG+(triggerNr-1)*4;  
  char modreg[32];
  char periodreg[32];
  sprintf(modreg,"MODE_EVTYPE_%d", triggerNr);
  sprintf(periodreg,"PERIOD_EVTYPE_%d",triggerNr);

  // Set the times for this trigger
  if(mode != 0)
    {
      daqmap |= 1<<daqNr;
      ipbus_write("DAQ_ACT_MAP",daqmap);
      ipbus_write(modreg,mode);
      ipbus_write(periodreg,period);
    }
  else
    {
      ipbus_write(modreg,0);
      ipbus_write(periodreg,0);
    }

  return true;
}

void IpbusTcsController::ResetFrontends(int daqNr)
{
  // Reset FE
  cout << "Reset ALL FEs:" << endl;
  ipbus_write("SLW_CMD", RESET_FE_START);
  // Reset all CATCHes belonging to this DAQ
  for(int i=0; i < MAX_REC; i++)
    {
      if((options->recMap[i] & (1<<daqNr)) != 0)
	{
	  cout << "Start of DAQ " << daqNr << ": Resetting receiver " << i << endl;
	  ipbus_write("CFG_FIFO", options->recRes[i]);
	  for (unsigned j = 0; j < options->nrRecCfg[i]; j++)
	    {
	      ipbus_write("CFG_FIFO", options->recCfg[i][j]);
	    }
	  if(options->recDel[i]!=0)
	    {
	      ipbus_write("CFG_FIFO", options->recDel[i]);
	    }
	  ipbus_write("CFG_FIFO", (REC_MAP_CODE+(i << REC_RES_POWER)+options->recMap[i]));
	}
    }
  // Reset FE
  cout << "Reset ALL FEs:" << endl;
  ipbus_write("SLW_CMD", RESET_FE_END);
}

void IpbusTcsController::ActivatePretriggers(int daqNr)
{
  list<ArtTriggers>::iterator it;
  for(it = art_triggers.begin(); it != art_triggers.end(); it++)
    {
      if(it->daqNr == daqNr)
	{
	  SendTrigger(daqNr,it->trigNr,it->period,it->mode);
	}
    }

  // activate PreTrigger for MainDAQ + all SubDAQs
  ipbus_write("DAQ_ACT_MAP",0xFF);
}

void IpbusTcsController::SetDeadtime(int)
{
  // Configure deadtimes
  for(int i = 0; i < MAX_DAQS; i++)
    {
      char reg_name[32];
      sprintf(reg_name,"FIXED_DT_DAQ%d",   i);
      ipbus_write(reg_name, options->deadTimeF[i]);
      sprintf(reg_name,"NR_TRG_DTA_DAQ%d", i);
      ipbus_write(reg_name,options->deadTimeN[i]);
      sprintf(reg_name,"WIN_DTA_DAQ%d",    i);
      ipbus_write(reg_name,options->deadTimeW[i]);
      // VMEwrite(TCS_DTF_CONFIG+sd_offs,options->deadTimeF[i]);
      // VMEwrite(TCS_DTN_CONFIG+sd_offs,options->deadTimeN[i]);
      // VMEwrite(TCS_DTW_CONFIG+sd_offs,options->deadTimeW[i]);
    }
  if(options->deadTimeSN)
    {
      ipbus_write("NR_TRG_DTB_DAQ0",options->deadTimeSN);
      ipbus_write("WIN_DTB_DAQ0",options->deadTimeSW);
      // VMEwrite(TCS_SDTN_CONFIG,options->deadTimeSN);
      // VMEwrite(TCS_SDTW_CONFIG,options->deadTimeSW);
    }
}

void IpbusTcsController::StartController()
{
  // # Start TCS controller
  ipbus_write("RC.BC2",0x1);
  ipbus_write("RC.RUN",0x1);
}

void IpbusTcsController::StartDaq(int daqNr)
{
  char reg_name[32];
  sprintf(reg_name,"TIME_SLICE%d", daqNr);

  // Set the sub timeslice corresponding to the selected DAQ
  options->timeSlice[daqNr] = options->subTimeSlice[daqNr];
  ipbus_write(reg_name,options->timeSlice[daqNr]);
}
