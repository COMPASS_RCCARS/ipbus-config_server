#include <stdio.h>
#include <stdint.h>

#include <uhal/uhal.hpp>
#include <iostream>
#include <sstream>
#include <fstream>

#include "main.h"
#include "Module.h"
#include "wait.h"
#include "IpbusModule.h"
#include "int2string.h"
#include "IpbusFeMux.h"
#include "IpbusCedarTdcCard.h"

using namespace std;
using namespace uhal;

IpbusCedarTdcCard::IpbusCedarTdcCard(IpbusFECard::FECardType cardType, const char* name, IpbusFeMux* parent, int _port) :
  IpbusFECard(cardType,name,parent,_port)
{
  DEBUG("Create CEDAR ifTDC Card with IPBUS name %s, in port %d.", name, _port);
  fw_id = 0;
  ipbus_read("FIRMWARE_ID", fw_id);
  DEBUG("ifTDC card firmware ID: %u", fw_id);
  uint32_t test = GetFwIdFromDB();
  DEBUG("ifTDC card firmware ID from DB: %u", test);
  if(fw_id != test)
    {
      throw Module::DBError("FW ID is not match for SrcID " + mydec(parent->getSourceID()) + " port " + mydec(GetPort()));
    }
}

IpbusCedarTdcCard::~IpbusCedarTdcCard()
{
  DEBUG("Destroy CEDAR ifTDC Card, port %d.", GetPort());
}

void IpbusCedarTdcCard::ReadStatus(std::string& status_str)
{
  char msg[1024];
  uint32_t status_reg   = 0;
  uint32_t tr_window    = 0;
  uint32_t tr_latency   = 0;
  uint32_t signal_delay = 0;
  ipbus_read("STATUS_REG",      status_reg);
  ipbus_read("TRIGGER_WINDOW",  tr_window);
  ipbus_read("TRIGGER_LATENCY", tr_latency);
  ipbus_read("SIGNAL_DELAY",    signal_delay);
  snprintf(msg,1023,"port = %2d Status = 0x%08x, Window = %u, Latency = %u, Delay = %u; ",
	   GetPort(), status_reg, tr_window, tr_latency, signal_delay);
  status_str += msg;
  
  for(int i = 0; i < 65; i++)
    {
      char scaler_name[64];
      uint32_t scaler = 0;
      sprintf(scaler_name,"SCALER_%02d",i);
      ipbus_read(scaler_name,scaler);
      snprintf(msg,1023,"SC%02d = %u ", i, scaler);
      status_str += msg;
    }
  status_str += "\n";
}

void IpbusCedarTdcCard::reset_module()
{
  DEBUG("IpbusCedarTdcCard::reset_module");
}

void IpbusCedarTdcCard::enable_channels()
{
  DEBUG("IpbusCedarTdcCard::enable_channels");
  // active channels on CEDAR_CH16 card: { 3, 5, 9, 14, 16, 24, 29, 34, 37, 41, 45, 49, 54, 56, 60, 63 };
  const uint64_t mask_CEDAR16 = 0x9142222421014228;
  // active channels on CEDAR_CH32 card:
  // { 0, 1, 2, 3, 4, 5, 6, 7, 14, 16, 18, 22, 25, 27, 29, 31,
  // 41, 43, 45, 47, 49, 52, 53, 54, 56, 57, 58, 59, 60, 61, 62, 63 };
  const uint64_t mask_CEDAR32 = 0xFF72AA00AA4540FF;
  // active channels on T0 card: { 64 };
  list<DatabaseEntryFrontend>& module_db = GetModule()->GetFrontendDB();
  list<DatabaseEntryFrontend>::iterator it;
  uint64_t ch_disabled = 0;
  //uint32_t card_format = 0;
  uint32_t cntvaluein_1 = 5;
  for(it=module_db.begin(); it!=module_db.end(); ++it)
    {
      if(it->port == GetPort() && it->state == 1)
	{
	  ch_disabled = it->fe_disabled;
	  //card_format = it->format_id;
	  map<string,string> config = parseSettings(it->config, string());
	  if(config.find("CNTVALUEIN_1") != config.end())
	    cntvaluein_1 = toUnsigned(config["CNTVALUEIN_1"]);
	}
    }
  if(GetCardType() == CEDAR_16CH)
    ch_disabled |= ~mask_CEDAR16;
  else if(GetCardType() == CEDAR_32CH)
    ch_disabled |= ~mask_CEDAR32;
  for(int i = 0; i < 65; i++)
    {
      char node_name[64];
      snprintf(node_name,63,"TDC_%02d.WRITE.CNTVALUEIN_0",i);
      ipbus_write(node_name, 0);
      snprintf(node_name,63,"TDC_%02d.WRITE.CNTVALUEIN_1",i);
      ipbus_write(node_name, cntvaluein_1);
      snprintf(node_name,63,"TDC_%02d.WRITE.LD_0",i);
      ipbus_write(node_name, 0);
      snprintf(node_name,63,"TDC_%02d.WRITE.LD_1",i);
      ipbus_write(node_name, 0);
      snprintf(node_name,63,"TDC_%02d.WRITE.MODE",i);
      if(GetCardType() == CEDAR_32CH)
	ipbus_write(node_name, 2);
      else
	ipbus_write(node_name, 0);
      snprintf(node_name,63,"TDC_%02d.WRITE.LD_0",i);
      ipbus_write(node_name, 1);
      snprintf(node_name,63,"TDC_%02d.WRITE.LD_1",i);
      ipbus_write(node_name, 1);
      snprintf(node_name,63,"TDC_%02d.WRITE.LD_0",i);
      ipbus_write(node_name, 0);
      snprintf(node_name,63,"TDC_%02d.WRITE.LD_1",i);
      ipbus_write(node_name, 0);
      snprintf(node_name,63,"TDC_%02d.WRITE.EN",i);
      if(i < 64)
	{
	  uint64_t mask = 1;
	  mask <<= i;
	  if((ch_disabled & mask) != 0 || GetCardType() == CEDAR_T0) // the channel is disabled in FEDB
	    {
	      ipbus_write(node_name, 0);	  
	    }
	  else
	    {
	      ipbus_write(node_name, 1);	  
	    }
	}
      else // enable or disable 65-th channel ("LEMO" input)
	{
	  if(GetCardType() == CEDAR_T0) // the card for T0 measurement
	    ipbus_write(node_name, ch_disabled?0:1);
	  else
	    ipbus_write(node_name, 0);
	}
    }
}

void IpbusCedarTdcCard::set_window()
{
  DEBUG("IpbusCedarTdcCard::set_window");
  list<DatabaseEntryFrontend>& module_db = GetModule()->GetFrontendDB();
  list<DatabaseEntryFrontend>::iterator it;
  for(it=module_db.begin(); it!=module_db.end(); ++it)
    {
      if(it->port == GetPort())
	{
	  map<string, mynumber<int> > params = parseSettings(it->gate_window, mynumber<int>("0"));
	  map<string, mynumber<int> >::iterator itt;
	  for(itt = params.begin(); itt != params.end(); itt++)
	    {
	      ipbus_write(itt->first.c_str(),(uint32_t)itt->second);
	    }
	}
	// {
	//   uint32_t tr_window = 0;
	//   int ret = sscanf(it->gate_window.c_str(),"TRIGGER_WINDOW=%u",&tr_window);
	//   if(ret != 1 || tr_window == 0 || tr_window > 0xFFFF)
	//     {
	//       throw Module::DBError(GetModule(),"Bad trigger window in FEDB '" + it->gate_window + "'");
	//     }
	//   ipbus_write("TRIGGER_WINDOW",tr_window);
	// }
    }
}

void IpbusCedarTdcCard::set_delay()
{
  DEBUG("IpbusCedarTdcCard::set_delay");
  list<DatabaseEntryFrontend>& module_db = GetModule()->GetFrontendDB();
  list<DatabaseEntryFrontend>::iterator it;
  for(it=module_db.begin(); it!=module_db.end(); ++it)
    {
      if(it->port == GetPort())
	{
	  map<string, mynumber<int> > params = parseSettings(it->gate_latency, mynumber<int>("0"));
	  map<string, mynumber<int> >::iterator itt;
	  for(itt = params.begin(); itt != params.end(); itt++)
	    {
	      ipbus_write(itt->first.c_str(),(uint32_t)itt->second);
	    }
	}
	// {
	//   uint32_t tr_delay = 0;
	//   int ret = sscanf(it->gate_latency.c_str(),"SIGNAL_DELAY=%u",&tr_delay);
	//   if(ret != 1 || tr_delay == 0 || tr_delay > 0xFFFF)
	//     {
	//       throw Module::DBError(GetModule(),"Bad trigger delay in FEDB '" + it->gate_latency + "'");
	//     }
	//   ipbus_write("SIGNAL_DELAY",tr_delay);
	// }
    }
}

void IpbusCedarTdcCard::set_thresholds()
{
  DEBUG("IpbusCedarTdcCard::set_thresholds");
  list<DatabaseEntryFrontend>& module_db = GetModule()->GetFrontendDB();
  list<DatabaseEntryFrontend>::iterator it;
  for(it=module_db.begin(); it!=module_db.end(); ++it)
    {
      if(it->port == GetPort())
	{
	  DEBUG("TODO: Load thresholds");
	  // todo...
	}
    }
}

uint32_t IpbusCedarTdcCard::GetFwIdFromDB()
{
  int id = -1;
  list<DatabaseEntryFrontend>& module_db = GetModule()->GetFrontendDB();
  list<DatabaseEntryFrontend>::iterator it;
  for(it=module_db.begin(); it!=module_db.end(); ++it)
    {
      if(it->port == GetPort())
	{
	  map<string, string > m = parseSettings(it->config, string());
	  sscanf(m["FIRMWARE_ID"].c_str(),"%d",&id);
	  break;
	}
    }
  if(id == -1)
    return 0;
  return (uint32_t)id;
}

