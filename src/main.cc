#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>
#include <mysql++.h>

#include <string>
#include <iostream>

#include <dis.hxx>

#include "Module.h"
#include "IpbusFeMux.h"
#include "IpbusTcsFpga.h"
#include "IpbusPrescalerA7.h"
#include "int2string.h"
#include "mysql++_defs.h"

FILE* _debug = NULL;
void DEBUG(const char* format, ...)
{
  char msg[1024];
  va_list argptr;
  va_start(argptr,format);
  vsprintf(msg,format,argptr);
  va_end(argptr);
  if(_debug)
    {
      fprintf(_debug,"DEBUG: %s\n", msg);
      fflush(_debug);
    }
}

using namespace std;

const char*  db_server;
const char*  host;
const char*  db_user;
const char*  db_db;
const char*  db_passwd;
unsigned int db_port = 0;
const char*  et_db_db;
const char*  et_db_server;
const char*  et_db_user;
const char*  et_db_passwd;

static list<Module*> modules;
static string version_tag;
static bool reexec;

static int getLocationInfo();
static void createModules();
static void clearModules();

// the DimCommand for recreating the modules
class DCRestart : public DimCommand
{
  void commandHandler()
  {
    cout<<"**** Restarting ****"<<endl;
    clearModules();
    string newversion=getString();
    if(newversion.size())
      {
	version_tag=newversion;
	cout<<"** new Version_tag "<<version_tag<<" **"<<endl;
      }
    createModules();
  }
public:
  DCRestart() : DimCommand((char*)(string(host)+"/Restart").c_str(),(char*)"C")
  {
    cout<<"Restart command ready"<<endl;
  }
};

// DimCommand for restarting the executable
class DCReexec : public DimCommand
{
  void commandHandler() { reexec=true; }
public:
  DCReexec() : DimCommand((char*)(string(host)+"/Reexec").c_str(),(char*)"I")
  {
    cout<<"Reexec command ready"<<endl;
  }
};


int main(int argc,char**argv)
{
  cout<<"*****************************************************************************"<<endl;
  cout<<"*** config_server v2.0, written by Roland Kuhn and Fritz-Herbert Heinsius ***"<<endl;
  cout<<"*** Christian Schill (CATCH & F1 part) and Roland Kuhn                    ***"<<endl;
  cout<<"*** IPBUS support by Vladimir FROLOV                                      ***"<<endl;
  cout<<"*****************************************************************************"<<endl;
  cout<<"  current version built on "<< __DATE__ << " " << __TIME__ << endl;
  cout<<"  started at "<<mytime()<<endl;

  char* debug_env = getenv("DEBUG_CONFIG_SERVER");
  if(debug_env)
    {
      if(strcmp(debug_env,"yes") == 0 || strcmp(debug_env,"1") == 0)
	{
	  _debug = stdout;
	  DEBUG("$DEBUG_CONFIG_SERVER=\"%s\"",debug_env);
	}
      else
	{
	  printf("$DEBUG_CONFIG_SERVER=\"%s\" debug output is disabled.\n",debug_env);
	}
    }
  else
    {
      printf("$DEBUG_CONFIG_SERVER is not defined\n");
    }

  // set default version tag
  version_tag="latest";

  // find out the names of the servers
  if(!getLocationInfo())
    {
      cout<<"missing location information"<<endl;
      exit(1);
    }

  cout << "DB_SERVER=" << db_server << ", DB_CLIENT=" << host << ", DB_PORT=" << db_port << endl;
  cout << "DB_USER=" << db_user << ", DB_PASSWD=" << db_passwd << "\n" << endl;

  try
    {
      // ask database about the modules in this crate
      createModules();

      // create restart command
      DCRestart restart_command;
      DCReexec reexec_command;
      reexec=false;

      // rock'n'roll
      DimServer::start(host);
      cout<<"DimServer started"<<endl;
      // only an error/signal can stop us now
      while(!reexec)
	dim_wait();

      clearModules();
    }
  catch(exception&e)
    {
      cerr<<e.what()<<endl;
      return 1;
    }
  return 0;
}

// get info about the current location and return the names of
// this host (for DimServer::start()), the dim dns node and the
// database server
static int getLocationInfo()
{
  // check for DEBUG
  if(getenv("CS_DEBUG_FILE"))
    {
      _debug = fopen(getenv("CS_DEBUG_FILE"),"w");
      if(!_debug)
	{
	  cerr << "Cannot open debug file $CS_DEBUG_FILE='" << getenv("CS_DEBUG_FILE") << 
	    "'" << endl;
	}
    }
  // check for DIM DNS
  if(!getenv("DIM_DNS_NODE"))
    {
      cerr<<"the DIM_DNS_NODE variable is not set"<<endl;
      return 0;
    }

  // check for database server
  if(!(db_server=getenv("DB_SERVER")))
    {
      cerr<<"the DB_SERVER variable is not set"<<endl;
      return 0;
    }

  // check for database server for ecal trigger
  if(!(et_db_server=getenv("ET_DB_SERVER")))
    {
      //cerr<<"the ET_DB_SERVER variable is not set => using DB_SERVER"<<endl;
      et_db_server = db_server;
    }


  // check for hostname (must coincide with db entry)
  if(!(host=getenv("DB_CLIENT")))
    {
      cerr<<"the DB_CLIENT variable is not set"<<endl;
      return 0;
    }

  // check for hostname (must coincide with db entry)
  if(!(db_user=getenv("DB_USER")))
    {
      cerr<<"the DB_USER variable is not set"<<endl;
      return 0;
    }

  // check for username to use for ecal trigger db
  if(!(et_db_user=getenv("ET_DB_USER")))
    {
      //cerr<<"the ET_DB_USER variable is not set => using DB_USER"<<endl;
      et_db_user = db_user;
    }

  // check for hostname (must coincide with db entry)
  if(!(db_db=getenv("DB_DB")))
    {
      cerr<<"the DB_DB variable is not set"<<endl;
      return 0;
    }

  // check for hostname (must coincide with db entry)
  if(!(et_db_db=getenv("ET_DB_DB")))
    {
      //cerr<<"the ET_DB_DB variable is not set => using DB_DB"<<endl;
      et_db_db = db_db;
    }
  
  // check for hostname (must coincide with db entry)
  if(!(db_passwd=getenv("DB_PASSWD")))
    {
      cerr<<"the DB_PASSWD variable is not set"<<endl;
      return 0;
    }
  const char* db_port_str = NULL;
  // check DB port number (must coincide with db entry)
  if(!(db_port_str = getenv("DB_PORT")))
    {
      //cerr << "the DB port number is not set. Will use 0" << endl;
      db_port = 0;
    }
  else
    {
      db_port = atoi(db_port_str);
    }
  
  // check for hostname (must coincide with db entry)
  if(!(et_db_passwd=getenv("ET_DB_PASSWD")))
    {
      //cerr<<"the ET_DB_PASSWD variable is not set => using DB_PASSWD"<<endl;
      et_db_passwd = db_passwd;
    }

  // return success
  return 1;
}

static void createModules()
{
  cout<<"creating Modules:"<<endl;

  // open DB connection to find modules plugged into this crate
  mysqlpp::Connection con(true);
  con.connect(db_db,db_server,db_user,db_passwd,db_port);
  mysqlpp::Query query=con.query();
  //mysqlpp::Result result;
  MYSQLPP_RESULT result;
  mysqlpp::Row row;
  query<<"select MODULE_source_ID,Type from MODULE where VME_host_name='"
       <<host<<"' and Version_tag='"<<version_tag<<"'";
  result=query.store();
  for(size_t i=0;i<result.size();++i)
    {
      row=result.at(i);
      Module* mod;
      try
	{
	  // first try the explicit type information
	  if(!row.at(1).is_null())
	    {
	      module_type_t type(module_type_t((int)row.at(1)));
	      string module_type;
	      bool isIpbusModule = false;
	      switch(type)
		{
		case _catch:
		  module_type = "CATCH";
		  break;
		case _gandalf:
		  module_type = "Gandalf";
		  break;
		case _gesica:
		  module_type = "GeSiCa";
		  break;
		case _hot_gesica:
		  module_type = "HotGeSiCA";
		  break;
		case _hot_gesica_sadc:
		  module_type = "HGeSiCA(SADC)";
		  break;
		case _hot_gesica_rich:
		  module_type = "HGeSiCA(RICH)";
		  break;
		case _hot_gesica_msadc:
		  module_type = "HGeSiCA(MSADC)";
		  break;
		case _hot_gesica_rw:
		  module_type = "HGeSiCA(RW)";
		  break;
		case _hot_gesica_sgadc:
		  module_type = "HGeSiCA(SGADC)";
		  break;
		case _tcs:
		  module_type = "A7_TCS";
		  type = _a7_tcs;
		  isIpbusModule = true;
		  break;
		case _tcs_prescaler:
		  module_type = "TCSprescaler";
		  isIpbusModule = true;
		  break;
		case _tcs_prescaler2:
		  module_type = "TCSprescaler2";
		  break;
		case _ipbus_mux:
		  module_type = "IpbusMux";
		  isIpbusModule = true;
		  break;
		case _a7_tcs:
		  module_type = "A7_TCS";
		  isIpbusModule = true;
		  break;
		default:
		  module_type = "Unknown";
		  break;
		}
	      int _srcid = row.at(0); 
	      if(!isIpbusModule)
		{
		  cerr << "Module '" << module_type << "' (SrcID: " << _srcid << ") "
		       << "is not supporting in this version of config server." << endl;
		  continue;
		}
	      cout << "Source ID: " << dec << _srcid << endl;
	      if(type == _a7_tcs)
		{
		  cout << "A7 TCS Module '" << module_type << "' (SrcID: " << _srcid << ") " << endl;
		  mod = new IpbusTcsFpga(_srcid, version_tag);
		}
	      else if(type == _ipbus_mux)
		{
		  mod = new IpbusFeMux(_srcid, version_tag);
		}
	      else if(type == _tcs_prescaler)
		{
		  mod = new IpbusPrescalerA7(_srcid, version_tag);
		}
	      if(mod)
		{
		  cout << "Module '" << dec << module_type << "' (SrcID: " << _srcid << ") is created." << endl;
		}
	    }
	}
      catch(DatabaseEntry::Error& e)
	{
	  cerr<<"Module "<<row.at(0)<<" database error: "<<e.what()<<endl;
	  continue;
	}
      catch(Module::Error& e)
	{
	  cerr << "Module "<<row.at(0)<<" exception: "<<e.what()<<endl;
	  continue;
	}
      catch(...)
	{
	  cerr << "Module "<<row.at(0)<<" unknown exception: "<<endl;
	  continue;
	}
      modules.push_back(mod);
    }
  cout<<"done"<<endl;
}

// clear the list of modules (these have to be deleted to stop
// their published DIM service and free the other resources)
static void clearModules()
{
  list<Module*>::iterator it;
  for(it=modules.begin();it!=modules.end();++it)
    {
      delete *it;
    }
  modules.clear();
}

