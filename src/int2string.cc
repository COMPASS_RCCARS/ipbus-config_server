#include "int2string.h"
#include <stdio.h>
#include <stdint.h>
#include <string>
#include <map>
#include <vector>

using namespace std;

string mydec(int i)
{
  char b[20];
  snprintf(b,20,"%d",i);
  return string(b);
}

string myhex(int i)
{
  char b[20];
  snprintf(b,20,"%X",i);
  return string(b);
}

string myoct(int i)
{
  char b[20];
  snprintf(b,20,"%o",i);
  return string(b);
}

string myfloat(double d)
{
  char b[30];
  snprintf(b,20,"%g",d);
  return string(b);
}

/* this function is not declared in the header, so it is safe to use '32' here
   :-) */
string mybitmask(unsigned int i, int len)
{
  unsigned int mask = 1 << len;
  string result;
  while(mask >>= 1)
    {
      result += '0' + !!(i & mask);
    }
  return result;
}

string mybitmask(unsigned int i)
{
  return mybitmask(i, 32);
}

string mybitmask(unsigned short i)
{
  return mybitmask(i, 16);
}

string mybitmask(unsigned char i)
{
  return mybitmask(i, 8);
}

vector<string> split(string s,char delim)
{
  vector<string> v;
  size_t pos,newpos;
  for(pos=0,newpos=0;newpos!=string::npos;pos=newpos+1)
    {
      newpos=s.find(delim,pos);
      if ( s[pos] == delim ) continue;
      v.push_back(s.substr(pos,newpos-pos));
    }
  return v;
}

template <class T> map<string,T> parseSettings(string s,T def,char ifs,char eq)
{
  map<string,T> m;
  vector<string> tokens(split(s,ifs));
  vector<string>::iterator token;
  for(token=tokens.begin();token!=tokens.end();++token)
    {
      size_t equal=token->find(eq);
      if(equal==string::npos)
	{
	  // create entry with default T
	  m[*token]=def;
	}
      else
	{
	  m[token->substr(0,equal)]=T(token->substr(equal+1));
	}
    }
  return m;
}

template map<string,mynumber<int> > parseSettings(string,mynumber<int>,char,char);
template map<string,mynumber<float> > parseSettings(string,mynumber<float>,char,char);
template map<string,mynumber<double> > parseSettings(string,mynumber<double>,char,char);
template map<string,string> parseSettings(string,string,char,char);

void replace_in_string(string&s,string find,string replace)
{
  size_t pos;
  while((pos=s.find(find))!=string::npos)
    {
      s.replace(pos,find.size(),replace);
    }
}

const char* mytime()
{
  static char buffer[100];
  time_t t;
  time(&t);
  asctime_r(localtime(&t),buffer);
  for(int i=0;buffer[i];buffer[i]=='\n'?buffer[i]=0:++i);
  return buffer;
}

uint32_t toUnsigned(std::string& str)
{
  uint32_t value = 0;
  if(sscanf(str.c_str(),"0x%x", &value) == 1)
    return value;
  if(sscanf(str.c_str(),"%u", &value) == 1)
    return value;
  return 0;
}
