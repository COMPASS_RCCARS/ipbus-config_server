#include <sstream>
#include <fstream>

#include "main.h"
#include "Module.h"
#include "wait.h"
#include "IpbusModule.h"
#include "int2string.h"
#include "IpbusFeMux.h"
#include "IpbusSpTimeTdcCard.h"

using namespace std;
using namespace uhal;

IpbusSpTimeTdcCard::IpbusSpTimeTdcCard(IpbusFECard::FECardType cardType, const char* name, IpbusFeMux* parent, int _port) :
  IpbusTdcCard(cardType,name,parent,_port)
{
  DEBUG("Create Speaker Time ifTDC Card with IPBUS name %s, in port %d.", name, _port);
  fw_id = 0;
  ipbus_read("FIRMWARE_ID", fw_id);
  DEBUG("ifTDC card firmware ID: %u", fw_id);
  uint32_t test = GetFwIdFromDB();
  DEBUG("ifTDC card firmware ID from DB: %u", test);
  if(fw_id != test)
    {
      throw Module::DBError("FW ID is not match for SrcID " + mydec(parent->getSourceID()) + " port " + mydec(GetPort()));
    }
}

IpbusSpTimeTdcCard::~IpbusSpTimeTdcCard()
{
  DEBUG("Destroy Speaker Time ifTDC Card, port %d.", GetPort());
}

void IpbusSpTimeTdcCard::ReadStatus(std::string& status_str)
{
  char msg[1024];
  uint32_t status_reg = 0;
  uint32_t tr_window  = 0;
  uint32_t tr_delay   = 0;
  uint32_t scaler     = 0;
  ipbus_read("STATUS_REG", status_reg);
  ipbus_read("TRIGGER_WINDOW", tr_window);
  ipbus_read("SIGNAL_DELAY", tr_delay);
  ipbus_read("SCALER", scaler);
  snprintf(msg,1023,"port = %2d Status = 0x%08x, Window = %u, Delay = %u, Scaler = %u; ",
	   GetPort(), status_reg, tr_window, tr_delay, scaler);
  status_str += msg;
  status_str += "\n";
}

void IpbusSpTimeTdcCard::enable_channels()
{
  DEBUG("IpbusSpTimeTdcCard::enable_channels");
  list<DatabaseEntryFrontend>& module_db = GetModule()->GetFrontendDB();
  list<DatabaseEntryFrontend>::iterator it;
  uint64_t ch_disabled = 0;
  for(it=module_db.begin(); it!=module_db.end(); ++it)
    {
      if(it->port == GetPort() && it->state == 1)
	{
	  ch_disabled = it->fe_disabled;
	}
    }
  for(int i = 0; i < 2; i++)
    {
      char node_name[64];
      snprintf(node_name,63,"TDC_%02d.WRITE.CNTVALUEIN_0",i);
      ipbus_write(node_name, 0);
      snprintf(node_name,63,"TDC_%02d.WRITE.CNTVALUEIN_1",i);
      ipbus_write(node_name, 13);
      snprintf(node_name,63,"TDC_%02d.WRITE.LD_0",i);
      ipbus_write(node_name, 0);
      snprintf(node_name,63,"TDC_%02d.WRITE.LD_1",i);
      ipbus_write(node_name, 0);
      snprintf(node_name,63,"TDC_%02d.WRITE.MODE",i);
      ipbus_write(node_name, 2);
      snprintf(node_name,63,"TDC_%02d.WRITE.LD_0",i);
      ipbus_write(node_name, 1);
      snprintf(node_name,63,"TDC_%02d.WRITE.LD_1",i);
      ipbus_write(node_name, 1);
      snprintf(node_name,63,"TDC_%02d.WRITE.LD_0",i);
      ipbus_write(node_name, 0);
      snprintf(node_name,63,"TDC_%02d.WRITE.LD_1",i);
      ipbus_write(node_name, 0);
      snprintf(node_name,63,"TDC_%02d.WRITE.EN",i);
      if(i < 64)
	{
	  uint64_t mask = 1;
	  mask <<= i;
	  if((ch_disabled & mask) != 0) // the channel is disabled in FEDB
	    {
	      ipbus_write(node_name, 0);	  
	    }
	  else
	    {
	      ipbus_write(node_name, 1);	  
	    }
	}
    }
}
