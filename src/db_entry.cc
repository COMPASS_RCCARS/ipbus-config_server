#include <stdio.h>
#include "db_entry.h"
#define MYSQL_NO_SHORT_NAMES
#include "int2string.h"
#include "mysql++_defs.h"

using namespace std;

// these macros shall make the code more readable
// (so they contain all the ugliness ;-) )
// MY_SET checks if the ColData d is NULL and calls my_set to
// save the actual value in v.
// MY_SET_S does the same for values grouped in struct s
// PRINT_DATA prints the value v depending on its availability
// (e.g. setup is only printed if has_setup==true)

#define MY_SET(v,d) has_##v=!d.is_null(); my_set(v,d)
#define MY_SET_S(s,v,d) s.has_##v=!d.is_null(); my_set(s.v,d)
#define PRINT_DATA(v) if(has_##v) out<<';'<<#v<<' '<<v
#define PRINT_DATA_S(s,v) if(s.has_##v) out<<';'<<#v<<' '<<s.v

// these three helpers set variables from ColData d
// if d is NULL a default value is used
#if MYSQLPP_HEADER_VERSION < MYSQLPP_VERSION(3, 0, 0)
void my_set(int&t,mysqlpp::ColData d)
{
  if(d.is_null()) {t=0;} else {t=atoi(d);}
}

void my_set(module_type_t&t,mysqlpp::ColData d)
{
  if(d.is_null()) {t=module_type_t(0);} else {t=module_type_t((int)d);}
}

void my_set(string&s,mysqlpp::ColData d)
{
  if(d.is_null()) {s.erase();} else {s=d.get_string();subst_macros(s);}
}

void my_set(bool&b,mysqlpp::ColData d)
{
  if(d.is_null()) {b=false;} else {b=(int)d!=0;}
}

void my_set(unsigned long long&l,mysqlpp::ColData d)
{
  if(d.is_null()) {l=0;} else {l=d;}
}

#else
void my_set(int&t,mysqlpp::String d)
{
  if(d.is_null()) {t=0;} else { t=int(d); }
}

void my_set(module_type_t&t, mysqlpp::String d)
{
  if(d.is_null()) {t=module_type_t(0);} else {t=module_type_t(int(d));}
}

void my_set(string&s,mysqlpp::String d)
{
  if(d.is_null()) {s.erase();} else {s=d.c_str();subst_macros(s);}
}

void my_set(bool&b,mysqlpp::String d)
{
  if(d.is_null()) {b=false;} else {b=int(d)!=0;}
}

void my_set(unsigned long long&l,mysqlpp::String d)
{
  if(d.is_null()) {l=0;} else {l=mysqlpp::ulonglong(d);}
}


#endif

// This method searches the the string s for the pattern $(.*) and tries
// to substitute it with the expansion of \1. It therefor uses the map macros
// that is filled in the beginning of read(). It returns if a change was made.
// The accesses to map macros have to be protected with macros_mutex because
// another thread could at the same time reread it from the DB.

map<string,string> macros;
pthread_mutex_t macros_mutex=PTHREAD_MUTEX_INITIALIZER;

bool subst_macros(string&s)
{
  bool changed=false;
  int nesting=0;
  string toplevel_macro;
  string::size_type pos=s.find("${");
  while(pos!=string::npos)
    {
      string::size_type end = s.find('}',pos);
      if(end==string::npos)
	{
	  throw DatabaseEntry::Error("mismatched braces in macro expansion of "+s);
	}
      // get the macro's name from between "${" and "}"
      string macro=s.substr(pos+2,end-pos-2);
      if(!nesting)
	{
	  toplevel_macro=macro;
	}
      // if the macro is not found, expansion is ""
      pthread_mutex_lock(&macros_mutex);
      string expansion=macros[macro];
      pthread_mutex_unlock(&macros_mutex);
      s.replace(pos,end-pos+1,expansion);
      changed = true;
      string::size_type newpos = s.find("${",pos);
      if(newpos == pos)
	{
	  // nesting of macros is allowed, but after 10 iterations some
	  // some progress has to be made.
	  if(++nesting==10)
	    {
	      throw DatabaseEntry::Error("nesting too deep when expanding "
					 +toplevel_macro);
	    }
	}
      else
	{
	  nesting=0;
	  pos=newpos;
	}
    }
  return changed;
}

/**************************************************************/
// This is the main method of DatabaseEntry. It retrieves from
// the database all information available for the module
// (identified by its source ID) and the connected frontends
/**************************************************************/
void
DatabaseEntry::read()
{
  // open db connection
  // which will be closed at the end of this method
  mysqlpp::Connection con(true);
  con.connect(db_db,db_server,db_user,db_passwd,db_port);
  mysqlpp::Query query=con.query();
  MYSQLPP_RESULT result;
  mysqlpp::Row row;

  // first clear and get the set of macros
  // To prevent other threads from reading macros while it is
  // reread from the DB, we hold the macros_mutex for this time
  pthread_mutex_lock(&macros_mutex);
  macros.clear();
  query<<"select Name,Expansion from MACROS where Version_tag="<<version;
  result=query.store();
  for(unsigned int i=0;i<result.size();++i) {
    row=result.at(i);
    macros[row.at(0).c_str()]=row.at(1).c_str();
  }
  pthread_mutex_unlock(&macros_mutex);

  // retrieve general module info
  query.reset();
  query<<"select State,"
    "VME_host_name,"
    "VME_base_address,"
    "Detector,"
    "MODULE_program,"
    "TCS_program,"
    "ROB_slot,"
    "Slink_format_ID,"
    "Slink_mode,"
    "Trigger_mode,"
    "CableName,"
    "MODULE_serial_number,"
    "Configuration,"
    "Modification_date,"
    "Type,"
    "Minor_modules, "
    "LOAD_A, "
    "LOAD_A_minors "
    "from MODULE where MODULE_source_ID="<<sourceID_
       <<" and Version_tag="<<version;
  result=query.store();
  if(!result.size()) {
    throw Error("MODULE_source_ID "+mydec(sourceID_)+" was not found");
  }
  row=result.at(0);
  MY_SET(state,row.at(0));
  MY_SET(hostname,row.at(1));
  MY_SET(base_address,row.at(2));
  MY_SET(detector,row.at(3));
  MY_SET_S(prog,name,row.at(4));
  MY_SET_S(tcs,name,row.at(5));
  MY_SET_S(rob,name,row.at(6));
  MY_SET(slink_format,row.at(7));
  MY_SET(slink_mode,row.at(8));
  MY_SET(trigger_mode,row.at(9));
  MY_SET(cable,row.at(10));
  MY_SET(serial,row.at(11));
  MY_SET(config,row.at(12));
  MY_SET(mod_date,row.at(13));
  MY_SET(type,row.at(14));
  MY_SET(minors,row.at(15));
  MY_SET(load_a,row.at(16));
  MY_SET(load_a_minors,row.at(17));

  // get info for module programming
  if(prog.has_name) {
    query.reset();
    query<<"select Description,"
      "MODULE_setup_file1,"
      "MODULE_setup_file2,"
      "MODULE_setup_file3,"
      "Modification_date "
      "from MODULE_PROGRAM where MODULE_program='"<<ref_link(prog.name)
	 <<"' and Version_tag="<<ref_version(prog.name);
    result=query.store();
    if(!result.size()) {
      throw Error("MODULE_program '"+ref_link(prog.name)+"' version "
		  +ref_version(prog.name)+" is missing in the database");
    }
    row=result.at(0);
    MY_SET_S(prog,desc,row.at(0));
    for(int i=0;i<3;i++) {
      MY_SET_S(prog,setup[i],row.at(i+1));
    }
    MY_SET_S(prog,mod_date,row.at(4));
  } else {
    prog.has_desc=false;
    prog.has_setup[0]=false;
    prog.has_setup[1]=false;
    prog.has_setup[2]=false;
    prog.has_mod_date=false;
  }

  // get info on tcs receiver
  if(tcs.has_name) {
    query.reset();
    query<<"select Description,"
      "TCS_setup_file,"
      "Modification_date "
      "from TCS_PROGRAM where TCS_program='"<<ref_link(tcs.name)
	 <<"' and Version_tag="<<ref_version(tcs.name);
    result=query.store();
    if(!result.size()) {
      throw Error("TCS_program '"+ref_link(tcs.name)+"' version "
		  +ref_version(tcs.name)+" is missing in the database");
    }
    row=result.at(0);
    MY_SET_S(tcs,desc,row.at(0));
    MY_SET_S(tcs,setup,row.at(1));
    MY_SET_S(tcs,mod_date,row.at(2));
  }

  // get info on the connection to the spill buffer
  if(rob.has_name) {
    query.reset();
    query<<"select Type,"
      "CableName,"
      "Modification_date "
      "from SPILLBUFFER where ROB_slot='"<<ref_link(rob.name)
	 <<"' and Version_tag="<<ref_version(rob.name);
    result=query.store();
    if(!result.size()) {
      throw Error("ROB_slot '"+ref_link(rob.name)+"' version '"
		  +ref_version(rob.name)+"' is missing in the database");
    }
    row=result.at(0);
    MY_SET_S(rob,type,row.at(0));
    MY_SET_S(rob,cable,row.at(1));
    MY_SET_S(rob,mod_date,row.at(2));
  }

  // get info on frontend cards
  query.reset();
  query<<"select Detector,"
    "Geographic_ID,"
    "Format_ID,"
    "State,"
    "Calibration_info,"
    "Configuration,"
    "FRONTEND_program,"
    "Port,"
    "Number_of_TDC,"
    "Frontend_disabled,"
    "Gate_window,"
    "Gate_latency,"
    "TBname,"
    "CableName,"
    "FRONTEND_serial_number,"
    "Modification_date "
    "from FRONTEND where MODULE_source_ID="<<sourceID_
       <<" and Version_tag="<<version
       <<" order by Geographic_ID desc;";
  result=query.store();
  for(unsigned int i=0;i<result.size();++i) {
    row=result.at(i);
    frontends.push_back(DatabaseEntryFrontend(row,query,version));
  }

  // reset port flags
  for(int i=0;i<16;++i) {
    ports[i]=false;
  }
  portflags=0;
  // and get them from the active frontends
  list<DatabaseEntryFrontend>::iterator i;
  for(i=frontends.begin();i!=frontends.end();++i) {
    ports[i->port]|=i->state;
    portflags|=i->state<<i->port;
  }
}

void
DatabaseEntry::clear()
{
  // delete frontends
  frontends.clear();
}

// print the whole record into the stream 'out'
// this is used to write the db contents with the raw data
// to tape: the stream will be published via DIM (as char*)
ostream&
DatabaseEntry::print(ostream&out)
{
  // table MODULE
  out<<"source "<<sourceID_;
  PRINT_DATA(state);
  PRINT_DATA(minors);
  PRINT_DATA(serial);
  PRINT_DATA(hostname);
  out<<";base_address 0x"<<myhex(base_address);
  PRINT_DATA(detector);
  PRINT_DATA(config);
  out<<";mod_prog "<<prog.name;
  out<<";tcs "<<tcs.name;
  PRINT_DATA_S(rob,name);
  PRINT_DATA(slink_format);
  PRINT_DATA(slink_mode);
  PRINT_DATA(trigger_mode);
  PRINT_DATA(cable);
  PRINT_DATA(mod_date);
  out<<endl;

  // table MODULE_PROGRAM
  out<<"mod_prog "<<prog.name;
  PRINT_DATA_S(prog,desc);
  PRINT_DATA_S(prog,setup[0]);
  PRINT_DATA_S(prog,setup[1]);
  PRINT_DATA_S(prog,setup[2]);
  PRINT_DATA_S(prog,mod_date);
  out<<endl;

  // table TCS_PROGRAM
  out<<"tcs "<<tcs.name;
  PRINT_DATA_S(tcs,desc);
  PRINT_DATA_S(tcs,setup);
  PRINT_DATA_S(tcs,mod_date);
  out<<endl;

  // table SPILLBUFFER
  if(rob.has_name) {
    out<<"rob "<<rob.name;
    PRINT_DATA_S(rob,cable);
    PRINT_DATA_S(rob,type);
    PRINT_DATA_S(rob,mod_date);
    out<<endl;
  }

  // print frontends info
  list<DatabaseEntryFrontend>::iterator it;
  for(it=frontends.begin();it!=frontends.end();++it) {
    it->print(out);
  }

  return out;
}

ostream& operator<<(ostream&out,DatabaseEntry&db)
{
  return db.print(out);
}

// this constructor sets the Frontend object from a
// db row (table FRONTEND) which was 'select'ed in DatabaseEntry::read()
DatabaseEntryFrontend::DatabaseEntryFrontend
(mysqlpp::Row&row,mysqlpp::Query&query,string version)
{
  if(0)
    {
      cout << "detector      " << row.at(0) <<endl;
      cout << "geographic_id " << row.at(1) <<endl;
      cout << "format_id     " << row.at(2) <<endl;
      cout << "state         " << row.at(3) <<endl;
      cout << "calib         " << row.at(4) <<endl;
      cout << "config        " << row.at(5) <<endl;
      cout << "fe_program    " << row.at(6) << endl;
      cout << "port          " << row.at(7) << endl;
      cout << "num_tdc       " << row.at(8) << endl;
      cout << "fe-disabled   " << row.at(9) << endl;
      cout << "gate_window   " << row.at(10) << endl;
      cout << "gate_latency  " << row.at(11) << endl;
      cout << "tb_name       " << row.at(12) << endl;
      cout << "cable name    " << row.at(13) << endl;
      cout << "serial number " << row.at(14) << endl;
      cout << "mod_date      " << row.at(15) << endl;
    }
  MY_SET(detector,row.at(0));
  MY_SET(geographic_id,row.at(1));
  MY_SET(format_id,row.at(2));
  MY_SET(state,row.at(3));
  MY_SET(calib,row.at(4));
  MY_SET(config,row.at(5));
  MY_SET_S(fe,name,row.at(6));
  MY_SET(port,row.at(7));
  MY_SET(num_tdc,row.at(8));
  MY_SET(fe_disabled,row.at(9));
  MY_SET(gate_window,row.at(10));
  MY_SET(gate_latency,row.at(11));
  MY_SET(tb_name,row.at(12));
  MY_SET(cable,row.at(13));
  MY_SET(serial,row.at(14));
  MY_SET(mod_date,row.at(15));

  if(fe.has_name) {

    // split fe.name in link/version parts
    string name,ver;
    size_t pos=fe.name.find(':');
    if(pos!=string::npos) {
      name=fe.name.substr(0,pos);
      ver="'"+fe.name.substr(pos+1)+"'";
    } else {
      name=fe.name;
      ver=version;
    }

    query.reset();
    query<<"select Description,"
      "FRONTEND_setup_file,"
      "Modification_date "
      "from FRONTEND_PROGRAM where FRONTEND_program='"<<name
	 <<"' and Version_tag="<<ver;
    MYSQLPP_RESULT result=query.store();
    if(!result.size()) {
      throw DatabaseEntry::Error("FRONTEND_program '"+name+"' version '"
				 +ver+"' is missing in the database");
    }
    mysqlpp::Row row=result.at(0);
    MY_SET_S(fe,desc,row.at(0));
    MY_SET_S(fe,setup,row.at(1));
    MY_SET_S(fe,mod_date,row.at(2));
  } else {
    fe.has_desc=false;
    fe.has_setup=false;
    fe.has_mod_date=false;
  }
}

ostream&
DatabaseEntryFrontend::print(ostream&out)
{
  out<<"detector "<<detector;
  PRINT_DATA(geographic_id);
  PRINT_DATA(serial);
  PRINT_DATA(tb_name);
  PRINT_DATA(format_id);
  PRINT_DATA(state);
  PRINT_DATA(calib);
  PRINT_DATA(config);
  PRINT_DATA(port);
  PRINT_DATA(num_tdc);
  PRINT_DATA(fe_disabled);
  PRINT_DATA(gate_window);
  PRINT_DATA(gate_latency);
  PRINT_DATA(cable);
  PRINT_DATA(mod_date);
  if(fe.has_name) out<<";fe_program "<<fe.name;
  if(fe.has_desc) out<<";fe_desc "<<fe.desc;
  if(fe.has_setup) out<<";fe_setup "<<fe.setup;
  if(fe.has_mod_date) out<<";fe_mod_date "<<fe.mod_date;
  out<<endl;

  return out;
}

string
DatabaseEntry::ref_link(string s)
{
  size_t pos=s.find(':');
  if(pos!=string::npos) {
    return s.substr(0,pos);
  } else {
    return s;
  }
}

string
DatabaseEntry::ref_version(string s)
{
  size_t pos=s.find(':');
  if(pos!=string::npos) {
    return "'"+s.substr(pos+1)+"'";
  } else {
    return version;
  }
}
