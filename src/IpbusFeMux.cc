#include <string.h>
#include <stdarg.h>
#include <stdint.h>

#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <sstream>
#include <fstream>

#include "IpbusFeMux.h"
#include "IpbusMwpcTdcCard.h"
#include "IpbusGemAdcCard.h"
#include "IpbusCedarTdcCard.h"
#include "IpbusSpTimeTdcCard.h"
#include "int2string.h"
#include "main.h"
#include "wait.h"
#include "gs_todo.h"

using namespace std;
using namespace uhal;

IpbusFeMux::IpbusFeMux(int sourceID, std::string version) : IpbusModule(sourceID,version)
{
  dim["ModuleType"] = MT_IPBUS_MUX;
  module_name="IPBUS FrontEnd Multiplexer ("+mydec(sourceID)+')';
  set_clock_freq = false;
  port_mask    = 0;
  port_status  = 0;
  port_enabled = 0;
  tcs_status   = 0;
  slink_status = 0;
  timeout      = 0;
  slink_format = 0;
  useGenClk    = 1;
  detector     = UNKNOWN;
  if(!check_status(IPBUS_MOD_ERROR_STATE) && check_status(IPBUS_MOD_CREATED) && check_db())
    {
      GetServiceMap()["i:PORT_STATUS"]  =  0;
      GetServiceMap()["i:PORT_MASK"]    =  0;
      GetServiceMap()["i:PORT_ENABLED"] =  0;
      GetServiceMap()["i:TCS_STATUS"]   =  0;
      GetServiceMap()["i:SLINK_STATUS"] =  0;
      GetServiceMap()["i:TIMEOUT"]      =  0;
      GetServiceMap()["c:FE_STATUS"]    = "";
      printf("%s is created.", module_name.c_str());
    }
}

IpbusFeMux::~IpbusFeMux()
{
  DEBUG("%s is destroyed.", module_name.c_str(), getSourceID());
  DeleteFECards();
}

bool IpbusFeMux::check_db()
{
  list<DatabaseEntryFrontend>::iterator it;
  set_clock_freq = false;
  port_mask = 0;
  dim["ModuleStatus"]  = MS_DBXS;
  try
    {
      if(!module_db.has_state)
	{
	  throw DBError(this, "missing 'State' info in FEDB for " + module_name); 
	}
      else if(module_db.state != 1)
	{
	  throw DBError(this, "Zero module 'State' in FEDB for " + module_name); 
	}
      if(!module_db.has_detector)
	{
	  throw DBError(this, "missing 'Detector' info in FEDB for " + module_name); 
	}
      else
	{
	  detector = UNKNOWN;
	  DEBUG("Detector name = %s", module_db.detector.c_str());
	  if(module_db.detector == "MWPC")
	    detector = MWPC;
	  else if(module_db.detector == "GEMADC")
	    detector = GEMADC;
	  else if(module_db.detector == "CEDAR")
	    detector = CEDAR;
	  else if(module_db.detector == "SPEAKING_TIME_TDC")
	    detector = SPEAKING_TIME_TDC;
	  else if(module_db.detector == "TRIGGER")
	    detector = TRIGGER;
	  else if(module_db.detector == "SciFi")
	    detector = SCI_FI;
	  else
	    throw DBError(this, "bad 'Detector' info (" + module_db.detector + 
			  ") in FEDB for " + module_name); 
	}
      if(!module_db.has_config)
	{
	  throw DBError(this,"missing module timeout in 'Configuration' for " + module_name); 
	}
      else if(1)
	{
	  if(GetConfig().find("TIMEOUT") == GetConfig().end())
	    {
	      throw DBError(this," Parameter 'TIMEOUT' is not found in 'Configuration' of the " + module_name);
	    }
	  if(sscanf(GetConfig()["TIMEOUT"].c_str(),"%x", &timeout) != 1)
	    {
	      throw DBError(this, "Bad module " + module_name + " timeout " + module_db.trigger_mode + "in FEDB.");
	    }
	}
      if(detector == GEMADC || detector == MWPC || detector == CEDAR || detector == TRIGGER || detector == SCI_FI)
	{
	  if(GetConfig().find("DO_NOT_SET_FREQ") == GetConfig().end())
	    set_clock_freq = true;
	  else if(detector == MWPC || detector == SCI_FI)
	    {
	      if(GetConfig().find("GEN_CLK") != GetConfig().end())
		{
		  if(sscanf(GetConfig()["GEN_CLK"].c_str(),"%u", &useGenClk) != 1 || useGenClk > 1)
		    {
		      throw DBError(this, "Bad module " + module_name + " GEN_CLK parameter in FEDB.");
		    }
		  else
		    {
		      DEBUG("%s MHz clock selected in DB.", useGenClk==0?"125":"155.52");
		    }
		}
	      else
		{
		  DEBUG("155.52 MHz clock selected by default.");
		}
	    }
	}
      if(!module_db.has_slink_format)
	{
	  throw DBError(this, "missing Slink format ID for " + module_name);
	}
      else if(module_db.slink_format != (module_db.slink_format&0xFF))
	{
	  throw DBError(this, "bad Slink format ID " + myhex(module_db.slink_format) + " in FEDB for " + module_name);
	}
      else
	{
	  slink_format = (uint32_t)module_db.slink_format;
	}
      for(it=module_db.frontends.begin(); it!=module_db.frontends.end(); ++it)
	{
	  if(!it->has_port)
	    throw DBError(this,"missing 'Port' for frontend "+it->detector);
	  if(!it->has_state)
	    throw DBError(this,"missing 'State' for frontend "+it->detector);
	  if(it->state != 1)
	    {
	      int32_t geo_id = -1;
	      if(it->has_geographic_id)
		geo_id = it->geographic_id;
	      DEBUG("FE card in port %u geoID = 0x%x has status %d. It is disabled.", it->port, (uint32_t)(geo_id&0xff), it->state);
	      continue;
	    }
	  else
	    {
	      uint32_t mask = (1 << it->port);
	      if(detector == GEMADC && it->geographic_id < 2) // ADC port
		port_mask |= mask;
	      else if(detector != GEMADC)
		port_mask |= mask;
	    }
	  if(detector != GEMADC && !it->has_cable)
	    throw DBError(this,"missing FE card name in 'CableName' for frontend " + it->detector);
	  if(detector == MWPC)
	    {
	      if(!it->has_format_id)
		throw DBError(this,"missing 'Format_ID' for frontend "+it->detector);
	      if(it->format_id < 0 || it->format_id > 1)
		throw DBError(this,"bad 'Format_ID' " + mydec(it->format_id) + " for frontend "+it->detector);
	      if(!it->has_calib)
		throw DBError(this,"missing 'Calibration_Info' for frontend "+it->detector);
	      if(!it->has_fe_disabled)
		throw DBError(this,"missing 'Frontend_disabled' parameter for frontend "+it->detector);
	      if(!it->has_gate_window)
		throw DBError(this,"missing 'Gate_window' parameter for frontend "+it->detector);
	      if(!it->has_gate_latency)
		throw DBError(this,"missing 'Gate_latency' parameter for frontend "+it->detector);
	      if(!it->has_config)
		throw DBError(this,"missing 'Configuration' for frontend "+it->detector);
	    }
	  else if(detector == SPEAKING_TIME_TDC)
	    {
	      if(!it->has_format_id)
		throw DBError(this,"missing 'Format_ID' for frontend "+it->detector);
	      if(it->format_id != 4)
		throw DBError(this,"bad 'Format_ID' " + mydec(it->format_id) + " for frontend "+it->detector +
			      " it has to be 4 for this detector type");
	      if(!it->has_gate_window)
		throw DBError(this,"missing 'Gate_window' parameter for frontend "+it->detector);
	      if(!it->has_gate_latency)
		throw DBError(this,"missing 'Gate_latency' parameter for frontend "+it->detector);
	      if(!it->has_config)
		throw DBError(this,"missing 'Configuration' for frontend "+it->detector);
	    }
	  else if(detector == GEMADC)
	    {
	      if(!it->has_geographic_id)
		throw DBError(this,"missing 'Geographic_ID' for frontend "+it->detector);
	      if(it->geographic_id < 2)
		{
		  if(!it->has_cable)
		    throw DBError(this,"missing FE card name in 'CableName' for frontend " + it->detector);
		  if(!it->has_calib)
		    throw DBError(this,"missing 'Calibration_info' for frontend "+it->detector+
				  " and Geographic_ID="+myhex(it->geographic_id));
		  if(access(it->calib.c_str(),R_OK))
		    throw DBError(this,"File in DB " + it->calib + " does not exist.");
		}
	      if(!it->has_format_id)
		throw DBError(this,"missing 'Format_ID' for frontend "+it->detector);
	      if(!it->has_config)
		throw DBError(this,"missing 'Configuration' for frontend "+it->detector);
	    }
	  else if(detector == CEDAR)
	    {
	      if(!it->has_format_id)
		throw DBError(this,"missing 'Format_ID' for frontend "+it->detector);
	      if(it->format_id < 1 || it->format_id > 3)
		throw DBError(this,"bad format id " + mydec(it->format_id) + " for frontend "+it->detector);
	      if(!it->has_config)
		throw DBError(this,"missing 'Configuration' for frontend "+it->detector);
	      // if(!it->fe.has_name)
	      // 	throw DBError(this,"missing 'FRONTEND_program' for frontend "+it->detector);
	      // else if(access(it->fe.name.c_str(),X_OK))
	      // 	throw DBError(this,"file in DB " + it->fe.name + " does not exist or is not executable.");
	    }
	}
      DEBUG("IpbusFeMux::check_db: SrcID %u, port_mask: 0x%x:  OK!", getSourceID(), port_mask);
    }
  catch(DatabaseEntry::Error& e)
    {
      ShowError("database error: " + e.what());
      set_status(IPBUS_MOD_ERROR_STATE);
      return false;
    }
  catch(DBError& e)
    {
      ShowError("DB error: " + e.what());
      set_status(IPBUS_MOD_ERROR_STATE);
      return false;
    }
  catch(std::exception& e)
    {
      ShowError(std::string("std error: ") + e.what());
      set_status(IPBUS_MOD_ERROR_STATE);
      return false;
    }
  return true;
}

void IpbusFeMux::InitModule(int todo)
{
  DEBUG("IpbusFeMux::InitModule(0x%08x)",todo);
  if(check_status(IPBUS_MOD_ERROR_STATE) || !check_status(IPBUS_MOD_CREATED))
    {
      throw Error("Attempt to use not created IPBUS module.");
    }

  // Do commands. Note: the order matters

  dim["ModuleStatus"]  = MS_INITIALIZE;
  dim["ErrorString"]   = "";
  dim["WarningString"] = "";
  dim["FE_STATUS"]     = "";
  dim["Programmed"]    =  0;
  bool isError = false;
  if((todo & GS_DO_ALL) != 0)
    {
      if(!do_all())
	{
	  isError = true;
	}
    }
  else // todo all procedures separately
    {
      while(todo != 0)
	{
	  if((todo & GS_DO_TCS) != 0)
	    {
	      todo &= ~GS_DO_TCS;
	      double  freq     = 0.; // clock frequency
	      double  measured = 0.; // measured clock frequency
	      uint8_t addr = 0;      // I2C address of clock generator
	      SetClockFrequency(freq,measured,addr);
	      char info_msg[256];
	      sprintf(info_msg,"Clock frequency: %.3lf MHz, I2C addr: 0x%02X\nMeasured TCS Clock frequency: %.3lf MHz\n",
		      freq, addr,measured);
	      dim["WarningString"] = info_msg;
	    }
	  else if((todo & GS_DO_GESICA) != 0)
	    {
	      todo &= ~GS_DO_GESICA;
	      if(!do_init_mux())
		{
		  isError = true;
		  break; // FATAL error, stop init.
		}
	    }
	  else if((todo & GS_DO_ADC_REGISTERS) != 0)
	    {
	      todo &= ~GS_DO_ADC_REGISTERS;
	      if(!do_reset_adc())
		{
		  isError = true;
		}
	    }
	  else if((todo & GS_DO_APV_RESET) != 0)
	    {
	      todo &= ~GS_DO_APV_RESET;
	      if(!do_reset_apv())
		{
		  isError = true;
		}
	    }
	  else if((todo & GS_DO_MODE) != 0)
	    {
	      todo &= ~GS_DO_MODE;
	      //DEBUG("*** %u %u %u", todo, GS_DO_MODE_OVERRIDE, GS_DO_MODE);
	      if((todo & GS_DO_MODE_OVERRIDE))
		{
		  todo &= ~GS_DO_MODE_OVERRIDE;
		  if(!do_set_latch_mode())
		    isError = true;
		}
	      else
		{
		  if(!do_set_sparse_mode())
		    isError = true;
		}
	    }
	  else if((todo & GS_DO_APV) != 0)
	    {
	      todo &= ~GS_DO_APV;
	      if(!do_load_apv_registers())
		isError = true;
	    }
	  else if((todo & GS_DO_THR) != 0)
	    {
	      todo &= ~GS_DO_THR;
	      if(!do_load_thresholds())
		isError = true;
	    }
	  else if((todo & GS_DO_PED) != 0)
	    {
	      todo &= ~GS_DO_PED;
	      if(!do_load_pedestals())
		isError = true;
	    }
	  else if((todo & GS_DO_VERIFY) != 0)
	    {
	      todo &= ~GS_DO_VERIFY;
	      if(!do_verify_registers())
		isError = true;
	    }
	  else if((todo & GS_SHOW_MUX_STATUS) != 0)
	    {
	      todo &= ~GS_SHOW_MUX_STATUS;
	      if(!do_show_status())
		isError = true;
	    }
	  else if(todo != 0)
	    {
	      ShowError("Unknown todo option.");
	      isError = true;
	      todo = 0;
	    }
	}
    }
  ReadStatus(0);
  if(!isError)
    {
      dim["Programmed"]=1;
    }
}

void IpbusFeMux::Reset(int i)
{
  DEBUG("IpbusFeMux::Reset(%d).", i);
  int status = dim["ModuleStatus"];
  dim["ModuleStatus"] = MS_RESET;
  try
    {
      if(detector == GEMADC || detector == MWPC || detector == CEDAR || detector == SCI_FI)
	{
	  ipbus_write("static_out0.LinkReset",1);
	  Sleep(1000000);
	  ipbus_write("static_out0",0);
	  Sleep(100000);
	  ipbus_write("static_out0.CDCReset",1);
	  Sleep(1000000);
	  ipbus_write("static_out0",0);
	  Sleep(100000);
	}
      else if(detector == SPEAKING_TIME_TDC)
	{
	  ipbus_write("RESET",2);
	  Sleep(100000);
	  ipbus_write("RESET",0);
	  Sleep(100000);
	}
      else
	{
	  ipbus_write("static_out0.LinkReset",0);
	  Sleep(100000);
	}
    }
  catch(Module::Error& e)
    {
      ShowError(e.what());
    }
  dim["ModuleStatus"] = status;
}

void IpbusFeMux::ReadStatus(int i)
{
  dim["ModuleStatus"] = MS_READSTATUS;
  DEBUG("IpbusFeMux::ReadStatus(%d).", i);
  try
    {
      if(i == 2040) // Bug fix for old version of LOAD program
	{
	  InitModule(GS_DO_VERIFY);
	  dim["ModuleStatus"] = MS_READY;
	  return;
	}
      if(check_status(IPBUS_MOD_ERROR_STATE) || !check_status(IPBUS_MOD_CREATED))
	throw IpbusError("IPBUS module is in error state.");
      if(detector == SPEAKING_TIME_TDC)
	{
	  ipbus_read("PORT_STATUS", port_status);
	  ipbus_read("PORT_EN",port_enabled);
	  ipbus_read("TIMEOUT", timeout);
	}
      else
	{
	  ipbus_read("ucf_status", port_status);
	  ipbus_read("static_out2.portEn", port_enabled);
	  ipbus_read("TCS.RDY", tcs_status);
	  ipbus_read("SLINK.RDY", slink_status);
	  // if(detector == GEMADC)
	  //   timeout = 0;
	  // else
	  ipbus_read("static_out3.timeout", timeout);
	}

      GetServiceMap()["PORT_STATUS"]  = (int)port_status;
      GetServiceMap()["PORT_MASK"]    = (int)port_mask;
      GetServiceMap()["PORT_ENABLED"] = (int)port_enabled;
      GetServiceMap()["TCS_STATUS"]   = (int)tcs_status;
      GetServiceMap()["SLINK_STATUS"] = (int)slink_status;
      GetServiceMap()["TIMEOUT"]      = (int)timeout;

      map<int,IpbusFECard*>::iterator fec_it;
      string fe_status;
      for(fec_it = GetFECards().begin(); fec_it != GetFECards().end(); fec_it++)
	{
	  try
	    {
	      fec_it->second->ReadStatus(fe_status);
	    }
	  catch(Module::Error& e)
	    {
	      ShowError(e.what());
	    }
	}
      GetServiceMap()["FE_STATUS"] = fe_status;
    }
  catch(Module::Error& e)
    {
      ShowError(e.what());
    }
}

void IpbusFeMux::Control(const char* cmd)
{
  dim["ErrorString"]   = "";
  DEBUG("IpbusFeMux::Control(\"%s\")", cmd);
  if(strncmp(cmd,"LOAD_port",9) == 0)
    {
      try
	{
	  int p = -1;
	  if(sscanf(cmd,"LOAD_port %d", &p) == 1 && p >= 0 && p < 0xFFFF)
	    LoadPort(p);
	}
      catch(std::exception& e)
	{
	  ShowError(e.what());
	}
      catch(Module::IpbusError& e)
	{
	  ShowError(e.what());
	}
      catch(...)
	{
	  ShowError("LoadPort exception.");
	}
      return;
    }
  if(check_status(IPBUS_MOD_ERROR_STATE) || !check_status(IPBUS_MOD_CREATED))
    {
      throw IpbusError("IPBUS module is in error state.");
    }
  if(strncmp(cmd,"ReadStatus",10) == 0)
    {
      int daqnum = 0;
      if(sscanf(cmd,"ReadStatus %d", &daqnum) == 1 && daqnum < 8 && daqnum >= 0)
	ReadStatus(daqnum);
    }
}

uint32_t IpbusFeMux::GetFirmwareID()
{
  DEBUG("IpbusFeMux::GetFirmwareID");
  uint32_t fw_id = 0xBAD;
  if(check_status(IPBUS_MOD_ERROR_STATE) || !check_status(IPBUS_MOD_CREATED))
    throw IpbusError("IPBUS module is in error state.");
  
  ipbus_read("fw_version", fw_id);
  return fw_id;
}

void IpbusFeMux::SetModuleSrcID()
{
  DEBUG("Setting source ID %u in module register.", getSourceID());
  if(detector == SPEAKING_TIME_TDC)
    ipbus_write("DAQ_REG.SRC_ID", getSourceID());
  else
    ipbus_write("static_out1.srcId", getSourceID());
}

void IpbusFeMux::SetBaseIpAddr()
{
  DEBUG("Setting start IP address of TDC cards.");
  if(detector == GEMADC || detector == MWPC || detector == CEDAR || detector == SCI_FI)
    {
      if(GetConfig().find("ip_addr_base") != GetConfig().end())
	{
	  string addr = GetConfig()["ip_addr_base"];
	  uint32_t ip_addr = 0;
	  if(sscanf(addr.c_str(),"0x%x",&ip_addr) == 1)
	    {
	      DEBUG("IP address: %08x", ip_addr);
	      ipbus_write("ip_addr_base", ip_addr);
	    }
	  else
	    {
	      char err_msg[256];
	      sprintf(err_msg,"Bad ip_addr_base '%s' in database.", addr.c_str());
	      throw DBError(this, err_msg);
	    }
	}
      else
	{
	  throw DBError(this, "No ip_addr_base 'Configuration' parameter in database.");
	}
    }
  else
    DEBUG("Not allowed for this module.");
}

extern bool SetClockFrequency(uhal::HwInterface* phw, double& freq, double& measured, uint8_t& addr); // defined in Si5338.cc

void IpbusFeMux::SetClockFrequency(double& freq, double& measured, uint8_t& addr)
{
  if(detector == UNKNOWN)
    return;
  if(!set_clock_freq)
    {
      DEBUG("Setting of clock frequency is disabled in DB.");
      return;
    }
  if(!::SetClockFrequency(GetHwInterface(), freq, measured, addr))
    throw Module::IpbusError("Si5338 driver error.");
  ipbus_write("static_out1.useGenClk", useGenClk);
}

void IpbusFeMux::SetSlinkFormat()
{
  if(detector == SPEAKING_TIME_TDC)
    {
      DEBUG("Setting Slink format 0x%x in module register.", slink_format);
      ipbus_write("DAQ_REG.FORMAT", slink_format);
    }
  else if(detector != GEMADC && detector != MWPC && detector != CEDAR && detector != TRIGGER && detector != SCI_FI)
    {
      DEBUG("Setting Slink format 0x%x in module register.", slink_format);
      ipbus_write("static_out1.type", slink_format);
    }
}

void IpbusFeMux::SetRegisters()
{
  map<string,string>::iterator it;
  for(it = GetConfig().begin(); it != GetConfig().end(); it++)
    {
      if(it->first == "TIMEOUT")
	continue;
      if(it->first == "FW")
	continue;
      string reg_name = it->first;
      uint32_t reg_val = 0;
      if(sscanf(it->second.c_str(),"0x%x", &reg_val) == 1)
	{
	  DEBUG("Write register %s = 0x%x", reg_name.c_str(), reg_val);
	  ipbus_write(reg_name.c_str(), reg_val);
	}
      else if(sscanf(it->second.c_str(),"%u", &reg_val) == 1)
	{
	  DEBUG("Write register %s = %u", reg_name.c_str(), reg_val);
	  ipbus_write(reg_name.c_str(), reg_val);
	}
      else
	{
	  DEBUG("Bad value (%s) for register '%s'", it->second.c_str(), reg_name.c_str());
	}
    }
}

void IpbusFeMux::ResetModule()
{
  DEBUG("Reset IPBUS TDC MUX module.");
  Reset(0);
}

void IpbusFeMux::LoadPort(int mask)
{
  DEBUG("IpbusFeMux::LoadPort");
   if(detector == MWPC || detector == CEDAR || detector == SPEAKING_TIME_TDC || detector == SCI_FI || detector == TRIGGER)
    {
      list<DatabaseEntryFrontend>::iterator it;
      for(it=module_db.frontends.begin(); it!=module_db.frontends.end(); ++it)
	{
	  int test = 1<< it->port;
	  if(it->state == 1 && (test & mask) != 0)
	    {
	      DEBUG("LOAD port %d of SrcID %d", it->port, getSourceID());
	      if(detector == MWPC)
		{
		  IpbusMwpcTdcCard* card = (IpbusMwpcTdcCard*)GetFECards()[it->port];
		  card->reset_module();   
		  card->enable_channels();
		  card->set_window();     
		  card->set_delay();
		  card->set_thresholds();
		}
	      else if(detector == TRIGGER || detector == SCI_FI)
		{
		  IpbusMwpcTdcCard* card = (IpbusMwpcTdcCard*)GetFECards()[it->port];
		  card->reset_module();   
		  card->enable_channels();
		  card->set_window();     
		  card->set_delay();
		}
	      else if(detector == CEDAR)
		{
		  IpbusCedarTdcCard* card = (IpbusCedarTdcCard*)GetFECards()[it->port];
		  card->reset_module();   
		  card->enable_channels();
		  card->set_window();     
		  card->set_delay();      
		  card->set_thresholds(); 
		}
	      else if(detector == SPEAKING_TIME_TDC)
		{
		  IpbusSpTimeTdcCard* card = (IpbusSpTimeTdcCard*)GetFECards()[it->port];
		  card->reset_module();
		  card->enable_channels();
		  card->set_window();
		  card->set_delay();
		}
	      //GetFECards()[it->port]->set_registers();
	    }
	}
    }
}

bool IpbusFeMux::ConnectFECards()
{
  DEBUG("IpbusFeMux::ConnectFECards");
  DeleteFECards();
  list<DatabaseEntryFrontend>::iterator it;
  port_mask = 0;
  for(it=module_db.frontends.begin(); it!=module_db.frontends.end(); ++it)
    {
      if(it->state == 1)
	{
	  if(detector == MWPC || detector == SCI_FI || detector == TRIGGER)
	    {
	      uint32_t mask = (1 << it->port);
	      if(it->state == 1)
		port_mask |= mask;
	      map<int,IpbusFECard*>::iterator fec_it = GetFECards().find(it->port);
	      if(fec_it != GetFECards().end())
		{
		  throw DBError(this,"2 cards connected to the same port of frontend" + it->detector);
		}
	      IpbusFECard::FECardType cardType = IpbusFECard::UNKNOWN;
	      if(it->format_id == 0)
		cardType = IpbusFECard::MWPC;
	      else if(it->format_id == 1)
		cardType = IpbusFECard::MWPC_T0;
	      GetFECards()[it->port] = new IpbusMwpcTdcCard(cardType, it->cable.c_str(), this, it->port);
	    }
	  else if(detector == GEMADC)
	    {
	      if(it->geographic_id < 2)
		{
		  uint32_t mask = (1 << it->port);
		  if(it->state == 1)
		    port_mask |= mask;
		  map<int,IpbusFECard*>::iterator fec_it = GetFECards().find(it->port);
		  if(fec_it != GetFECards().end())
		    {
		      throw DBError(this,"2 cards connected to the same port of frontend" + it->detector);
		    }
		  GetFECards()[it->port] = new IpbusGemAdcCard(IpbusFECard::GEMADC, it->cable.c_str(), this, it->port);
		  DEBUG("GEMADC FE card is connected to port %u", it->port);
		}
	    }
	  else if(detector == CEDAR)
	    {
	      IpbusFECard::FECardType cardType = IpbusFECard::UNKNOWN;
	      if(it->format_id == 1)
		cardType = IpbusFECard::CEDAR_16CH;
	      else if(it->format_id == 2)
		cardType = IpbusFECard::CEDAR_32CH;
	      else if(it->format_id == 3)
		cardType = IpbusFECard::CEDAR_T0;
	      GetFECards()[it->port] = new IpbusCedarTdcCard(cardType, it->cable.c_str(), this, it->port);
	      DEBUG("CEDAR ifTDC card type %d '%s' is connected to port %u", cardType, it->cable.c_str(), it->port);
	    }
	  else if(detector == SPEAKING_TIME_TDC)
	    {
	      uint32_t mask = (1 << it->port);
	      if(it->state == 1)
		port_mask |= mask;
	      IpbusFECard::FECardType cardType = IpbusFECard::SPEAKING_TIME_TDC;
	      GetFECards()[it->port] = new IpbusSpTimeTdcCard(cardType, it->cable.c_str(), this, it->port);
	    }
	}
    }
  return true;
}

bool IpbusFeMux::LoadFECards()
{
  bool isError = false;
  map<int,IpbusFECard*>::iterator fec_it;
  for(fec_it = GetFECards().begin(); fec_it != GetFECards().end(); fec_it++)
    {
      try
	{
	  if(detector == MWPC || detector == SCI_FI || detector == TRIGGER)
	    {
	      IpbusMwpcTdcCard* card = (IpbusMwpcTdcCard*)fec_it->second;
	      card->reset_module();   
	      card->enable_channels();
	      card->set_window();     
	      card->set_delay();
	      if(detector == MWPC)
		card->set_thresholds();
	    }
	  else if(detector == MWPC || detector == TRIGGER)
	    {
	      IpbusMwpcTdcCard* card = (IpbusMwpcTdcCard*)fec_it->second;
	      card->reset_module();   
	      card->enable_channels();
	      card->set_window();     
	      card->set_delay();      
	    }
	  else if(detector == GEMADC)
	    {
	      IpbusGemAdcCard* card = (IpbusGemAdcCard*)fec_it->second;
	      card->reset_module();       // *
	      card->init_i2c_bus();       // *
	      int retCode = card->set_apv_registers();  // *
	      if(retCode == -1)
		isError = true;
	      card->set_data_mode(0);     // * set default mode (from fedb)
	      card->set_thresholds();     // * uses config parameter 'thr'
	      card->set_pedestals();      // * sets pedestals and sigmas from FRONTEND.Calibration_info
	      card->set_adc_registers();  // *
	    }
	  else if(detector == CEDAR)
	    {
	      IpbusCedarTdcCard* card = (IpbusCedarTdcCard*)fec_it->second;
	      card->reset_module();   
	      card->enable_channels();
	      card->set_window();     
	      card->set_delay();      
	      card->set_thresholds(); 
	    }
	  else if(detector == SPEAKING_TIME_TDC)
	    {
	      IpbusSpTimeTdcCard* card = (IpbusSpTimeTdcCard*)fec_it->second;
	      card->reset_module();
	      card->enable_channels();
	      card->set_window();
	      card->set_delay();
	    }
	  fec_it->second->set_registers();
	}
      catch(Module::Error& e)
	{
	  ShowError(e.what());
	  isError = true;
	}
    }
  if(isError)
    {
      set_status(IPBUS_MOD_ERROR_STATE);
      return false;
    }
  return true;
}

void IpbusFeMux::DeleteFECards()
{
  port_mask = 0;
  map<int,IpbusFECard*>::iterator fec_it;
  for(fec_it = GetFECards().begin(); fec_it != GetFECards().end(); fec_it++)
    delete fec_it->second;
  GetFECards().clear();
}

void IpbusFeMux::EnablePorts()
{
  DEBUG("Enable ports with mask 0x%08X.", port_mask);
  if(detector == SPEAKING_TIME_TDC)
    ipbus_write("PORT_EN", port_mask);
  else
    ipbus_write("static_out2.portEn", port_mask);
}

void IpbusFeMux::ReadPortStatus()
{
  DEBUG("ReadPortStatus");
  uint32_t status = 0xBAD;
  if(detector != SPEAKING_TIME_TDC)
    {
      ipbus_read("ucf_status", status);
    }
  GetServiceMap()["PORT_STATUS"] = (int)status;
}

void IpbusFeMux::SetTimeout()
{
  DEBUG("Setting timeout 0x%X",timeout);
  if(detector == SPEAKING_TIME_TDC)
    ipbus_write("TIMEOUT", timeout);
  else
    ipbus_write("static_out3.timeout", timeout);
  GetServiceMap()["TIMEOUT"] = (int)timeout;
}

void IpbusFeMux::ReadTimeout()
{
  DEBUG("Read timeout");
  uint32_t t = 0xBAD;
  if(detector == GEMADC || detector == MWPC || detector == CEDAR || detector == SCI_FI || detector == TRIGGER)
    {
      ipbus_read("static_out3.timeout", t);
    }
  else if(detector == SPEAKING_TIME_TDC)
    {
      ipbus_read("TIMEOUT", t);
    }
  GetServiceMap()["TIMEOUT"] = (int)t;
  if(t != timeout)
    {
      char msg[256];
      sprintf(msg,"Timeout value is not match with DB value (%x != %x).",t,timeout);
      ShowError(msg);
    }
}

bool IpbusFeMux::do_all()
{
  DEBUG("IpbusFeMux::do_all");
  bool isError = false;
  double  freq = 0.;
  double  meas = 0.;
  uint8_t addr = 0;
  SetClockFrequency(freq,meas,addr);

  if(do_init_mux())
    {
      //ConnectFECards();
      if(!ConnectFECards() || !LoadFECards())
	isError = true;
    }
  else
    {
      isError = true;
    }

  if(isError)
    {
      set_status(IPBUS_MOD_ERROR_STATE);
      return false;
    }
  return true;
}

bool IpbusFeMux::do_init_mux()
{
  DEBUG("IpbusFeMux::do_init_mux");
  // double  freq = 0.;
  // double  meas = 0.;
  // uint8_t addr = 0;
  // SetClockFrequency(freq,meas,addr);
  SetBaseIpAddr();
  ResetModule();
  SetModuleSrcID();
  SetSlinkFormat();
  EnablePorts();
  SetTimeout();
  SetRegisters();
  ReadPortStatus();
  ReadTimeout();
  return true;
}

bool IpbusFeMux::do_load_thresholds()
{
  bool isError = false;
  if(detector == MWPC)
    {
      DEBUG("IpbusFeMux::do_load_thresholds.");
      map<int,IpbusFECard*>::iterator fec_it;
      for(fec_it = GetFECards().begin(); fec_it != GetFECards().end(); fec_it++)
	{
	  IpbusMwpcTdcCard* card = (IpbusMwpcTdcCard*)fec_it->second;
	  try
	    {
	      card->set_thresholds();
	    }
	  catch(Module::Error& e)
	    {
	      ShowError(e.what());
	      isError = true;
	    }
	}
    }
  else if(detector == GEMADC)
    {
      DEBUG("IpbusFeMux::do_load_thresholds.");
      map<int,IpbusFECard*>::iterator fec_it;
      for(fec_it = GetFECards().begin(); fec_it != GetFECards().end(); fec_it++)
	{
	  IpbusGemAdcCard* card = (IpbusGemAdcCard*)fec_it->second;
	  try
	    {
	      card->set_thresholds();
	    }
	  catch(Module::Error& e)
	    {
	      ShowError(e.what());
	      isError = true;
	    }
	}
    }
  else if(detector == CEDAR)
    {
      DEBUG("IpbusFeMux::do_load_thresholds.");
      map<int,IpbusFECard*>::iterator fec_it;
      for(fec_it = GetFECards().begin(); fec_it != GetFECards().end(); fec_it++)
	{
	  IpbusCedarTdcCard* card = (IpbusCedarTdcCard*)fec_it->second;
	  try
	    {
	      card->set_thresholds();
	    }
	  catch(Module::Error& e)
	    {
	      ShowError(e.what());
	      isError = true;
	    }
	}
    }
  return (isError == false);
}

bool IpbusFeMux::do_load_apv_registers()
{
  bool isError = false;
  if(detector == GEMADC)
    {
      DEBUG("IpbusFeMux::do_load_apv_registers.");
      map<int,IpbusFECard*>::iterator fec_it;
      for(fec_it = GetFECards().begin(); fec_it != GetFECards().end(); fec_it++)
	{
	  IpbusGemAdcCard* card = (IpbusGemAdcCard*)fec_it->second;
	  try
	    {
	      card->reset_APV();
	      card->set_apv_registers();
	    }
	  catch(Module::Error& e)
	    {
	      ShowError(e.what());
	      isError = true;
	    }
	}
    }
  else if(detector == MWPC || detector == SCI_FI)
    {
      if(!ConnectFECards())
	isError = true;
    }
  return (isError == false);
}

bool IpbusFeMux::do_set_sparse_mode()
{
  bool isError = false;
  if(detector == GEMADC)
    {
      DEBUG("IpbusFeMux::do_set_sparse_mode.");
      map<int,IpbusFECard*>::iterator fec_it;
      for(fec_it = GetFECards().begin(); fec_it != GetFECards().end(); fec_it++)
	{
          DEBUG("IpbusFeMux::do_set_sparse_mode. Programming card port = %d",
		fec_it->second->GetPort());
	  IpbusGemAdcCard* card = (IpbusGemAdcCard*)fec_it->second;
	  try
	    {
	      card->set_data_mode(1);
	    }
	  catch(Module::Error& e)
	    {
	      ShowError(e.what());
	      isError = true;
	    }
	}
    }
  return (isError == false);
}

bool IpbusFeMux::do_set_latch_mode()
{
  bool isError = false;
  if(detector == GEMADC)
    {
      DEBUG("IpbusFeMux::do_set_latch_mode.");
      map<int,IpbusFECard*>::iterator fec_it;
      for(fec_it = GetFECards().begin(); fec_it != GetFECards().end(); fec_it++)
	{
	  DEBUG("IpbusFeMux::do_set_latch_mode. Programming card port = %d",
		fec_it->second->GetPort());
	  IpbusGemAdcCard* card = (IpbusGemAdcCard*)fec_it->second;
	  try
	    {
	      card->set_data_mode(2);
	    }
	  catch(Module::Error& e)
	    {
	      ShowError(e.what());
	      isError = true;
	    }
	}
    }
  return (isError == false);
}

bool IpbusFeMux::do_load_pedestals()
{
  bool isError = false;
  if(detector == GEMADC)
    {
      DEBUG("IpbusFeMux::do_load_pedestals.");
      map<int,IpbusFECard*>::iterator fec_it;
      for(fec_it = GetFECards().begin(); fec_it != GetFECards().end(); fec_it++)
	{
	  IpbusGemAdcCard* card = (IpbusGemAdcCard*)fec_it->second;
	  try
	    {
	      card->set_pedestals();
	    }
	  catch(Module::Error& e)
	    {
	      ShowError(e.what());
	      isError = true;
	    }
	}
    }
  return (isError == false);
}

bool IpbusFeMux::do_reset_apv()
{
  bool isError = false;
  if(detector == GEMADC)
    {
      DEBUG("IpbusFeMux::do_reset_apv.");
      map<int,IpbusFECard*>::iterator fec_it;
      for(fec_it = GetFECards().begin(); fec_it != GetFECards().end(); fec_it++)
	{
	  IpbusGemAdcCard* card = (IpbusGemAdcCard*)fec_it->second;
	  try
	    {
	      card->reset_APV();
	    }
	  catch(Module::Error& e)
	    {
	      ShowError(e.what());
	      isError = true;
	    }
	}
    }
  return (isError == false);
}

bool IpbusFeMux::do_reset_adc()
{
  bool isError = false;
  if(detector == GEMADC)
    {
      DEBUG("IpbusFeMux::do_reset_adc.");
      map<int,IpbusFECard*>::iterator fec_it;
      for(fec_it = GetFECards().begin(); fec_it != GetFECards().end(); fec_it++)
	{
	  IpbusGemAdcCard* card = (IpbusGemAdcCard*)fec_it->second;
	  try
	    {
	      card->reset_module();
	      card->init_i2c_bus();
	      card->set_adc_registers();
	    }
	  catch(Module::Error& e)
	    {
	      ShowError(e.what());
	      isError = true;
	    }
	}
    }
  else if(detector == MWPC || detector == CEDAR || detector == SCI_FI)
    {
      isError = (!ConnectFECards() || !LoadFECards());
    }
  return (isError == false);
}

bool IpbusFeMux::do_verify_registers()
{
  bool isError = false;
  if(detector == GEMADC)
    {
      map<int,IpbusFECard*>::iterator fec_it;
      for(fec_it = GetFECards().begin(); fec_it != GetFECards().end(); fec_it++)
	{
	  IpbusGemAdcCard* card = (IpbusGemAdcCard*)fec_it->second;
	  try
	    {
	      if(card->VerifyAll())
		{
		  ShowError("ADC card verification error.");
		  isError = true;
		}
	    }
	  catch(Module::Error& e)
	    {
	      ShowError(e.what());
	      isError = true;
	    }
	}      
    }
  return (isError == false);
}

bool IpbusFeMux::do_show_status()
{
  bool isError = false;
  if(!check_status(IPBUS_MOD_HW_INERFACE))
    {
      dim["ModuleStatus"]  = MS_ERROR;
      ShowError("Module is not initialized yet. Do 'LOAD -A ...'");
      isError = true;
    }
  else
    {
      dim["ModuleStatus"]  = MS_INFO;
      uint32_t fw_version  = 0;
      uint32_t ucf_status  = 0;
      uint32_t tcs_status  = 0;
      uint32_t src_id      = 0;
      uint32_t port_en     = 0;
      uint32_t timeout     = 0;
      double   freq        = 0.0;

      ipbus_read("fw_version",          fw_version);
      ipbus_read("ucf_status",          ucf_status);
      ipbus_read("TCS",                 tcs_status);
      ipbus_read("static_out1.srcId",   src_id);
      ipbus_read("static_out2.portEn",  port_en);
      ipbus_read("static_out3.timeout", timeout);

      ValWord<uint32_t> clock_freq = GetHwInterface()->getNode("freq1.freq").read();
      GetHwInterface()->dispatch();
      freq = clock_freq/1e6;

      char status_msg[1024];

      sprintf(status_msg,
	      "SrcID           = %4u\n"
	      "Fimware Version = 0x%08x (%u)\n"
	      "UCF Status      = 0x%08x\n"
	      "Ports Enabled   = 0x%08x\n"
	      "TCS Status      = 0x%08x\n"
	      "Clock Frequency = %.3lf MHz\n"
	      "Timeout         = 0x%08x (%u)\n",
	      src_id, fw_version, fw_version, ucf_status, port_en, tcs_status, 
	      freq, timeout, timeout);
      
      dim["WarningString"] = status_msg;
    }
  return (isError == false);
}
