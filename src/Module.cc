#include <iostream>
#include <string>

#include "Module.h"
#include "wait.h"
#include "int2string.h"

using namespace std;

#define DEBUG(x) cout<<x<<endl;

// This method searches a MyDimService in the ServiceMap and
// returns it (if found) to update its value. If no service with
// the given name exists, a new one is created; this needs
// information on the data type being served, so you have to prepend
// the name with "[ifdc]:" when first trying to access it.
// The return value is a reference to MyDimService, so that you can go
// on to update the value with MyDimService::operator= like
//
//   service_map["buh"]=12;
//
// This will notify all clients of the change.
MyDimService& ServiceMap::operator[](string n)
{
  if(!count(n))
    {
      // creation of the object requires type information
      // in the form of a name prefix, e.g. "i:blub" for an int
      
      // make type letter lowercase
      char type=n[0]|0x20;
      char mode=n[1];
      n=n.substr(2);
      char *name;
      string name_;
      if(mode == ':')
	{
	  name_ = prefix + n;
	}
      else if(mode == '=')
	{
	  name_ = n;
	}
      else
	{
	  throw no_type();
	}
      name=const_cast<char*>(name_.c_str());
      switch(type)
	{
	case 'i':
	  insert(make_pair(n,new MyDimService(name,(int)0)));
	  break;
	case 'f':
	  insert(make_pair(n,new MyDimService(name,(float)0)));
	  break;
	case 'd':
	  insert(make_pair(n,new MyDimService(name,(double)0)));
	  break;
	case 'c':
	  insert(make_pair(n,new MyDimService(name,(char*)"")));
	  break;
	default:
	  throw no_type();
	}
    }
  return *(find(n)->second);
}

MyDimComm::MyDimComm(string n,void(*f)(Module*,int),Module*m)
  : DimCommand((char*)n.c_str(),(char*)"I"),func_i(f),func_c(0),module(m),name(n)
{
  mutex=new pthread_mutex_t;
  pthread_mutex_init(mutex,NULL);
  queue_com=false;
  check_db=false;
}

MyDimComm::MyDimComm(string n,void(*f)(Module*,const char*),Module*m)
  : DimCommand((char*)n.c_str(),(char*)"C"),func_i(0),func_c(f),module(m),name(n)
{
  mutex=new pthread_mutex_t;
  pthread_mutex_init(mutex,NULL);
  queue_com=false;
  check_db=false;
}

MyDimComm::~MyDimComm()
{
  while(pthread_mutex_destroy(mutex))
    {
      mywait(100000);
    }
  delete mutex;
}

struct wrapper_param_t
{
  MyDimComm *comm;
  int i;
  char *c;
};

// This wrapper function can be used for thread creation
// It is declared a friend of MyDimComm, so it can access
// the contents of the object passed as (void*)c
void* MyDimComm::wrapper(void* param)
{
  wrapper_param_t *p = (wrapper_param_t*)param;
  MyDimComm *comm=p->comm;
  Module *module=comm->module;
  void (*func_i)(Module*,int)=comm->func_i;
  void (*func_c)(Module*,const char*)=comm->func_c;
  int i=p->i;
  const char *c=p->c;
  free(p);

  if(comm->queue_com)
    {
      pthread_mutex_lock(comm->mutex);
    }
  else if(pthread_mutex_trylock(comm->mutex))
    {
      cout<<"DIM command "<<comm->name
	  <<" invoked when already running, ignored"<<endl;
      return NULL;
    }
  // only proceed if there currently is no other thread working on this command
  // create a new thread that will service this command
  // and detach it as nobody will be interested
  if (func_i)
    {
      DEBUG("** executing "<<comm->name<<' '<<myhex(i)<<" at "<<mytime());
      if(comm->check_db)
	{
	  module->check_db();
	}
      func_i(module,i);
    }
  else
    {
      DEBUG("** executing "<<comm->name<<' '<<c<<" at "<<mytime());
      if(comm->check_db)
	{
	  module->check_db();
	}
      func_c(module,c);
    }
  pthread_mutex_unlock(comm->mutex);
  free((void*)c);
  return NULL;
}

// This command handler creates a new thread via the wrapper function.
// To prevent two simultanous threads for the command a mutex is held
// until the return of the wrapper.
void MyDimComm::commandHandler()
{
  pthread_t t;

  DEBUG("** command "<<name<<" has arrived at "<<mytime());

  // somehow the getXXX() call _has_ to be made by the dim tcpip thread
  // the reason is not clear, but else I got SIGSEGV
  wrapper_param_t *p = new wrapper_param_t;
  p->comm = this;
  p->c = NULL;
  if (func_i)
    {
      if (getSize() > 3)
	{
	  p->i=getInt();
	}
      else
	{
	  p->i=0;
	}
    }
  else
    {
      p->c = (char*)malloc(getSize() + 1);
      p->c[getSize()] = 0;
      memcpy(p->c, getData(), getSize());
    }

  pthread_create(&t,NULL,wrapper,p);
  pthread_detach(t);
}

// The constructor of Module mainly initializes the DIM services and
// commands, reads the database, publishes initial values for some of
// the services and checks the status of its physical counterpart.
Module::Module(int sourceID,string version)
  : module_source_id(sourceID), module_db(sourceID,version),dim(sourceID)
{
  dim["i:ModuleStatus"]=MS_UNKNOWN;
  dim["i:ModuleType"]=MT_UNKNOWN;
  dim["c:Configuration"]="";
  dim["c:ErrorString"]="";
  dim["i:PortFlags"]=module_db.portflags;
  dim["c:RobSlot"]=module_db.rob.name;
  dim["i:Programmed"]=0;

  char p[10];
  snprintf(p,10,"%d/",sourceID);
  string pre(p);
  dimComm.push_back(new MyDimComm(pre+"Initialize",Initialize_,this));
  dimComm.push_back(new MyDimComm(pre+"Reset",Reset_,this));
  dimComm.push_back(new MyDimComm(pre+"ReadStatus",ReadStatus_,this));
}

Module::Module(int sourceID, string dim_prefix, string version)
  : module_source_id(sourceID), module_db(sourceID,version), dim(dim_prefix)
{
  dim["i:ModuleStatus"]=MS_UNKNOWN;
  dim["i:ModuleType"]=MT_UNKNOWN;
  dim["c:Configuration"]="";
  dim["c:ErrorString"]="";
  dim["i:PortFlags"]=module_db.portflags;
  dim["c:RobSlot"]=module_db.rob.name;
  dim["i:Programmed"]=0;

  char p[10];
  snprintf(p,10,"%d/",sourceID);
  string pre(p);
  dimComm.push_back(new MyDimComm(pre+"Initialize",Initialize_,this));
  dimComm.push_back(new MyDimComm(pre+"Reset",Reset_,this));
  dimComm.push_back(new MyDimComm(pre+"ReadStatus",ReadStatus_,this));
}

// In the destructor we have to take care of the MyDimComm objects
// as they can only be stored via pointers in a list (copy constructor
// does not work).
Module::~Module()
{
  list<MyDimComm*>::iterator it;
  for(it=dimComm.begin();it!=dimComm.end();++it)
    delete *it;
}

// Provide some standard (non-) reactions to the published DIM commands
void Module::Initialize(int i)
{
  cout<<module_source_id<<"::Initialize "<<i<<endl;
}

void Module::Reset(int i)
{
  cout<<module_source_id<<"::Reset "<<i<<endl;
}

void Module::ReadStatus(int i)
{
  cout<<module_source_id<<"::ReadStatus "<<i<<endl;
}

void Module::Control(const char*s)
{
  cout<<module_name<<"::Control "<<s<<endl;
}

// These wrapper methods are necessary as member pointers to virtual
// methods cannot be called afterwards. Maybe this is a compiler
// problem, but I don't care as long as the workaround is okay.
void Module::Initialize_(Module*m,int i)
{
  m->Initialize(i);
}

void Module::Reset_(Module*m,int i)
{
  m->Reset(i);
}

void Module::ReadStatus_(Module*m,int i)
{
  m->ReadStatus(i);
}

void Module::Control_(Module*m,const char*s)
{
  m->Control(s);
}
