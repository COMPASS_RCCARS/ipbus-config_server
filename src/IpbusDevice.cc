#include <stdio.h>

#include "IpbusDevice.h"
#include "Module.h"
#include "main.h"

using namespace std;
using namespace uhal;

IpbusDevice::IpbusDevice() : ipbus_manager(NULL), hw_interface(NULL), hw_mutex(NULL), isDummyModule(false)
{
  hw_mutex = new pthread_mutex_t;
  if(!hw_mutex || pthread_mutex_init(hw_mutex, NULL) != 0)
    {
      throw Module::IpbusError("IpbusDevice error (cannot create mutex).");
    }
  isSlaveDevice = false;
}

IpbusDevice::~IpbusDevice()
{
  if(hw_mutex)
    {
      pthread_mutex_destroy(hw_mutex);
      delete hw_mutex;
    }
  delete hw_interface;
  if(!isSlaveDevice)
    delete ipbus_manager;
}

bool IpbusDevice::Init(const char* connection_file_name, const char* interface_name)
{
  if(hw_interface)
    {
      delete hw_interface;
      hw_interface = NULL;
    }
  if(!isSlaveDevice && ipbus_manager)
    {
      delete ipbus_manager;
    }
  ipbus_manager = NULL;
  isDummyModule = true;
  isSlaveDevice = false;

  uhal::disableLogging();
  ipbus_manager = new ConnectionManager(connection_file_name);
  isSlaveDevice = false;
  if(ipbus_manager)
    {
      try
	{
	  hw_interface = new HwInterface(ipbus_manager->getDevice(interface_name));
	}
      catch(std::exception& e)
	{
	  printf("%s\n", e.what());
	}
      catch(...)
	{
	  printf("Unknown exception.\n");
	}
      if(!hw_interface)
	return false;
    }
  else
    {
      return false;
    }
  isDummyModule = false;
  return true;
}

bool IpbusDevice::Init(uhal::ConnectionManager* manager, const char* interface_name)
{
  if(hw_interface)
    {
      delete hw_interface;
    }
  isSlaveDevice = true;
  // if(!isSlaveDevice && ipbus_manager)
  //   {
  //     delete ipbus_manager;
  //   }
  hw_interface  = NULL;
  isDummyModule = true;
  ipbus_manager = manager;

  if(ipbus_manager)
    {
      hw_interface = new HwInterface(ipbus_manager->getDevice(interface_name));
      if(!hw_interface)
	return false;
    }
  else
    {
      return false;
    }
  isDummyModule = false;
  return true;
}

void IpbusDevice::ipbus_write(const char* node, const uint32_t value, bool verify)
{
  DEBUG("IpbusDevice::ipbus_write(\"%s\",0x%04X);", node, value);
  char errMsg[1024];

  if(isDummyModule)
    return;

  if(!hw_interface || !ipbus_manager)
    throw Module::IpbusError("IPBUS Device is not initialized.");

  pthread_mutex_lock(hw_mutex);

  try
    {
      bool ok = true;
      const int maxret = 5;
      int ret = 0;
      do
	{
	  ret++;
	  hw_interface->getNode(node).write(value);
	  hw_interface->dispatch();
	  if(verify)
	    {
	      ValWord<uint32_t> readback = hw_interface->getNode(node).read();
	      hw_interface->dispatch();
	      if(readback.value() != value)
		{
		  ok = false;
		}
	      else
		{
		  ok = true;
		}
	    }
	  else
	    ok = true;
	} while(!ok && ret < maxret);
      if(!ok)
	{
	  throw Module::IpbusError("IPBUS verification error after writing value to register.");
	}
      pthread_mutex_unlock(hw_mutex);
      return;
    }
  catch(std::exception& e)
    {
      sprintf(errMsg,"ERROR: IpbusDevice::ipbus_write: %s\n", e.what());
    }
  catch(Module::Error& e)
    {
      sprintf(errMsg,"ERROR: IpbusDevice::ipbus_write: %s\n", e.what().c_str());
    }
  catch(...)
    {
      sprintf(errMsg,"ERROR: IpbusDevice::ipbus_write: unknown error\n");
    }

  pthread_mutex_unlock(hw_mutex);
  throw Module::IpbusError(errMsg);
}

void IpbusDevice::ipbus_read(const char* node, uint32_t& data)
{
  if(isDummyModule)
    return;

  char errMsg[1024];

  if(!hw_interface || !ipbus_manager)
    throw Module::IpbusError("IPBUS Device is not initialized.");

  pthread_mutex_lock(hw_mutex);

  try
    {
      ValWord<uint32_t> value = hw_interface->getNode(node).read();
      hw_interface->dispatch();
      data = value.value();

      pthread_mutex_unlock(hw_mutex);
      return;
    }
  catch(Module::Error& e)
    {
      sprintf(errMsg,"IpbusDevice::ipbus_read 0: node '%s': %s", node, e.what().c_str());
    }
  catch(std::exception& e)
    {
      sprintf(errMsg,"IpbusDevice::ipbus_read 1: node '%s': %s", node, e.what());
    }
  catch(...)
    {
      sprintf(errMsg,"IpbusDevice::ipbus_read 2: node '%s': unknown error.", node);
    }

  pthread_mutex_unlock(hw_mutex);
  throw Module::IpbusError(errMsg);
}

void IpbusDevice::ipbus_write_block(const char* node, const std::vector<uint32_t>& block)
{
  char errMsg[256];
  if(isDummyModule)
    return;

  if(!hw_interface || !ipbus_manager)
    throw Module::IpbusError("IPBUS Device is not initialized.");

  pthread_mutex_lock(hw_mutex);

  try
    {
      hw_interface->getNode(node).writeBlock(block);
      hw_interface->dispatch();

      pthread_mutex_unlock(hw_mutex);
      return;
    }
  catch(Module::Error& e)
    {
      sprintf(errMsg,"ERROR: IpbusDevice::ipbus_write_block: %s\n", e.what().c_str());
    }
  catch(std::exception& e)
    {
      sprintf(errMsg,"ERROR: IpbusDevice::ipbus_write_block: %s\n", e.what());
    }
  catch(...)
    {
      sprintf(errMsg,"ERROR: IpbusDevice::ipbus_write_block: unknown error\n");
    }

  pthread_mutex_unlock(hw_mutex);
  throw Module::IpbusError(errMsg);
}

void IpbusDevice::ipbus_read_block(const char* node, std::vector<uint32_t>& data, uint32_t size)
{
  char errMsg[256];
  if(isDummyModule)
    return;

  if(!hw_interface || !ipbus_manager)
    throw Module::IpbusError("IPBUS Device is not initialized.");

  pthread_mutex_lock(hw_mutex);

  try
    {
      ValVector<uint32_t> value = hw_interface->getNode(node).readBlock(size);
      hw_interface->dispatch();
      data.resize(size);
      for(uint32_t i = 0; i < size; i++)
	data[i] = value[i];

      pthread_mutex_unlock(hw_mutex);
      return;
    }
  catch(Module::Error& e)
    {
      sprintf(errMsg,"ERROR: IpbusModule::ipbus_read_block: %s\n", e.what().c_str());
    }
  catch(std::exception& e)
    {
      sprintf(errMsg,"ERROR: IpbusModule::ipbus_read: %s\n", e.what());
    }
  catch(...)
    {
      sprintf(errMsg,"ERROR: IpbusModule::ipbus_read: unknown error\n");
    }

  pthread_mutex_unlock(hw_mutex);
  throw Module::IpbusError(errMsg);
}

