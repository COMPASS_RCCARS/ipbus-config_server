#include <stdio.h>
#include <stdint.h>

#include <string>

#include "main.h"
#include "int2string.h"
#include "IpbusTcsController.h"
#include "IpbusTcsFpga.h"

using namespace std;

IpbusTcsFpga::IpbusTcsFpga(int sourceID, std::string version) : IpbusModule(sourceID,version)
{
  opt = NULL;
  dim["ModuleType"] = MT_A7_TCS;
  module_name="A7 TCS Controller ("+mydec(sourceID)+')';
  if(!check_status(IPBUS_MOD_ERROR_STATE) && check_status(IPBUS_MOD_CREATED) && check_db())
    {
      dim["i:TCS_RST"]      = 0;
      dim["i:CLK_SRC"]      = 0;
      dim["i:SPS_SRC"]      = 0;
      dim["i:TRG_SRC"]      = 0;
      dim["i:STATIC_IN_1"]  = 0;
      dim["i:STATIC_IN_2"]  = 0;
      dim["i:STATIC_IN_3"]  = 0;
      dim["i:SPILL_LENGTH"] = 0;
      dim["i:PAUSE_LENGTH"] = 0;
      dim["i:TRG_PERIOD"]   = 0;
      dim["c:FE_STATUS"]    = "";
      // DIM services for compatibility with old TCS Controllers:
      dimComm.push_back(new MyDimComm("TCS/"+tcs_name+"/ReadStatus", ReadStatus_, this));
      MyDimComm *command = new MyDimComm("TCS/"+tcs_name+"/Control", Control_, this);
      command->setQueueMode(true);
      dimComm.push_back(command);
      list<MyDimComm*>::iterator it;
      // set all DIM commands (Initialize/ReadStatus/Reset) to automatically
      // update/check the DB
      for(it=dimComm.begin(); it!=dimComm.end(); ++it)
	{
	  (*it)->setCheckMode(false);
	}
      printf("%s is created.\n", module_name.c_str());
    }
}

IpbusTcsFpga::~IpbusTcsFpga()
{
  DEBUG("%s is destroyed.", module_name.c_str());
  
  map<int,IpbusFECard*>::iterator fec_it;
  for(fec_it = GetFECards().begin(); fec_it != GetFECards().end(); fec_it++)
    delete fec_it->second;
}

bool IpbusTcsFpga::check_db()
{
  if(!IpbusModule::check_db())
    return false;
  //if(!set_module_config())
  //  return false;
  list<DatabaseEntryFrontend>::iterator it;
  try
    {
      if(!module_db.has_detector)
	{
	  throw DBError(this, "missing 'Detector' info in FEDB for " + module_name); 
	}
      for(it=module_db.frontends.begin(); it!=module_db.frontends.end(); ++it)
	{
	  if(!it->has_state)
	    throw DBError(this,"missing 'State' for frontend "+it->detector);
	  if(!it->has_cable)
	    throw DBError(this,"missing FE card name in 'CableName' for frontend " + it->detector);
	  if(!it->has_format_id)
	    throw DBError(this,"missing 'Format_ID' for frontend "+it->detector);
	  if(!it->has_calib)
	    throw DBError(this,"missing 'Calibration_Info' for frontend "+it->detector);
	  if(!it->has_config)
	    throw DBError(this,"missing 'Configuration' for frontend "+it->detector);
	}
      DEBUG("IpbusTcsFpga::check_db: %s:  OK!", module_name.c_str());
    }
  catch(DatabaseEntry::Error& e)
    {
      ShowError("database error: " + e.what());
      set_status(IPBUS_MOD_ERROR_STATE);
      return false;
    }
  catch(DBError& e)
    {
      ShowError("DB error: " + e.what());
      set_status(IPBUS_MOD_ERROR_STATE);
      return false;
    }
  catch(std::exception& e)
    {
      ShowError(std::string("std error: ") + e.what());
      set_status(IPBUS_MOD_ERROR_STATE);
      return false;
    }
  return true;
}

bool IpbusTcsFpga::set_module_config()
{
  IpbusModule::set_module_config();
  map<string,string>& config = GetConfig();
  map<string,string>::iterator it = config.find("NAME");
  if(it != config.end())
    {
      tcs_name = config["NAME"];
      config.erase(it);
    }
  else
    {
      ShowError(" the TCS name parameter is not found in 'Configuration'");
      set_status(IPBUS_MOD_ERROR_STATE);
      return false;
    }

  for(it = config.begin(); it != config.end(); it++)
    {
      DEBUG("CONFIG: %s=%s", it->first.c_str(), it->second.c_str());
      ipbus_write(it->first.c_str(),atoi(it->second.c_str()));
    }

  if(opt) delete opt;

  opt = new tcsOpt("tcs.config", tcs_name);

  if(!opt || !opt->parse())
    {
      return false;
    }

  return true;
}

void IpbusTcsFpga::InitModule(int todo)
{
  if(check_status(IPBUS_MOD_ERROR_STATE) || !check_status(IPBUS_MOD_CREATED))
    {
      throw Error("Attempt to use not created IPBUS module.");
    }

  // Do commands. Note: the order matters

  dim["ModuleStatus"]  = MS_INITIALIZE;
  dim["ErrorString"]   = "";
  dim["WarningString"] = "";
  dim["FE_STATUS"]     = "";
  dim["Programmed"]    =  0;

  if((todo & GS_DO_ALL) != 0)
    {
      do_all();
    }
  else // todo all procedures separately
    {
      while(todo)
	{
	  if((todo & GS_DO_GESICA) != 0)
	    {
	      todo &= ~GS_DO_GESICA;
	      do_init_fpga();
	    }
	  else
	    todo = 0;
	}
    }

  ReadStatus(0);
}

void IpbusTcsFpga::Reset(int i)
{
  DEBUG("IpbusTcsFpga::Reset(%d).", i);
  ipbus_write("SET_REG.CLK_SRC",0);
  ipbus_write("SET_REG.SPS_SRC",0);
  ipbus_write("SET_REG.TRG_SRC",0);
  ipbus_write("SET_REG.TCS_RST",1);
  ipbus_write("SET_REG.TCS_RST",0);
  
  map<int,IpbusFECard*>::iterator fec_it;
  for(fec_it = GetFECards().begin(); fec_it != GetFECards().end(); fec_it++)
    {
      IpbusTcsController* cntrl = dynamic_cast<IpbusTcsController*>(fec_it->second);
      if(cntrl)
	{
	  cntrl->Reset(false);
	}
      ReadStatus(i);
    }
}

void IpbusTcsFpga::ReadStatus(int i)
{
  DEBUG("IpbusTcsFpga::ReadStatus(%d).", i);
  if(check_status(IPBUS_MOD_ERROR_STATE) || !check_status(IPBUS_MOD_CREATED))
    throw IpbusError("IPBUS module is in error state.");
  uint32_t TCS_RST      = 0;
  uint32_t CLK_SRC      = 0;
  uint32_t SPS_SRC      = 0;
  uint32_t TRG_SRC      = 0;
  uint32_t STATIC_IN_1  = 0;
  uint32_t STATIC_IN_2  = 0;
  uint32_t STATIC_IN_3  = 0;
  uint32_t SPILL_LENGTH = 0;
  uint32_t PAUSE_LENGTH = 0;
  uint32_t TRG_PERIOD   = 0;
  
  ipbus_read("SET_REG.TCS_RST",TCS_RST);
  ipbus_read("SET_REG.CLK_SRC",CLK_SRC);
  ipbus_read("SET_REG.SPS_SRC",SPS_SRC);
  ipbus_read("SET_REG.TRG_SRC",TRG_SRC);
  ipbus_read("STATIC_IN_1",STATIC_IN_1);
  ipbus_read("STATIC_IN_2",STATIC_IN_2);
  ipbus_read("STATIC_IN_3",STATIC_IN_3);
  ipbus_read("SPILL_LENGTH",SPILL_LENGTH);
  ipbus_read("PAUSE_LENGTH",PAUSE_LENGTH);
  ipbus_read("TRG_PERIOD",TRG_PERIOD);
  GetServiceMap()["TCS_RST"]      = (int)TCS_RST;
  GetServiceMap()["CLK_SRC"]      = (int)CLK_SRC;
  GetServiceMap()["SPS_SRC"]      = (int)SPS_SRC;
  GetServiceMap()["TRG_SRC"]      = (int)TRG_SRC;
  GetServiceMap()["STATIC_IN_1"]  = (int)STATIC_IN_1;
  GetServiceMap()["STATIC_IN_2"]  = (int)STATIC_IN_2;
  GetServiceMap()["STATIC_IN_3"]  = (int)STATIC_IN_3;
  GetServiceMap()["SPILL_LENGTH"] = (int)SPILL_LENGTH;
  GetServiceMap()["PAUSE_LENGTH"] = (int)PAUSE_LENGTH;
  GetServiceMap()["TRG_PERIOD"]   = (int)TRG_PERIOD;
  map<int,IpbusFECard*>::iterator fec_it;
  string fe_status;
  for(fec_it = GetFECards().begin(); fec_it != GetFECards().end(); fec_it++)
    {
      int port = fec_it->second->GetPort();
      if(port == i)
	fec_it->second->ReadStatus(fe_status);
    }
  GetServiceMap()["FE_STATUS"] = fe_status;
}

void IpbusTcsFpga::Control(const char* cmd)
{
  DEBUG("IpbusTcsFpga::Control(\"%s\")", cmd);
  if(check_status(IPBUS_MOD_ERROR_STATE) || !check_status(IPBUS_MOD_CREATED))
    {
      throw IpbusError("IPBUS module is in error state.");
    }
  if(strncmp(cmd,"ReadStatus",10) == 0)
    {
      int daqnum = 0;
      if(sscanf(cmd,"ReadStatus %d", &daqnum) == 1 && daqnum < 8 && daqnum >= 0)
	ReadStatus(daqnum);
      return;
    }
  if(strncmp(cmd,"SetFpgaReg",10) == 0)
    {
      string p = &cmd[10];
      map<string, mynumber<int> > params = parseSettings(p, mynumber<int>("0"));
      map<string, mynumber<int> >::iterator it;
      try
	{
	  for(it = params.begin(); it != params.end(); it++)
	    {
	      DEBUG("Write register: %s=%d", it->first.c_str(),it->second);
	      ipbus_write(it->first.c_str(),(uint32_t)it->second);
	    }
	}
      catch(Module::IpbusError& e)
	{
	  GetServiceMap()["ErrorString"] = e.what();
	  DEBUG("TCS SetFpgaReg error: %s", e.what().c_str());
	}
      return;
    }
  map<int,IpbusFECard*>::iterator fec_it;
  string fe_status;
  try
    {
      for(fec_it = GetFECards().begin(); fec_it != GetFECards().end(); fec_it++)
	{
	  IpbusTcsController* cntrl = dynamic_cast<IpbusTcsController*>(fec_it->second);
	  if(cntrl)
	    cntrl->Control(cmd);
	}
    }
  catch(Module::IpbusError& e)
    {
      GetServiceMap()["ErrorString"] = e.what();
      DEBUG("TCS Control error: %s", e.what().c_str());
    }
}

uint32_t IpbusTcsFpga::GetFirmwareID()
{
  DEBUG("IpbusTcsFpga::GetFirmwareID");
  if(check_status(IPBUS_MOD_ERROR_STATE) || !check_status(IPBUS_MOD_CREATED))
    throw IpbusError("IPBUS module is in error state.");

  uint32_t fw_id = 0xBAD;

  ipbus_read("FW_ID", fw_id);

  return fw_id;
}

void IpbusTcsFpga::do_all()
{
  DEBUG("IpbusTcsFpga::do_all");
  do_init_fpga();
  if(opt->readStatusFile(get_tcs_name()))
    {
      map<int,IpbusFECard*>::iterator fec_it;
      string fe_status;
      for(fec_it = GetFECards().begin(); fec_it != GetFECards().end(); fec_it++)
	{
	  IpbusTcsController* tcs = dynamic_cast<IpbusTcsController*>(fec_it->second);
	  if(tcs)
	    tcs->Configure();
	}
    }
  else
    ShowError("Status file error in DB.");
}

void IpbusTcsFpga::do_init_fpga()
{
  DEBUG("IpbusTcsFpga::do_init_fpga");
  // Reset(0);
  IpbusTcsController* tcs = NULL;
  map<int,IpbusFECard*>::iterator fec_it;
  for(fec_it = GetFECards().begin(); fec_it != GetFECards().end(); fec_it++)
    {
      tcs = dynamic_cast<IpbusTcsController*>(fec_it->second);
    }
  list<DatabaseEntryFrontend>::iterator it;
  for(it=module_db.frontends.begin(); it!=module_db.frontends.end(); ++it)
    {
      if(it->state == 1)
	{
	  GetFECards()[it->port] = tcs ? tcs : new IpbusTcsController(it->cable.c_str(), this);
	}
    }
}

 bool IpbusTcsFpga::UpdateOptions()
 {
   if(!set_module_config())
     {
       return false;
     }
   return true;
}
