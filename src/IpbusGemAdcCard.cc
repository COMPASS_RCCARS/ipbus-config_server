#include <string.h>
#include <stdarg.h>
#include <stdint.h>

#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <sstream>
#include <fstream>

#include "IpbusFeMux.h"
#include "IpbusGemAdcCard.h"
#include "int2string.h"
#include "main.h"
#include "wait.h"
#include "gs_todo.h"

using namespace std;
using namespace uhal;

static void decode_gid(uint32_t gid, uint8_t& bus, uint8_t& addr)
{
  gid >>= 1;
  bus  = (uint8_t)((gid & 0x20) >> 5);
  addr = (uint8_t)((gid & 0x1F) | 0x20);
  // DEBUG("*** gid = 0x%02x, bus = 0x%02x, addr = 0x%02x", gid, (unsigned)bus, (unsigned)addr);
}

IpbusGemAdcCard::IpbusGemAdcCard(IpbusFECard::FECardType cardType, const char* name, IpbusFeMux* parent, int _port) :
  IpbusFECard(cardType,name,parent,_port)
{
  DEBUG("Create GEM ADC Card type %d, IPBUS name %s, in port %d.", cardType, name, _port);
  fw_id = 0;
  ipbus_read("REVISION.REVISION", fw_id);
  DEBUG("GEMADC card firmware ID: %u", fw_id);
  uint32_t test = GetFwIdFromDB();
  DEBUG("GEMADC card firmware ID from DB: %u", test);
  if(fw_id != test)
    {
      throw Module::DBError("FW ID is not match for SrcID " + mydec(parent->getSourceID()) + " port " + mydec(GetPort()));
    }
  pedestals_set = false;
}

IpbusGemAdcCard::~IpbusGemAdcCard()
{
  DEBUG("Destroy GEM ADC Card, port %d.", GetPort());
}

uint32_t IpbusGemAdcCard::GetFwIdFromDB()
{
  int id = -1;
  list<DatabaseEntryFrontend>& module_db = GetModule()->GetFrontendDB();
  list<DatabaseEntryFrontend>::iterator it;
  for(it=module_db.begin(); it!=module_db.end(); ++it)
    {
      if(it->port == GetPort() && it->geographic_id < 2 && it->state == 1)
	{
	  map<string, string > m = parseSettings(it->config, string());
	  sscanf(m["version"].c_str(),"%x",&id);
	  break;
	} 
    }
  if(id == -1)
    return 0;
  return (uint32_t)id;
}

void IpbusGemAdcCard::reset_module()
{
  DEBUG("IpbusGemAdcCard::reset_module");
  ResetVerify();
  ipbus_write("CONFIG3.PWR_UP",0);
  GetModule()->Sleep(200000);
  // Enable power on ADC board:
  ipbus_write("CONFIG3.PWR_UP",1);
  GetModule()->Sleep(200000);
  // APV Reset:
  ipbus_write("CONFIG3.APV_RESET",1);
  ipbus_write("CONFIG3.APV_RESET",0);
  ipbus_write("CONFIG3.APV_RESET",1);
  ipbus_write("CONFIG3.APV_RESET",0);
  GetModule()->Sleep(200000);
  ipbus_write("APV_TRG_MODE.ILA_SEL",12);
  // Enable automatic adc receiver phase adjustment:
  ipbus_write("CONFIG3.ENABLE_PHASE_DET",1);
  // configure ADC and DACs:
  ipbus_write("CONFIG3.ADC_CONFIG_REG0",0);
  ipbus_write("CONFIG3.ADC_CONFIG_REG1",0);
  ipbus_write("CONFIG3.ADC_POWERDOWN_MASK",0);
  ipbus_write("CONFIG3.ADC_CUSTOM_PATTERN",0);
  ipbus_write("DAC_VALUE.VALUE0",64000);
  ipbus_write("DAC_VALUE.VALUE1",64000);
  ipbus_write("RST_REG.ADC_WRITE",1);
  GetModule()->Sleep(100000);
  ipbus_write("RST_REG.ADC_WRITE",0);
  // SYNC receiver to ADCs
  // ipbus_write("RST_REG.ADC_RECEIVER_RESET",1);
}

void IpbusGemAdcCard::init_i2c_bus()
{
  DEBUG("IpbusGemAdcCard::init_i2c_bus");
  list<DatabaseEntryFrontend>& module_db = GetModule()->GetFrontendDB();
  list<DatabaseEntryFrontend>::iterator it;
  for(it=module_db.begin(); it!=module_db.end(); ++it)
    {
      if(it->port == GetPort() && it->geographic_id < 2 && it->state == 1)
	{
	  i2cInit(DEFAULT_CLOCK_PRESCALE);
	}
    }
}

void IpbusGemAdcCard::i2cInit(int clockPrescale)
{
  DEBUG("I2C clock prescale = 0x%x.", clockPrescale);
  for(int bus = 0; bus < 2; bus++)
    {
      char cnode[256];
      sprintf(cnode, "I2C_APV%d", bus);
      string node = cnode;
      uint32_t sr = 0;
      ipbus_read(node+".SR", sr); 
      //ipbus_read(node+".SR", sr); // fvn: is it really necessary?
      
      DEBUG("I2C Status at initialization = 0x%x.", sr);
      ipbus_write(node+".CTR", 0);
      ipbus_write(node+".PRERLO", clockPrescale & 0xFF);
      ipbus_write(node+".PRERHI", (clockPrescale >> 8) & 0xFF);

      //enable master, disable interrupts
      ipbus_write(node+".CTR", 0x80);
    }
}

bool IpbusGemAdcCard::i2cWriteByte(string node, uint8_t data, uint8_t cr)
{
  //DEBUG("i2cWriteByte %s, 0x%x, 0x%x", node.c_str(), uint32_t(data), uint32_t(cr));
  const int LOOPS_MAX = 10;
  ipbus_write(node+".TXR", data);
  ipbus_write(node+".CR", cr);

  uint32_t sr = 0;
  ipbus_read(node+".SR", sr);

  int loopCnt = 0;
  while((sr&0x2) != 0 && loopCnt < LOOPS_MAX)
    {
      usleep(1000);
      loopCnt++;
      ipbus_read(node+".SR", sr);
    }
  if(loopCnt >= LOOPS_MAX)
    return false;
  ipbus_read(node+".SR", sr);
  if((sr&0x80) != 0)
    {
      char msg[256];
      sprintf(msg,"Slave does not acknowledge write. SR = 0x%x, node: %s",
	      sr, node.c_str());
      GetModule()->ShowError(msg);
      return false;
    }
  return true;
}

void IpbusGemAdcCard::i2cReadByte(string node, uint8_t& data)
{
  const int LOOPS_MAX = 10;
  ipbus_write(node+".CR", 0x68);
  usleep(1000);
  uint32_t sr = 0;
  ipbus_read(node+".SR", sr);

  int loopCnt = 0;
  while((sr&0x2) != 0 && loopCnt < LOOPS_MAX)
    {
      usleep(1000);
      loopCnt++;
      ipbus_read(node+".SR", sr);
      DEBUG("i2cReadByte: SR = 0x%x.", sr);      
    }
  
  ipbus_read(node+".SR", sr);

  uint32_t readword = 0;
  ipbus_read(node+".RXR", readword);
  data = (uint8_t)(readword&0xFF);
}

void IpbusGemAdcCard::i2cApvWritePedestals(int ped[2048], int sigma[2048], bool verify)
{
  try
    {
      memcpy(ped_values,ped,sizeof(ped_values));
      memcpy(sigma_values,sigma,sizeof(sigma_values));
      std::vector<uint32_t> values;
      values.resize(128);
      ipbus_write("PED_BRAM_ADDR",0);
      for (int i = 0; i < MAX_NUM_APV_PER_BUS; i++)
	{
	  for(int j = 0; j < 128; j++)
	    {
	      // the pedestals have to be loaded in the same order as the ADC
	      // processes the data, which is the order coming from the
	      // multiplexer in the APV
	      int channel_after_mux=16*(j%8)+4*(j/8)-15*(j/32);
	      //channel_after_mux |= i<<7;
	      values[channel_after_mux] = (uint32_t)ped[i*128+j];
	    }
	  for(int j = 0; j < 128; j++)
	    {
	      ped[i*128+j] = (int)values[j];
	    }
	  DEBUG("Try to write ped block %d...", i);
	  ipbus_write_block("PED_BRAM_DATA",values);
	  DEBUG("Block %d is written successfully.", i);
	}
      ipbus_write("THR_BRAM_ADDR",0);
      for(int i = 0; i < MAX_NUM_APV_PER_BUS; i++)
	{
	  for(int j = 0; j < 128; j++)
	    {
	      int channel_after_mux=16*(j%8)+4*(j/8)-15*(j/32);
	      values[channel_after_mux] = (uint32_t)sigma[i*128+j];
	    }
	  for(int j = 0; j < 128; j++)
	    {
	      sigma[i*128+j] = (int)values[j];
	    }
	  ipbus_write_block("THR_BRAM_DATA",values);
	}
      if(verify)
	{
	  ipbus_write("PED_BRAM_ADDR",0);
	  for(int i = 0; i < MAX_NUM_APV_PER_BUS; i++)
	    {
	      std::vector<uint32_t> readback;
	      readback.resize(128);
	      ipbus_read_block("PED_BRAM_DATA",readback,128);
	      for(int j = 0; j < 128; j++)
		{
		  if(readback[j] != (uint32_t)ped[i*128+j])
		    {
		      GetModule()->ShowWarning("pedestal verifying error");
		    }
		}
	    }
	  ipbus_write("THR_BRAM_ADDR",0);
	  for(int i = 0; i < MAX_NUM_APV_PER_BUS; i++)
	    {
	      std::vector<uint32_t> readback;
	      readback.resize(128);
	      ipbus_read_block("THR_BRAM_DATA",readback,128);
	      for(int j = 0; j < 128; j++)
		{
		  if(readback[j] != (uint32_t)sigma[i*128+j])
		    {
		      GetModule()->ShowWarning("sigma/threshold verifying error");
		    }
		}
	    }
	}
    }
  catch(Module::Error& e)
    {
      throw Module::I2CError("Cannot load pedestals to IpbusGemAdc module: " + e.what());
    }
  catch(...)
    {
      throw Module::I2CError("Cannot load pedestals to IpbusGemAdc module dou to unknown error.");
    }
  pedestals_set = true;
}

bool IpbusGemAdcCard::i2cApvWrite(uint32_t gid, uint8_t reg, uint8_t data)
{
  // DEBUG("i2cApvWrite(0x%0x, 0x%0x, 0x%0x)", gid,(uint32_t)reg,(uint32_t)data);
  uint8_t apv  = 0;
  uint8_t addr = 0;
  decode_gid(gid,apv,addr);
  string node = "I2C_APV" + mydec(apv);
  if(!i2cWriteByte(node, addr<<1, 0x90) ||
     !i2cWriteByte(node, reg<<1,  0x10) ||
     !i2cWriteByte(node, data,    0x50))
    return false;
  // save data value for later verification
  uint32_t key = (reg<<24)|(gid&0x00FFFFFF);
  i2cApvWriteValues[key] = data;
  return true;
}

bool IpbusGemAdcCard::i2cApvRead(uint32_t gid, uint8_t reg, uint8_t& data)
{
  uint8_t apv  = 0;
  uint8_t addr = 0;
  decode_gid(gid,apv,addr);
  string node = "I2C_APV" + mydec(apv);
  if(!i2cWriteByte(node, addr<<1,    0x90) ||
     !i2cWriteByte(node, (reg<<1)|1,  0x50) ||
     !i2cWriteByte(node, (addr<<1)|1, 0x90))
    return false;
  i2cReadByte (node, data);
  return true;
}

bool IpbusGemAdcCard::MCP23017Write(uint32_t gid, uint8_t addr, uint8_t reg, uint8_t data)
{
  uint8_t apv  = 0;
  uint8_t addrr = 0;
  decode_gid(gid,apv,addrr);
  string node = "I2C_APV" + mydec(apv);
  if(!i2cWriteByte(node, addr<<1, 0x90) ||
     !i2cWriteByte(node, reg<<1,  0x10) ||
     !i2cWriteByte(node, data,    0x50))
    return false;
  // save data value for later verification
  uint32_t key = (reg<<24)|(addr<<16)|(gid&0x0000FFFF);
  MCP23017WriteValues[key] = data;
  return true;
}

bool IpbusGemAdcCard::MCP23017Read(uint32_t gid,  uint8_t addr, uint8_t reg, uint8_t& data)
{
  uint8_t apv  = 0;
  uint8_t addrr = 0;
  decode_gid(gid,apv,addrr);
  string node = "I2C_APV" + mydec(apv);
  if(!i2cWriteByte(node, addr<<1,    0x90) ||
     !i2cWriteByte(node, (reg<<1)|1,  0x50) ||
     !i2cWriteByte(node, (addr<<1)|1, 0x90))
    return false;
  i2cReadByte (node, data);
  return true;
}

void IpbusGemAdcCard::reset_APV()
{
  uint32_t reset_values[2] = { 1, 0 };
  list<DatabaseEntryFrontend>& module_db = GetModule()->GetFrontendDB();
  list<DatabaseEntryFrontend>::iterator it;
  for(it=module_db.begin(); it!=module_db.end(); ++it)
    {
      if(it->port == GetPort() && it->geographic_id < 2 && it->format_id == 0 && it->state == 1)
	{
	  reset_values[0] = 0;
	  reset_values[1] = 1;	  
	}
    }

  DEBUG("reset APV: %u %u", reset_values[0],reset_values[1]);

  ipbus_write("RST_REG.APV4ADC_RESET",1);
  ipbus_write("CONFIG3.APV_RESET",reset_values[0]);
  mywait(100000);
  ipbus_write("RST_REG.APV4ADC_RESET",0);
  ipbus_write("CONFIG3.APV_RESET",reset_values[1]);
  GetModule()->Sleep(1000000);
}

void IpbusGemAdcCard::set_data_mode(int mode)
{
  DEBUG("Set data mode: %d", mode);
  bool isLatch = false; // default mode is sparse
  if(mode == 2)
    isLatch = true;
  list<DatabaseEntryFrontend>& module_db = GetModule()->GetFrontendDB();
  list<DatabaseEntryFrontend>::iterator it;
  for(it=module_db.begin(); it!=module_db.end(); ++it)
    {
      if(it->port == GetPort() && it->geographic_id < 2 && it->state == 1)
	{
	  if(mode == 0)
	    {
	      map<string,string> m;
	      map<string,string>::iterator set_iter;
	      m=parseSettings(it->config,string());
	      if((set_iter=m.find("sparse"))!=m.end())
		{
		  m.erase(set_iter);
		  isLatch = false;
		}
	      if((set_iter=m.find("latch"))!=m.end())
		{
		  m.erase(set_iter);
		  isLatch = true;
		}
	    }
	}
    }
  uint32_t main_data_format = 0;
  uint32_t main_data_format_old = 0;
  ipbus_read("CONFIG0.MAIN_DATA_FORMAT", main_data_format_old);
  main_data_format = isLatch ? ADC_MODE_RICH_LATCH:  ADC_MODE_RICH_SPARSE;
  DEBUG("Setting main_dataformat value from 0x%0x to 0x%0x",
        main_data_format_old, main_data_format);
  //ipbus_write("CONFIG0.MAIN_DATA_FORMAT", main_data_format);
  ipbus_write("CONFIG2.DATA_MODE",isLatch?1:0);
  DEBUG("Set data mode: %s", isLatch?"Latch":"Sparse");
}

void IpbusGemAdcCard::set_thresholds()
{
  DEBUG("IpbusGemAdcCard::set_threshold.");
  bool isDone = false;
  list<DatabaseEntryFrontend>& module_db = GetModule()->GetFrontendDB();
  list<DatabaseEntryFrontend>::iterator it;
  for(it=module_db.begin(); it!=module_db.end(); ++it)
    {
      if(it->port == GetPort() && it->geographic_id < 2 && it->state == 1)
	{
	  map<string,string> m;
	  map<string,string>::iterator set_iter;
	  m=parseSettings(it->config,string());
	  if(m.count("thr"))
	    {
	      int32_t threshold = strtol(m["thr"].c_str(),NULL,0);
	      ipbus_write("CONFIG0.AD1",threshold,true); // load and verify threshold value
	      DEBUG("Set threshold value %d", threshold);
	      isDone = true;
	    }
	}
    }
  DEBUG("Threshold: %s", isDone?"Done":"Not defined");
}

// the pedestals have to be preprocessed because the ADC inverts and
// shifts the data
static void read_pedestals(istream& in, int* ped, int* sigma, double mult)
{
  int smallest=1023;
  int flag;
  const int nch     = MAX_NUM_APV_CHANNELS;
  const int maxval  = 2048;
  const int flagval = 2047;

  double ped_, sigma_, dummy;

  // read all 128 value pairs, looking for the smallest pedestal
  for(int i=0; i<nch; ++i)
    {
      in >> flag >> ped_ >> sigma_ >> dummy >> dummy;
      ped[i] = maxval - static_cast<int>(ped_ + 0.5);
      if(flag != 0)
	{
	  sigma[i]=static_cast<int>(sigma_ * mult);
	}
      else
	{
	  // If this channel is flagged out, set its threshold to the
	  // maximum so that it will never be written out.  This is only a
	  // temporary solution since in the presence of many noisy strips
	  // the common mode noise correction will go wrong.
	  sigma[i] = flagval;
	}
      if(ped[i] < smallest)
	{
	  smallest = ped[i];
	}
    }
  
  // now shift the pedestals so that the smallest is at zero
  for(int i = 0; i < nch; i++)
    {
      ped[i] -= smallest;
    }
}

void IpbusGemAdcCard::set_pedestals()
{
  DEBUG("IpbusGemAdcCard::set_pedestals");
  list<DatabaseEntryFrontend>& module_db = GetModule()->GetFrontendDB();
  list<DatabaseEntryFrontend>::iterator it;
  for(it=module_db.begin(); it!=module_db.end(); ++it)
    {
      if(it->port == GetPort() && it->geographic_id < 2 && it->state == 1)
	{
	  //const int nch = MAX_NUM_APV_CHANNELS;
	  map<string,string> m;
	  map<string,string>::iterator set_iter;
	  m=parseSettings(it->config,string());
	  double mult = 1.;
	  if(m.count("mult"))
	    {
	      mult = strtod(m["mult"].c_str(),NULL);
	      m.erase(m.find("mult"));
	      DEBUG("Parameter 'mult' = %f", mult);
	    }
	  string fname = it->calib;
	  DEBUG("Calibration file: '%s'",fname.c_str());
	  // open the pedestal file
	  ifstream in(fname.c_str());
	  if(!in)
	    {
	      throw Module::FileError(GetModule(),"cannot open "+fname);
	    }
	  DEBUG("loading pedestals from %s with multiplier %f.", fname.c_str(), mult);
	  int ped[MAX_NUM_APV_CHANNELS*MAX_NUM_APV_PER_BUS], sigma[MAX_NUM_APV_CHANNELS*MAX_NUM_APV_PER_BUS];
	  memset(ped,0,sizeof(ped));
	  memset(sigma,0,sizeof(sigma));
	  while(!in.eof())
	    {
	      string name;
	      int rob,sid,adc,apv;

	      // try to read apv header line
	      in >> name >> rob >> sid >> adc >> apv;
	      if(in.eof())
		break;

	      DEBUG("setting pedestals for %s at SID %d, ADC %d, APV %d", name.substr(1).c_str(),sid,adc,apv);

	      // the pedestal file must match this module's specs
	      if(sid != GetModule()->getSourceID())
		{
		  throw Module::FileError(GetModule(), fname + " contains calibration info for the wrong "
				  "source ID (" + mydec(sid) + ")" );
		}
	      if(apv < 0 || apv >= MAX_NUM_APV_PER_BUS)
		{
		  throw Module::FileError(GetModule(), fname + " contains calibration info for the wrong "
				  "APV number (" + mydec(apv) + ")" );
		}

	      // read all 128 pedestals/sigmas for an APV
	      read_pedestals(in, &ped[apv*MAX_NUM_APV_CHANNELS], &sigma[apv*MAX_NUM_APV_CHANNELS], mult);
	    }
	  i2cApvWritePedestals(ped,sigma,true);
	}
    }
}

int IpbusGemAdcCard::ADC_write_verify(std::string reg, uint32_t value, uint32_t mask, uint32_t& readback)
{
  const int maxret=50;
  int ret = 0;

  while(ret < maxret)
    {
      ipbus_write(reg.c_str(), value);
      ipbus_read(reg.c_str(),readback);
      if((readback&mask) == (value&mask))
	{
	  break;
	}
      ret++;
    } while((readback&mask) != (value&mask));

  if((readback&mask) != (value&mask))
    ret = -1;
  ADCWriteValues[reg] = value&mask;
  return ret;
}

int IpbusGemAdcCard::APV_write_verify(uint32_t geo_id, uint8_t reg, uint8_t value, uint8_t mask)
{
  const int maxret  = 50;
  int       ret     = 0;
  uint8_t  readback = 0;
  // DEBUG("APV write with verify: geo_id=%x, reg=%u, value=%u, mask=%u", geo_id,
  //  	(uint32_t)reg, (uint32_t)value, (uint32_t)mask);
  while(ret < maxret)
    {
      if(!i2cApvWrite(geo_id, reg, value) || !i2cApvRead(geo_id, reg, readback))
	{
	  return -2;
	}

      if((readback&mask) == (value&mask))
	{
	  break;
	}
      ret++;
      if(ret > maxret)
	break;
    }

  if((readback&mask) != (value&mask))
    ret = -1;
  return ret;
}

void IpbusGemAdcCard::set_adc_registers()
{
  DEBUG("IpbusGemAdcCard::set_adc_registers");
  list<DatabaseEntryFrontend>& module_db = GetModule()->GetFrontendDB();
  list<DatabaseEntryFrontend>::iterator it;
  for(it=module_db.begin(); it!=module_db.end(); ++it)
    {
      if(it->port == GetPort() && it->geographic_id < 2 && it->state == 1)
	{
	  //const int nch = MAX_NUM_APV_CHANNELS;
	  map<string,string> m;
	  map<string,string>::iterator set_iter;
	  m=parseSettings(it->config,string());
	  //map<string,string>::iterator itc = m.begin();
	  for(map<string,string>::iterator itc = m.begin(); itc != m.end(); itc++)
	    {
	      string reg = itc->first.c_str();
	      char* tail = NULL;
	      // uint32_t reg_num =strtoul(itc->first.c_str(), &tail, 0);
	      // //DEBUG("Reg_num = %u (%s)", reg_num, itc->first.c_str());
	      // if(tail != itc->first.c_str()) // this is digital form of the register name (for compatibilities 
	      // 	{                            // with old gesica cards)
	      // 	  if(reg_num == 0x1000)
	      // 	    reg = "CONFIG0.AD1";
	      // 	  else if(reg_num == 0x1001)
	      // 	    reg = "CONFIG0.MAIN_DATA_FORMAT";
	      // 	  else if(reg_num == 0x1002)
	      // 	    reg = "CONFIG0.LAST_WD_FORMAT";
	      // 	  else if(reg_num == 0x1003)
	      // 	    reg = "CONFIG0.LAST_WD_FORMAT";
	      // 	  else if(reg_num == 0x1005)
	      // 	    reg = "CONFIG1.FLT_TIME_OUT";
	      // 	  else if(reg_num == 0x1006)
	      // 	    reg = "CONFIG0.SEC_DATA_FORMAT";
	      // 	  else if(reg_num == 0x1007)
	      // 	    reg = "CONFIG1.NR_SYNC_APV";
	      // 	  else if(reg_num == 0x1008)
	      // 	    reg = "CONFIG2.ADC_CLK_PHASE";
	      // 	  else
	      // 	    throw Module::DBError(GetModule(),"Bad register name \'" + reg + "\' in config string for frontend "
	      // 			  +it->detector);
	      // 	}
	      uint32_t val = strtoul(itc->second.c_str(), &tail, 0);
	      // DEBUG("*** reg  %s = %s (0x%x) %p %p", reg.c_str(), itc->second.c_str(), val, itc->second.c_str(), tail);
	      if(tail != itc->second.c_str())
	       	{
	       	  DEBUG("setting '%s'=0x%x",reg.c_str(),val);
		  // all was valid, so we try to process this register but first
		  // clean up the map (need to increment before 'it' gets invalid)
		  // 	  m.erase(itc++);
		  if(reg == "version")
		    {
		      if(val != fw_id)
			{
			  GetModule()->ShowWarning("FW version mismatch" + myhex(val) + " != " + myhex(fw_id));
			}
		    }
		  else if(reg == "mult")
		    {
		      ;
		    }
		  else if(reg == "dac_values")
		    {
		      DEBUG("setting DAC values: 0x%04X", val);
		      ipbus_write("DAC_VALUE.VALUE0",val);
		      ipbus_write("DAC_VALUE.VALUE1",val);
		      ipbus_write("RST_REG.ADC_WRITE",1);
		      usleep(100000);
		      ipbus_write("RST_REG.ADC_WRITE",0);
		    }
		  else if(reg == "delay_samples")
		    {
		      DEBUG("setting delay_samples=%u", val);
		    }
		  else
		    {
		      uint32_t readback = 0;
		      int ret = ADC_write_verify(reg, val, 0xffff, readback);
		      if(ret == -1)
			{
			  throw Module::ChipError(GetModule(),"verification failed (["+reg+
						  "]=0x"+myhex(val)+" => 0x"+myhex(readback)+")");
			}
		      else if(ret != 0)
			{
			  throw Module::ChipWarning(GetModule(),mydec(ret)+" retries");
			}
		    }
	       	}
	      else
		{
	       	  DEBUG("malformed setting %s=%s.",itc->first.c_str(),itc->second.c_str());
	       	  GetModule()->ShowWarning("malformed setting "+itc->first+'='+itc->second);
	       	  ++itc;
	       	}
	    }
	}
    }
  // SYNC receiver to ADCs:
  ipbus_write("RST_REG.ADC_WRITE", 0);
  ipbus_write("RST_REG.ADC_RECEIVER_RESET",1);
  ipbus_write("RST_REG.ADC_RECEIVER_RESET",0);
}

int IpbusGemAdcCard::set_apv_registers()
{
  DEBUG("IpbusGemAdcCard::set_apv_registers");
  list<DatabaseEntryFrontend>& module_db = GetModule()->GetFrontendDB();
  list<DatabaseEntryFrontend>::iterator it;
  bool isError = false;
  bool isWarning = false;
  for(it=module_db.begin(); it!=module_db.end(); ++it)
    {
      // DEBUG("FEC port = %u, geo_id = %x", it->port, it->geographic_id);
       if(it->state != 1)
	{
	  DEBUG("APV card geo_id 0x%x in port %u is disabled because its state %u",
		it->geographic_id, it->port, it->state);
	  continue;
	}
      if(it->port == GetPort() && it->geographic_id >= 2)
	{
	  try
	    {
	      //int v = it->format_id;
	      // this map holds the symbolic names (like MODE), register addresses
	      // and default register values for the APV settings
	      map<string,pair<int,int> > apv_map;

	      // // Set I2C subaddresses and default values for the APV settings;
	      // // uses (addr,value) pairs.  The S0 and S1 versions need different
	      // // default settings, v=APV version.
	      // apv_map["MUXGAIN"]=make_pair(0x06,v?  4:  4);
	      // apv_map["LATENCY"]=make_pair(0x04,v? 28: 72);
	      // apv_map["MODE"]=   make_pair(0x02,v? 13: 13);
	      // apv_map["CSEL"]=   make_pair(0x3A,v?  1:  1);
	      // apv_map["CDRV"]=   make_pair(0x38,v? 75: 75);
	      // apv_map["VPSP"]=   make_pair(0x36,v? 40: 45);
	      // apv_map["VFS"]=    make_pair(0x34,v? 60: 67);
	      // apv_map["VFP"]=    make_pair(0x32,v? 30: 67);
	      // apv_map["ICAL"]=   make_pair(0x30,v? 25: 40);
	      // apv_map["ISPARE"]= make_pair(0x2E,v?  0:  0);
	      // apv_map["IMUXIN"]= make_pair(0x2C,v? 30: 50);
	      // apv_map["IPSP"]=   make_pair(0x2A,v? 48: 80);
	      // apv_map["ISSF"]=   make_pair(0x28,v? 30: 50);
	      // apv_map["ISHA"]=   make_pair(0x26,v? 30: 50);
	      // apv_map["IPSF"]=   make_pair(0x24,v? 30: 50);
	      // apv_map["IPCASC"]= make_pair(0x22,v? 45: 60);
	      // apv_map["IPRE"]=   make_pair(0x20,v? 85:115);
	  
	      apv_map["MUXGAIN"]=make_pair(0x03,  4);
	      apv_map["LATENCY"]=make_pair(0x02,100);
	      apv_map["MODE"]=   make_pair(0x01, 29);
	      apv_map["CSEL"]=   make_pair(0x1D,  1);
	      apv_map["CDRV"]=   make_pair(0x1C,  1);
	      apv_map["VPSP"]=   make_pair(0x1B, 10);
	      apv_map["VFS"]=    make_pair(0x1A, 60);
	      apv_map["VFP"]=    make_pair(0x19, 30);
	      apv_map["ICAL"]=   make_pair(0x18, 25);
	      apv_map["ISPARE"]= make_pair(0x17,  0);
	      apv_map["IMUXIN"]= make_pair(0x16, 30);
	      apv_map["IPSP"]=   make_pair(0x15, 48);
	      apv_map["ISSF"]=   make_pair(0x14, 30);
	      apv_map["ISHA"]=   make_pair(0x13, 30);
	      apv_map["IPSF"]=   make_pair(0x12, 30);
	      apv_map["IPCASC"]= make_pair(0x11, 45);
	      apv_map["IPRE"]=   make_pair(0x10, 85);
	  
	      // parse settings and update apv_map accordingly
	      map<string, mynumber<int> > m = parseSettings(it->config, mynumber<int>("-1"));
	      DEBUG("setting APV 0x%x with", it->geographic_id);
	      for(map<string, mynumber<int> >::iterator itc = m.begin(); itc != m.end(); ++itc)
		{
		  // check if name is known
		  string name=itc->first;
		  if(!apv_map.count(name))
		    {
		      DEBUG("unkown APV setting %s ignored", name.c_str());
		      continue;
		    }

		  // check if value was found (they should be >=0 )
		  int value = itc->second;
		  if(value < 0)
		    {
		      DEBUG("malformed APV setting %s ignored", name.c_str());
		      continue;
		    }

		  // check for an overflow
		  if((value&(~0xff)) != 0)
		    {
		      DEBUG("overflowing value %d for APV setting %s will be truncated", value, name.c_str());
		    }

		  DEBUG("%s=%d", name.c_str(), value);

		  // write new value into apv_map
		  pair<int,int> reg_val = apv_map[name];
		  reg_val.second = value & 0xff;
		  apv_map[name]  = reg_val;
		}

	      // now write and verify the settings
	      int retries=0;
	      int failures=0;
	      map<string,pair<int,int> >::iterator itm;
	      for(itm = apv_map.begin(); itm != apv_map.end(); ++itm)
		{
		  // the I2C address is taken from the db, then the first byte on
		  // the wire is the register number, the second byte contains the
		  // value for the readback the register number has to be
		  // incremented by one as the last bit is (read)/(not write)

		  int ret = APV_write_verify(it->geographic_id, itm->second.first, itm->second.second,0xFF);
		  if(ret==-1)
		    {
		      ++failures;
		    }
		  else if(ret == -2) // i2c write byte error (fatal error)
		    {
		      ++failures;
		      break;
		    }
		  else if(ret)
		    {
		      retries+=ret;
		    }
		}
	      if(failures)
		{
		  isError = true;
		  char msg[256];
		  sprintf(msg,"%d failures when set APV registers on port %d gid 0x%02x", 
			  failures, it->port, it->geographic_id);
		  GetModule()->ShowError(msg);
		}
	      if(retries)
		{
		  isWarning = true;
		  char msg[256];
		  sprintf(msg,"%d retries when set APV registers on port %d gid 0x%02x", 
			  retries, it->port, it->geographic_id);
		  GetModule()->ShowError(msg);		  
		}
	    }
	  catch(Module::Error& e)
	    {
	      isError = true;
	      GetModule()->ShowError(e.what());
	    }
	}
    }
  // did something go wrong?
  if(isError)
    {
      return -1;
    }
  else if(isWarning)
    {
      return 1;
    }
  return 0;
}

void IpbusGemAdcCard::ReadStatus(string& status_str)
{
  char msg[1024];
  uint32_t temp = 0;
  uint32_t volt = 0;
  uint32_t pwr  = 0;
  ipbus_read("TEMP_MONITOR", temp);
  ipbus_read("VOLTAGE_MONITOR", volt);
  ipbus_read("PWR", pwr);
  snprintf(msg,1023,"port = %2d temp = 0x%08x, volt = 0x%08x pwr = 0x%08x\n",
	   GetPort(), temp, volt, pwr);
  status_str += msg;
}

void IpbusGemAdcCard::ResetVerify()
{
  i2cApvWriteValues.clear();
  MCP23017WriteValues.clear();
  ADCWriteValues.clear();
  pedestals_set = false;
}

bool IpbusGemAdcCard::VerifyAll()
{
  // for the moment only verification of i2c APV registers is done. FVN:2022/10/12
  bool isError = false;
  int count = 0;
  std::map<uint32_t,uint8_t>::iterator it =  i2cApvWriteValues.begin();
  DEBUG("Verification of %s port %d:", GetCardName(), GetPort());
  DEBUG("\tAPV registers:");
  for(;it != i2cApvWriteValues.end(); it++)
    {
      if((count++ % 10) == 0) GetModule()->SendDot();
      uint32_t key  = it->first;
      uint32_t gid  = key & 0x00FFFFFF;
      uint8_t  reg  = (key >> 24) & 0xFF;
      uint8_t  data = it->second;
      uint8_t  test = 0;
      i2cApvRead(gid, reg, test);
      if(test != data)
	{
	  char msg[256];
	  sprintf(msg,"gid 0x%04x APV reg 0x%02x verify error: 0x%02x != 0x%02x",
		  gid,reg,test,data);
	  GetModule()->ShowError(msg);
	  isError = true;
	}
    }
  if(!isError)
    DEBUG("\t\tOK!");
  DEBUG("\tMCP23017 registers:");
  it = MCP23017WriteValues.begin();
  for(;it != MCP23017WriteValues.end(); it++)
    {
      if((count++ % 10) == 0) GetModule()->SendDot();
      uint32_t key  = it->first;
      uint32_t gid  = key & 0x0000FFFF;
      uint8_t addr  = (key >> 16) & 0xFF;
      uint8_t  reg  = (key >> 24) & 0xFF;
      uint8_t  data = it->second;
      uint8_t  test = 0;
      MCP23017Read(gid, addr, reg, test);
      if(test != data)
	{
	  char msg[256];
	  sprintf(msg,"gid 0x%04x MCP23017 reg 0x%02x verify error: 0x%02x != 0x%02x",
		  gid,reg,test,data);
	  GetModule()->ShowError(msg);
	  isError = true;
	}
    }
  if(!isError)
    DEBUG("\t\tOK!");
  DEBUG("\tPedestals:");
  if(pedestals_set)
    {
      ipbus_write("PED_BRAM_ADDR",0);
      for(int i = 0; i < MAX_NUM_APV_PER_BUS; i++)
	{
	  GetModule()->SendDot();
	  std::vector<uint32_t> readback;
	  readback.resize(128);
	  ipbus_read_block("PED_BRAM_DATA",readback,128);
	  for(int j = 0; j < 128; j++)
	    {
	      if(readback[j] != (uint32_t)ped_values[i*128+j])
		{
		  char msg[256];
		  sprintf(msg,"Pedestal value verification failed: 0x%08x != 0x%08x",
			  readback[j], (uint32_t)ped_values[i*128+j]);
		  GetModule()->ShowWarning(msg);
		}
	    }
	}
      ipbus_write("THR_BRAM_ADDR",0);
      for(int i = 0; i < MAX_NUM_APV_PER_BUS; i++)
	{
	  GetModule()->SendDot();
	  std::vector<uint32_t> readback;
	  readback.resize(128);
	  ipbus_read_block("THR_BRAM_DATA",readback,128);
	  for(int j = 0; j < 128; j++)
	    {
	      if(readback[j] != (uint32_t)sigma_values[i*128+j])
		{
		  char msg[256];
		  sprintf(msg,"sigma/threshold verification failed:  0x%08x != 0x%08x",
			  readback[j], (uint32_t)sigma_values[i*128+j]);
		  GetModule()->ShowWarning(msg);
		}
	    }
	}
    }
  if(!isError)
    DEBUG("\t\tOK!");
  DEBUG("\tADC registers:");
  if(ADCWriteValues.size() != 0)
    {
      std::map<std::string,uint32_t>::iterator it =  ADCWriteValues.begin();
      for(;it != ADCWriteValues.end(); it++)
	{
	  uint32_t readback = 0;
	  ipbus_read(it->first.c_str(),readback);
	  if((readback&0xffff) != it->second)
	    {
	      char msg[256];
	      sprintf(msg,"ADC reg '%s' verification error: 0x%04x != 0x%04x",
		      it->first.c_str(), readback&0xffff, it->second);
	      GetModule()->ShowError(msg);
	      isError = true;
	    }
	}
    }
  if(!isError)
    DEBUG("\t\tOK!");

  return isError;
}
