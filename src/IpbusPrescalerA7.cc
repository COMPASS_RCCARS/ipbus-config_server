#include "IpbusPrescalerA7.h"
#include "mysql++_defs.h"

#include <stdio.h>
#include <stdint.h>

#include <string>

#include "main.h"
#include "int2string.h"

using namespace std;

IpbusPrescalerA7::IpbusPrescalerA7(int sourceID, std::string version) : IpbusModule(sourceID,version),
									spill_tid(0),
									spill_mutex(NULL),
									isThreadRunning(false),
									isConfigured(false)
{
  dim["ModuleType"] = MT_TCS_PRESCALER;
  module_name="A7 Prescaler ("+mydec(sourceID)+')';
  spill_mutex = new pthread_mutex_t;
  if(!check_status(IPBUS_MOD_ERROR_STATE) && check_status(IPBUS_MOD_CREATED) && check_db())
    {
      dim["i:OR_CNT"]        = 0;
      dim["i:OR_BSY_CNT"]    = 0;
      dim["i:LS_OR_CNT"]     = 0;
      dim["i:LS_OR_BSY_CNT"] = 0;
      dim["i:BUSY_EN"]       = 0;
      // DIM services for compatibility with old TCS Prescaler:
      string name = "TCS/"+tcs_name + "/";
      dim["c="+name+"PrescalerStatus"] = "";
      dim["i="+name+"prescalerID"]     = sourceID;
      MyDimComm *command = new MyDimComm(name+"Prescaler", Control_, this);
      // command->setQueueMode(true);
      dimComm.push_back(command);
      printf("%s is created.\n", module_name.c_str());
    }
  tcsBurstInfo = new DimInfo("TCS/main/Burst",-1);
  pthread_mutex_init(spill_mutex,NULL);
  spill_tid = pthread_create(&spill_tid, NULL, spill_thread, this);
}

IpbusPrescalerA7::~IpbusPrescalerA7()
{
  DEBUG("%s is destroyed.", module_name.c_str());
  isConfigured = false;
  isThreadRunning = false;
  pthread_join(spill_tid,NULL);
  if(pthread_mutex_destroy(spill_mutex))
    {
      printf("%s\n", strerror(errno));
      //mywait(100000);
    }
  delete spill_mutex;
  delete tcsBurstInfo;
  DEBUG("Done!");
}

bool IpbusPrescalerA7::check_db()
{
  if(!IpbusModule::check_db())
    return false;
  try
    {
      if(!module_db.has_detector)
	{
	  throw DBError(this, "missing 'Detector' info in FEDB for " + module_name); 
	}
      DEBUG("IpbusPrescalerA7::check_db: %s:  OK!", module_name.c_str());
    }
  catch(DatabaseEntry::Error& e)
    {
      ShowError("database error: " + e.what());
      set_status(IPBUS_MOD_ERROR_STATE);
      return false;
    }
  catch(DBError& e)
    {
      ShowError("DB error: " + e.what());
      set_status(IPBUS_MOD_ERROR_STATE);
      return false;
    }
  catch(std::exception& e)
    {
      ShowError(std::string("std error: ") + e.what());
      set_status(IPBUS_MOD_ERROR_STATE);
      return false;
    }
  return true;
}

void* IpbusPrescalerA7::spill_thread(void* p)
{
  IpbusPrescalerA7* tcs = (IpbusPrescalerA7*)p;
  tcs->isThreadRunning = true;
  int status     = 0;
  int oldstatus  = 0;
  try
    {
      while(tcs->isThreadRunning)
	{
	  if(tcs->isConfigured)
	    {
	      oldstatus = status;
	      tcs->lock_mutex();
	      status = tcs->GetTcsSpillStatus();
	      if(status != oldstatus && status == 0) // wait for end of burst
		{
		  tcs->UpdateStatus();
		}
	      tcs->unlock_mutex();
	    }
	  usleep(PRESCALER_WAIT_INTERVAL);
	}
    }
  catch(...)
    {
      printf("FATAL ERROR: exception in the burst thread.\n");
    }
  tcs->isThreadRunning = false;
  return NULL;
}

bool IpbusPrescalerA7::ReadDB(string filename)
{
  DEBUG("loading prescaler data from file %s", filename.c_str());
  mysqlpp::Connection conn(true);
  conn.connect(db_db, db_server, db_user, db_passwd);
  mysqlpp::Query query = conn.query();

  MYSQLPP_RESULT result;
  mysqlpp::Row row;

  query << "select VALUE from FILES_PROT where PATH='/tcs/prescaler/"
	<< filename << "' and HOST='" << tcs_name << "'";
  result = query.store();
  if (result.size() != 1)
    {
      query << "select VALUE from FILES where PATH='/tcs/prescaler/"
	    << filename << "' and HOST='" << tcs_name << "'";
      result = query.store();
    }
  if (result.size() != 1)
    {
      throw DBError(this, "cannot find prescaler config "+filename+
		    " for DAQ "+tcs_name);
    }
  row = result.at(0);

  string file = row.at(0).c_str();
  istringstream in(file);

  const int lineSize = 1024;
  char line[lineSize];
  while (in.getline(line, lineSize))
    {
      if (strchr("%#;*C", line[0])) continue;
      istringstream l(line);
      uint32_t channel, divisor;
      l >> channel >> divisor;
      LoadChannel(channel, divisor);
    }
  return true;
}

bool IpbusPrescalerA7::LoadChannel(uint32_t channel, uint32_t divisor)
{
  if(channel >= PRESCALER_CHANNELS_A7)
    {
      DEBUG("Invalid prescaler channel %u", channel);
      return false;
    }
  char prescaler_name[256];
  sprintf(prescaler_name,"PRESCALER%u", channel);
  ipbus_write(prescaler_name, divisor, true);
  return true;
}

bool IpbusPrescalerA7::UpdateStatus()
{
  ostringstream out;
  out << "board ID okay" << endl;
  out << "ch divide incount outcount" << endl;
  for (int i = 0; i < PRESCALER_CHANNELS; ++i)
    {
      char reg_name[256];
      uint32_t cnt = 0;
      uint32_t cnt_pre = 0;
      uint32_t ls_cnt = 0;
      uint32_t ls_cnt_pre = 0;
      uint32_t divisor = 0;
      if(i < PRESCALER_CHANNELS_A7)
	{
	  sprintf(reg_name,"CNT_%d", i);
	  ipbus_read(reg_name, cnt);
	  sprintf(reg_name,"CNT_PRE_%d", i);
	  ipbus_read(reg_name, cnt_pre);
	  sprintf(reg_name,"LS_CNT_%d", i);
	  ipbus_read(reg_name, ls_cnt);
	  sprintf(reg_name,"LS_CNT_PRE_%d", i);
	  ipbus_read(reg_name, ls_cnt_pre);
	  sprintf(reg_name,"PRESCALER%d", i);
	  ipbus_read(reg_name, divisor);
	}
      out << i
	  << ' ' << divisor
	  << ' ' << cnt
	  << ' ' << cnt_pre
	  << ' ' << ls_cnt
	  << ' ' << ls_cnt_pre << endl;
    }
  dim["TCS/"+tcs_name+"/PrescalerStatus"] = out.str();
  uint32_t reg_value = 0;
  ipbus_read("OR_CNT", reg_value);
  dim["OR_CNT"] = (int)reg_value;
  ipbus_read("OR_BSY_CNT", reg_value);
  dim["OR_BSY_CNT"] = (int)reg_value;
  ipbus_read("LS_OR_CNT", reg_value);
  dim["LS_OR_CNT"] = (int)reg_value;
  ipbus_read("LS_OR_BSY_CNT", reg_value);
  dim["LS_OR_BSY_CNT"] = (int)reg_value;
  ipbus_read("SCR.BUSY_EN", reg_value);
  dim["BUSY_EN"] = (int)reg_value;
  return true;
}

bool IpbusPrescalerA7::set_module_config()
{
  IpbusModule::set_module_config();
  map<string,string>& config = GetConfig();
  map<string,string>::iterator it = config.find("NAME");

  if(it != config.end())
    {
      tcs_name = config["NAME"];
      config.erase(it);
    }
  else
    {
      ShowError(" the TCS name parameter is not found in 'Configuration'");
      set_status(IPBUS_MOD_ERROR_STATE);
      return false;
    }
  string default_config;
  it = config.find("DEFAULT");

  if(it != config.end())
    {
      default_config = config["DEFAULT"];
      config.erase(it);
    }
  else
    {
      ShowError(" the default configuration name is not found in 'Configuration'");
      set_status(IPBUS_MOD_ERROR_STATE);
      return false;
    }

  for(it = config.begin(); it != config.end(); it++)
    {
      DEBUG("CONFIG: %s=%s", it->first.c_str(), it->second.c_str());
      ipbus_write(it->first.c_str(),atoi(it->second.c_str()));
    }

  return ReadDB(default_config);
}

uint32_t IpbusPrescalerA7::GetFirmwareID()
{
  DEBUG("IpbusPrescalerA7::GetFirmwareID");
  if(check_status(IPBUS_MOD_ERROR_STATE) || !check_status(IPBUS_MOD_CREATED))
    throw IpbusError("IPBUS module is in error state.");

  uint32_t fw_id = 0xBAD;

  ipbus_read("FW_ID", fw_id);

  return fw_id;
}

void IpbusPrescalerA7::InitModule(int todo)
{
  if(check_status(IPBUS_MOD_ERROR_STATE) || !check_status(IPBUS_MOD_CREATED))
    {
      throw Error("Attempt to use not created IPBUS module.");
    }

  // Do commands. Note: the order matters

  dim["ModuleStatus"]  = MS_INITIALIZE;
  dim["ErrorString"]   = "";
  dim["WarningString"] = "";
  dim["Programmed"]    =  0;

  // 
  isConfigured = false;
  Reset(0);
  set_module_config();
  UpdateStatus();  
  isConfigured = true;
}

void IpbusPrescalerA7::Reset(int i)
{
  DEBUG("IpbusPrescalerA7::Reset(%d).", i);
  ipbus_write("SCR.IPB_RST",1);
  ipbus_write("SCR.IPB_RST",0);
  UpdateStatus();
}

void IpbusPrescalerA7::Control(const char* cmdstr)
{
  DEBUG("IpbusPrescalerA7::Control(\"%s\")", cmdstr);
  if(check_status(IPBUS_MOD_ERROR_STATE) || !check_status(IPBUS_MOD_CREATED))
    {
      throw IpbusError("IPBUS module is in error state.");
    }
  istringstream cmd(cmdstr);
  try
    {
      string token;
      cmd >> token;
      if(token == "loadfile")
	{
	  cmd >> token;
	  ReadDB(token);
	  UpdateStatus();
	  WriteStatusDB();
	}
      else if(token == "loadchannel")
	{
	  int32_t channel = -1;
	  int32_t divisor = -1;
	  cmd >> channel >> divisor;
	  if(channel == -1 || divisor == -1)
	    printf("Bad prescaler channel/divisor: %s\n", cmdstr);
	  else
	    {
	      LoadChannel(channel,divisor);
	      UpdateStatus();
	      WriteStatusDB();
	    }
	}
      else if(token == "loadchannels")
	{
	  while(1)
	    {
	      int32_t channel = -1;
	      int32_t divisor = -1;
	      cmd >> channel >> divisor;
	      printf("Load channel %d, divisor %d\n",channel, divisor);
	      if(channel == -1 || divisor == -1)
		{
		  printf("Bad prescaler channel/divisor: %d/%d\n", channel, divisor);
		  break;
		}
	      LoadChannel(channel,divisor);
	    } 
	  UpdateStatus();
	  WriteStatusDB();
	}
      else if(token == "update")
	{
	  UpdateStatus();
	}
      else if(token == "savefile")
	{
	  cmd >> token;
	  SaveToDB(token);
	}
      else
	{
	  DEBUG("Prescaler: unknown control command %s\n", cmdstr);
	}
    }
  catch(Module::IpbusError& e)
    {
      GetServiceMap()["ErrorString"] = e.what();
      DEBUG("A7 Prescaler error: %s", e.what().c_str());
    }
}

int IpbusPrescalerA7::GetTcsSpillStatus()
{
  return tcsBurstInfo->getInt();
}

bool IpbusPrescalerA7::ReadStatus(mysqlpp::Query& query, int* val)
{
  query << "select VALUE from FILES where PATH='/tcs/prescaler/status' and HOST='" << get_tcs_name() << "'";

  MYSQLPP_RESULT result = query.store();
  if (result.size() != 1)
    { // no status yet
      return false;
    }

  mysqlpp::Row row = result.at(0);
  string file = row.at(0).c_str();
  
  istringstream in(file);
  
  const int lineSize = 1024;
  char line[lineSize];
 
  while (in.getline(line, lineSize))
   {
     if(strchr("%#;*C", line[0]))
       continue;
     istringstream l(line);
     int channel, divisor;
     l >> channel >> divisor;
     if(channel >= PRESCALER_CHANNELS)
       break;
     if(channel < 0 || divisor < 0)
       {
	 throw Module::DBError(this, "error of reading prescaler\'s status configuration for DAQ " + get_tcs_name());	 
       }
     val[channel] = divisor;
   }
  return true;
}

bool IpbusPrescalerA7::WriteStatusDB()
{
  int val[PRESCALER_CHANNELS];
  memset(val,sizeof(val),0);
  mysqlpp::Connection conn(true);
  conn.connect(db_db,db_server,db_user,db_passwd);
  mysqlpp::Query query = conn.query();
  if(ReadStatus(query, &val[0]))
    {
      ostringstream status;
      for (int i = 0; i < PRESCALER_CHANNELS; ++i)
	{
	  uint32_t reg_val = 0;
	  char reg_name[256];
	  sprintf(reg_name,"PRESCALER%d", i);
	  if(i < PRESCALER_CHANNELS_A7)
	    ipbus_read(reg_name, reg_val);
	  val[i] = (int)reg_val;
	  status << i << ' ' << reg_val << endl;
	}
      query.reset();
      query << "replace FILES (PATH,HOST,VALUE) values ('/tcs/prescaler/status','"
	    << tcs_name << "','" << status.str() << "')";
      query.execute();
      return true;
    }
  return false;
}

bool IpbusPrescalerA7::SaveToDB(std::string filename)
{
  DEBUG("Save current prescaler configuration to DB file %s", filename.c_str());
  
  int val[PRESCALER_CHANNELS];
  memset(val,sizeof(val),0);
  mysqlpp::Connection conn(true);
  conn.connect(db_db,db_server,db_user,db_passwd);
  mysqlpp::Query query = conn.query();
  if(1)
    {
      ostringstream status;
      for (int i = 0; i < PRESCALER_CHANNELS; ++i)
	{
	  uint32_t reg_val = 0;
	  char reg_name[256];
	  sprintf(reg_name,"PRESCALER%d", i);
	  if(i < PRESCALER_CHANNELS_A7)
	    ipbus_read(reg_name, reg_val);
	  val[i] = (int)reg_val;
	  status << i << ' ' << reg_val << endl;
	}
      query.reset();
      query << "replace FILES (PATH,HOST,VALUE) values ('/tcs/prescaler/" << filename << "','"
	    << tcs_name << "','" << status.str() << "')";
      query.execute();
      DEBUG("Done!");
      return true;
    }
  DEBUG("Error.");
  return false;
}
