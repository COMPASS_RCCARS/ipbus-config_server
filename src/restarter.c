#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>

#define SHELL "/bin/bash"

void execute(char**argv)
{
  pid_t pid;
  int status;

  switch((pid=fork())) {
  case -1:
    perror("cannot create process");
    sleep(1);
    break;
  case 0: // child process
    execvp(argv[0],argv);
    perror("exec");
    _exit(1);
    break;
  default:
    waitpid(pid,&status,0);
    if(!WIFEXITED(status)||WEXITSTATUS(status)) {
      printf("child exited with error\n");
      sleep(3);
    }
  }
}

int main(int argc,char**argv)
{
  setlinebuf(stdout);
  while(1)
    execute(argv+1);
  return 0;
}
