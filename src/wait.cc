#include <sys/time.h>
#include <time.h>

int difftime(struct timeval*a,struct timeval*b)
{
  return (b->tv_sec-a->tv_sec)*1000000+b->tv_usec-a->tv_usec;
}

// wait for a given time (in microseconds)
// if necessary it will busy-wait
void mywait(int us)
{
  int timeslice=1000000/CLOCKS_PER_SEC;
  if(us<timeslice)
    {
      struct timeval start,end;
      gettimeofday(&start,NULL);
      do
	{
	  gettimeofday(&end,NULL);
	} while (difftime(&start,&end)<us);
    }
  else
    {
      struct timespec wait,rest;
      rest.tv_sec=us/1000000;
      rest.tv_nsec=(us%1000000)*1000;
      do
	{
	  wait.tv_sec=rest.tv_sec;
	  wait.tv_nsec=rest.tv_nsec;
	} while(nanosleep(&wait,&rest));
    }
}

