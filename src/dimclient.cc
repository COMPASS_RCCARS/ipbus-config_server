#include "dic.hxx"
#include <stdio.h>
#include <string>
#include <sstream>
#include <iostream>
#include "dimutil.h"

using namespace std;

int getInt(char*service)
{
  DimCurrentInfo i(service,-1);
  return i.getInt();
}

string getString(char*service)
{
  DimCurrentInfo i(service,(char*)"");
  return i.getString();
}

float getFloat(char*service)
{
  DimCurrentInfo i(service,(float)0);
  return i.getFloat();
}

double getDouble(char*service)
{
  DimCurrentInfo i(service,(double)0);
  return i.getDouble();
}

void list(char* match, bool print)
{
  DimBrowser br;
  char *name,*format;
  DimServiceType type;
  br.getServices(match);
  while((type=(DimServiceType)br.getNextService(name,format)))
    {
      bool display=false;
      switch(type)
	{
	case DimSERVICE:
	  cout<<"srv: ";
	  display=print;
	  break;
	case DimCOMMAND:
	  cout<<"cmd: ";
	  break;
	case DimRPC:
	  cout<<"rpc: ";
	  break;
	}
    switch(*format) {
    case 'I':
    case 'L':
      cout<<"(int)    "<<name;
      if(display) {
	cout<<'='<<getInt(name);
      }
      break;
    case 'F':
      cout<<"(float)  "<<name;
      if(display) {
	cout<<'='<<getFloat(name);
      }
      break;
    case 'D':
      cout<<"(double) "<<name;
      if(display) {
	cout<<'='<<getDouble(name);
      }
      break;
    case 'C':
      cout<<"(char*)  "<<name;
      if(display) {
        string s=getString(name);
        if(s.find('\n') != string::npos) {
          s.erase(s.find('\n'), string::npos);
          s+="...";
        }
	cout<<'='<<s;
      }
      break;
    default:
      cout<<"         "; break;
    }
    cout<<endl;
  }
}

void cmd(int argc,char**argv)
{
  int value=0;
  char *val_str = NULL;
  if(argc>3)
    {
      istringstream in(argv[3]);
      in>>value;
      val_str=argv[3];
    }

  DimBrowser br;
  br.getServices(argv[2]);
  char *name,*format;
  while(br.getNextService(name,format))
    {
      int ret;
      if(format[0]=='C')
	{
	  ret=DimClient::sendCommand(name,val_str?val_str:"");
	}
      else
	{
	  ret=DimClient::sendCommand(name,value);
	}
    if(!ret)
      {
	cout<<"could not deliver command "<<name<<endl;
      }
    }
}

#ifndef INT_MAX
#define INT_MAX         ((int)(~0U>>1))
#endif

int wait_int(char*service, int value, int timeout, bool edge)
{
  DimInt v(service, !value, value, edge);
  time_t start = time(NULL);

  if (timeout<0) timeout=INT_MAX;

  while (time(NULL)-start<timeout && !v.wait4finished())
    usleep(100000);

  // return code is shell convention
  return !v.wait4success();
}

void print(char*service)
{
  DimBrowser br;
  char *name,*format;
  DimServiceType type;
  br.getServices(service);
  type=(DimServiceType)br.getNextService(name,format);
  if (!type || type != DimSERVICE) return;
  switch(*format) {
  case 'I':
  case 'L':
    cout<<getInt(name);
    break;
  case 'F':
    cout<<getFloat(name);
    break;
  case 'D':
    cout<<getDouble(name);
    break;
  case 'C':
    cout<<getString(name);
    break;
  }
  cout<<endl;
}

void monitor(char*service)
{
  DimBrowser br;
  char *name,*format;
  DimServiceType type;
  br.getServices(service);
  type=(DimServiceType)br.getNextService(name,format);
  if (!type || type != DimSERVICE ||
      (*format != 'I' && *format != 'L' && *format != 'C' && *format != 'F')) {
    cerr<<"there is not INTEGER, FLOAT or STRING service named "
        <<service<<endl;
    return;
  }
  if (*format == 'C') {
    DimString s(service,"");
    while (1) {
      s.wait();
      cout << (string)s << endl;
    }
  } else if (*format == 'F') {
    DimFloat v(service,0);
    while (1) {
      v.wait();
      cout << (float)v << endl;
    }
  } else {
    DimInt v(service,0);
    while (1) {
      v.wait();
      cout << (int)v << endl;
    }
  }
}

int main(int argc, char* argv[])
{
  // find correct setting for DIM_DNS_NODE
  char *dim_dns;
  if(!(dim_dns=getenv("DIM_DNS_NODE_CS")))
    {
      cerr<<"you forgot to set DIM_DNS_NODE_CS"<<endl;
      return 1;
    }
  DimClient::setDnsNode(dim_dns);

  if(argc<2)
    {
      cerr<<"usage: "<<argv[0]<<" <cmd> [<args>]"<<endl;
      cerr<<"  where <cmd> = list|cmd|wait|print|monitor"<<endl;
      cerr<<"  try them without further arguments for help"<<endl;
      return 1;
    }
  if(strcmp(argv[1],"list") == 0 || strcmp(argv[1],"ls") == 0)
    {
      if(argc<3)
	{
	  cerr<<"usage: "<<argv[0]<<" list <glob>"<<endl;
	  cerr << "\twhere <glob> is like 'domain/*'" << endl;
	  return 1;
	}
      list(argv[2], true);
    }
  else if(!strcmp(argv[1],"lsfast")) {
    if(argc<3) {
      cerr<<"usage: "<<argv[0]<<" lsfast <glob>"<<endl;
      return 1;
    }
    list(argv[2], false);
  } else if(!strcmp(argv[1],"cmd")) {
    if(argc<3) {
      cerr<<"usage: "<<argv[0]<<" cmd <cmd> [<value>]"<<endl;
      return 1;
    }
    cmd(argc,argv);
  } else if(!strcmp(argv[1],"wait")) {
    if(argc<6 || (strcmp(argv[5],"level")&&strcmp(argv[5],"edge"))) {
      cerr<<"usage: "<<argv[0]<<" wait <service> <value> <timeout> "
	  <<"<edge|level>"<<endl;
      return 1;
    }
    return wait_int(argv[2],atoi(argv[3]),atoi(argv[4]),
		    strcmp(argv[5],"level"));
  } else if(!strcmp(argv[1],"print")) {
    if(argc<3) {
      cerr<<"usage: "<<argv[0]<<" print <service>"<<endl;
      return 1;
    }
    print(argv[2]);
  } else if(!strcmp(argv[1],"monitor")) {
    if(argc<3) {
      cerr<<"usage: "<<argv[0]<<" monitor <service>"<<endl;
      return 1;
    }
    monitor(argv[2]);
  }
  return 0;
}
