#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include <pthread.h>

#include <string>
#include <vector>

#include <uhal/uhal.hpp>

#include "main.h"
#include "Module.h"
#include "wait.h"
#include "IpbusModule.h"
#include "int2string.h"

using namespace std;
using namespace uhal;

///----------------------------------------------

IpbusModule::IpbusModule(int sourceID, string version) : Module(sourceID, version),
							 hw_mutex(NULL),
							 status(IPBUS_MOD_INITIAL_STATE)
{
  if(!check_db())
    throw IpbusError("IpbusModule::check_db error.");
  if(check_status(IPBUS_MOD_ERROR_STATE))
    return;
  dim["c:WarningString"]= "";
  dim["i:FirmwareID"]   = 0;
  if(!IpbusInit(true))
    throw IpbusError("IpbusModule::IpbusInit error.");
  hw_mutex = new pthread_mutex_t;
  if(!hw_mutex || pthread_mutex_init(hw_mutex, NULL) != 0)
    {
      dim["ModuleStatus"] = MS_ERROR;
      dim["ErrorString"] += " cannot init hw mutex";
      set_status(IPBUS_MOD_ERROR_STATE);
    }
  else
    {
      set_status(IPBUS_MOD_HW_MUTEX);
    }
  set_status(IPBUS_MOD_CREATED);
}

IpbusModule::~IpbusModule()
{
  if(check_status(IPBUS_MOD_HW_MUTEX))
    {
      while(pthread_mutex_destroy(hw_mutex))
	{
	  mywait(100000);
	}
      delete hw_mutex;
    }
}

void IpbusModule::lock_hw()
{
  if (pthread_equal(owner, pthread_self()))
    {
      // provoke SIGSEGV to allow deadlock debugging
      cout << *(char*)NULL;
    }
  pthread_mutex_lock(hw_mutex);
  owner = pthread_self();
}

void IpbusModule::unlock_hw()
{
  owner = 0;
  pthread_mutex_unlock(hw_mutex);
}

#define CHECK(x,y) if(!module_db.x)  \
    { dim["ModuleStatus"]=MS_ERROR;  \
      dim["ErrorString"]+=y;         \
      status|=IPBUS_MOD_ERROR_STATE; }

bool IpbusModule::check_db()
{
  DEBUG("IpbusModule::check_db");
  drop_status(IPBUS_MOD_FEDB_OK);
  try
    {
      dim["ModuleStatus"]=MS_DBXS;
      dim["ErrorString"]="";
      module_db.refresh();
    }
  catch(Error& e)
    {
      ShowError(e.what());
      set_status(IPBUS_MOD_ERROR_STATE);
      return false;
    }
  CHECK(has_state,"missing state for the module in Database");
  CHECK(has_minors,"missing Minor_modules string for the module in Database (name of module)");
  CHECK(prog.has_setup[0],"missing MODULE_setup_file1 in Database (connection.xml file) in "
	+ module_db.prog.name);
  CHECK(has_config,"missing Configuration in Database");
  CHECK(has_slink_format,"missing Slink_format_ID in FEDB");
  if(module_db.state != 1)
    {
      throw IpbusError("IpbusModule::check_db module is off in FEDB.");
    }
  if(!set_module_config())
    {
      ShowError(" 'Configuration' of module error");
      set_status(IPBUS_MOD_ERROR_STATE);
    }
  // check if file exists
  if (module_db.has_minors && access(module_db.prog.setup[0].c_str(),R_OK) != 0)
    {
      ShowError(" file in Database does not exist: " + module_db.prog.setup[0]);
      set_status(IPBUS_MOD_ERROR_STATE);
    }
  if(!check_status(IPBUS_MOD_ERROR_STATE))
    {
      set_status(IPBUS_MOD_FEDB_OK);
      return true;
    }
  ShowError(dim["ErrorString"]);
  return false;
}

uint32_t IpbusModule::GetFirmwareID()
{
  if(check_status(IPBUS_MOD_ERROR_STATE))
    throw IpbusError("IPBUS module is in error state.");
  uint32_t fw = 0;
  ipbus_read("FW_ID", fw);
  return fw;
}

void IpbusModule::ShowError(std::string msg)
{
  int status = dim["ModuleStatus"];
  DEBUG("ERROR: %s", msg.c_str());
  dim["ModuleStatus"] = MS_ERROR_CONT;
  dim["ErrorString"] += msg;
  dim["ModuleStatus"] = status;
}

void IpbusModule::ShowWarning(std::string msg)
{
  int status = dim["ModuleStatus"];
  DEBUG("WARNING: %s", msg.c_str());
  dim["ModuleStatus"] = MS_WARN_CONT;
  dim["WarningString"] += msg;
  dim["ModuleStatus"] = status;
}

bool IpbusModule::IpbusInit(bool force)
{
  if(force)
    {
      set_status(IPBUS_MOD_CREATED);
    }
  try
    {
      string connection_file = "file://";
      connection_file += module_db.prog.setup[0];
      if(IsDummyModule())
	IpbusDevice::Init();
      else if(!IpbusDevice::Init(connection_file.c_str(), module_db.minors.c_str()))
	{
	  throw Error("IpbusInit error.");
	}
      set_status(IPBUS_MOD_HW_INERFACE);
      if(!force) // the virtual function GetFirmwareID cannot be called in the constructor
	{
	  uint32_t fw = GetFirmwareID();
	  DEBUG("Real module Firmware ID: %u (0x%X)", fw, fw);
	  if(firmware_id != fw && !IsDummyModule())
	    {
	      dim["ModuleStatus"] = MS_ERROR;
	      dim["ErrorString"] += "Bad FW version: " + myhex(fw) + " != Configuration.FW = : "
		+ myhex( (uint32_t) firmware_id ) + ")";
	      set_status(IPBUS_MOD_ERROR_STATE);
	    }
	  dim["FirmwareID"] = (int)fw;
	  set_status(IPBUS_MOD_HAS_FW_ID);
	}
    }
  catch(Error& e)
    {
      dim["ModuleStatus"] = MS_ERROR;
      dim["ErrorString"] += e.what();
      DEBUG("IpbusInit error %s", e.what().c_str());
      set_status(IPBUS_MOD_ERROR_STATE);      
    }
  catch(...)
    {
      set_status(IPBUS_MOD_ERROR_STATE);
    }
  if(force)
    {
      drop_status(IPBUS_MOD_CREATED);
    }
  return !check_status(IPBUS_MOD_ERROR_STATE);
}

void IpbusModule::Initialize(int todo)
{
  if((todo&GS_SHOW_MUX_STATUS) != 0)
    {
      todo = GS_SHOW_MUX_STATUS;
    }
  if((todo&GS_DO_INIT_PORT) != 0)
    {
      int portMask = (todo&0xFFFF);
      todo = 0;
      char cntrl_cmd[256];
      sprintf(cntrl_cmd,"LOAD_port %d", portMask);
      Control(cntrl_cmd);
      return;
    }
  if(todo == 0x7FF) todo = GS_DO_ALL;
  DEBUG("IpbusModule::Initialize: todo = 0x%x.", todo);
  if(check_status(IPBUS_MOD_CREATED) && check_status(IPBUS_MOD_ERROR_STATE))
    {
      drop_status(IPBUS_MOD_ERROR_STATE);
    }
  else if(!check_status(IPBUS_MOD_CREATED))
    {
      dim["ModuleStatus"] = MS_ERROR;
      dim["ErrorString"] += " IPBUS Module is not yet created.";
      return;      
    }

  if(!IpbusModule::check_db() || !check_db())
    {
      dim["ModuleStatus"] = MS_ERROR;
      dim["ErrorString"] += " DB error.";
      return;
    }

  // Take the hardware mutex to be sure we are the only one
  // accessing the hardware in this moment. As all DIM commands
  // are protected against reentrant calls this will only block
  // when ReadStatus calls Initialize while the latter is already
  // running.

  lock_hw();

  dim["ModuleStatus"]  = MS_INITIALIZE;
  dim["ErrorString"]   = "";
  dim["WarningString"] = "";
  dim["Programmed"]    =  0;

  try
    {
      if(!IpbusInit())
	{
	  throw IpbusError("Cannot initialize IPBUS device.");
	}
      else
	InitModule(todo);
    }
  catch(Error& e)
    {
      string msg=e.what();
      dim["ModuleStatus"]=MS_ERROR;
      dim["ErrorString"]+=msg;
      set_status(IPBUS_MOD_ERROR_STATE);
    }

  if(!check_status(IPBUS_MOD_ERROR_STATE))
    { 
      dim["Programmed"]=1;
      dim["ModuleStatus"]=MS_READY;
      set_status(IPBUS_MOD_INITIALIZED);
    }
  else
    {
      dim["ModuleStatus"] = MS_ERROR;
      dim["ErrorString"] += " IpbusModule initializing error.";
    }
  unlock_hw();
}

void IpbusModule::Reset(int i)
{
  DEBUG("IpbusModule: SrcID %d, Reset(%d) command.\n",getSourceID(), i);
}

void IpbusModule::ReadStatus(int i)
{
  DEBUG("IpbusModule: SrcID %d, ReadStatus(%d) command.\n",getSourceID(), i);
}

void IpbusModule::Control(const char* cmd)
{
  DEBUG("IpbusModule: SrcID %d, Control(\"%s\") command.\n",getSourceID(), cmd);
}

bool IpbusModule::set_module_config()
{
  bool isOK = false;
  config.clear();
  config = parseSettings(module_db.config, string());
  map<string, string >::iterator it;
  vector<map<string, string >::iterator> to_delete;
  for(it = config.begin(); it != config.end(); it++)
    {
      if(it->first == "FW")
	{
	  uint32_t fw = 0;
	  if(sscanf(it->second.c_str(),"%u", &fw) == 1)
	    {
	      firmware_id = fw;
	      isOK = true;
	    }
	  to_delete.push_back(it);
	}
      else if(it->first == "TIMEOUT")
	{
	  DEBUG("Timeout = %s\n", it->second.c_str());
	}
      else if(it->first == "DO_NOT_SET_FREQ")
	{
	  DEBUG("Setting of clock frequency is disabled.");
	  to_delete.push_back(it);
	}
      else if(it->first == "DUMMY")
	{
	  uint32_t dummy = 0;
	  if(sscanf(it->second.c_str(),"%u", &dummy) == 1)
	    {
	      isDummyModule = (dummy != 0);
	      if(IsDummyModule())
		{
		  DEBUG("The module is not installed. Using emulation.");
		}
	    }
	  else
	    ShowWarning(" bad 'DUMMY' parameter in 'Configuration'");
	  to_delete.push_back(it);
	}
    }
  for(unsigned i = 0; i < to_delete.size(); i++)
    config.erase(to_delete[i]);
  if(!isOK)
    ShowError(" bad 'FW' parameter in 'Configuration' of FEDB");
  dim["Configuration"] = module_db.config.c_str();
  return isOK;
}

IpbusFECard::IpbusFECard(FECardType cardType, const char* name, IpbusModule* parent, int _port) : 
  IpbusDevice(), module(parent),
  card_type(cardType),
  card_name(name),
  port(_port)
{
  if(module && name && name[0])
    {
      if(!IpbusDevice::Init(parent->GetIpbusManager(),name))
	{
	  std::string msg = "IpbusFECard '";
	  msg += name;
	  msg += "' failed.";
	  throw Module::IpbusError(msg);
	}
      else
	DEBUG("FE card '%s' is connected to '%s' module", name, parent->getName().c_str());
    }
}

void IpbusFECard::set_registers()
{
  list<DatabaseEntryFrontend> fe_db = GetModule()->GetFrontendDB();
  list<DatabaseEntryFrontend>::iterator it;
  DEBUG("IpbusFECard::set_registers");
  for(it = fe_db.begin(); it != fe_db.end(); it++)
    {
      if(it->port == port)
	{
	  map<string,string> config = parseSettings(it->config, string());
	  map<string, string >::iterator cit;
	  for(cit = config.begin(); cit != config.end(); cit++)
	    {
	      if(cit->first == "FIRMWARE_ID") continue;
	      if(cit->first == "SW_VERSION") continue;
	      if(cit->first == "CNTVALUEIN_1") continue;
	      if(cit->first == "") continue;
	      uint32_t value = toUnsigned(cit->second);
	      //DEBUG("Write to register: '%s'=%u", cit->first.c_str(), value);
	      ipbus_write(cit->first.c_str(), value);
	    }
	  break;
	}
    }
}

void IpbusModule::Sleep(uint64_t usec)
{
  uint64_t TICK = 500000; //0.5 sec
  int status = dim["ModuleStatus"];
  dim["ModuleStatus"] = MS_SLEEP;
  uint64_t time = 0;
  for(time = 0; time < usec; time += TICK)
    {
      uint64_t sleep = TICK;
      if((time+TICK) > usec)
	sleep = usec%TICK;
      usleep(sleep);
    }
  dim["ModuleStatus"] = status;
}

void IpbusModule::SendDot()
{
  int status = dim["ModuleStatus"];
  dim["ModuleStatus"] = MS_DBXS;
  dim["ModuleStatus"] = status;
}
