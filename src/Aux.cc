#include <iostream>
#include <memory>
#include "Aux.h"
#include "wait.h"
#include "gs_todo.h"
#include "cs_types.h"
#include "int2string.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <errno.h>
#include <stdio.h>
#include <stdint.h>

using namespace std;

extern uint32_t DB_PORT;

/*****************************************************************************/
pthread_mutex_t screenout_mutex  = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t cmd_mutex        = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t db_mutex         = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t cmd_logger_mutex = PTHREAD_MUTEX_INITIALIZER;

pthread_mutex_t& ExecContext::cmdMutex()
{
  return ::cmd_mutex;
}

pthread_mutex_t& ExecContext::dbMutex()
{
  return ::db_mutex;
}

int ExecContext::sout( const ostringstream &Out )
{
  MutexLock scrmtx( screenout_mutex );
  cout << Out.str() << flush;
  return 0;
}

int ExecContext::serr( const ostringstream &Out )
{
  MutexLock scrmtx( screenout_mutex );
  cerr << Out.str() << flush;
  return 0;
}

/*****************************************************************************/
CommandOptions::CommandOptions( ExecContext &Context )
  : todo(0),
    stropts("-"),
    command_("/Initialize"),
    is_sequential(false),
    logging_disabled(true),
    ctx(Context)
{}

void CommandOptions::usage( char* opts )
{
  ostringstream sout;
  if ( ! opts )
    {
      sout << "written 2002 by Roland Kuhn" << endl << endl
	   << "LOAD <options> <module-spec>" << endl << endl
	   << "  <module-spec> can be" << endl
	   << "    <host name>  all modules connected to IPBUS from the host" << endl
	   << "    <source IDs> all specified source IDs" << endl << endl
	   << "  <options> are" << endl << endl
	   << "   general:" << endl
	   << "    -s  sequential mode (one module after the other)" << endl
	   << "    -A  do everything" << endl
	   << "    -y  disable logging to database" << endl
	   << "    -h  this help" << endl
	   << "    -S  show ifTDC-MUX status" << endl << endl;
      opts = (char*)"ACRaghmlprsvyf";
    }
  sout << "   IPBUS:" << endl;
  for ( char *p=opts; *p; ++p )
    {
      switch ( *p )
	{
	case 'g': sout << "    -g  IpbusModule" << endl; break;
	case 'm': sout << "    -m  mode                   (ADC)" << endl; break;
	case 'l': sout << "    -l  latch mode (needs -m)  (ADC)" << endl; break;
	case 'p': sout << "    -p  pedestals              (ADC)" << endl; break;
	case 'R': sout << "    -R  misc registers         (ADC)" << endl; break;
	case 'C': sout << "    -C  misc checks            (ADC)" << endl; break;
	case 'S': sout << "    -S  show ifTDC-MUX status"        << endl; break;
	case 'r': sout << "    -r  APV reset"                    << endl; break;
	case 'a': sout << "    -a  APV (or ifTDC card)"          << endl; break;
	case 'v': sout << "    -v  only verify"                  << endl; break;
	case 'f': sout << "    -f  restart clock frequency generator (ipbus MUX)" << endl; break;
	}
    }
  ctx.sout( sout );
}

int CommandOptions::parseOption( char   c,
				 int   &todo,
				 int   &rv )
{
  cout << c;
  switch( c )
    {
    case 'A': todo |= GS_DO_ALL; break;
    case 'C': todo |= GS_DO_ADC_CHECKS; break;
    case 'R': todo |= GS_DO_ADC_REGISTERS; break;
    case 'T': todo |= GS_DO_THR; break;
    case 'a': todo |= GS_DO_APV; break;
    case 'd': todo |= GS_DO_DELAY; break;
    case 'g': todo |= GS_DO_GESICA; break;
    case 'f': todo |= GS_DO_TCS; break;
    case 'h': usage(); rv = 0; return 1;
    case 'l': todo |= GS_DO_MODE_OVERRIDE; break;
    case 'm': todo |= GS_DO_MODE; break;
    case 'p': todo |= GS_DO_PED; break;
    case 'r': todo |= GS_DO_APV_RESET; break;
    case 's': is_sequential = true; break; // do one after the other
    case 'v': todo |= GS_DO_VERIFY; break; // only verify
    case 'y': logging_disabled = true; break;
    case 'z': todo |= GS_DO_ADC; break;
    case 'S': todo |= GS_SHOW_MUX_STATUS; break;
    default:
      ostringstream serr;
      serr << c << ": unknown option " << endl;
      ctx.serr( serr );
      usage();
      rv = 1;
      return 1;
    }
  //printf("** LOAD option: 0x%x\n", todo);
  return 0;
}

int CommandOptions::parseToDoMasks( string          AllOpts,
				    vector<int>    &ToDo,
				    vector<string> &StrToDo )
{
  if ( ! AllOpts.size() )
    return 0;
  StrToDo = split( AllOpts, ' ' );
  int rv=0;
  for ( vector<string>::const_iterator it=StrToDo.begin();
        it!=StrToDo.end();
        ++it )
    {
      string::const_iterator c=it->begin();
      if ( *c != '-' )
	{
	  ostringstream serr;
	  serr << *it << ": wrong format of database LOAD options" << endl;
	  ctx.serr( serr );
	  usage( (char*) "ghdmlTpRravA" );
	  return 5;
	}
      ++c;
      int tmp=0, todo_mask=0;
      for ( ; c!=it->end(); ++c )
	{
	  if ( parseOption( *c, tmp, rv ) )
	    exit( rv );
	  todo_mask |= tmp;
	}
      if ( todo_mask )
	ToDo.push_back( todo_mask );
    }
  return 0;
}

int CommandOptions::processOptions( int argc, char* argv[] )
{
  int rv = 0;
  int c;
  todo = 0;
  while( (c=getopt( argc, argv, "ghdmlSTpRravAf" )) != -1 )
    {
      if ( parseOption( c, todo, rv ) )
	exit( rv );
      else
	stropts += c;
    }
  if(todo == 0 || optind==argc)
    {
      usage();
      return 1;
    }
  return 0;
}

int CommandOptions::toDo() const
{
  return todo;
}

const string& CommandOptions::strOpts() const
{
  return stropts;
}

const string& CommandOptions::command() const
{
  return command_;
}

bool CommandOptions::isSequential() const
{
  return is_sequential;
}

bool CommandOptions::loggingDisabled() const
{
  return logging_disabled;
}


/*****************************************************************************/
DBConnection::DBConnection( const char  *Server,
                            const char  *User,
                            const char  *Passwd,
                            const char  *DB,
                            ExecContext &Ctx,
                            bool        IsConnectionFake )
  : server(Server),
    user(User),
    passwd(Passwd),
    db(DB),
    conn(NULL),
    ctx(Ctx),
    is_connection_fake(IsConnectionFake)
{
}

void DBConnection::debprint( const string &out )
{
#ifdef DEBUG_RUN
  ostringstream sout;
  sout << "DBConnection::"
       << out << endl;
  ctx.sout( sout );
#endif
}

int DBConnection::connect()
{
  if ( conn )
    return 0;
  if ( !server || !strlen(server) ||
       !user   || !strlen(user)   ||
       !passwd || !strlen(passwd) ||
       !db     || !strlen(db) )
    return 4;
  if ( !is_connection_fake )
    {
      conn = mysql_init( NULL );
      if ( !mysql_real_connect( conn, server, user, passwd, db, DB_PORT, NULL, 0 ) )
	{
	  {
	    ostringstream serr;
	    serr << "MySQL connection problem: " << mysql_error( conn ) << endl;
	    ctx.serr( serr );
	  }
	  mysql_close( conn );
	  conn = NULL;
	  return 3;
	}
    }
  else
    {
      ostringstream sout;
      sout << "DBConnection::connect("
	   << server << ","
	   << user << ","
	   << passwd << ","
	   << db << ")" << endl;
      debprint( sout.str() );
    }
  return 0;
}

int DBConnection::doQuery( const char *Query )
{
  int rv = 0;
  if ( ! conn && (rv=connect()) )
    return rv;
  if ( !is_connection_fake )
    {
      if ( mysql_query( conn, Query ) )
	{
	  ostringstream serr;
	  serr << "MySQL query not successful: " << mysql_error( conn ) << endl;
	  serr << "query >" << Query << "<" << endl;
	  ctx.serr( serr );
	  return 2;
	}
    }
  else
    {
      debprint( "doQuery("+string(Query)+")" );
    }
  return 0;
}

int DBConnection::doQuery( const string &Query )
{
  return doQuery( Query.c_str() );
}

int DBConnection::lastInsertedId()
{
  if ( ! conn  )
    return -1;
  if ( !is_connection_fake )
    {
      return mysql_insert_id( conn );
    }
  else
    {
      debprint( "lastInsertedId()" );
    }
  return 0;
}

int DBConnection::query( const char *Query )
{
  int rv = 0;
  if ( ! conn && (rv=connect()) )
    return rv;
  if ( !is_connection_fake )
    {
      if ( mysql_query( conn, Query ) )
	{
	  ostringstream serr;
	  serr << "MySQL query not successful: " << mysql_error( conn ) << endl;
	  serr << "query >" << Query << "<" << endl;
	  ctx.serr( serr );
	  return 2;
	}
      result = mysql_store_result( conn );
    }
  else
    {
      debprint( "doQuery("+string(Query)+")" );
    }
  return 0;
}

int DBConnection::query( const string &Query )
{
  return query( Query.c_str() );
}

char** DBConnection::fetchRow()
{
  if ( ! conn  )
    return 0;
  return (row=mysql_fetch_row( result ));
}

int DBConnection::freeResult()
{
  if ( ! conn  )
    return -1;
  mysql_free_result( result );
  return 0;
}

DBConnection::~DBConnection()
{
  mysql_close( conn );
}

/*****************************************************************************/
CommandLogger::CommandLogger( DBConnection &DevLogConn, ExecContext  &Ctx )
  : state(UnknownState),
    modules_num(0),
    command_num(0),
    success_num(0),
    warning_num(0),
    failure_num(0),
    max_query_buffer_size(MAX_QUERY_BUFFER_SIZE),
    devlogconn(DevLogConn),
    ctx(Ctx)
{
}

int CommandLogger::checkSrcID( int SrcID, const string &what )
{
  if ( sid_cmd_log.find( SrcID ) == sid_cmd_log.end() )
    {
      ostringstream serr;
      serr << "ERROR: " << what << " for not declared SrcID=" << SrcID << endl;
      ctx.serr( serr );
      return 1;
    }
  return 0;
}

int CommandLogger::commandStart( const string& StrOpts )
{
  MutexLock lock( cmd_logger_mutex );
  state = UnknownState;
  modules_num = 0;
  command_num = 0;
  success_num = 0;
  warning_num = 0;
  failure_num = 0;
  start_time = time( NULL );
  finish_time = 0;
  options = StrOpts;
  sid_cmd_log.clear();

  return 0;
}

const char* CommandLogger::stateStr( int state )
{
  switch ( state )
    {
    case SuccessState: return "'S'"; break;
    case WarningState: return "'W'"; break;
    case FailureState: return "'F'"; break;
    }
  return "'U'";
}

int CommandLogger::setMaxQueryBufferSize( unsigned int Size )
{
  max_query_buffer_size = Size;
  return 0;
}

string prepareString( const string &str )
{
  string res;
  for( size_t i=0; i<str.size(); ++i ) {
    if (str[i] == '\'' || str[i] == '\\')
      res += '\\';
    res += str[i];
  }
  return res;
}

int CommandLogger::commandFinish()
{
  MutexLock lock( cmd_logger_mutex );
  if ( !(state&WarningState) && !(state&FailureState) )
    state = SuccessState;
  finish_time = time( NULL );
  ostringstream out;
  out << "INSERT INTO " COMMAND_TABLE " (State,SuccessNum,WarningNum,FailureNum,StartDatetime,FinishDatetime,Options) VALUES ("
      << stateStr( state ) << ","
      << success_num << ","
      << warning_num << ","
      << failure_num << ",FROM_UNIXTIME("
      << start_time << "),FROM_UNIXTIME("
      << finish_time << "),'"
      << prepareString( options.c_str() ) << "')";
#ifdef USE_DB_DEVLOG_DB
  MutexLock db_lock( ctx.dbMutex() );
  devlogconn.connect();
  devlogconn.doQuery( out.str() );
  int cmdId = devlogconn.lastInsertedId();
  string sql = "INSERT INTO " COMMAND_SRCID_TABLE " (CommandId,SrcID,State,StartDatetime,FinishDatetime,ModuleType,Detector,Options,WarningStr,ErrorStr) VALUES";
  string buffer;
  string values;
  bool put_comma = false;
  ostringstream out2;
  for ( map< int, queue<cmd_log_t> >::iterator it=sid_cmd_log.begin(); it!=sid_cmd_log.end(); ++it )
    {
    queue<cmd_log_t>& cmd = it->second;
    while ( !cmd.empty() )
      {
	out2 << "\n("
	     << cmdId << ","
	     << it->first << ","
	     << stateStr( cmd.front().state ) << ",FROM_UNIXTIME("
	     << cmd.front().start_time << "),FROM_UNIXTIME("
	     << cmd.front().finish_time << "),'"
	     << prepareString( cmd.front().module_type ) << "','"
	     << prepareString( cmd.front().detector ) << "','"
	     << prepareString( cmd.front().options ) << "','"
	     << prepareString( cmd.front().warn_str ) << "','"
	     << prepareString( cmd.front().err_str ) << "')";
	cmd.pop();
	if ( sql.size() + buffer.size() + out2.str().size() > max_query_buffer_size )
	  {
	    if ( !buffer.size() )
	      {
		buffer = out2.str();
		out2.str( string() );
	      }
	    devlogconn.doQuery( sql+buffer );
	    buffer = string();
	    buffer = out2.str();
	    out2.str( string() );
	    put_comma = true;
	    if ( !buffer.size() )
	      put_comma = false;
	    continue;
	  }
	if ( put_comma )
	  {
	    buffer += ',';
	  }
	buffer += out2.str();
	out2.str( string() );
	put_comma = true;
      }
    }
  if ( buffer.size() )
    devlogconn.doQuery( sql+buffer );
#endif
#ifdef DEBUG_RUN
  ostringstream sout;
  sout << "commandFinish(), state=" << state << endl;
  ctx.sout( sout );
#endif    
  return 0;
}

int CommandLogger::moduleAdded( int SrcID )
{
  MutexLock lock( cmd_logger_mutex );
  ++modules_num;
  if ( sid_cmd_log.find( SrcID ) == sid_cmd_log.end() )
    {
      sid_cmd_log[SrcID] = queue<cmd_log_t>();
    }
  else
    {
      ostringstream serr;
      serr << "SrcID=" << SrcID << " added already" << endl;
      ctx.serr( serr );
    }
#ifdef DEBUG_RUN
  ostringstream sout;
  sout << "moduleAdded(" << SrcID << "), modules_num=" << modules_num << endl;
  ctx.sout( sout );
#endif    
  return 0;
}

int CommandLogger::moduleCommandStart( int           SrcID,
				       const string &ModuleType,
				       const string &Detector,
				       const string &StrOpts )
{
  MutexLock lock( cmd_logger_mutex );
  int rv=0;
  if ( (rv=checkSrcID( SrcID, "moduleCommandStart" )) )
    return rv;

  sid_cmd_log[SrcID].push( cmd_log_t( UnknownState,
                                      time( NULL ),
                                      0,
                                      ModuleType,
                                      Detector,
                                      StrOpts ) );
#ifdef DEBUG_RUN
  ostringstream sout;
  sout << "moduleCommandStart(" << SrcID
       << "," << StrOpts << ")" << endl;
  ctx.sout( sout );
#endif
  return 0;
}

int CommandLogger::moduleCommandFinish( int           SrcID,
					int           State,
					const string &Opts,
					const string &WarnStr,
					const string &ErrStr )
{
  MutexLock lock( cmd_logger_mutex );
  int rv=0;
  if ( (rv=checkSrcID( SrcID, "moduleCommandFinish" )) )
    return rv;
  ++command_num;
  if (state == UnknownState)
    state = State;
  else
    state |= State;
  cmd_log_t &cmd = sid_cmd_log[SrcID].back();
  cmd.finish_time = time( NULL );
  cmd.state = State;
  cmd.warn_str = WarnStr;
  cmd.err_str = ErrStr;

  ostringstream sout;
  sout << endl << "** module " << SrcID << ", command '" << Opts << "' ";
  if ( State & FailureState )
    {
      ++failure_num;
      sout << "errors:" << endl << ErrStr << endl;
    }
  if ( State & WarningState )
    {
      ++warning_num;
      sout << "warnings:" << endl << WarnStr << endl;
    }
  if ( State == InfoState )
    {
      sout << "Info: " << endl << WarnStr << endl;
    }
  if ( State == SuccessState )
    {
      ++success_num;
      sout << "completed successfully" << endl;
    }
  ctx.sout( sout );
  return 0;
}

int CommandLogger::report()
{
  MutexLock lock( cmd_logger_mutex );
  // print summary
  char m[8];
  char c[8];
  char s[8];
  char w[8];
  char f[8];
  snprintf( m, 8, "% 4d", modules_num );
  snprintf( c, 8, "% 4d", command_num );
  snprintf( s, 8, "% 4d", success_num );
  snprintf( w, 8, "% 4d", warning_num );
  snprintf( f, 8, "% 4d", failure_num );
  ostringstream sout;
  sout << endl
       << "+-----------------------------+" << endl
       << "| modules selected:      " << m << " |" << endl
       << "| commands executed:     " << c << " |" << endl
       << "|         successful:    " << s << " |" << endl
       << "|         with warnings: " << w << " |" << endl
       << "|         failed:        " << f << " |" << endl
       << "+-----------------------------+" << endl << endl;
  ctx.sout( sout );
  return 0;
}

CommandLogger::~CommandLogger()
{
}

/*****************************************************************************/
Module::Module( int              Sid,
                const string    &Source,
                const string    &Command,
                const vector<int>    &Todo,
                const vector<string> &StrTodo,
                ExecContext     &Context,
                CommandLogger   &Logger,
                bool             IsMinor )
  : sid(Sid),
    source(Source),
    command(Command),
    todo(Todo),
    strtodo(StrTodo),
    is_minor(IsMinor),
    status( Source+"/ModuleStatus", -1 ),
    state(SuccessState),
    ctx(Context),
    logger(Logger)
{
  error=false;
  //Default option is sending commands via DIM
  useDirectConn = false;
}

/**
 * Enables direct connection via builtin connection class
 * Do not use DIM, connect directly to VMEHostname
 *
 * @author Julian Harms
 * @param string VMEHostname Hostname to connect to
 * @param string Port Connection-Port (TCP)
 * @param string Configuration Configuration string with Macros
 * @return void
 */
void Module::enableDirectConn(string VMEHostname, string Port, string Configuration)
{
  //cout << VMEHostname << " " << Port << " " << Configuration << endl;
  useDirectConn = true;
  vmeHostName = VMEHostname;
  port = Port;
  configuration = Configuration;
}

int Module::sendCommand( int           ToDoMask,
			 const string &ToDoOpts )
{
  last_strtodo = ToDoOpts;
#ifndef DEBUG_RUN
  MutexLock cmd_lock( ctx.cmdMutex() );
  logger.moduleCommandStart( sid, module_type, detector, ToDoOpts );
  DimClient::sendCommandNB( (char*)(source+command).c_str(), ToDoMask );
#else
  logger.moduleCommandStart( sid, module_type, detector, ToDoOpts );
  ostringstream sout;
  sout << "  2 sendCommand(" << ToDoMask << ") sid: " << sid << endl;
  ctx.sout( sout );
#endif
  return 0;
}

void Module::wait()
{
#ifndef DEBUG_RUN
  time_t now,start;

  if(error)
    return;

  // check if the module is available
  time(&start);
  time(&now);
  while( status==-1 && difftime(now,start) < 10 )
    {
      mywait(100000);
      time(&now);
    }
  if(status==-1)
    {
      err_str="cannot receive status from module "+source+" (no config_server?)";
      error=true;
      return;
    }

  // wait for the status to finalize
  //if(verbose) cout<<endl<<"waiting "<<flush;

  // first see if we got started
  while(status<10&&(time(&now),difftime(now,start))<10)
    {
      mywait(30000);
    }
  if(status<10)
    { // it's already over or will never start
      return;
    }

  status.lock();
  do
    {
      status.wait();
      //printf("*** %d\n",(int)status);
      //if(verbose) {
      ostringstream sout;
      switch(status)
	{
	case 10:                     // initialize
	case 11:                     // reset state
	case 12:                     // read state
	case 13:                     // database access
	case 14: sout << '.'; break; // sleeping state
	case  2: sout << 'E'; break; // fatal error
	case 20: sout << 'e'; break; // error (cont)
	case 21: sout << '!'; break; // warning (cont)
	}
      ctx.sout( sout );
      //}
    } while(status>9);
  //if(verbose) {
  //  cout<<endl;
  //}
#else
  sleep( 1 );
#endif
}

void Module::finish()
{
#ifndef DEBUG_RUN
  if(error)
    {

      state = FailureState;
      DimCurrentInfo err((char*)(source+"/ErrorString").c_str(),(char*)"");
      DimCurrentInfo warn((char*)(source+"/WarningString").c_str(),(char*)"");
      err_str = err.getString();
      warn_str = warn.getString();
    }
  else if(status==2)
    {

      DimCurrentInfo err((char*)(source+"/ErrorString").c_str(),(char*)"");
      err_str = err.getString();
      state = FailureState;
      
    }
  else if(status==3)
    {

      DimCurrentInfo warn((char*)(source+"/WarningString").c_str(),(char*)"");
      warn_str = warn.getString();
      state = WarningState;

    }
  else if(status==4)
    {

      DimCurrentInfo err((char*)(source+"/ErrorString").c_str(),(char*)"");
      DimCurrentInfo warn((char*)(source+"/WarningString").c_str(),(char*)"");
      err_str = err.getString();
      warn_str = warn.getString();
      state = FailureState | WarningState;
    }
  else
    {
      DimCurrentInfo warn((char*)(source+"/WarningString").c_str(),(char*)"");
      warn_str = warn.getString();
      if(warn_str != "")
        state = InfoState;
    }
#endif
  logger.moduleCommandFinish( sid,
                              state,
                              last_strtodo,
                              warn_str,
                              err_str );
}

ExecContext& Module::execContext()
{
  return ctx;
}

CommandLogger& Module::commandLogger()
{
  return logger;
}


/*****************************************************************************/
ModuleContainer::ModuleContainer( CommandOptions &CoOpts,
                                  DBConnection   &DevConn,
                                  ExecContext    &Context,
                                  CommandLogger  &Logger )
  : co(CoOpts),
    devconn(DevConn),
    ctx(Context),
    logger(Logger)
{
  is_sequential = co.isSequential();
  command = co.command();
  //Load all macros into memory
  macros = new Macro (co, devconn, ctx, logger);
}


Module *
ModuleContainer::module( int n )
{
  if ( module_.find( n ) != module_.end() )
    return module_[n];
  else
    return 0;
}

ModuleContainer::~ModuleContainer() {
  //allocated in constr.
  delete macros;
}

void
ModuleContainer::findSourceIDs( string        spec,
                                int           todo_mask,
                                const string &strtodo )
{
  char buffer[20],dummy;

  // check if it's a simple source ID
  istringstream in(spec);
  unsigned long id=0;
  in >> id;
  if(id) {
    do {
      snprintf(buffer,20,"%lu",id);
      module_[id] = new Module( id,
                                buffer,
                                command,
                                vector<int>(1,todo_mask),
                                vector<string>(1,strtodo),
                                ctx,
                                logger );
      logger.moduleAdded( id );
      in >> dummy;
      id=0;
      in >> id;
    } while(id);
    return;
  }
  
    // check, it's a direct connection without dim
  if( spec.substr(0,4)=="dtbp" ) {
    //cout<<endl<<"DTBP"<<endl;
    // try MySQL
    if ( ! devconn.connect() ) {
      int counter = 0;
      /* build and execute query */
      string query = "SELECT MODULE_source_ID,VME_host_name,VME_base_address,Configuration FROM MODULE "
        "WHERE VME_host_name LIKE '" + spec + "%' AND "
        "Version_tag='latest' AND "
        "State=1 "
        "ORDER BY MODULE_source_ID";
      //cout<<query<<endl;
      if ( ! devconn.query( query ) ) {

        ostringstream sout;
        while ( devconn.fetchRow() ) {


          if (!counter) {
            sout << "Received Source IDs from FEDB (by VME_host_name): ";
          }

          sout << devconn.row[0] << " ";
	  unsigned long sid = strtoul( devconn.row[0], NULL, 0 );
          module_[sid] = new Module( sid,
                                     devconn.row[0],
                                     command,
                                     vector<int>(1,todo_mask),
                                     vector<string>(1,strtodo),
                                     ctx,
                                     logger );
	  //this requires direct conn (NO DIM!)
	  string conf = "";
	  if ( devconn.row[3] )
	    conf=devconn.row[3];
	  else {
	    ostringstream sout2;
	    sout << "No configuration in FEDB for " <<  devconn.row[0] << "!\n";
	    ctx.serr( sout2 ); 
	  }    
	  //tx.moduleContainer().macros->expandMacro (conf);
    module_[sid]->enableDirectConn (devconn.row[1], devconn.row[2], conf);
	  	  
	  logger.moduleAdded( sid );
          counter++;
        }
        if (counter)
          sout << endl;
        //ctx.sout( sout );
        /* clean up */
        devconn.freeResult();
      }

      if ( counter )
        return;
      else {
        ostringstream sout;
        sout << "Query FEDB (by VME_host_name) without success.\n";
        ctx.serr( sout );
      }
    }
  }


  // the next methods will all use DIM
  char *name,*format;
  DimBrowser br;

  // check if it's all/compass or allcatch or allgesica
  if( spec.substr(0,3)=="all" || spec=="compass" ) {
    int type=MT_CATCH|MT_GESICA|MT_TCS;
    if( spec.substr(3)=="catch" ) {
      type=MT_CATCH;
    } else if( spec.substr(3)=="gesica" ) {
      type=MT_GESICA;
    } else if( spec.substr(3)=="tcs" ) {
      type=MT_TCS;
    }
    br.getServices("*/Initialize");
    while(br.getNextService(name,format)) {
      string src(name);
      src=src.substr(0,src.find('/'));
      DimCurrentInfo typ((src+"/ModuleType").c_str(),0);
      if(typ.getInt()&type) {
        unsigned long sid = strtoul(src.c_str(),NULL,0);
        module_[sid] = new Module( sid,
                                   src,
                                   command,
                                   vector<int>(1,todo_mask),
                                   vector<string>(1,strtodo),
                                   ctx,
                                   logger );
        logger.moduleAdded( sid );
      }
    }
    return;
  }



  // check, if it's a ROB/slink
  if( spec.substr(0,6)=="pccorb" ) {
    // try MySQL
    if ( ! devconn.connect() ) {
      int counter = 0;

      /* build and execute query */
      string query = "SELECT MODULE_source_ID FROM MODULE "
        "WHERE ROB_slot LIKE '" + spec + "%' AND "
        "Version_tag = 'latest' AND "
        "State = 1 "
        "ORDER BY MODULE_source_ID";
      if ( ! devconn.query( query ) ) {
	
        ostringstream sout;
        while ( devconn.fetchRow() ) {
          if (!counter)
            sout << "Received Source IDs from FEDB: ";
          sout << devconn.row[0] << " ";
          unsigned long sid = strtoul( devconn.row[0], NULL, 0 );
          module_[sid] = new Module( sid,
                                     devconn.row[0],
                                     command,
                                     vector<int>(1,todo_mask),
                                     vector<string>(1,strtodo),
                                     ctx,
                                     logger );
          logger.moduleAdded( sid );
          counter++;
        }
        if (counter)
          sout << endl;
        ctx.sout( sout );
        /* clean up */
        devconn.freeResult();
      }

      if ( counter )
        return;
      else {
        ostringstream sout;
        sout << "Query FEDB without success.\n";
        ctx.sout( sout );
      }
    }

    // fallback to dim
    ostringstream sout;
    sout << "Using DIM." << endl;
    br.getServices((char*)"*/RobSlot");
    while(br.getNextService(name,format)) {
      DimCurrentInfo rob(name,(char*)"");
      if(string(rob.getString()).find(spec)==0) {
	for(int pos=0;name[pos];name[pos]=='/'?name[pos]=0:pos++);
        unsigned long sid = strtoul(name,NULL,0);
        module_[sid] = new Module( sid,
                                   name,
                                   command,
                                   vector<int>(1,todo_mask),
                                   vector<string>(1,strtodo),
                                   ctx,
                                   logger );
        logger.moduleAdded( sid );
      }
    }
    ctx.sout( sout );
    return;
  }
  


  // check, if it's a FE machine
  br.getServices("*/Restart");
  while(br.getNextService(name,format)) {
    string host(name);
    host=host.substr(0,host.find('/'));
    if(host.find(spec)==0) {
      br.getServerServices((char*)host.c_str());
      while(br.getNextServerService(name,format)) {
	if(atoi(name)) {
	  for(int pos=0;name[pos];name[pos]=='/'?name[pos]=0:pos++);
          unsigned long sid = strtoul(name,NULL,0);
          module_[sid] = new Module( sid,
                                     name,
                                     command,
                                     vector<int>(1,todo_mask),
                                     vector<string>(1,strtodo),
                                     ctx,
                                     logger );
          logger.moduleAdded( sid );
	}
      }
    }
  }
}

string
ModuleContainer::strSrcIDs( bool with_minors )
{
  string ids;
  bool is_first = true;
  for ( map<unsigned long, Module *>::const_iterator it=module_.begin();
        it!=module_.end();
        ++it ) {
    if ( !is_first )
      ids += ", " + it->second->source;
    else {
      ids = it->second->source;
      is_first = false;
    }
    if ( with_minors ) {
      for ( unsigned int i=0; i<it->second->minor_module.size(); ++i ) {
        ids += ", " + it->second->minor_module[i]->source;
      }
    }
  }
  return ids;
}

int
ModuleContainer::findModuleOptions()
{
  int rv=0;
  string ids = strSrcIDs();
  string query = "SELECT MODULE_source_ID, LOAD_A FROM MODULE "
    "WHERE MODULE_source_ID IN (" + ids + ") AND "
    "LENGTH(LOAD_A) AND "
    "Version_tag = 'latest' AND Type<>14 "
    "ORDER BY MODULE_source_ID";
  //cout << "query: " << query.c_str()<< endl;
  //"State = 1 "
  //"order by MODULE_source_ID";
  if ( (rv=devconn.query( query )) )
    return rv;

  while ( devconn.fetchRow() ) {

    if ( devconn.row[0] ) {
      unsigned long sid = strtoul( devconn.row[0], NULL, 0 );
#ifdef DEBUG_RUN
      ostringstream sout;
      sout << sid << ">>>" << devconn.row[1] << "<<<" << endl << flush;
      ctx.sout( sout );
#endif
      vector<int> opts;
      vector<string> str_opts;
      if ( (rv=co.parseToDoMasks( devconn.row[1], opts, str_opts )) )
        return rv;
      if ( opts.size() ) {
        module_[sid]->todo = opts;
        module_[sid]->strtodo = str_opts;
      }
    }
  }
  /* clean up */
  devconn.freeResult();
  return 0;
}

int
ModuleContainer::findMinorModulesAndOptions()
{
  int rv=0;
  string ids = strSrcIDs();
  string query = "SELECT MODULE_source_ID, Minor_modules, LOAD_A_minors FROM MODULE "
    "WHERE MODULE_source_ID IN (" + ids + ") AND "
    "LENGTH(Minor_modules) AND "
    "Version_tag = 'latest' "
    "ORDER BY MODULE_source_ID";
  //"Version_tag = 'latest' and "
  //"State = 1 "
  if ( (rv=devconn.query( query )) )
    return rv;

  while ( devconn.fetchRow() ) {

    if ( ! devconn.row[0] )
      continue;
    unsigned long sid = strtoul( devconn.row[0], NULL, 0 );
    vector<string> minor_source;
    if ( devconn.row[1] != NULL ) {
      minor_source = split( devconn.row[1], ' ' );
    }
#ifdef DEBUG_RUN
    ostringstream sout;
    if ( devconn.row[1] )
      sout << sid << ", minors: " << devconn.row[1] << endl;
    if ( devconn.row[2] )
      sout << sid << ", minor_opts: " << devconn.row[2] << endl;
    ctx.sout( sout );
#endif
    vector<int> minor_opts;
    vector<string> strminor_opts;
    if ( devconn.row[2] != NULL ) {
      if ( (rv=co.parseToDoMasks( devconn.row[2], minor_opts, strminor_opts )) )
        return rv;
    }
    if ( minor_opts.size() ) {
      module_[sid]->minor_todo = minor_opts;
      module_[sid]->strminor_todo = strminor_opts;
    }
    for ( unsigned long i=0; i<minor_source.size(); ++i ) {
      unsigned long msid = strtoul( minor_source[i].c_str(), NULL, 0 );
      module_[sid]->minor_module.push_back( new Module( msid,
                                                        minor_source[i],
                                                        module_[sid]->command,
                                                        module_[sid]->todo,
                                                        module_[sid]->strtodo,
                                                        ctx,
                                                        logger,
                                                        true ) );
      logger.moduleAdded( msid );
    }
  }
  /* clean up */
  devconn.freeResult();
  return 0;
}

int
ModuleContainer::findModuleType()
{
  int rv=0;
  if ( (rv=devconn.query( "SELECT dropdown FROM METATABLE WHERE tablename='MODULE' AND colname='Type'" )) )
    return rv;

  devconn.fetchRow();
  if ( !devconn.row[0] )
    return 10;
  vector<string> vtypes = split( devconn.row[0], ',' );
  map<string,string> types;
  for ( unsigned int i=1; i<vtypes.size(); ++i ) {
    vector<string> vtype = split( vtypes[i], ':' );
    if ( vtype.size() == 2 )
      types[vtype[0]] = vtype[1];
  }
#ifdef DEBUG_RUN
  ostringstream sout;
  for ( map<string,string>::const_iterator it=types.begin();
        it!=types.end();
        ++it )
    sout << it->first << ":" << it->second << ", ";
  sout << endl;
  ctx.sout( sout );
#endif

  string ids = strSrcIDs( true );
  string query = "SELECT MODULE_source_ID, Type, Detector FROM MODULE "
    "WHERE MODULE_source_ID IN (" + ids + ") AND "
    "Version_tag = 'latest' "
    "ORDER BY MODULE_source_ID";
  if ( (rv=devconn.query( query )) )
    return rv;

  while ( devconn.fetchRow() ) {

    if ( ! devconn.row[0] )
      continue;
    unsigned long sid = strtoul( devconn.row[0], NULL, 0 );
    string strmodtype;
    string strdet;
    if ( devconn.row[1] ) {
      if (types.find(devconn.row[1]) != types.end())
        strmodtype = types[devconn.row[1]];
    }
    if ( devconn.row[2] ) {
      strdet = devconn.row[2];
    }
    if (module_.find(sid) != module_.end()) {
      module_[sid]->module_type = strmodtype;
      module_[sid]->detector = strdet;
    }
    for ( map<unsigned long, Module *>::iterator it=module_.begin();
          it!=module_.end();
          ++it ) {
      for ( unsigned int j=0; j<it->second->minor_module.size(); ++j ) {
        if ( it->second->minor_module[j]->sid == sid ) {
          it->second->minor_module[j]->module_type = strmodtype;
          it->second->minor_module[j]->detector = strdet;
        }
      }
    }
#ifdef DEBUG_RUN
    ostringstream sout;
    A
    if ( devconn.row[1] )
      sout << sid << ", ModuleType: " << devconn.row[1] << "(" << strmodtype << ")" << endl;
    if ( devconn.row[2] )
      sout << sid << ", Detector: " << strdet << endl;
    ctx.sout( sout );
#endif
  }
  return 0;
}

// start_routine for pthread_create
void* startThread( void *ptx )
{
  if ( !ptx )
    return 0;
  ThreadContext &tx = *(ThreadContext *)ptx;
  auto_ptr<ThreadContext> tx_destructor( &tx );

  Module      &m   = tx.module();

  // ExecContext &ctx = tx.execContext();

  int overall_todo = 0;
  if ( m.todo.size() )
    {
      for ( unsigned int i=0; i<m.todo.size(); ++i )
	{
	  if (m.useDirectConn)
	    {
	      //do not use dim, connect direct
	      if (m.module_type == DTBP_TYPE_NAME && m.todo[0]==GS_DO_ALL)
		{

		  //using builtin connection class for dtbp
		  DTBPCommunicator *com = new DTBPCommunicator ( tx.module(),
								 tx.moduleContainer(),
								 tx.execContext(),
								 tx.commandLogger());
		  tx.commandLogger().moduleCommandStart(  m.sid,
							  m.module_type,
							  m.detector,
							  m.strtodo[0]);
		  //for resolving the nested macros
		  tx.moduleContainer().macros->expandMacro (m.configuration);
		  int state = SuccessState;
		  //use hostname resolving here (last param)
		  if (!com->connectToDevice(m.vmeHostName, m.port, false))
		    {
		      if (!com->authenticate())
			{
			  if (!com->setMode(htonl (DTBP_MODE_PROGRAM_REGISTER)))
			    {
			      //PROGRAM REGISTERS
			      //cout << m.configuration << endl;
			      vector<string> eqns = split (m.configuration, ' ');
		      
			      for (unsigned int i = 0;i < eqns.size ();i++)
				{
				  vector<string> eqn = split (eqns[i], '=');
				  //cout << "Programming at " << eqn[0] << " value " << eqn[1]<<endl;
				  if ( com->sendData ( tx.moduleContainer().macros->stringToInt (eqn[0]),
						       tx.moduleContainer().macros->stringToInt (eqn[1]),
						       1) )
				    {
				      state = FailureState;
				    }
				}	    
			      m.error = false;                      
			    }
			  else
			    {
			      m.error = true;
			      state = FailureState; 
			    }
			}
		    }
		  //run destructor (disconnect)
		  delete com;
		  tx.commandLogger().moduleCommandFinish(m.sid, state,
							 (char*)"DTBP loading",(char*)"",(char*)"");
		}
	    }
	  else
	    {
	      m.sendCommand( m.todo[i], m.strtodo[i] );
	      m.wait();
	      m.finish();
	    }
	  overall_todo |= m.todo[i];
	}
    }
  // load minor modules sequentially after major loading if firmware was loaded
  if ( !m.is_minor &&
       (overall_todo & GS_LOAD_FW) &&
       m.minor_module.size() )
    {
      const vector<int> *minor_todo;
      const vector<string> *strminor_todo;
      
      if ( m.minor_todo.size() )
	{
	  minor_todo = &(m.minor_todo);
	  strminor_todo = &(m.strminor_todo);
	}
      else
	{
	  minor_todo = &(m.todo);
	  strminor_todo = &(m.strtodo);
	}
    for ( unsigned int i=0; i<minor_todo->size(); ++i )
      {
	for ( unsigned int j=0; j<m.minor_module.size(); ++j )
	  {
	    m.minor_module[j]->sendCommand( (*minor_todo)[i], (*strminor_todo)[i] );
	    m.minor_module[j]->wait();
	    m.minor_module[j]->finish();
#ifdef DEBUG_RUN
	    ostringstream sout;
	    sout << "  2 minor SrcID: " << m.minor_module[j]->sid
		 << ", todo: " << (*minor_todo)[i]
		 << ", opts: " << (*strminor_todo)[i] << endl;
	    ctx.sout( sout );
#endif
	  }
      }
    }
  return NULL;
}

int ModuleContainer::runCommand()
{
  pthread_t children[512];
  int child = 0;
  map<unsigned long, Module *>::reverse_iterator it;
  for( it=module_.rbegin(); it!=module_.rend(); ++it )
    {
    if( is_sequential ||
        (it->first>1023 && it->first<2000) )
      {
	//verbose=1;
	//if( it->first>1023 && it->first<2000 ) {
	//  MutexLock scr_lock( ctx.screenoutMutex() );
	//	cout << "loading TCS component:" << endl;
	//}
	startThread( new ThreadContext( *(it->second), *this, ctx, logger ) );
#ifdef DEBUG_RUN
	ostringstream sout;
	sout << "1 SrcID: " << it->second->sid << endl;
	ctx.sout( sout );
#endif
	//verbose=0;
      }
    else
      {
	pthread_create( children+child++,
			NULL,
			startThread,
			new ThreadContext( *(it->second), *this, ctx, logger ) );
#ifdef DEBUG_RUN
	ostringstream sout;
	sout << "2 SrcID: " << it->second->source << endl;
	ctx.sout( sout );
#endif
      }
    }
  // wait for children
  while(child)
    {
      pthread_join(children[--child],NULL);
    }

  return 0;
}

/*****************************************************************************/
/**
 * DTBP Cumunicator class
 *
 * Wrapper class for c-style socket funcs to directly communicate with card
 * provide authentication and mode selection, send fgpa register programming cmds
 *
 * @author Julian Harms
 * @version 1.0
 */

/**
 * Constructor
 *
 * nothing special
 * @param Module Reference to Module
 * @param ModuleContainer Reference to ModuleContainer
 * @param ExecContext Reference to ExecContect
 * @param CommandLogger Reference to CommandLogger
 */
DTBPCommunicator::DTBPCommunicator( Module          &Mod,
                                    ModuleContainer &ModContainer,
                                    ExecContext     &Context,
                                    CommandLogger   &Logger)
  : m(Mod), mc(ModContainer), ctx(Context), logger(Logger)
{
  ;
}

/**
 * Connect to Device
 *
 * Connects to the device given by address on tcp port port. If noDNS is true, device has to be a ip-address, otherwise the method will try to resolve the hostname
 *
 * @param address Hostname / Address of peer
 * @param port TCP Port for connection
 * @param noDNS Wheter to use DNS or not
 * @return 0 on success, otherwise non zero
 */
int DTBPCommunicator::connectToDevice(string address, string port, bool noDNS)
{
  ostringstream sout, sout2;
  sout2<<"Connecting to "<<address<<" on port "<<port<<endl;
  //ctx.sout(sout2);
  
  if (noDNS)
    {
      //open socket directly
      if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
	{
	  sout<<"failed to create socket"<<endl;
	  ctx.serr(sout);
	  return 1;
	}

      //note: convert strings to c-like strings with c_str()
      struct sockaddr_in ServAddr;
      memset(&ServAddr, 0, sizeof(ServAddr));
      ServAddr.sin_family      = AF_INET;
      ServAddr.sin_addr.s_addr = inet_addr(address.c_str());
      ServAddr.sin_port        = htons(strtoul(port.c_str(), NULL, 0));

      if (connect(sock, (struct sockaddr *) &ServAddr, sizeof(ServAddr)) < 0)
	{
	  sout<<"could not connect"<<endl;
	  ctx.serr(sout);
	  return 2;
	}
      ostringstream sout;
      //sout<<"Connected to "<<address.c_str()<<" on port "<<port<<endl;
      //ctx.sout(sout);
      return 0;
    }
  else
    {
      struct addrinfo hints;
      struct addrinfo *result, *rp;
      char buffer[20];
      int s;

      memset(&hints, 0, sizeof(struct addrinfo));
      hints.ai_family = AF_UNSPEC;    /* Allow IPv4 or IPv6 */
      hints.ai_socktype = SOCK_STREAM;
      hints.ai_flags = 0;
      hints.ai_protocol = IPPROTO_TCP;
      
      s = getaddrinfo(address.c_str(), port.c_str(), &hints, &result);
      if (s != 0)
	{
	  sout<<"could not connect (2)"<<endl;
	  ctx.serr(sout);
	  return 2;
	}

      for (rp = result; rp != NULL; rp = rp->ai_next)
	{
	  //try for each result to connect
	  sock = socket( rp->ai_family, rp->ai_socktype,
			 rp->ai_protocol);
	  if (sock == -1)
	    continue;
	  buffer[0] = 0;
	  if (connect(sock, rp->ai_addr, rp->ai_addrlen) != -1) //connection was successfull	   
	    inet_ntop (rp->ai_family, &(((struct sockaddr_in *)rp->ai_addr)->sin_addr), buffer, sizeof (buffer));

	  string ip = string(buffer);
	  //sout<<"Connected to "<<ip<<" on port "<<port<<endl;
	  ctx.sout(sout);
	  break;
	}

      if (rp == NULL)
	{
	  sout<<"could not connect (3)"<<endl;
	  ctx.serr(sout);
	  return 3;
	}

      freeaddrinfo(result);
      return 0;
    }
}

/**
 * Provide Authentication
 *
 * Send password to peer after connection, PW is hardcoded in DTBP_PASSWD (Aux.h)
 *
 * @return zero on success
 */
int DTBPCommunicator::authenticate()
{
  char password[12] = DTBP_PASSWD;
  int readback;
  int bytesRcvd ;
  char retstring[12];
  
  readback=send(sock, password, sizeof (password), MSG_DONTWAIT);
  if ( readback == -1 )
    {
      ostringstream sout;
      sout << "TBPCommunicator::authenticate() : Encounter error during send() : " 
	   << strerror ( errno ) << " (" << errno << ")\n";
      ctx.serr(sout);
      return 1;    
    }
  else if(sizeof (password) !=readback)
    {
      ostringstream sout;
      sout<<"send() sent a different number of bytes than expected "<<readback<<endl;
      ctx.serr(sout);
      return 1;
    }
  bytesRcvd = recv(sock, retstring, sizeof (retstring), 0);
  if(bytesRcvd<=0)
    {
      ostringstream sout;
      sout<<"recv() failed or connection closed prematurely"<<endl;
      ctx.serr(sout);
      return 2;
    }
  return 0;
}

/**
 * Set Connection mode
 *
 * Board is able to do serveral things ofer interface: flash programming, network settings, register setting ...
 *
 * @param mode Connection mode, see constants in Aux.h
 * @return zero on success
 */
int DTBPCommunicator::setMode(int mode)
{
  int readback;
  int bytesRcvd ;
  char retstring[12];
  
  readback=send(sock, &mode, 128, MSG_DONTWAIT);
  if(readback < 128)
    {
      ostringstream sout;
      sout<<"send() failed or connection closed prematurely"<<endl;
      ctx.serr(sout);
      return 3;
    }
  bytesRcvd = recv(sock, retstring, sizeof (retstring), 0);
  if(bytesRcvd<=0)
    {
      ostringstream sout;
      sout<<"recv() failed or connection closed prematurely"<<endl;
      ctx.serr(sout);
      return 3;
    }

  return 0;
}

/**
 * Send com-data
 *
 * Send address, value in mode mode to device
 *
 * @param address Address of register
 * @param value Value of register
 * @param mode Sending mode
 * @return zero on success
 */
int DTBPCommunicator::sendData(int address, int value, int mode)
{
  int readback;
  int bytesRcvd;
  unsigned int h[2];
  send_data data;
  ostringstream sout;

  data.address = htonl ( address );
  data.value = htonl ( value );
  data.mode = htonl ( mode );

  sout <<" Write   : Address = 0x"<< hex  << (unsigned int) ntohl(data.address)
       <<" , Value = 0x"<< ntohl(data.value)
       << " , Mode = 0x" << ntohl(data.mode) << dec << endl;
  unsigned int error = 0;
  readback = send(sock, &data, sizeof (data), MSG_DONTWAIT);
  if(readback != sizeof (data))
    {
      sout << "sendData: send() error: " << strerror(errno) << endl;
      error = 3;
    }
  if(error == 0)
    {
      bytesRcvd = recv(sock, &h, 8, 0);
      if(bytesRcvd != 8)
	{
	  sout << "sendData: recv() error: " << strerror(errno) << endl;
	  error = 3;
	}
      if(error == 0)
	error=htonl(h[0]);  
    }
  unsigned int warn = 0;
  if(error==0)
    {
      if ( h[1] != data.value )
	warn = 1;
      sout<<" Readback: Address = 0x"<< hex  << (unsigned int) ntohl(data.address)
	  <<" , Value = 0x"<<ntohl(h[1])<<dec<<endl;
    }
  else if(error==1)
    {
      sout<<"Error access to undefined register "<<ntohl(data.address)<<endl;
    }
  else if(error==2)
    {
      sout<<"Error writing to read only register "<<ntohl(data.address)<<endl;
    }
  else
    {
      sout<<"Unknown error"<<endl;
    }
  if ( error || warn )
    ctx.sout(sout);
  return (error<<16)+warn;
}

/**
 * Destructor
 *
 * Don't forget to close socket
 */
DTBPCommunicator::~DTBPCommunicator()
{
  //ostringstream sout;
  //sout<<"Connection closed"<<endl;
  //ctx.sout(sout);
  close (sock);
}

/*****************************************************************************/
/**
 * Macro-Class
 *
 * Reads all macros from db and parses strings with macros
 * Provides stringToInt for 0x hex-notation
 *
 * @author Julian Harms
 * @version 1.0
 */

/**
 * Constructor
 *
 * reads all macros from db (needs opened DBConnection)
 *
 * @param CoOpts Reference to CommandOptions
 * @param DevConn Reference to DBConnection
 * @param Context Reference to ExecContext
 * @param Logger Reference to Logger
 */
Macro::Macro(     CommandOptions &CoOpts,
                  DBConnection   &DevConn,
                  ExecContext    &Context,
                  CommandLogger  &Logger )
  : co(CoOpts),
    devconn(DevConn),
    ctx(Context),
    logger(Logger)
{
  int rv=0;
  if ( (rv=devconn.query( "SELECT Name, Expansion FROM MACROS WHERE Version_tag='latest'" )) )
    return;

  while (devconn.fetchRow())
    {
      if (devconn.row[0])
	{
	  macros[devconn.row[0]] = devconn.row[1];
	}
    }
/*#ifdef DEBUG_RUN
  ostringstream sout;
  for ( map<string,string>::const_iterator it=macros.begin();
        it!=macros.end();
        ++it )
    sout << it->first << ":" << it->second << ", ";
  sout << endl;
  ctx.sout( sout );
#endif*/
}


/**
 * expands Macros in given string
 *
 * @see Code in other file of this project, copied from there
 * @param s Reference to string to parse
 * @return true if macros were replaced in string
 */
bool Macro::expandMacro(string& s)
{
  bool changed=false;
  int nesting=0;
  string toplevel_macro;
  ostringstream sout;
  
  unsigned int pos=s.find("${");
  while(pos<string::npos)
    {
      unsigned int end=s.find('}',pos);
      if(end==string::npos)
	{
	  sout<<"mismatched braces in macro expansion of "<<s<<endl;
	  ctx.serr(sout);
	  return false;
	}
      // get the macro's name from between "${" and "}"
    
      string macro=s.substr(pos+2,end-pos-2);
      if(!nesting)
	{
	  toplevel_macro=macro;
	}
      // if the macro is not found, expansion is ""


      string expansion=macros[macro];
      s.replace(pos,end-pos+1,expansion);
      changed=true;

      unsigned int newpos=s.find("${",pos);
      if(newpos==pos)
	{
	  // nesting of macros is allowed, but after 10 iterations some
	  // some progress has to be made.
	  if(++nesting==10)
	    {
	      sout<<"nesting too deep when expanding "<<toplevel_macro<<endl;
	      ctx.serr(sout);
	      return false;
	    }
	}
      else
	{
	  nesting=0;
	  pos=newpos;
	}
    }
  return changed;
}

/**
 * Converts string to int
 *
 * Supports '123' for decimal and '0x123' for hex-notation
 *
 * @param s String to convert
 * @return Result (unsigned int)
 */
unsigned int Macro::stringToInt (string s)
{
  unsigned int ret;
  stringstream ss;
  
  //get number base
  if (s.find ("0x") != string::npos)
    {
      //hex
      s.replace (s.find ("0x"), 2, "");
      ss<<std::hex<<s;
    }
  else
    {
      //decimal
      ss<<s;
    }
  ss>>ret;
  return ret;
}

