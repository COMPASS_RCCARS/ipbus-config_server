################################################################
#                        where's what                          #
################################################################

# when checking out a copy please don't touch these, instead create symlinks
# to the respective directories with these names

DIM__PATH = ${DIM_PATH}
UHAL_PATH = ${CACTUS_PATH}

HOSTNAME = $(shell hostname -s)

WORKDIR = $(shell pwd)

PROGRAMS = dimclient LOAD restarter config_server

UHAL_INC = -I$(UHAL_PATH)/include -I$(UHAL_PATH)/include/boost
UHAL_LIB = -L$(UHAL_PATH)/lib -lboost_system -lcactus_uhal_uhal -lcactus_uhal_log -lboost_regex

MYSQL_INC = -I/usr/include/mysql++ $(shell mysql_config --cflags)
MYSQL_LIB = -L$/usr/lib64 -Wl,-rpath,/usr/lib64 -lmysqlpp $(shell mysql_config --libs)

DIM_INC = -I$(DIM__PATH)/dim
DIM_LIB = -L$(DIM__PATH)/linux -Wl,-rpath,$(DIM__PATH)/linux -ldim -lpthread

WORK_INC = -I$(WORKDIR)/include
WORK_LIB = -L$(WORKDIR)/lib

INSTALL_PATH_BIN = $(INSTALL_PATH)/bin
INSTALL_PATH_DOC = $(INSTALL_PATH)/doc

DEBUG_FLAGS = -g

################################################################
#        you should not need to modify anything below          #
################################################################

SOURCES = $(wildcard src/*.cc)
HEADERS = $(wildcard include/*.h)
INCLUDES = $(MYSQL_INC) $(DIM_INC) $(UHAL_INC) $(WORK_INC)
LIBS = $(MYSQL_LIB) $(DIM_LIB) $(UHAL_LIB) $(WORK_LIB)

#CFLAGS = $(DEBUG_FLAGS) -Wall -std=c++11 $(INCLUDES)
CFLAGS = $(DEBUG_FLAGS) -Wall -std=c++0x $(INCLUDES)

OBJECTS := main.o Module.o wait.o int2string.o db_entry.o IpbusModule.o IpbusGemAdcCard.o IpbusTdcCard.o IpbusFeMux.o \
	IpbusCedarTdcCard.o IpbusMwpcTdcCard.o IpbusTcsFpga.o IpbusTcsController.o tcsOpt.o IpbusDevice.o Si5338.o \
	IpbusPrescalerA7.o IpbusSpTimeTdcCard.o
OBJECTS := $(addprefix obj/,$(OBJECTS))
PROGRAMS := LOAD config_server dimclient restarter fdb_interface
PROGRAMS := $(addprefix bin/,$(PROGRAMS))
PROGRAMS := $(OBJECTS) $(PROGRAMS)

vpath %.h include
vpath %.cc src

all: ./obj ./bin $(PROGRAMS)

#always build main.cc to have correct version built time in the debug output
touch:
	touch src/main.cc

show:
	@echo $(OBJECTS)
	@echo "HOSTNAME: $(HOSTNAME)"
	@echo "LIBS: $(LIBS)"
	@echo "INCLUDES: $(INCLUDES)"
	@echo "OS name: $(SYS_NAME)"
	@echo "ALL: $(PROGRAMS)"
	@echo "Sources: $(SOURCES)" 

clean:
	rm -f $(PROGRAMS) obj/*.o depend.mk

# for emacs / xemacs
%macs:
	$@ $(SOURCES) $(HEADERS) $(INSTALL_S) Makefile &

tar:
	tar cvzf ../config_server.tgz -C .. config_server

%.tgz:
	tar cvzf $@ -C .. config_server

depend.mk:	include/*.h
	@echo "Create dependencies file"
	g++ $(CFLAGS) $(SOURCES) -MM > depend.mk

bin/config_server: $(OBJECTS)
	g++ $(DEBUG_FLAGS) -o $@ $(OBJECTS) $(LIBS)

bin/dimclient: src/dimclient.cc
	g++ $(CFLAGS) -o $@ $< $(DIM_LIB)

bin/LOAD: src/LOAD.cc obj/Aux.o obj/wait.o obj/int2string.o
	g++ $(CFLAGS) -o $@ $^ -L/usr/lib64 -Wl,-rpath,/usr/lib64 -lmysqlpp -L/usr/lib64/mysql \
	-lmysqlclient -lpthread -lz -lm -ldl -L$(DIM__PATH)/linux -Wl,-rpath,$(DIM__PATH)/linux -ldim

bin/MONITOR: src/MONITOR.cc lib.$(SYS_NAME)/wait.o lib.$(SYS_NAME)/Module.o
	g++ $(CFLAGS) -o $@ $^ $(LIBS)

bin/restarter: src/restarter.c
	gcc $(CFLAGS) -L/usr/lib64 -o $@ $<

bin/fdb_interface:  src/file_db_interface.cc
	g++ $(CFLAGS) -o $@ $^ $(LIBS)

obj/%.o: %.cc depend.mk
	g++ $(CFLAGS) -c $< -o $@

./obj:
	mkdir ./obj

./bin:
	mkdir ./bin
