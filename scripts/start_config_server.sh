#!/bin/bash

export DB_CLIENT=`hostname -s`
export DEBUG_CONFIG_SERVER=1

[[ $- == *i* ]] && echo "DB_CLIENT=$DB_CLIENT"

if [ $DB_CLIENT == "pcamma01" ]; then
    # set some environment so that config_server finds its way
    export DB_SERVER=dbod-amber-01
    export DB_DB=devdb
    export DB_USER=daq
    export DB_PASSWD=na66daq
    export DB_PORT=5506
    export DIM_DNS_NODE=pcamma01
    export DIM_DNS_NODE_CS=pcamma01
    export LD_LIBRARY_PATH=/online/soft/opt/dim/latest/linux:/online/soft/opt/cactus/latest/lib:$LD_LIBRARY_PATH

    BIN_DIR=/online/soft/IPBUS/ipbus-config_server/bin

    if test -z "`pgrep -f restarter.*config_server`"; then
	pkill config_server
	cd /tmp/
	echo "Start config_server"
	$BIN_DIR/restarter $BIN_DIR/config_server </dev/null &>/tmp/config_server.log &
    fi
elif [ $DB_CLIENT == "pccore15" ]; then
    # set some environment so that config_server finds its way
    export DB_SERVER=pccodb00
    export DB_DB=devdb
    export DB_USER=daq
    export DB_PASSWD=na58daq
    export DIM_DNS_NODE=pccofs00
    export LD_LIBRARY_PATH=/online/daq/lib:/online/soft/lib:/opt/cactus/lib:$LD_LIBRARY_PATH

    BIN_DIR=/online/soft/IPBUS/ipbus-config_server/bin

    if test -z "`pgrep -f restarter.*config_server`"; then
	pkill config_server
	cd /online/tmp/
	echo "Start config_server"
	$BIN_DIR/restarter $BIN_DIR/config_server </dev/null &>/online/tmp/config_server.$DB_CLIENT.log &
    fi
elif [ $DB_CLIENT == "pccore11" ]; then
    # set some environment so that config_server finds its way
    export DB_SERVER=pccodb00
    export DB_DB=devdb
    export DB_USER=daq
    export DB_PASSWD=na58daq
    export DIM_DNS_NODE=pccofs00
    export LD_LIBRARY_PATH=/online/daq/lib:/online/soft/lib:/opt/cactus/lib:$LD_LIBRARY_PATH

    BIN_DIR=/online/soft/IPBUS/ipbus-config_server/bin

    if test -z "`pgrep -f restarter.*config_server`"; then
	pkill config_server
	cd /online/tmp/
	echo "Start config_server"
	$BIN_DIR/restarter $BIN_DIR/config_server </dev/null &>/online/tmp/config_server.$DB_CLIENT.log &
    fi
elif [ $DB_CLIENT == "pccore12" ]; then
    # set some environment so that config_server finds its way
    export DB_SERVER=pccodb00
    export DB_DB=devdb
    export DB_USER=daq
    export DB_PASSWD=na58daq
    export DIM_DNS_NODE=pccofs00
    export LD_LIBRARY_PATH=/online/daq/lib:/online/soft/lib:/opt/cactus/lib:$LD_LIBRARY_PATH

    BIN_DIR=/online/soft/IPBUS/ipbus-config_server/bin

    if test -z "`pgrep -f restarter.*config_server`"; then
	pkill config_server
	cd /online/tmp/
	echo "Start config_server"
	$BIN_DIR/restarter $BIN_DIR/config_server </dev/null &>/online/tmp/config_server.$DB_CLIENT.log &
    fi
elif [ $DB_CLIENT == "pccore01" ]; then
    # set some environment so that config_server finds its way
    export DB_SERVER=pccore01
    export DB_DB=devdb
    export DB_USER=daq
    export DB_PASSWD=na58daq
    export DIM_DNS_NODE=pccore01
    export LD_LIBRARY_PATH=/online/daq/lib:/online/soft/lib:/opt/cactus/lib:$LD_LIBRARY_PATH

    BIN_DIR=/online/soft/IPBUS/config_server/bin

    if test -z "`pgrep -f restarter.*config_server`"; then
	pkill config_server
	cd /online/tmp/
	echo "Start config_server"
	$BIN_DIR/restarter $BIN_DIR/config_server </dev/null &>/online/tmp/config_server.$DB_CLIENT.log &
    fi
elif [ $DB_CLIENT == "pcdmre01" ]; then
    # set some environment so that config_server finds its way
    export DB_SERVER=pcdmfs01
    export DB_DB=devdb
    export DB_USER=daq
    export DB_PASSWD=daq
    export DIM_DNS_NODE=pcdmfs01
    export LD_LIBRARY_PATH=/online/daq/lib:/online/soft/lib:/opt/cactus/lib:$LD_LIBRARY_PATH

    BIN_DIR=/online/soft/IPBUS/config_server/bin

    if test -z "`pgrep -f restarter.*config_server`"; then
	pkill config_server
	cd /online/tmp/
	echo "Start config_server"
	$BIN_DIR/restarter $BIN_DIR/config_server </dev/null &>/online/tmp/config_server.$DB_CLIENT.log &
    fi
fi
