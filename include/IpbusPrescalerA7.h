#ifndef __IPBUS_PRESCALER_A7_H__
#define __IPBUS_PRESCALER_A7_H__

#include <stdint.h>
#include <string>
#include <pthread.h>
#include <dic.hxx>

#include "IpbusModule.h"

class IpbusPrescalerA7 : public IpbusModule
{
  std::string      tcs_name;
  pthread_t        spill_tid;
  pthread_mutex_t* spill_mutex;
  bool             isThreadRunning;
  bool             isConfigured;
  DimInfo*         tcsBurstInfo;
 protected:
  // checks for inconsistence of FE database; returns false in case of any error:
  bool check_db();
 public:
  IpbusPrescalerA7(int sourceID, std::string version);
  ~IpbusPrescalerA7();

  const std::string& get_tcs_name() const { return tcs_name; }

  virtual void     InitModule(int todo); // can throw Error
  virtual void     Reset(int);
  virtual void     ReadStatus(int) {};
  virtual void     Control(const char*);
  virtual uint32_t GetFirmwareID();
  
  int              GetTcsSpillStatus();
 private:
  virtual bool     set_module_config();
  bool             ReadDB(std::string filename);
  bool             SaveToDB(std::string filename);
  bool             WriteStatusDB();
  bool             LoadChannel(uint32_t channel, uint32_t divisor);
  bool             UpdateStatus();
  bool             ReadStatus(mysqlpp::Query& query, int* val);
  void lock_mutex()   { pthread_mutex_lock(spill_mutex);   }
  void unlock_mutex() { pthread_mutex_unlock(spill_mutex); }
  static void* spill_thread(void*);  
};

#define PRESCALER_CHANNELS       12
#define PRESCALER_CHANNELS_A7     8
#define PRESCALER_WAIT_INTERVAL 100

#endif
