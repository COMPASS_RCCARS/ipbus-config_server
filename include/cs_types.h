#ifndef _CS_TYPES_H_
#define _CS_TYPES_H_

#define MT_GESICA                   0
#define MT_CATCH                    1
#define MT_TCS                      2
#define MT_HOT_GESICA               3
#define MT_HOT_GESICA_SADC          4
#define MT_TCS_PRESCALER            5
#define MT_HOT_GESICA_RICH          6
#define MT_HOT_GESICA_MSADC         7
#define MT_HOT_GESICA_RW            8
#define MT_IPBUS_MUX                9
#define MT_HOT_GESICA_MSADC_MASTER 10
#define MT_HOT_GESICA_SGADC        11
#define MT_GANDALF                 13
//#define MT_GEMADC                  14
#define MT_TCS_PRESCALER2          15
#define MT_A7_TCS                  16
#define MT_UNKNOWN                100

enum module_type_t
  {
    _gesica                  = MT_GESICA,
    _catch                   = MT_CATCH,
    _tcs                     = MT_TCS,
    _hot_gesica              = MT_HOT_GESICA,
    _hot_gesica_sadc         = MT_HOT_GESICA_SADC,
    _tcs_prescaler           = MT_TCS_PRESCALER,
    _hot_gesica_rich         = MT_HOT_GESICA_RICH,
    _hot_gesica_msadc        = MT_HOT_GESICA_MSADC,
    _hot_gesica_rw           = MT_HOT_GESICA_RW,
    _ipbus_mux               = MT_IPBUS_MUX,
    _hot_gesica_msadc_master = MT_HOT_GESICA_MSADC_MASTER,
    _hot_gesica_sgadc        = MT_HOT_GESICA_SGADC,
    _gandalf                 = MT_GANDALF,
//    _gemadc                  = MT_GEMADC,
    _tcs_prescaler2          = MT_TCS_PRESCALER2,
    _a7_tcs                  = MT_A7_TCS,
  };

#define MS_UNKNOWN        0
// final status
#define MS_READY          1
#define MS_ERROR          2
#define MS_WARNING        3
#define MS_ERROR_WARNING  4
#define MS_INFO           5
// 'work in progress' status
#define MS_INITIALIZE    10
#define MS_RESET         11
#define MS_READSTATUS    12
#define MS_DBXS          13
#define MS_SLEEP         14
#define MS_ERROR_CONT    20
#define MS_WARN_CONT     21

#endif
