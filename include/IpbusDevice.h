#ifndef __IPBUS_DEVICE_H__
#define __IPBUS_DEVICE_H__

#include <stdint.h>
#include <pthread.h>
#include <string>
#include <vector>
#include <uhal/uhal.hpp>

class IpbusDevice
{
  uhal::ConnectionManager*   ipbus_manager;
  uhal::HwInterface*         hw_interface;
  pthread_mutex_t*           hw_mutex;
  bool                       isSlaveDevice;
 protected:
  bool                       isDummyModule;
 public:
  IpbusDevice();
  ~IpbusDevice();

  bool Init() { isDummyModule = true; return true; }
  bool Init(const char* connection_file_name, const char* interface_name);
  bool Init(uhal::ConnectionManager* manager, const char* interface_name);

  uhal::ConnectionManager*    GetIpbusManager()     { return ipbus_manager; }
  uhal::HwInterface*          GetHwInterface()      { return hw_interface;  }
  bool                        IsDummyModule() const { return isDummyModule; }
 public:
  // IPBUS I/O functions, throw IpbusError in case of problems:
  void ipbus_write(const char* node, const uint32_t value, bool verify = false);
  void ipbus_read(const char* node, uint32_t& data);
  void ipbus_write(std::string node, const uint32_t value, bool verify = false)
  {
    ipbus_write(node.c_str(),value,verify);
  }
  void ipbus_read(std::string node, uint32_t& data)
  {
    ipbus_read(node.c_str(), data);
  }
  void ipbus_write_block(const char* node, const std::vector< uint32_t >& aValues);
  void ipbus_read_block(const char* node, std::vector<uint32_t>& data, uint32_t size);
};

#endif
