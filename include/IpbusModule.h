#ifndef __IPBUS_MODULE_H__
#define __IPBUS_MODULE_H__

#include <uhal/uhal.hpp>
#include "Module.h"
#include "IpbusDevice.h"
#include <map>

class IpbusModule;

class IpbusFECard : public IpbusDevice
{
 public:
  enum FECardType
  {
    UNKNOWN, TCS_CONTROLLER, MWPC, MWPC_T0, GEMADC, CEDAR_16CH, CEDAR_32CH, CEDAR_T0,
    SPEAKING_TIME_TDC, TRIGGER, SCI_FI
  };
 private:
  IpbusModule*       module;
  FECardType         card_type;
  std::string        card_name;
  int                port;
 public:
  IpbusFECard(FECardType cardType, const char* name, IpbusModule* parent, int _port);
  virtual ~IpbusFECard() { ; }

  IpbusModule*       GetModule()                      { return module;                  }
  FECardType         GetCardType()              const { return card_type;               }
  const char*        GetCardName()              const { return card_name.c_str();       }
  int                GetPort()                  const { return port;                    }

  virtual void ReadStatus(std::string& status_info) = 0;
  virtual void set_registers();
};

class IpbusModule : public Module, public IpbusDevice
{
  pthread_mutex_t*           hw_mutex;
  std::map<int,IpbusFECard*> frontends;
  std::map<std::string,std::string> config;
  uint64_t                   status;
  pthread_t                  owner;
  uint32_t                   firmware_id;
 public:
  IpbusModule(int sourceID, std::string version);
  virtual ~IpbusModule();

  std::map<int,IpbusFECard*>& GetFECards()          { return frontends;           }
  uint32_t                    GetFwID()       const { return firmware_id;         }
  std::map<std::string,std::string>& GetConfig()    { return config;              }
  std::list<DatabaseEntryFrontend>& GetFrontendDB() { return module_db.frontends; }
 public:

  // Main DIM control functions, overload corresponding virtual functions of Module class:
  void Initialize(int);
  virtual void Reset(int);
  virtual void ReadStatus(int);
  virtual void Control(const char*);

  bool check_status(uint64_t state) { return ((status&state) != 0); }
  void set_status(uint64_t state)   { status |= state; }
  void drop_status(uint64_t state)  { status &= ~state; }

  void ShowError(std::string msg);
  void ShowWarning(std::string msg);
  void Sleep(uint64_t usec);
  void SendDot();
  ServiceMap& GetServiceMap()       { return dim; }
 private:
  void lock_hw();
  void unlock_hw();
  bool IpbusInit(bool force = false); // force if the module is not yet initialized
 protected:
  virtual void InitModule(int todo) { }
  virtual bool set_module_config();
  virtual bool check_db();           // refreshs db.module, checks connections.xml file
  virtual uint32_t GetFirmwareID();  // reads FW_ID from module; throws exceptions in case of IPBUS error
};

// IPBUS Module States:

#define IPBUS_MOD_INITIAL_STATE 0x0000000000000000ULL
#define IPBUS_MOD_ERROR_STATE   0x8000000000000000ULL
#define IPBUS_MOD_CONN_MANAGER  0x0000000000000001ULL
#define IPBUS_MOD_FEDB_OK       0x0000000000000002ULL
#define IPBUS_MOD_HW_INERFACE   0x0000000000000004ULL
#define IPBUS_MOD_HW_MUTEX      0x0000000000000008ULL
#define IPBUS_MOD_CREATED       0x0000000000000010ULL
#define IPBUS_MOD_HAS_FW_ID     0x0000000000000020ULL
#define IPBUS_MOD_INITIALIZED   0x0000000000000040ULL
#define IPBUS_A7_MOD_CREATED    0x0000000000000080ULL
#define IPBUS_A7_MOD_READY      0x0000000000000100ULL
#define IPBUS_A7_FEC_ADDED      0x0000000000000200ULL
#define IPBUS_A7_DESTROYING     0x0000000000000400ULL

// types of IPBUS FE Cards:
#define IPBUS_A7_TCS_CONTROLLER 0

#endif
