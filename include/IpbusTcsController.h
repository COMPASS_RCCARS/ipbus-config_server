#ifndef __IPBUS_TCS_CONTROLLER_H__
#define __IPBUS_TCS_CONTROLLER_H__

#include <stdint.h>
#include <pthread.h>
#include <unistd.h>
#include <string>
#include <map>

#include "IpbusModule.h"

#define MAX_REC                       256
#define MAX_CFG                        16
#define MAX_DAQS                        8
#define MAX_CAL_TRG                    26
#define MAX_PERIOD                    256

#define OFFSPILL_WAIT_INTERVAL      10000
#define OFFSPILL_WAIT_MAXCOUNT       6000

#define ONSPILL_MASK           0x00000010
#define BURST_NR_MASK          0x7ff00000
#define BURST_POWER                    20
#define EVENT_NR_MASK          0x000fffff
#define SPILL_STATUS_MASK      0x00fff111

#define RESET_FE_START         0x18800
#define RESET_FE_END           0x10800

class tcsOpt;
class IpbusTcsFpga;

class IpbusTcsController : public IpbusFECard
{
  friend class IpbusTcsFpga;
  struct ArtTriggers
  {
    int daqNr;
    int trigNr;
    int period;
    int mode;
  };
  IpbusTcsFpga*                     fpga;
  ServiceMap                        tcs_dim;
  std::map<std::string,std::string> config;
  std::map<std::string,uint32_t>    calib;
  tcsOpt*                           options;
  pthread_t                         spill_tid;
  pthread_mutex_t*                  spill_mutex;
  bool                              isThreadRunning;
  std::list<ArtTriggers>            art_triggers;

  uint32_t spillStatus[MAX_DAQS+1];     // Stores spill status and burst number
  uint32_t calTrigMode[MAX_CAL_TRG];    // modes of calibration triggers
  uint32_t calTrigPeriod[MAX_CAL_TRG];  // periods for each calibration triggers
  uint32_t tcsThrottle;                 // Throttle value to reduce spill
  volatile uint32_t spillcastnum;       // offspill cast number
 public:
  IpbusTcsController(const char* name, IpbusTcsFpga* parent);
  ~IpbusTcsController();

  void Control(const char*);
  void Configure();

 private:
  virtual void ReadStatus(std::string& status_info);

  void ReadConfigCalib();
  void UpdateServices();
  void ReadStatus(int daqNr);
  
  // Control commands:
  bool Init(bool wait_off_spill = true);
  bool ResetSpillCounter();
  bool Start(int daqNr, bool wait = true);
  bool Stop(int daqNr, bool wait = true);
  bool SuperStop(int daqNr);
  bool SuperStart(int daqNr);
  bool RapidStart(int daqNr);
  bool RapidStop(int daqNr);
  bool StartFast(int daqNr);
  bool Pause(int daqNr);
  bool Continue(int daqNr);
  bool Reset(bool wait = true);

  // Starting procedures:
  void ResetFrontends(int daqNr);
  void ActivatePretriggers(int daqNr);
  void SetDeadtime(int daqNr);
  void StartDaq(int daqNr);
  void StartController();
  
  // Spill/Trigger commands:
  int  SpillStatus();    // reads spill status from controller
  int  GetSpillStatus(); // gets preread (by spill thread) spill status
  int  BurstNumber(int daqNr);
  int  EventNumber(int daqNr);
  int  Burst();
  void Throttle(uint32_t throttle) { tcsThrottle = throttle; Throttle(); }
  void Throttle();
  void SetBurstStatus();
  void SetBurstNumber();
  bool StoreTrigger(int daqNr, int triggerNr, int period, int mode);
  bool FindTrigger(int daqNr, int triggerNr, int& period, int& mode);
  bool SendTrigger(int daqNr, int triggerNr, int period, int mode);
  bool WaitOffspill();

  void lock_mutex()   { pthread_mutex_lock(spill_mutex);   }
  void unlock_mutex() { pthread_mutex_unlock(spill_mutex); }
  static void* spill_thread(void*);
  // For debug purposes:
  void IgorStart();
};

// command code definitions
#define QUERY_COMMAND       '?'
#define SUPERSTART_COMMAND  'A'
#define BURST_COMMAND       'B'
#define CONTINUE_COMMAND    'C'
#define RESET_SPILL_COUNTER 'E'
#define STARTFAST_COMMAND   'F'
#define RAPIDSTART_COMMAND  'G'
#define RAPIDSTOP_COMMAND   'H'
#define THROTTLE_COMMAND    'L'
#define NUMBER_COMMAND      'N'
#define SUPERSTOP_COMMAND   'O'
#define PAUSE_COMMAND       'P'
#define START_COMMAND       'R'
#define STOP_COMMAND        'S'
#define TRIGGER_COMMAND     'T'
#define Q_TAG               'Q'
#define RESET_COMMAND       'Z'

// Some masks to analyze TCS receiver configuration words
#define REC_MAP_MASK         0xf0000
#define REC_MAP_CODE         0xd0000
#define REC_MAP_DAQ          0xff
#define REC_MAP_ID           0xff00
#define REC_MAP_POWER        8
#define REC_CFG_MASK         0xc0000
#define REC_CFG_CODE         0x80000
#define REC_CFG_ID           0x3fc00
#define REC_CFG_POWER        10
#define REC_DEL_MASK         0xf0000
#define REC_DEL_CODE         0xe0000
#define REC_DEL_ID           0xff00
#define REC_DEL_POWER        8
#define REC_RES_CODE         0xf0000
#define REC_RES_BITS         0x3
#define REC_RES_POWER        8

#define SW_VERSION "2021113000"

#endif
