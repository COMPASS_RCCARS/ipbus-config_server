#ifndef _MODULE_H_
#define _MODULE_H_

#include <stdint.h>
#include <stdio.h>
#include <pthread.h>

#include <string>
#include <map>
#include <exception>

#include <dis.hxx>
#include <dic.hxx>

#include "cs_types.h"
#include "db_entry.h"

// A private version of DimService that can easily be
// updated with operator= and stores its value internally.
// By default the updates are pushed to the clients, but
// this can be deactivated with setPull(). The cast operators
// are overloaded to have simple access to the stored value.

class MyDimService : public DimService
{
  int         i;
  float       f;
  double      d;
  std::string s;
  bool        push;

 public:
 MyDimService(char*name,int i_) : DimService(name,i),i(i_) {push=true;}
 MyDimService(char*name,float f_) : DimService(name,f),f(f_) {push=true;}
 MyDimService(char*name,double d_) : DimService(name,d),d(d_) {push=true;}
 MyDimService(char*name,char* s_) : DimService(name,s_),s(s_) {push=true;}

  void setPush() {push=true;}
  void setPull() {push=false;}
  void operator=(int i_) {i=i_;if(push) updateService(i);}
  void operator=(float f_) {f=f_;if(push) updateService(f);}
  void operator=(double d_) {d=d_;if(push) updateService(d);}
  void operator=(std::string s_) {s=s_;if(push) updateService((char*)s.c_str());}
  void operator+=(std::string s_)
    {if(s.size())s+="\n";s+=s_;if(push) updateService((char*)s.c_str());}

  operator int()        const {return i;}
  operator float()      const {return f;}
  operator double()     const {return d;}
  operator std::string()const {return s;}
};

// a map of MyDimService objects that allows easy updates:
//   ServiceMap dim(15);
//   dim["i:buh"]=12;
// creates a DimService with name "15/buh" and sets the value to 12
// After creation the type specifier ("i:" in the above example)
// has to be left out:
//   dim["buh"]=13;

class ServiceMap : public std::map<std::string,MyDimService*>
{
  char        p[10];
  std::string prefix;

 public:

  ServiceMap(int id) {snprintf(p,10,"%d/",id);prefix=p;}
  ServiceMap(std::string s) {prefix=s;}
  ~ServiceMap() {for(iterator it=begin();it!=end();++it) delete it->second;}
  MyDimService& operator[](std::string n);

  class no_type : public std::exception
  {
  public:
    const char* what()
    {return "type information needed when creating MyDimService";}
  };
};

// This class holds a DIM command that receives an (int) value.
// It calls the given function pointer for the object that is
// registered via the (Module*) parameter. Be careful to destroy
// it only when the service should die! And don't forget to do so
// before recreating a command with the same name.
// IMPORTANT: the trick with the function pointer does not work for
// virtual methods! This is the reason for the Initialize_() wrapper
// below.
// Two aspects of the behaviour are configurable:
//   setQueueMode informs the command that reentrant calls should be
//                queued instead of dropped
//   setCheckMode informs the command that virtual Module::check_db()
//                should be called before the execution of the actual
//                method

class Module;
class MyDimComm : public DimCommand
{
  pthread_mutex_t *mutex;
  void (*func_i)(Module*,int);
  void (*func_c)(Module*,const char*);
  Module *module;
  std::string name;
  bool queue_com,check_db;

  void commandHandler();
  static void* wrapper(void*);

 public:

  MyDimComm(std::string n,void(*f)(Module*,int),Module*m);
  MyDimComm(std::string n,void(*f)(Module*,const char*),Module*m);
  ~MyDimComm();
  void setQueueMode(bool q) {queue_com=q;}
  void setCheckMode(bool c) {check_db=c;}
};

// The module base class is not abstract for test reasons. It already
// handles much of the functionality: it reads the database, publishes
// common services and has default handlers for the required DIM commands
class Module
{
 private:

  int module_source_id;

 protected:

  DatabaseEntry         module_db;
  ServiceMap            dim;
  std::list<MyDimComm*> dimComm;
  std::string           module_name;

 public:

 Module(std::string name, std::string dim_prefix) :
  module_source_id(0),
    module_db(),
    dim(dim_prefix),
    module_name(name) {}
  Module(int sourceID, std::string version);
  Module(int sourceID, std::string dim_prefix, std::string version);
  virtual ~Module();

  // general functions

  int         getSourceID() const {return module_source_id;}
  std::string getName()     const {return module_name;}

  // module commands

  virtual bool check_db() { return false; }
  virtual void Initialize(int);
  virtual void Reset(int);
  virtual void ReadStatus(int);
  virtual void Control(const char*);

  // wrapper functions to allow the passing of function pointers
  // during MyDimComm creation

  static void Initialize_(Module*,int);
  static void Reset_(Module*,int);
  static void ReadStatus_(Module*,int);
  static void Control_(Module*,const char*);

  // My own error class, used throughout all Modules.
  // The difference to std::exception is the use of string instead
  // of (const char*), but as these errors only go into the DIM
  // ErrorString it does not matter. Use StdError to pass
  // errors to the main program.
  class Error
  {
  protected:
    std::string msg;
  public:
  Error() : msg() {}
  Error(std::string s) : msg(s) {}
  Error(Module*m) : msg(m->getName()+": generic error") {}
  Error(Module*m,std::string s) : msg(m->getName()+": "+s) {}
    virtual ~Error() {}
    
    virtual std::string what() {return msg;}
    virtual std::string getDimName() {return "ErrorString";}
    virtual int getStatus() {return MS_ERROR_CONT;}
  };

  class ErrorContainer : public Error
  {
    std::list<Error> errors;
  public:
    ErrorContainer& operator+=(Error e) {errors.push_back(e);return *this;}
    operator bool() {return errors.size()!=0;}
    std::string what()
      {
	std::string ret;
	std::list<Error>::iterator it;
	for(it=errors.begin();it!=errors.end();++it)
	  ret+=(ret.size()?"\n":"")+it->what();
	return ret;
      }
  };

  class Warning : public Error
  {
  public:
  Warning() : Error() {}
  Warning(std::string s) : Error(s) {}
  Warning(Module*m) : Error(m,"generic warning") {}
  Warning(Module*m,std::string s) : Error(m,s) {}
    virtual ~Warning() {}
    virtual std::string getDimName() const {return "WarningString";}
    virtual int         getStatus()  const {return MS_WARN_CONT;}
  };

  class WarningContainer : public Warning
  {
    std::list<Warning> warnings;
  public:
    WarningContainer& operator+=(Warning w)
      {warnings.push_back(w);return *this;}
    operator bool() {return warnings.size()!=0;}
    std::string what()
      {
	std::string ret;
	std::list<Warning>::iterator it;
	for(it=warnings.begin();it!=warnings.end();++it)
	  ret+=(ret.size()?"\n":"")+it->what();
	return ret;
      }
  };

  class FileError : public Error
  {
  public:
  FileError(Module*m) : Error(m,"generic file error") {}
  FileError(Module*m,std::string s) : Error(m,"file error: "+s) {}
  FileError(std::string s) : Error(s) {}
  };

  class I2CError : public Error
  {
  public:
  I2CError(Module*m) : Error(m,"generic I2C error") {}
  I2CError(Module*m,std::string s) : Error(m,"I2C error: "+s) {}
  I2CError(std::string s) : Error(s) {}
  };

  class IpbusError : public Error
  {
  public:
  IpbusError(Module*m) : Error(m,"generic IPBUS error") {}
    IpbusError(Module*m,std::string s) : Error(m,"IPBUS error: "+s) {}
    IpbusError(std::string s) : Error(s) {}
  };

  class VMEError : public Error
  {
  public:
  VMEError(Module*m) : Error(m,"generic VME error") {}
  VMEError(Module*m,std::string s) : Error(m,"VME error: "+s) {}
  VMEError(std::string s) : Error(s) {}
  };

  class DBError : public Error
  {
  public:
  DBError(Module*m) : Error(m,"generic database error") {}
  DBError(Module*m,std::string s) : Error(m,"database error: "+s) {}
  DBError(std::string s) : Error(s) {}
  };

  class ChipError : public Error
  {
  public:
  ChipError(Module*m) : Error(m,"generic chip error") {}
  ChipError(Module*m,std::string s) : Error(m,"chip error: "+s) {}
  ChipError(std::string s) : Error(s) {}
  };

  class StdError : public std::exception
  {
    std::string msg;
  public:
  StdError(Module*m,std::string s):msg(m->getName()+": "+s){}
    virtual ~StdError() throw() {}
    const char* what() {return msg.c_str();}
  };
  
  class ChipWarning : public Warning
  {
  public:
  ChipWarning(Module*m) : Warning(m,"generic chip warning") {}
  ChipWarning(Module*m,std::string s) : Warning(m,"chip warning: "+s) {}
  ChipWarning(std::string s) : Warning(s) {}
  };
};

#endif
