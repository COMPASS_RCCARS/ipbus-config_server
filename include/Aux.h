/*****************************************************************************
 * Copyright (C) 2009  Alexey Filin
 *
 * full refactoring of original LOAD by Roland Kuhn
 */

#ifndef _AUX_H_
#define _AUX_H_

#include <mysql.h>
#include <pthread.h>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <queue>

#include "dimutil.h"

//#define DEBUG_RUN 1
#define USE_DB_DEVLOG_DB 0

#define COMMAND_TABLE "Command"
#define COMMAND_SRCID_TABLE "SrcID_Command"
#define MAX_QUERY_BUFFER_SIZE 1<<20

#define DTBP_TYPE_NAME "DTBP(ECAL2)"
#define DTBP_MODE_PROGRAM_REGISTER 0x3
#define DTBP_PASSWD "pea1u8l"

enum ExitState
  {
    SuccessState = 0,
    WarningState = 1,
    FailureState = 1<<1,
    InfoState    = 1<<2,
    UnknownState = 1<<3
  };

/*****************************************************************************/
class MutexLock
{
  pthread_mutex_t& mutex;
  
 public:
 MutexLock( pthread_mutex_t &Mutex )
   : mutex(Mutex)
  {
    pthread_mutex_lock( &mutex );
  }

  virtual ~MutexLock()
    {
      pthread_mutex_unlock( &mutex );
    }
};

/*****************************************************************************/
class ExecContext
{
 public:
  ExecContext() {};
  
  virtual pthread_mutex_t& cmdMutex();
  virtual pthread_mutex_t& dbMutex();
  
  virtual int sout( const std::ostringstream & );
  virtual int serr( const std::ostringstream & );
  
  virtual ~ExecContext() {};
};

/*****************************************************************************/
class CommandOptions
{
  int todo;
  std::string stropts;
  std::string command_;
  bool is_sequential;
  bool logging_disabled;
  ExecContext &ctx;

 public:
  CommandOptions( ExecContext &Context );
  
  void usage( char* opts=0 );
  
  int parseOption( char c, int& todo, int& rv );

  int parseToDoMasks( std::string          AllOpts,
                      std::vector<int>&    Opts,
                      std::vector<std::string>& StrOpts );
  int processOptions( int argc, char* argv[] );

  int toDo() const;
  const std::string &strOpts() const;
  const std::string &command() const;
  bool isSequential() const;
  bool loggingDisabled() const;
};

/*****************************************************************************/
class DBConnection
{
 protected:
  const char  *server;
  const char  *user;
  const char  *passwd;
  const char  *db;
  MYSQL       *conn;
  MYSQL_RES   *result;
  ExecContext &ctx;
  bool         is_connection_fake;
  
  void debprint( const std::string &out );

public:
  MYSQL_ROW  row;

  DBConnection( const char  *Server,
                const char  *User,
                const char  *Passwd,
                const char  *DB,
                ExecContext &Ctx,
                bool         IsConnectionFake=false );

  virtual int connect();
  virtual int doQuery( const char *Query );
  virtual int doQuery( const std::string &Query );
  virtual int lastInsertedId();
  virtual int query( const char *Query );
  virtual int query( const std::string &Query );

  virtual char** fetchRow();

  virtual int freeResult();

  virtual ~DBConnection();
};

/*****************************************************************************/
class CommandLogger
{
 protected:
  int state;
  int modules_num;
  int command_num;
  int success_num;
  int warning_num;
  int failure_num;

  unsigned int max_query_buffer_size;

  DBConnection    &devlogconn;
  ExecContext     &ctx;

  time_t start_time;
  time_t finish_time;
  std::string options;

  struct cmd_log_t
  {
    unsigned int  state;
    time_t        start_time;
    time_t        finish_time;
    std::string   module_type;
    std::string   detector;
    std::string   options;
    std::string   warn_str;
    std::string   err_str;
    cmd_log_t( unsigned int  State,
               time_t        StartTime,
               time_t        FinishTime,
               const std::string &ModuleType,
               const std::string &Detector,
               const std::string &Options )
      : state(State),
         start_time(StartTime),
         finish_time(FinishTime),
         module_type(ModuleType),
         detector(Detector),
         options(Options)
    {}
  };

  std::map< int, std::queue<cmd_log_t> > sid_cmd_log;

public:
  CommandLogger( DBConnection &DevLogConn,
                 ExecContext  &Ctx );

  virtual int checkSrcID( int           SrcID,
                          const std::string &what );
  virtual int commandStart( const std::string &StrOpts );
  virtual int commandFinish();
  virtual int moduleAdded( int sid );
  virtual int moduleCommandStart( int           SrcID,
                                  const std::string &ModuleType,
                                  const std::string &Detector,
                                  const std::string &StrOpts );
  virtual int moduleCommandFinish( int           SrcID,
                                   int           State,
                                   const std::string &cmd,
                                   const std::string &WarnStr,
                                   const std::string &ErrStr );
  virtual int report();

  const char *stateStr( int state );
  int setMaxQueryBufferSize( unsigned int Size );

  virtual ~CommandLogger();
};

/*****************************************************************************/
class ModuleContainer;
class Module
{

  unsigned long            sid;
  std::string              source;
  std::string              module_type;
  std::string              detector;
  std::string              command;

  //Only used if connection is not via dim (dtbp)
  bool                     useDirectConn;
  std::string              vmeHostName;
  std::string              port;
  std::string              configuration;

  std::vector<int>         todo;
  std::vector<std::string> strtodo;

  bool                     is_minor;
  std::vector<Module *>    minor_module;
  std::vector<std::string> minor_source;
  std::vector<int>         minor_todo;
  std::vector<std::string> strminor_todo;

  std::string              err_str;
  std::string              warn_str;
  DimInt                   status;
  bool                     error;
  int                      state;
  std::string              last_strtodo;


  ExecContext&             ctx;
  CommandLogger&           logger;

public:

  Module( int                        Sid,
          const std::string&         Source,
          const std::string&         Command,
          const std::vector<int>&    Todo,
          const std::vector<std::string>& StrTodo,
          ExecContext&               Context,
          CommandLogger&             Logger,
          bool                       IsMinor=false );

  int sendCommand( int                ToDoMask,
                   const std::string& ToDoOpts );
  void wait();
  void finish();
  void enableDirectConn(std::string VMEHostname, std::string Port, std::string Configuration);

  ExecContext&     execContext();
  CommandLogger&   commandLogger();

  friend void* startThread( void *i );
  friend class ModuleContainer;
};

/*****************************************************************************/
class Macro;
class ModuleContainer
{
  bool                               is_sequential;
  std::string                        command;
  std::map<unsigned long, Module *>  module_;
  CommandOptions&                    co;
  DBConnection&                      devconn;
  ExecContext&                       ctx;
  CommandLogger&                     logger;

public:
  ModuleContainer( CommandOptions& CoOpts,
                   DBConnection&   DevConn,
                   ExecContext&    Context,
                   CommandLogger&  Logger );
  void findSourceIDs( std::string        spec,
                      int                todo,
                      const std::string& strtodo );
  int findModuleOptions();
  int findMinorModulesAndOptions();
  int findModuleType();

  Module* module( int n );
  virtual int runCommand();

  Macro                       *macros;

  std::string strSrcIDs( bool with_minors=false );
  virtual ~ModuleContainer();
};

/*****************************************************************************/
class ThreadContext
{
  Module          &m;
  ModuleContainer &mc;
  ExecContext     &ctx;
  CommandLogger   &logger;
public:
  ThreadContext( Module          &Mod,
                 ModuleContainer &ModContainer,
                 ExecContext     &Context,
                 CommandLogger   &Logger ) : m(Mod), mc(ModContainer), ctx(Context), logger(Logger)
  { ; }

  Module          &module()          { return m; }
  ModuleContainer &moduleContainer() { return mc; }
  ExecContext     &execContext()     { return ctx; }
  CommandLogger   &commandLogger()   { return logger; }

  virtual ~ThreadContext() { ; }
};

/*****************************************************************************/
class DTBPCommunicator
{
  Module          &m;
  ModuleContainer &mc;
  ExecContext     &ctx;
  CommandLogger   &logger;

  int sock;
  int mode;

  struct send_data
  {
    unsigned int address;
    unsigned int value;
    unsigned int mode;
  };

public:
  DTBPCommunicator( Module          &Mod,
                    ModuleContainer &ModContainer,
                    ExecContext     &Context,
                    CommandLogger   &Logger);

  Module          &module()           { return m; }
  ModuleContainer &moduleContainer()  { return mc; }
  ExecContext     &execContext()      { return ctx; }
  CommandLogger   &commandLogger()    { return logger; }

  int connectToDevice (std::string address, std::string port, bool noDNS);
  int authenticate ();
  int setMode (int mode);
  int sendData (int address, int value, int mode);

  virtual ~DTBPCommunicator();


};

/*****************************************************************************/
class Macro
{
  std::map<std::string, std::string>           macros;
  CommandOptions                &co;
  DBConnection                  &devconn;
  ExecContext                   &ctx;
  CommandLogger                 &logger;

public:
  Macro          ( CommandOptions  &CoOpts,
                     DBConnection  &DevConn,
                     ExecContext   &Context,
                     CommandLogger &Logger );
  bool expandMacro(std::string &io);
  unsigned int stringToInt(std::string s);
};

#endif // _AUX_H_
