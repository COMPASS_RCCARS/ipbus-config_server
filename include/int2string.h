#ifndef _INT2STRING_H_
#define _INT2STRING_H_

#include <stdint.h>
#include <string>
#include <map>
#include <vector>
#include <sstream>

std::string mydec(int i);
std::string myhex(int i);
std::string myoct(int i);
std::string myfloat(double d);
std::string mybitmask(unsigned int);
std::string mybitmask(unsigned short);
std::string mybitmask(unsigned char);

std::string mybitmask(unsigned int, int);

template <class T>  std::map<std::string,T> parseSettings(std::string,T,char c=' ',char eq='=');
std::vector<std::string> split(std::string, char=' ');
void replace_in_string(std::string& s, std::string find, std::string replace);

template <class T> class mynumber
{
  T num;

 public:

  mynumber(std::istream &s) {s>>num;}
  mynumber(std::string s="0") {std::istringstream(s)>>num;}

  operator T() {return num;}

};

const char* mytime();
uint32_t toUnsigned(std::string& str);

#endif
