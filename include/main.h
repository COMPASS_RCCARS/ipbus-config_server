#ifndef __MAIN_H__
#define __MAIN_H__

#include <stdio.h>
#include "gs_todo.h"

extern FILE* _debug;
extern void DEBUG(const char* format, ...);

extern const char* db_server;
extern const char* host;
extern const char* db_user;
extern const char* db_db;
extern const char* db_passwd;
extern unsigned int db_port;
extern const char* et_db_db;
extern const char* et_db_server;
extern const char* et_db_user;
extern const char* et_db_passwd;

#endif
