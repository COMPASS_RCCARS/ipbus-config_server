#ifndef __IPBUS_MWPC_TDC_CARD_H__
#define __IPBUS_MWPC_TDC_CARD_H__


#include <stdint.h>
#include <string>

#include "IpbusModule.h"
#include "IpbusFeMux.h"
#include "IpbusTdcCard.h"

class IpbusMwpcTdcCard : public IpbusTdcCard
{
  friend class IpbusFeMux;
 public:
  IpbusMwpcTdcCard(IpbusFECard::FECardType cardType, const char* name, IpbusFeMux* parent, int _port);
  virtual ~IpbusMwpcTdcCard();

  //virtual void ReadStatus(std::string& status_info);
 protected:
  //virtual uint32_t GetFwIdFromDB();
  //virtual void reset_module();
  //virtual void enable_channels();
  //virtual void set_window();
  //virtual void set_delay();
  virtual void set_thresholds();
  virtual void set_registers();
 private:
  void select_cmad(uint32_t n);
  void load_ch_config(uint32_t ch_num, uint32_t C, uint32_t R, uint32_t baseline, uint32_t thr);

  void rst(const char* node_name);
  void load_bit(const char* node_name, uint32_t value);
  void load_comm(const char* node_name);
};

#endif
