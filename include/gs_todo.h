#ifndef _GS_TODO_H_
#define _GS_TODO_H_

// actions
#define GS_DO_GESICA           0x00000001   // vme, merger, hotlink
#define GS_DO_TCS              0x00000002   // tcs receiver
#define GS_DO_ADC              0x00000004   // zero chip
#define GS_DO_PED              0x00000008   // pedestals and thresholds
#define GS_DO_APV_RESET        0x00000010   // reset apvs
#define GS_DO_APV              0x00000020   // load apvs
#define GS_DO_DELAY            0x00000040   // clk/trg delay chips
#define GS_DO_MODE             0x00000080   // adc data mode
#define GS_DO_THR              0x00000100   // adc 0/1 threshold
#define GS_DO_ADC_REGISTERS    0x00000200   // load misc ADC registers
#define GS_DO_ADC_CHECKS       0x00000400   // check misc ADC registers after loading
#define GS_LOAD_FW             0x00000800   // load firmware on Mux
// modifiers
#define GS_DO_MODE_OVERRIDE    0x00001000   // set mode to "latch" temporarily
#define GS_DO_FAST             0x00002000   // do not verify
#define GS_DO_VERIFY           0x00004000   // read status and verify

// added for ecal trigger
#define GS_DO_BACKPLANE        0x00080000
#define GS_DO_TRIGGER_COMMON   0x00010000   // program command parameters for the CFT
#define GS_DO_TRIGGER_MON      0x00020000   // write monitor values into db
#define GS_DO_TRIGGER_CALIB    0x00040000   // load time and energy constants

// others
#define GS_DO_ALL              0x10000000   // do all load procedures for selected module (depends on module type)
#define GS_DO_INIT_PORT        0x20000000   // load procedures for selected port (16 LSBs) of the module
#define GS_SHOW_MUX_STATUS     0x40000000   // show status of the ifTDC-MUX

#endif
