#ifndef __IPBUS_GEM_ADC_CARD_H__
#define __IPBUS_GEM_ADC_CARD_H__
#include "IpbusModule.h"
#include "IpbusFeMux.h"

#include <stdint.h>
#include <string>

#define ADC_MODE_TRIPLE 0x1
#define ADC_MODE_SPARSIFY 0x2
#define ADC_MODE_PEDESTAL 0x4
#define ADC_MODE_COMMON 0x8
#define ADC_MODE_REORDER 0x10
#define ADC_MODE_HEADERSUPPRESSION 0x20
#define ADC_MODE_SPARSE2 0x40
#define ADC_MODE_INTERLEAVE 0x80
#define ADC_MODE_SPARSE (ADC_MODE_SPARSIFY|ADC_MODE_PEDESTAL|\
                         ADC_MODE_COMMON|ADC_MODE_TRIPLE)
#define ADC_MODE_LATCH ADC_MODE_TRIPLE

#define ADC_MODE_RICH_SPARSE (ADC_MODE_TRIPLE|ADC_MODE_SPARSIFY|\
                        ADC_MODE_PEDESTAL|ADC_MODE_COMMON|\
                        ADC_MODE_HEADERSUPPRESSION)
#define ADC_MODE_RICH_LATCH (ADC_MODE_TRIPLE|ADC_MODE_INTERLEAVE)

class IpbusGemAdcCard : public IpbusFECard
{
  std::map<uint32_t,uint8_t> i2cApvWriteValues;
  std::map<uint32_t,uint8_t> MCP23017WriteValues;
  std::map<std::string,uint32_t> ADCWriteValues;
  int  ped_values[2048];
  int  sigma_values[2048];
  bool pedestals_set;
  friend class IpbusFeMux;
  uint32_t fw_id;
 public:
  IpbusGemAdcCard(IpbusFECard::FECardType cardType, const char* name, IpbusFeMux* parent, int _port);
  virtual ~IpbusGemAdcCard();

  virtual void ReadStatus(std::string& status_info);
 private:
  uint32_t GetFwIdFromDB();
  int      ADC_write_verify(std::string reg, uint32_t value, uint32_t mask, uint32_t& readback);
  int      APV_write_verify(uint32_t geo_id, uint8_t reg, uint8_t value, uint8_t mask);
  // I2C functions:
  void     i2cInit(int clockPrescale);
  bool     i2cWriteByte(std::string node, uint8_t data, uint8_t cr);
  void     i2cReadByte(std::string node, uint8_t& data);
  void     i2cApvWritePedestals(int ped[2048], int sigma[2048], bool verify);
  bool     i2cApvWrite(uint32_t gid, uint8_t reg, uint8_t data);
  bool     i2cApvRead(uint32_t gid, uint8_t reg, uint8_t& data);
  bool     MCP23017Write(uint32_t gid, uint8_t addr, uint8_t reg, uint8_t data);
  bool     MCP23017Read(uint32_t gid, uint8_t addr, uint8_t reg, uint8_t& data);
  void     ResetVerify();
  bool     VerifyAll();
  /////////////////////

  void     reset_module();
  void     init_i2c_bus();
  void     reset_APV();
  void     set_data_mode(int mode);
  void     set_thresholds();
  void     set_pedestals();
  void     set_adc_registers();
  int      set_apv_registers();
  virtual void set_registers() {};
};

#define DEFAULT_CLOCK_PRESCALE 0x0A
#define MAX_NUM_APV_CHANNELS    128
#define MAX_NUM_APV_PER_BUS      16
#define REAL_NUM_APV_PER_BUS     12

#endif
