#ifndef _DIMUTIL_H_
#define _DIMUTIL_H_

#include "dic.hxx"
#include <semaphore.h>
#include <string>

// This class opens a DIM service and stores the value
// in an easily accessible way. Upon each update of the
// value a semaphore is posted, so that a thread can
// explicitly wait for a change in this DIM service.
// The method lock() brings the semaphore down to zero
// so that the next wait() will wait for the next update.
class DimInt : public DimInfo
{
  void infoHandler()
  {
    i=getInt();
    if (wait4flag && !wait4fin && (i&0x7fff) == wait4num &&
	(changes || !wait4edge))
      {
	wait4fin = true;
	wait4succ = i < 0x8000;
      }
    sem_post(&waiter);
    changes++;
  }
  sem_t waiter;
  int i;
  int changes;
  bool wait4flag;
  bool wait4edge;
  int wait4num;
  bool wait4succ;
  bool wait4fin;
public:
  DimInt(std::string b,int i_) : DimInfo((char*)b.c_str(),i_), changes(0),
			    wait4flag(false)
  {
    i=i_;
    sem_init(&waiter,0,0);
  }
  DimInt(std::string b, int i_, int wait4, bool edge)
    : DimInfo((char*)b.c_str(),i_), changes(0), wait4flag(true),
      wait4edge(edge), wait4num(wait4), wait4succ(false), wait4fin(false)
    {
      i=i_;
      sem_init(&waiter,0,0);
    }
  void wait() { sem_wait(&waiter); }
  bool wait4success() { return wait4succ; }
  bool wait4finished() { return wait4fin; }
  void lock() { while(!sem_trywait(&waiter)); }
  operator int() { return i; }
  int changed() { return changes; }
};

class DimString : public DimInfo
{
  void infoHandler()
  {
    s=getString();
    if (wait4flag && s == wait4string && (changes || !wait4edge))
      wait4ready = true;
    sem_post(&waiter);
    changes++;
  }
  sem_t waiter;
  std::string s;
  int changes;
  bool wait4flag;
  bool wait4edge;
  std::string wait4string;
  bool wait4ready;
public:
  DimString(std::string b,std::string s_) : DimInfo((char*)b.c_str(),(char*)s_.c_str()),
				  changes(0), wait4flag(false)
    {
      s=s_;
      sem_init(&waiter,0,0);
    }
  DimString(std::string b, std::string s_, std::string wait4, bool edge)
    : DimInfo((char*)b.c_str(),(char*)s_.c_str()), changes(0), wait4flag(true),
      wait4edge(edge), wait4string(wait4), wait4ready(false)
    {
      s=s_;sem_init(&waiter,0,0);
    }
  void wait() {sem_wait(&waiter);}
  bool wait4success() {return wait4ready;}
  void lock() {while(!sem_trywait(&waiter));}
  operator std::string() {return s;}
  int changed() {return changes;}
};

class DimFloat : public DimInfo
{
  void infoHandler()
  {
    f=getFloat();
    if (wait4flag && f == wait4float && (changes || !wait4edge))
      wait4ready = true;
    sem_post(&waiter);
    changes++;
  }
  sem_t waiter;
  float f;
  int changes;
  bool wait4flag;
  bool wait4edge;
  float wait4float;
  bool wait4ready;
public:
 DimFloat(std::string b,float f_) : DimInfo((char*)b.c_str(),f_),
    changes(0), wait4flag(false)
    {
      f=f_;
      sem_init(&waiter,0,0);
    }
 DimFloat(std::string b, float f_, float wait4, bool edge)
   : DimInfo((char*)b.c_str(),f_), changes(0), wait4flag(true),
    wait4edge(edge), wait4float(wait4), wait4ready(false)
    {
      f=f_;
      sem_init(&waiter,0,0);
    }
  void wait() {sem_wait(&waiter);}
  bool wait4success() {return wait4ready;}
  void lock() {while(!sem_trywait(&waiter));}
  operator float() {return f;}
  int changed() {return changes;}
};

#endif
