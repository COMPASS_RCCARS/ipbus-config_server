#ifndef __IPBUS_FE_MUX_H__
#define __IPBUS_FE_MUX_H__

#include <stdint.h>
#include <string>

#include "IpbusModule.h"

class IpbusFeMux : public IpbusModule
{
  // module status:
  uint32_t   port_mask;
  uint32_t   port_status;
  uint32_t   port_enabled;
  uint32_t   tcs_status;
  uint32_t   slink_status;
  uint32_t   timeout;
  uint32_t   slink_format;
  enum DetectorType
  {
    UNKNOWN, MWPC, GEMADC, CEDAR, SPEAKING_TIME_TDC, TRIGGER, SCI_FI
  } detector;
  bool       set_clock_freq;
  uint32_t   useGenClk; // 0 - 125 MHz, 1 - 155.52 MHz from SiLab (for MWPC ifTDC MUXes)
 protected:
  // checks for inconsistence of FE database; returns false in case of any error:
  bool check_db();
 public:
  IpbusFeMux(int sourceID, std::string version);
  ~IpbusFeMux();
  
  virtual void     InitModule(int todo); // can throw Error
  virtual void     Reset(int);
  virtual void     ReadStatus(int);
  virtual void     Control(const char*);
  virtual uint32_t GetFirmwareID();

private:
  // main multiplexer configuration functions:
  void SetModuleSrcID();
  void SetSlinkFormat();
  void ResetModule();
  void DeleteFECards();
  void EnablePorts();
  void ReadPortStatus();
  void SetTimeout();
  void ReadTimeout();
  void SetRegisters();
  // GEMADC MUX functions:
  void SetBaseIpAddr();
  void SetClockFrequency(double& freq, double& measured, uint8_t& addr);
  void LoadPort(int n);
 private:
  // Initializing functions, throw IpbusError, I2CError, etc.. in case of an error:
  bool do_all();                // initialize everything (option -A of LOAD program)
  bool do_init_mux();           // initialize IPBUS FE multiplexer (option -g)
  bool do_load_thresholds();    // option -T
  bool do_load_apv_registers(); // option -a
  bool do_set_sparse_mode();    // option -m
  bool do_set_latch_mode();     // option -ml
  bool do_load_pedestals();     // load APV pedestals and sigmas (option -p)
  bool do_reset_apv();          // option -r
  bool do_reset_adc();          // reset and init ADC, load ADC registers
  bool do_verify_registers();   // verify APV registers
  bool do_show_status();        // show MUX status (option -S)

  bool ConnectFECards();
  bool LoadFECards();
};


#endif
