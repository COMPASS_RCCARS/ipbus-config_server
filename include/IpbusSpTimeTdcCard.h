#ifndef __IPBUS_SP_TIME_TDC_CARD_H__
#define __IPBUS_SP_TIME_TDC_CARD_H__

#include <stdint.h>
#include <string>

#include "IpbusModule.h"
#include "IpbusFeMux.h"
#include "IpbusTdcCard.h"

class IpbusSpTimeTdcCard : public IpbusTdcCard
{
  friend class IpbusFeMux;
  uint32_t fw_id;
 public:
  IpbusSpTimeTdcCard(IpbusFECard::FECardType cardType, const char* name, IpbusFeMux* parent, int _port);
  virtual ~IpbusSpTimeTdcCard();

  virtual void ReadStatus(std::string& status_info);
 private:
  virtual void enable_channels();
};

#endif
