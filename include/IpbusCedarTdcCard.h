#ifndef __IPBUS_CEDAR_TDC_CARD_H__
#define __IPBUS_CEDAR_TDC_CARD_H__

#include <stdint.h>
#include <string>

#include "IpbusModule.h"
#include "IpbusFeMux.h"

class IpbusCedarTdcCard : public IpbusFECard
{
  friend class IpbusFeMux;
  uint32_t fw_id;
 public:
  IpbusCedarTdcCard(IpbusFECard::FECardType cardType, const char* name, IpbusFeMux* parent, int _port);
  virtual ~IpbusCedarTdcCard();

  virtual void ReadStatus(std::string& status_info);
 private:
  uint32_t GetFwIdFromDB();
  void reset_module();
  void enable_channels();
  void set_window();
  void set_delay();
  void set_thresholds();
};

#endif
