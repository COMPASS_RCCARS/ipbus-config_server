#ifndef __SI5338_H__
#define __SI5338_H__

#include <stdint.h>
#include <uhal/uhal.hpp>

class I2CDeviceOpencores
{
  uint8_t            addr;
  uhal::HwInterface& hw;
  uint8_t            failure_cnt;
 public:
  I2CDeviceOpencores(uhal::HwInterface* phw, uint16_t clockPrescale);
  int read(uint8_t regAddr, uint8_t &readData);
  int reada16d8(uint16_t regAddr, uint8_t &readData);

  /**
   * \Brief Write data to the given 8 bit register address. 
   *      In case of errors the function will try to repeat the process
   *      by calling itself recursively until maximum number of retries is reached.
   * \param RegAddr register address
   * \param Data data to write
   * \return Write status
   */
  int write(uint8_t regAddr, uint8_t data);

  /**
   * \Brief Write 8 bit data into the given 16 bit register address. 
   *      In case of errors the function will try to repeat the process
   *      by calling itself recursively until maximum number of retries is reached.
   * \param RegAddr register address
   * \param Data data to write
   * \return Write status
   */
  int writea16d8(uint16_t regAddr, uint8_t data);

  /**
   * \Brief Write 16 bit data into the given 16 bit register address. 
   *      In case of errors the function will try to repeat the process
   *      by calling itself recursively until maximum number of retries is reached.
   * \param RegAddr register address
   * \param Data data to write
   * \return Write status
   */
  int writea16d16(uint16_t regAddr, uint16_t data);

  /**
   * \Brief Set address of the device which will be used during the communication
   * \param addr Device address
   */
  void setSlaveAddr(uint8_t _addr) { addr = _addr; }
 protected:
  void sleep();
  int readByte(uint8_t &data);
  int writeByte(uint8_t data, uint8_t cr);
};

class Si5338Driver3
{
 public:
 Si5338Driver3(uhal::HwInterface* phw, uint32_t slaveAddr, uint32_t prescaler) : 
  si5338(phw, prescaler & 0xFFFF)
    {
      si5338.setSlaveAddr(slaveAddr);
    }
  ~Si5338Driver3() {};

  int  initSi5338() { return initSi5338_manual(); }
  bool readClockFrequency(double& freq);
  bool readI2CAddress(uint8_t& addr) { return (si5338.read(27, addr) == 0); }

 protected:
  I2CDeviceOpencores si5338;
        
  int initSi5338_manual();
  int initSi5338_web();

  bool programRegisters();
  bool validateInputClockStatus();
  bool configurePLLforLocking();
  bool confirmPLLLockStatus();
  bool copyFCALValues(bool useWebProcedure = false);
  bool setPLLtoUseFCAL();
  bool disable_outputs()          { return (si5338.write(230, 0x10) == 0); }
  bool pauseLOL()                 { return (si5338.write(241, 0xE5) == 0); }
  bool softReset()                { return (si5338.write(246, 0x02) == 0); }
  bool restartLOL()               { return (si5338.write(241, 0x65) == 0); }
  bool enableOutput3()            { return (si5338.write(230, 0x07) == 0); }
  bool enableOutputs()            { return (si5338.write(230, 0x00) == 0); }

  bool readMSDivisionFactor(uint8_t firstAddr, double& value);
};

#endif
