#ifndef __IPBUS_TDC_CARD_H__
#define __IPBUS_TDC_CARD_H__


#include <stdint.h>
#include <string>

#include "IpbusModule.h"
#include "IpbusFeMux.h"

class IpbusTdcCard : public IpbusFECard
{
  friend class IpbusFeMux;
 protected:
  uint32_t fw_id;
  uint32_t cntvaluein_1;
 public:
  IpbusTdcCard(IpbusFECard::FECardType cardType, const char* name, IpbusFeMux* parent, int _port);
  virtual ~IpbusTdcCard();

  virtual void ReadStatus(std::string& status_info);
 protected:
  virtual uint32_t GetFwIdFromDB();
  virtual void reset_module();
  virtual void enable_channels();
  virtual void set_window();
  virtual void set_delay();
  virtual void set_thresholds();
};

#endif
