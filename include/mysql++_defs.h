#ifndef MYSQL_DEFS_H_
#define MYSQL_DEFS_H_

#include <mysql++.h>

#ifdef MYSQLPP_LIB_VERSION 
#define MYSQLPP_HEADER_VERSION MYSQLPP_LIB_VERSION 
#endif
#if MYSQLPP_HEADER_VERSION < MYSQLPP_VERSION(3, 0, 0)
#define MYSQLPP_RESULT mysqlpp::Result
#define MYSQLPP_USERESULT mysqlpp::ResUse
#else
#define MYSQLPP_RESULT mysqlpp::StoreQueryResult
#define MYSQLPP_USERESULT mysqlpp::UseQueryResult
#endif

#endif
