#ifndef __IPBUS_TCS_FPGA_H__
#define __IPBUS_TCS_FPGA_H__

#include <stdint.h>
#include <string>

#include "IpbusModule.h"
#include "tcsOpt.h"

class IpbusTcsFpga : public IpbusModule
{
  std::string tcs_name;
  tcsOpt*     opt;
 protected:
  // checks for inconsistence of FE database; returns false in case of any error:
  bool check_db();
 public:
  IpbusTcsFpga(int sourceID, std::string version);
  ~IpbusTcsFpga();

  const std::string& get_tcs_name() const { return tcs_name; }
  tcsOpt*            GetOptions() { return opt; }
  bool               UpdateOptions();

  virtual void     InitModule(int todo); // can throw Error
  virtual void     Reset(int);
  virtual void     ReadStatus(int);
  virtual void     Control(const char*);
  virtual uint32_t GetFirmwareID();
  
 private:
  bool set_module_config();
  // Initializing functions, throw IpbusError, I2CError, etc.. in case of an error:
  void do_all();                // initialize everything (option -A of LOAD program)
  void do_init_fpga();          // initialize A7 TCS FPGA part (option -g)
};

#endif
