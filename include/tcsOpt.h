// $Id: tcsOpt.h 1799 2006-04-21 20:33:46Z rkuhn $

/*!
   \file    tcsOpt.h
   \brief   Compass Options Interpreter Class.
   \author  Benigno Gobbo 
   \version $Revision: 1799 $
   \date    $Date: 2006-04-21 22:33:46 +0200 (Fri, 21 Apr 2006) $
*/

#ifndef tcsOpt_h
#define tcsOpt_h

#include <stdint.h>

#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <string>
#include <list>
#include <vector>
#include <map>
#include <set>
#include <mysql++.h>

#include "IpbusTcsController.h"

/*! \class tcsOpt tcsOpt.h
    \brief  Compass Options Interpreter Class. 

    It takes argc and argv from main program and interpretes them.
    It assumes that (at least) an option file is given as argument.
    Polymorphic getOpt methods are available to retrieve information 
    from input file. 
    */

class tcsOpt
{

 public:

  std::vector<uint32_t> confRec;          // Array of receiver config words
  std::vector<uint32_t> confSubRec;       // Temporary array of receiver config words
  uint32_t subTimeSlice[MAX_DAQS];        // Time share of one sub DAQ (0x0...0x1f)
  int spillCounter[MAX_DAQS];             // Internal spill counter for sub DAQ
  std::vector<uint32_t> timeSlice;        // Time slice words (4 DAQs per word)
  uint32_t configWord;   	       	  // Configures ECC bit and long/short word
  uint32_t resetWord;   	       	  // Reset Word to reset TCS receivers and CATCHes
  std::vector<uint32_t> deadTimeF;	  // Deadtimes (fixed and variable)
  std::vector<uint32_t> deadTimeN;	  // Variable deadtime # of triggers
  std::vector<uint32_t> deadTimeW;	  // Variable deadtime window
  uint32_t deadTimeSN;                    // Second variable deadtime # of triggers
  uint32_t deadTimeSW;                    // Second variable deadtime window
  uint32_t recMap[MAX_REC];               // Map of receivers to DAQs
  uint32_t recCfg[MAX_REC][MAX_CFG];      // List of configuration words for receivers
  uint32_t nrRecCfg[MAX_REC];             // Counter for config words for receivers
  uint32_t recDel[MAX_REC];               // Delay settings for receivers
  uint32_t recRes[MAX_REC];               // Reset words for receivers

  tcsOpt( const char *, std::string host );
  ~tcsOpt();

  /*! \fn void getOptions( const char* optfile );;
    \brief reads a file in, usually the default file at instantiation.
    \param optfile - filename of option file
  */
  void getOptions( const char* optfile, int nesting = 0 );

  /*! \fn void dump( int dumplevel );
    \brief dumps the list of strings read from input file, and eventally
    the list of unused lines and the list of lines with errors inside.
    \param dumplevel if 0 only the list of read lines is printed;
    if 1 also the list of unused lines is printed; if greather that 1
    also the list of lines with errors is printed.
  */
  void dump( int dumplevel );

  /*! \fn bool getOpt( string key );   
    \brief looks for a line with key string. Returns \c TRUE if string
    was found.
    \param key is a (list of) string(s) to be used to identify the
    opportune line in the input file.
  */
  bool getOpt( std::string key );   


  /*! \fn bool getOpt( std::string key, int& inum ); 
    \brief looks for a line with key string and get the following
    integer number. 

    Returns \c TRUE if string and integer number were found 
    and the integer number is read correctly.
    \param key is a (list of) string(s) to be used to identify the
    opportune line in the input file.
    \param imun reference to the variable where to write the read
    integer number.
  */
  bool getOpt( std::string key, int& inum ); 

  /*! \fn bool getOpt( std::string key, double& rnum ); 
    \brief looks for a line with key string and get the following
    real number. 

    Returns \c TRUE if string and double precision real number were found 
    and the number is read correctly.
    \param key is a (list of) string(s) to be used to identify the
    opportune line in the input file.
    \param rmun reference to the variable where to write the read
    double precision real number.
  */
  bool getOpt( std::string key, uint32_t& inum ); 

  /*! \fn bool getOpt( std::string key, double& rnum ); 
    \brief looks for a line with key string and get the following
    real number. 

    Returns \c TRUE if string and double precision real number were found 
    and the number is read correctly.
    \param key is a (list of) string(s) to be used to identify the
    opportune line in the input file.
    \param rmun reference to the variable where to write the read
    double precision real number.
  */
  bool getOpt( std::string key, unsigned long& inum ); 

  /*! \fn bool getOpt( std::string key, double& rnum ); 
    \brief looks for a line with key string and get the following
    real number. 

    Returns \c TRUE if string and double precision real number were found 
    and the number is read correctly.
    \param key is a (list of) string(s) to be used to identify the
    opportune line in the input file.
    \param rmun reference to the variable where to write the read
    double precision real number.
  */
  bool getOpt( std::string key, double& rnum ); 

  /*! \fn bool getOpt( std::string key, float& rnum ); 
    \brief looks for a line with key string and get the following
    real number. 

    Returns \c TRUE if string and single precision real number were found 
    and the number is read correctly.
    \param key is a (list of) string(s) to be used to identify the
    opportune line in the input file.
    \param rmun reference to the variable where to write the read
    single precision real number.
  */
  bool getOpt( std::string key, float& rnum ); 

  /*! \fn bool getOpt( std::string key, std::string& str ); 
    \brief looks for a line with key string and get the following
    character string. 

    Returns \c TRUE if strings and were found 
    and the output string is read correctly.
    \param key is a (list of) string(s) to be used to identify the
    opportune line in the input file.
    \param str reference to the variable where to write the read
    character string.
  */
  bool getOpt( std::string key, std::string& str );

  /*! \fn bool getOptRec( std::string key, std::string& str ); 
    \brief looks recursively for a line with key string and get the 
    following character string. 

    Returns \c TRUE if strings and were found 
    and the output string is read correctly.
    \param key is a (list of) string(s) to be used to identify the
    opportune line in the input file.
    \param str reference to the variable where to write the read
    character string.
  */
  bool getOptRec( std::string key, std::string& str );


  /*! \fn bool getOpt( std::string key, std::list<int>& numbers );
    \brief looks for a line with key string and get the following
    list of integer numbers. 

    Returns \c TRUE if string and at least an 
    integer number were found and the list of integer numbers is read 
    correctly.
    \param key is a (list of) string(s) to be used to identify the
    opportune line in the input file.
    \param numbers reference to the variable where to write 
    the read list of integer numbers.
  */
  inline bool getOpt( std::string key, std::list<int>& numbers )
  { return myGetOpt( key, numbers ); }

  /*! \fn bool getOpt( std::string key, std::list<double>& numbers );
    \brief looks for a line with key string and get the following
    list of real numbers. 

    Returns \c TRUE if string and at least a double precision
    real number were found and the list of numbers is read 
    correctly.
    \param key is a (list of) string(s) to be used to identify the
    opportune line in the input file.
    \param numbers reference to the variable where 
    to write the read list of numbers.
  */
  inline bool getOpt( std::string key, std::list<double>& numbers )
  { return myGetOpt( key, numbers ); }

  /*! \fn bool getOpt( std::string key, std::list<float>& numbers );
    \brief looks for a line with key string and get the following
    list of real numbers. 

    Returns \c TRUE if string and at least a single precision 
    real number were found and the list of numbers is read 
    correctly.
    \param key is a (list of) string(s) to be used to identify the
    opportune line in the input file.
    \param numbers reference to the variable where 
    to write the read list of numbers.
  */
  inline bool getOpt( std::string key, std::list<float>& numbers )
  { return myGetOpt( key, numbers ); }

  /*! \fn bool getOpt( std::string key, std::list<std::string>& strlist );
    \brief looks for a line with key string and get the following
    list of character strings. 

    Returns \c TRUE if key and at least
    one more string were found and the list of strings is read 
    correctly.
    \param key is a (list of) string(s) to be used to identify the
    opportune line in the input file.
    \param strlist reference to the variable where 
    to write the read list of character strings.
  */
  bool getOpt( std::string key, std::list<std::string>& strlist );

  /*! \fn bool getOptRec( std::string key, std::list<std::string>& strlist );
    \brief looks recursively for a line with key string and get the 
    following list of character strings. 

    Returns \c TRUE if key and at least
    one more string were found and the list of strings is read 
    correctly.
    \param key is a (list of) string(s) to be used to identify the
    opportune line in the input file.
    \param strlist reference to the variable where 
    to write the read list of character strings.
  */
  bool getOptRec( std::string key, std::list<std::string>& strlist );


  /*! \fn bool getOpt( std::string key, std::vector<int>& numbers );
    \brief looks for lines with key string. In addition it expects
    a list of vector components and a list of integer values. 
    e.g. "mykey [1,4,6-8]  0 7 10 11 12".
      
    Returns \c TRUE if the key string, the list of vector 
    components and the list of integer values were found and everything 
    is read correctly.
    \param key is a (list of) string(s) to be used to identify the
    opportune line in the input file.
    \param numbers reference to the variable where 
    to write the read vector elements.
  */
  inline bool getOpt( std::string key, std::vector<int>& numbers )
  { return myGetOpt( key, numbers ); }

  /*! \fn bool getOpt( std::string key, std::vector<double>& numbers );
    \brief looks for lines with key string. In addition it expects
    a list of vector components and a list of double precision real values. 
    e.g. "mykey [1,4,6-8]  0 7 10 11 12".
      
    Returns \c TRUE if the key string, the list of vector 
    components and the list of double precision real values were found 
    and everything is read correctly.
    \param key is a (list of) string(s) to be used to identify the
    opportune line in the input file.
    \param numbers reference to the variable where 
    to write the read vector elements.
  */
  inline bool getOpt( std::string key, std::vector<uint32_t>& numbers )
  { return myGetOpt( key, numbers ); }

  /*! \fn bool getOpt( std::string key, std::vector<double>& numbers );
    \brief looks for lines with key string. In addition it expects
    a list of vector components and a list of double precision real values. 
    e.g. "mykey [1,4,6-8]  0 7 10 11 12".
      
    Returns \c TRUE if the key string, the list of vector 
    components and the list of double precision real values were found 
    and everything is read correctly.
    \param key is a (list of) string(s) to be used to identify the
    opportune line in the input file.
    \param numbers reference to the variable where 
    to write the read vector elements.
  */
  inline bool getOpt( std::string key, std::vector<double>& numbers )
  { return myGetOpt( key, numbers ); }

  /*! \fn bool getOpt( std::string key, std::vector<float>& numbers );
    \brief looks for lines with key string. In addition it expects
    a list of vector components and a list of single precision real values. 
    e.g. "mykey [1,4,6-8]  0 7 10 11 12".
      
    Returns \c TRUE if the key string, the list of vector 
    components and the list of single precision real values were 
    found and everything is read correctly.
    \param key is a (list of) string(s) to be used to identify the
    opportune line in the input file.
    \param numbers reference to the variable where 
    to write the read vector elements.
  */
  inline bool getOpt( std::string key, std::vector<float>& numbers )
  { return myGetOpt( key, numbers ); }

  void setIntMode( std::string mode );

  bool parse();
  bool readStatusFile(std::string host);
  bool writeStatusFile(std::string host);

 private:

  mysqlpp::Connection *conn;
  std::string host;

  std::list<std::string> options_;
  std::list<std::string> unused_;
  std::list<std::string> wrong_;

  int intMode_; // 0: dec, 1: hex, 2: oct

  void clearComments( char* line );
  void cleanFinalBlanks( char* line );
  bool checkTags( std::string key, std::istringstream& is );
  template <class T> bool getNumber( std::istringstream& is, T& num ); 
  template <class T> bool getNumbersList( std::istringstream& is, std::list<T>& numbers );
  bool getElementsList( char* text, std::list<int>& elem );
  template <class T> bool myGetOpt( std::string key, std::list<T>& numbers );
  template <class T> bool myGetOpt( std::string key, std::vector<T>& numbers );
};
#endif // tcsOpt_h
