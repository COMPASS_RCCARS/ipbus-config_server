/* These classes store the information available from the database  */
/* for a particular source ID. Simply call the constructor of  */
/* DatabaseEntry with this number, and the rest will be retrieved */
/* automatically. Upon DatabaseEntry::refresh() the objects holding */
/* the old information are destroyed. Each object retrieves its own */
/* information through the DB query object handed over to the constructor. */
/* The connection is local to DatabaseEntry::read() and dropped after */
/* each read cycle. */

#ifndef _DB_ENTRY_H_
#define _DB_ENTRY_H_

#include <mysql++.h>
#include <pthread.h>
#include <stdint.h>

#include <list>
#include <string>
#include <map>

#include "cs_types.h"

// this macro is used to declare variables that hold column data
// the bool is needed as the variable can also be 'unset'
#define DECLARE_DATA(type,name) bool has_##name ; type name

// This class holds one row of the FRONTEND table. As many Frontends
// will use the same F1_program, only a pointer to the F1 object is
// stored while the F1 objects are collected in a map.
class DatabaseEntryFrontend
{
 public:

  DatabaseEntryFrontend(mysqlpp::Row&,mysqlpp::Query&,std::string);

  DECLARE_DATA(std::string,detector);     // name of the associated detector
  DECLARE_DATA(int,geographic_id);        // position on this detector
  DECLARE_DATA(int,serial);               // frontend serial number
  DECLARE_DATA(int,format_id);            // data format
  DECLARE_DATA(bool,state);               // enabled/disabled on the module
  DECLARE_DATA(std::string,calib);        // calibration data/file
  DECLARE_DATA(std::string,config);       // configuration data/file
  DECLARE_DATA(int,port);                 // port number on the module
  DECLARE_DATA(std::string,cable);        // cable label (to module)
  DECLARE_DATA(int,num_tdc);              // number of TDCs connected
  DECLARE_DATA(unsigned long long,fe_disabled);
                                          // bit field of disabled frontends
  DECLARE_DATA(std::string,gate_window);  // ???
  DECLARE_DATA(std::string,gate_latency); // ???
  DECLARE_DATA(std::string,tb_name);      // TBname of the corresponding detector
  DECLARE_DATA(std::string,mod_date);     // modification date of the entry

  struct
  {
    DECLARE_DATA(std::string,name);
    DECLARE_DATA(std::string,desc);
    DECLARE_DATA(std::string,setup);
    DECLARE_DATA(std::string,mod_date);
  } fe;

  // print info to stream
  std::ostream& print(std::ostream&);
};

// for strings a macro substition is possible
// macros are looked up in the table MACROS by this function
bool subst_macros(std::string& s);

// This class contains all information about a specific module
// --identified by its source ID-- that is available from the
// database. All it needs is the name of the db server and this
// source ID. Upon construction the informatio is retrieved and
// stored in freely accesible variables and structs, where each
// value has an associated boolean to indicate if the db entry
// was NOT NULL.
// The method refresh() should be called whenever an update of the
// stored information is desired.

class DatabaseEntry
{
  int         sourceID_;
  std::string version;

  void        clear();
  void        read();
  std::string ref_link(std::string);
  std::string ref_version(std::string);

 public:

  DatabaseEntry() {};
 DatabaseEntry(int sourceID, std::string v)
   : sourceID_(sourceID),version("'"+v+"'") {read();}
  ~DatabaseEntry() {clear();}

  // retrieve latest information from the database
  
  void refresh() {clear();read();}
  void refresh(std::string v) {clear();version="'"+v+"'";read();}
  
  // configuration data for this module

  DECLARE_DATA(module_type_t,type);        // module type (see above)
  DECLARE_DATA(int,serial);                // module serial number
  DECLARE_DATA(bool,state);                // on/off
  DECLARE_DATA(std::string,hostname);      // vme cpu host name
  DECLARE_DATA(int,base_address);          // vme base address
  DECLARE_DATA(std::string,detector);      // detector name
  DECLARE_DATA(std::string,config);        // misc module configuration
  DECLARE_DATA(int,slink_format);          // slink format ID
  DECLARE_DATA(int,slink_mode);            // slink mode
  DECLARE_DATA(std::string,trigger_mode);  // ???
  DECLARE_DATA(std::string,cable);         // cable label (slink to patch panel)
  DECLARE_DATA(std::string,mod_date);      // modification date of the entry
  DECLARE_DATA(std::string,minors);        // minor modules of the entry
  DECLARE_DATA(std::string,load_a);        // LOAD_A module parameters
  DECLARE_DATA(std::string,load_a_minors); // LOAD_A_minors module parameters

  // the module_program entry
  struct
  {
    DECLARE_DATA(std::string,name);          // name of the module_program
    DECLARE_DATA(std::string,desc);          // description
    DECLARE_DATA(std::string,setup[3]);      // 3 setup files
    DECLARE_DATA(std::string,mod_date);      // modification date of the entry
  } prog;

  // configuration data for the tcs receiver
  struct
  {
    DECLARE_DATA(std::string,name);          // name of the tcs_program
    DECLARE_DATA(std::string,desc);          // description
    DECLARE_DATA(std::string,setup);         // setup data/file
    DECLARE_DATA(std::string,mod_date);      // modification date of the entry
  } tcs;
  
  // configuration data for the spill buffer
  struct
  {
    DECLARE_DATA(std::string,name);          // name of the rob slot
    DECLARE_DATA(std::string,cable);         // cable label (patch panel to rob)
    DECLARE_DATA(std::string,type);          // type of spill buffer
    DECLARE_DATA(std::string,mod_date);      // modification date of the entry
  } rob;

  // configuration data for the connected frontends

  std::list<DatabaseEntryFrontend> frontends;
  bool ports[16];                            // frontend ports on/off
  int portflags;

  // we have to foresee errors in the DB ...
  class Error
  {
    std::string msg;
  public:
  Error() : msg("generic error") {}
  Error(std::string s) : msg(s) {}
    virtual std::string what() {return msg;}
    virtual ~Error() {}
  };

  // print db info to stream
  std::ostream& print(std::ostream&);

};

std::ostream& operator<<(std::ostream& out, DatabaseEntry& db);

extern const char *db_db, *db_server, *db_user, *db_passwd,
  *et_db_db, *et_db_server, *et_db_user, *et_db_passwd;
extern uint32_t db_port;

#endif
